/*
 *  TOPPERS/JSP Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Just Standard Profile Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2006 by Monami Software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/include/kernel_debug.h 6183 2006-11-05T03:43:32.794177Z monaka  $
 */

/*
 *	μITRON4.0仕様 デバッグ用インクルードファイル
 *
 *  このファイルは，μITRON4.0仕様のスタンダードプロファイル外の定義と，
 *  ITRONデバッギングインタフェース仕様に含まれる定義の中で，JSPカーネ
 *  ルのデバッグサポート機能に必要な定義を含む．
 */

#ifndef TOPPERS_KERNEL_DEBUG_H
#define TOPPERS_KERNEL_DEBUG_H

#ifdef __cplusplus
extern "C" {
#endif

/*
 *  オブジェクト定数の定義（ITRONデバッギングインタフェース仕様）
 */
#define OBJ_SEMAPHORE		(128)
#define OBJ_EVENTFLAG		(129)
#define OBJ_DATAQUEUE		(130)
#define OBJ_MAILBOX		(131)
#define OBJ_MUTEX		(132)
#define OBJ_MESSAGEBUFFER	(133)
#define OBJ_RENDEZVOUSPORT	(135)
#define OBJ_RENDEZVOUS		(136)
#define OBJ_FMEMPOOL		(137)
#define OBJ_VMEMPOOL		(138)
#define OBJ_TASK		(139)
#define OBJ_READYQUEUE		(141)
#define OBJ_TIMERQUEUE		(142)
#define OBJ_CYCLICHANDLER	(144)
#define OBJ_ALARMHANDLER	(145)
#define OBJ_OVERRUNHANDLER	(146)
#define OBJ_ISR			(147)
#define OBJ_KERNELSTATUS	(148)
#define OBJ_TASKEXCEPTION	(149)
#define OBJ_CPUEXCEPTION	(150)

/*
 *  機能コードの定義
 */
#include "fncd.h"

#ifdef __cplusplus
}
#endif

#endif /* TOPPERS_KERNEL_DEBUG_H */
