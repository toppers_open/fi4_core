/*
 *  TOPPERS/JSP Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Just Standard Profile Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2006 by Monami Software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/include/fncd.h 6183 2006-11-05T03:43:32.794177Z monaka  $
 */

/*
 *  μITRON4.0仕様 機能コードの定義
 */
#ifndef TOPPERS_KERNEL_FNCD_H
#define TOPPERS_KERNEL_FNCD_H

#ifdef __cplusplus
extern "C" {
#endif

#define TFN_CRE_TSK	(-5)
#define TFN_DEL_TSK	(-6)
#define TFN_ACT_TSK	(-7)
#define TFN_CAN_ACT	(-8)
#define TFN_STA_TSK	(-9)
#define TFN_EXT_TSK	(-10)
#define TFN_EXD_TSK	(-11)
#define TFN_TER_TSK	(-12)
#define TFN_CHG_PRI	(-13)
#define TFN_GET_PRI	(-14)
#define TFN_REF_TSK	(-15)
#define TFN_REF_TST	(-16)

#define TFN_SLP_TSK	(-17)
#define TFN_TSLP_TSK	(-18)
#define TFN_WUP_TSK	(-19)
#define TFN_CAN_WUP	(-20)
#define TFN_REL_WAI	(-21)
#define TFN_SUS_TSK	(-22)
#define TFN_RSM_TSK	(-23)
#define TFN_FRSM_TSK	(-24)
#define TFN_DLY_TSK	(-25)

#define TFN_DEF_TEX	(-27)
#define TFN_RAS_TEX	(-28)
#define TFN_DIS_TEX	(-29)
#define TFN_ENA_TEX	(-30)
#define TFN_SNS_TEX	(-31)
#define TFN_REF_TEX	(-32)

#define TFN_CRE_SEM	(-33)
#define TFN_DEL_SEM	(-34)
#define TFN_SIG_SEM	(-35)
#define TFN_WAI_SEM	(-37)
#define TFN_POL_SEM	(-38)
#define TFN_TWAI_SEM	(-39)
#define TFN_REF_SEM	(-40)

#define TFN_CRE_FLG	(-41)
#define TFN_DEL_FLG	(-42)
#define TFN_SET_FLG	(-43)
#define TFN_CLR_FLG	(-44)
#define TFN_WAI_FLG	(-45)
#define TFN_POL_FLG	(-46)
#define TFN_TWAI_FLG	(-47)
#define TFN_REF_FLG	(-48)

#define TFN_CRE_DTQ	(-49)
#define TFN_DEL_DTQ	(-50)
#define TFN_SND_DTQ	(-53)
#define TFN_PSND_DTQ	(-54)
#define TFN_TSND_DTQ	(-55)
#define TFN_FSND_DTQ	(-56)
#define TFN_RCV_DTQ	(-57)
#define TFN_PRCV_DTQ	(-58)
#define TFN_TRCV_DTQ	(-59)
#define TFN_REF_DTQ	(-60)

#define TFN_CRE_MBX	(-61)
#define TFN_DEL_MBX	(-62)
#define TFN_SND_MBX	(-63)
#define TFN_RCV_MBX	(-65)
#define TFN_PRCV_MBX	(-66)
#define TFN_TRCV_MBX	(-67)
#define TFN_REF_MBX	(-68)

#define TFN_CRE_MPF	(-69)
#define TFN_DEL_MPF	(-70)
#define TFN_REL_MPF	(-71)
#define TFN_GET_MPF	(-73)
#define TFN_PGET_MPF	(-74)
#define TFN_TGET_MPF	(-75)
#define TFN_REF_MPF	(-76)

#define TFN_SET_TIM	(-77)
#define TFN_GET_TIM	(-78)

#define TFN_CRE_CYC	(-79)
#define TFN_DEL_CYC	(-80)
#define TFN_STA_CYC	(-81)
#define TFN_STP_CYC	(-82)
#define TFN_REF_CYC	(-83)

#define TFN_ROT_RDQ	(-85)
#define TFN_GET_TID	(-86)
#define TFN_LOC_CPU	(-89)
#define TFN_UNL_CPU	(-90)
#define TFN_DIS_DSP	(-91)
#define TFN_ENA_DSP	(-92)
#define TFN_SNS_CTX	(-93)
#define TFN_SNS_LOC	(-94)
#define TFN_SNS_DSP	(-95)
#define TFN_SNS_DPN	(-96)
#define TFN_REF_SYS	(-97)

#define TFN_DEF_INH	(-101)
#define TFN_CRE_ISR	(-102)
#define TFN_DEL_ISR	(-103)
#define TFN_REF_ISR	(-104)
#define TFN_DIS_INT	(-105)
#define TFN_ENA_INT	(-106)
#define TFN_CHG_IXX	(-107)
#define TFN_GET_IXX	(-108)

#define TFN_DEF_SVC	(-109)
#define TFN_DEF_EXC	(-110)
#define TFN_REF_CFG	(-111)
#define TFN_REF_VER	(-112)

#define TFN_IACT_TSK	(-113)
#define TFN_IWUP_TSK	(-114)
#define TFN_IREL_WAI	(-115)
#define TFN_IRAS_TEX	(-116)
#define TFN_ISIG_SEM	(-117)
#define TFN_ISET_FLG	(-118)
#define TFN_IPSND_DTQ	(-119)
#define TFN_IFSND_DTQ	(-120)
#define TFN_IROT_RDQ	(-121)
#define TFN_IGET_TID	(-122)
#define TFN_ILOC_CPU	(-123)
#define TFN_IUNL_CPU	(-124)
#define TFN_ISIG_TIM	(-125)

#define TFN_CRE_MTX	(-129)
#define TFN_DEL_MTX	(-130)
#define TFN_UNL_MTX	(-131)
#define TFN_LOC_MTX	(-133)
#define TFN_PLOC_MTX	(-134)
#define TFN_TLOC_MTX	(-135)
#define TFN_REF_MTX	(-136)

#define TFN_CRE_MBF	(-137)
#define TFN_DEL_MBF	(-138)
#define TFN_SND_MBF	(-141)
#define TFN_PSND_MBF	(-142)
#define TFN_TSND_MBF	(-143)
#define TFN_RCV_MBF	(-144)
#define TFN_PRCV_MBF	(-145)
#define TFN_TRCV_MBF	(-146)
#define TFN_REF_MBF	(-147)

#define TFN_CRE_POR	(-149)
#define TFN_DEL_POR	(-150)
#define TFN_CAL_POR	(-151)
#define TFN_TCAL_POR	(-152)
#define TFN_ACP_POR	(-153)
#define TFN_PACP_POR	(-154)
#define TFN_TACP_POR	(-155)
#define TFN_FWD_POR	(-156)
#define TFN_RPL_RDV	(-157)
#define TFN_REF_POR	(-158)
#define TFN_REF_RDV	(-159)

#define TFN_CRE_MPL	(-161)
#define TFN_DEL_MPL	(-162)
#define TFN_REL_MPL	(-163)
#define TFN_GET_MPL	(-165)
#define TFN_PGET_MPL	(-166)
#define TFN_TGET_MPL	(-167)
#define TFN_REF_MPL	(-168)

#define TFN_CRE_ALM	(-169)
#define TFN_DEL_ALM	(-170)
#define TFN_STA_ALM	(-171)
#define TFN_STP_ALM	(-172)
#define TFN_REF_ALM	(-173)

#define TFN_DEF_OVR	(-177)
#define TFN_STA_OVR	(-178)
#define TFN_STP_OVR	(-179)
#define TFN_REF_OVR	(-180)

#define TFN_ACRE_TSK	(-193)
#define TFN_ACRE_SEM	(-194)
#define TFN_ACRE_FLG	(-195)
#define TFN_ACRE_DTQ	(-196)
#define TFN_ACRE_MBX	(-197)
#define TFN_ACRE_MTX	(-198)
#define TFN_ACRE_MBF	(-199)
#define TFN_ACRE_POR	(-200)
#define TFN_ACRE_MPF	(-201)
#define TFN_ACRE_MPL	(-202)
#define TFN_ACRE_CYC	(-203)
#define TFN_ACRE_ALM	(-204)
#define TFN_ACRE_ISR	(-205)

#define TFN_VXSNS_CTX	(-225)
#define TFN_VXSNS_LOC	(-226)
#define TFN_VXSNS_DSP	(-227)
#define TFN_VXSNS_DPN	(-228)
#define TFN_VXSNS_TEX	(-229)
#define TFN_VSNS_INI	(-232)

#define TFN_VXGET_TIM	(-233)

#ifdef __cplusplus
}
#endif

#endif /* TOPPERS_KERNEL_FNCD_H */
