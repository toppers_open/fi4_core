/*
 *  TOPPERS/JSP Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Just Standard Profile Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2004 by Embedded and Real-Time Systems Laboratory
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2006 by Monami Software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/include/kernel.h 6183 2006-11-05T03:43:32.794177Z monaka  $
 */

/*
 *	μITRON4.0仕様標準インクルードファイル
 *
 *  このファイルでは，スタンダードプロファイルで必要なものだけを定義し
 *  ている．データ型の定義は，スタンダードプロファイルを満たすちょうど
 *  の長さにはしていない．
 *
 *  アセンブリ言語のソースファイルやシステムコンフィギュレーションファ
 *  イルからこのファイルをインクルードする時は，_MACRO_ONLY を定義して
 *  おくことで，マクロ定義以外の記述を除くことができる．
 *
 *  このインクルードファイルは，標準インクルードファイル（t_services.h 
 *  と jsp_kernel.h）でインクルードされる．また，他の ITRON仕様OS から
 *  ソフトウェアをポーティングする場合などには，このファイルを直接イン
 *  クルードしてもよい．この例外を除いて，他のファイルから直接インクルー
 *  ドされることはない．
 *
 *  この中でインクルードしているファイルを除いて，他のインクルードファ
 *  イルに依存していない．
 */

#ifndef TOPPERS_KERNEL_H
#define TOPPERS_KERNEL_H

#ifdef __cplusplus
extern "C" {
#endif

/*
 *  カーネル・アプリケーション 共通インクルードファイル
 */
#include <t_stddef.h>

/*
 *  ITRON仕様共通規定のデータ型・定数・マクロ
 */
#include <itron.h>

/*
 *  システムやプロセッサに依存する定義
 */
#include <sys_defs.h>
#include <cpu_defs.h>

/*
 *  システムログサービスのための定義
 */
#include <t_syslog.h>

/*
 *  補助マクロ
 */
#define	TROUND_VP(sz)	(((sz) + sizeof(VP) - 1) & ~(sizeof(VP) - 1))
#define	TCOUNT_VP(sz)	(((sz) + sizeof(VP) - 1) / sizeof(VP))

/*
 *  データ型の定義
 */
#ifndef _MACRO_ONLY

typedef	UINT		TEXPTN;		/* タスク例外要因のビットパターン */
typedef	UINT		FLGPTN;		/* イベントフラグのビットパターン */

typedef UW		OVRTIM;		/* プロセッサ時間 */
typedef UW		RDVPTN;		/* ランデブ条件 */
typedef UW		RDVNO;		/* ランデブ番号 */

typedef	struct t_msg {			/* メールボックスのメッセージヘッダ */
	struct t_msg	*next;
} T_MSG;

typedef	struct t_msg_pri {		/* 優先度付きメッセージヘッダ */
	T_MSG		msgque;		/* メッセージヘッダ */
	PRI		msgpri;		/* メッセージ優先度 */
} T_MSG_PRI;


typedef struct t_ctsk {
	ATR	tskatr;			/* タスク属性 */
	VP_INT	exinf;			/* タスクの拡張情報 */
	FP	task;			/* タスクの起動番地 */
	PRI	itskpri;		/* タスクの起動時優先度 */
	SIZE	stksz;			/* タスクのスタック領域のサイズ（バイト数）*/
	VP	stk;			/* タスクのスタック領域の先頭番地 */
} T_CTSK;

typedef struct t_rtsk {
	STAT   tskstat;			/* タスク状態 */
	PRI    tskpri;			/* タスクの現在優先度 */
	PRI    tskbpri;			/* タスクのベース優先度 */
	STAT   tskwait;			/* 待ち要因 */
	ID     wobjid;			/* 待ち対象のオブジェクトのID番号 */
	TMO    lefttmo;			/* タイムアウトするまでの時間 */
	UINT   actcnt;			/* 起動要求キューイング数 */
	UINT   wupcnt;			/* 起床要求キューイング数 */
	UINT   suscnt;			/* 強制待ち要求ネスト数 */
} T_RTSK;

typedef struct t_rtst {
	STAT   tskstat;			/* タスク状態 */
	STAT   tskwait;			/* 待ち要因 */
} T_RTST;

typedef struct t_dtex {
	ATR    texatr;			/* タスク例外処理ルーチン属性 */
	FP     texrtn;			/* タスク例外処理ルーチンの起動番地 */
} T_DTEX;

typedef struct t_rtex {
	STAT   texstat;			/* タスク例外処理の状態 */
	TEXPTN pndptn;			/* 保留例外要因 */
} T_RTEX;

typedef struct t_csem {
	ATR    sematr;			/* セマフォ属性 */
	UINT   isemcnt;			/* セマフォの資源数の初期値 */
	UINT   maxsem;			/* セマフォの再大資源数 */
} T_CSEM;

typedef struct t_rsem {
	ID	wtskid;			/* セマフォの待ち行列の先頭のタスクのID番号 */
	UINT	semcnt;			/* セマフォの現在の資源数 */
} T_RSEM;

typedef struct t_cflg {
	ATR    flgatr;			/* イベントフラグ属性 */
	FLGPTN iflgptn;			/* イベントフラグのビットパターンの初期値 */
} T_CFLG;

typedef struct t_rflg {
	ID     wtskid;			/* イベントフラグのの待ち行列の先頭のタスクのID番号 */
	FLGPTN flgptn;			/* イベントフラグのの現在のビットパターン */
} T_RFLG;

typedef struct t_cdtq {
	ATR    dtqatr;			/* データキュー属性 */
	UINT   dtqcnt;			/* データキュー領域の容量（データの個数）*/
	VP     dtq;			/* データキュー領域の先頭番地 */
} T_CDTQ;

typedef struct t_rdtq {
	ID     stskid;			/* データキューの送信待ち行列の先頭のタスクのID番号 */
	ID     rtskid;			/* データキューの受信待ち行列の先頭のタスクのID番号 */
	UINT   sdtqcnt;			/* データキューに入っているデータの数 */
} T_RDTQ;

typedef struct t_cmbx {
	ATR    mbxatr;			/* メールボックス属性 */
	PRI    maxmpri;			/* 送信されるメッセージの優先度の最大値 */
	VP     mprihd;			/* 優先度別のメッセージキューヘッダ領域の先頭番地 */
} T_CMBX;

typedef struct t_rmbx {
	ID     wtskid;			/* メールボックスの待ち行列の先頭のタスクのID番号 */
	T_MSG *pk_msg;			/* メッセージキューの先頭のメッセージパケットの先頭番地 */
} T_RMBX;

typedef struct t_cmtx {
	ATR    mtxatr;			/* ミューテックス属性 */
	PRI    ceilpri;			/* ミューテックスの上限優先度 */
} T_CMTX;

typedef struct t_rmtx {
	ID     htskid;			/* ミューテックスをロックしているタスクのID番号 */
	ID     wtskid;			/* ミューテックスの待ち行列の先頭のタスクのID番号 */
} T_RMTX;

typedef struct t_cmbf {
	ATR    mbfatr;			/* メッセージバッファ属性 */
	UINT   maxmsz;			/* メッセージの最大サイズ（バイト数） */
	SIZE   mbfsz;			/* メッセージバッファ領域のサイズ（バイト数） */
	VP     mbf;			/* メッセージバッファ領域の先頭番地 */
} T_CMBF;

typedef struct t_rmbf {
	ID     stskid;			/* メッセージバッファの送信待ち行列の先頭のタスクのID番号 */
	ID     rtskid;			/* メッセージバッファの受信待ち行列の先頭のタスクのID番号 */
	UINT   smsgcnt;			/* メッセージバッファに入っているメッセージの数 */
	SIZE   fmbfsz;			/* メッセージバッファ領域の空き領域のサイズ（バイト数、最低限の管理領域を除く） */
} T_RMBF;

typedef struct t_cpor {
	ATR    poratr;			/* ランデブポート属性 */
	UINT   maxcmsz;			/* 呼び出しメッセージの最大サイズ（バイト数） */
	UINT   maxrmsz;			/* 返答メッセージの最大サイズ（バイト数） */
} T_CPOR;

typedef struct t_rpor {
	ID     ctskid;			/* ランデブポートの呼出し待ち行列の先頭のタスクのID番号 */
	ID     atskid;			/* ランデブポートの受付待ち行列の先頭のタスクのID場号 */
} T_RPOR;

typedef struct t_rrdv {
	ID     wtskid;			/* ランデブ終了待ち状態のタスクのID番号 */
} T_RRDV;

typedef struct t_cmpf {
	ATR    mpfatr;			/* 固定長メモリプール属性 */
	UINT   blkcnt;			/* 獲得できるメモリブロック数（個数） */
	UINT   blksz;			/* メモリブロックのサイズ（バイト数） */
	VP     mpf;			/* 固定長メモリプール領域の先頭番地 */
} T_CMPF;

typedef struct t_rmpf {
	ID     wtskid;			/* 固定長メモリプールの待ち行列の先頭のタスクのID番号 */
	UINT   fblkcnt;			/* 固定長メモリプールの空きメモリブロック数（個数） */
} T_RMPF;

typedef struct t_cmpl {
	ATR    mplatr;			/* 可変長メモリプール属性 */
	UINT   mplsz;			/* メモリブロックのサイズ（バイト数） */
	VP     mpl;			/* 可変長メモリプール領域の先頭番地 */
} T_CMPL;

typedef struct t_rmpl {
	ID     wtskid;			/* 可変長メモリプールの待ち行列の先頭のタスクのID番号 */
	UINT   fmplsz;			/* 可変長メモリプールの空き領域の合計サイズ（バイト数） */
	UINT   fblksz;			/* すぐに獲得可能な最大メモリブロックサイズ（バイト数） */
} T_RMPL;

typedef struct t_ccyc {
	ATR    cycatr;			/* 周期ハンドラ属性 */
	VP_INT exinf;			/* 周期ハンドラの拡張情報 */
	FP     cychdr;			/* 周期ハンドラの起動番地 */
	RELTIM cyctim;			/* 周期ハンドラの起動周期 */
	RELTIM cycphs;			/* 周期ハンドラの起動位相 */
} T_CCYC;

typedef struct t_rcyc {
	STAT   cycstat;			/* 周期ハンドラの動作状態 */
	RELTIM lefttim;			/* 周期ハンドラを継ぎに起動する時刻までの時間 */
} T_RCYC;

typedef struct t_calm {
	ATR    almatr;			/* アラームハンドラ属性 */
	VP_INT exinf;			/* アラームハンドラの拡張情報 */
	FP     almhdr;			/* アラームハンドラの起動番地 */
} T_CALM;

typedef struct t_ralm {
	STAT   almstat;			/* アラームハンドラの動作状態 */
	RELTIM lefttim;			/* アラームハンドラの起動時刻までの時間 */
} T_RALM;

typedef struct t_dovr {
	ATR    ovratr;			/* オーバランハンドラ属性 */
	FP     ovrhdr;			/* オーバランハンドラの起動番地 */
} T_DOVR;

typedef struct t_rovr {
	STAT   ovrstat;			/* オーバランハンドラの動作状態 */
	OVRTIM leftotm;			/* 残りのプロセッサ時間 */
} T_ROVR;

typedef struct t_rsys {
	UH dummy;
} T_RSYS;

typedef struct t_dinh {
	ATR    inhatr;			/* 割込みハンドラ属性 */
	FP     inthdr;			/* 割込みハンドラの起動番地 */
} T_DINH;

typedef struct t_cisr {
	ATR    isratr;			/* 割込みサービスルーチン属性 */
	VP_INT exinf;			/* 割込みサービスルーチンの拡張情報 */
	INTNO  intno;			/* 割込みサービスルーチンを付加する割込み番号 */
	FP     isr;			/* 割込みサービスルーチンの起動番地 */
} T_CISR;

typedef struct t_risr {
	/*
	 * pk_risrの内容は実装定義。
	 * 本バージョンではpk_risrは空。
	 */
	UINT	dummy;			/* dummy */
} T_RISR;

typedef struct t_dsvc {
	ATR    svcatr;			/* 拡張サービスコール属性 */
	FP     svcrtn;			/* 拡張サービスコールルーチンの起動番地 */
} T_DSVC;

typedef struct t_dexc {
	ATR    excatr;			/* CPU例外ハンドラ属性 */
	FP     exchdr;			/* CPU例外ハンドラの起動番地 */
} T_DEXC;

typedef struct t_rcfg {
  /*
   * pk_rcfgの中身は実装依存。
   * 本バージョンでは、pk_rcfg は空。
   */
	UINT	dummy;			/* dummy */
} T_RCFG;

typedef struct t_rver {
	UH     maker;			/* カーネルのメーカコード */
	UH     prid;			/* カーネルの識別番号 */
	UH     spver;			/* ITRON仕様のバージョン番号 */
	UH     prver;			/* カーネルのバージョン番号 */
	UH     prno[4];			/* カーネル製品の管理情報 */
} T_RVER;

#endif /* _MACRO_ONLY */

/*
 *  サービスコールの宣言
 */
#ifndef _MACRO_ONLY

/*
 *  タスク管理機能
 */
extern ER	cre_tsk(ID tskid, const T_CTSK *pk_ctsk) throw();
extern ER_ID	acre_tsk(const T_CTSK *pk_ctsk) throw();
extern ER	del_tsk(ID tskid);
extern ER	act_tsk(ID tskid) throw();
extern ER	iact_tsk(ID tskid) throw();
extern ER_UINT	can_act(ID tskid) throw();
extern ER	sta_tsk(ID tskid, VP_INT stacd) throw();
extern void	ext_tsk(void) throw();
extern void	exd_tsk(void) throw();
extern ER	ter_tsk(ID tskid) throw();
extern ER	chg_pri(ID tskid, PRI tskpri) throw();
extern ER	get_pri(ID tskid, PRI *p_tskpri) throw();
extern ER	ref_tsk(ID tskid, T_RTSK *pk_rtsk) throw();
extern ER	ref_tst(ID tskid, T_RTST *pk_rtst) throw();


/*
 *  タスク付属同期機能
 */
extern ER	slp_tsk(void) throw();
extern ER	tslp_tsk(TMO tmout) throw();
extern ER	wup_tsk(ID tskid) throw();
extern ER	iwup_tsk(ID tskid) throw();
extern ER_UINT	can_wup(ID tskid) throw();
extern ER	rel_wai(ID tskid) throw();
extern ER	irel_wai(ID tskid) throw();
extern ER	sus_tsk(ID tskid) throw();
extern ER	rsm_tsk(ID tskid) throw();
extern ER	frsm_tsk(ID tskid) throw();
extern ER	dly_tsk(RELTIM dlytim) throw();

/*
 *  タスク例外処理機能
 */
extern ER	def_tex(ID tskid, const T_DTEX *pk_dtex) throw();
extern ER	ras_tex(ID tskid, TEXPTN rasptn) throw();
extern ER	iras_tex(ID tskid, TEXPTN rasptn) throw();
extern ER	dis_tex(void) throw();
extern ER	ena_tex(void) throw();
extern BOOL	sns_tex(void) throw();
extern ER	ref_tex(ID tskid, T_RTEX *pk_rtex) throw();

/*
 *  同期・通信機能
 */
extern ER	cre_sem(ID semid, const T_CSEM *pk_csem) throw();
extern ER_ID	acre_sem(const T_CSEM *pk_csem) throw();
extern ER	del_sem(ID semid) throw();
extern ER	sig_sem(ID semid) throw();
extern ER	isig_sem(ID semid) throw();
extern ER	wai_sem(ID semid) throw();
extern ER	pol_sem(ID semid) throw();
extern ER	twai_sem(ID semid, TMO tmout) throw();
extern ER	ref_sem(ID semid, T_RSEM *pk_rsem) throw();

extern ER	cre_flg(ID flgid, const T_CFLG *pk_cflg) throw();
extern ER_ID	acre_flg(const T_CFLG *pk_cflg) throw();
extern ER	del_flg(ID flgid) throw();
extern ER	set_flg(ID flgid, FLGPTN setptn) throw();
extern ER	iset_flg(ID flgid, FLGPTN setptn) throw();
extern ER	clr_flg(ID flgid, FLGPTN clrptn) throw();
extern ER	wai_flg(ID flgid, FLGPTN waiptn,
			MODE wfmode, FLGPTN *p_flgptn) throw();
extern ER	pol_flg(ID flgid, FLGPTN waiptn,
			MODE wfmode, FLGPTN *p_flgptn) throw();
extern ER	twai_flg(ID flgid, FLGPTN waiptn,
			MODE wfmode, FLGPTN *p_flgptn, TMO tmout) throw();
extern ER	ref_flg(ID flgid, T_RFLG *pk_rflg) throw();

extern ER	cre_dtq(ID dtqid, const T_CDTQ *pk_cdtq) throw();
extern ER_ID	acre_dtq(const T_CDTQ *pk_cdtq) throw();
extern ER	del_dtq(ID dtqid) throw();
extern ER	snd_dtq(ID dtqid, VP_INT data) throw();
extern ER	psnd_dtq(ID dtqid, VP_INT data) throw();
extern ER	ipsnd_dtq(ID dtqid, VP_INT data) throw();
extern ER	tsnd_dtq(ID dtqid, VP_INT data, TMO tmout) throw();
extern ER	fsnd_dtq(ID dtqid, VP_INT data) throw();
extern ER	ifsnd_dtq(ID dtqid, VP_INT data) throw();
extern ER	rcv_dtq(ID dtqid, VP_INT *p_data) throw();
extern ER	prcv_dtq(ID dtqid, VP_INT *p_data) throw();
extern ER	trcv_dtq(ID dtqid, VP_INT *p_data, TMO tmout) throw();
extern ER	ref_dtq(ID dtqid, T_RDTQ *pk_rdtq) throw();

extern ER	cre_mbx(ID mbxid, const T_CMBX *pk_cmbx) throw();
extern ER_ID	acre_mbx(const T_CMBX *pk_cmbx) throw();
extern ER	del_mbx(ID mbxid) throw();
extern ER	snd_mbx(ID mbxid, T_MSG *pk_msg) throw();
extern ER	rcv_mbx(ID mbxid, T_MSG **ppk_msg) throw();
extern ER	prcv_mbx(ID mbxid, T_MSG **ppk_msg) throw();
extern ER	trcv_mbx(ID mbxid, T_MSG **ppk_msg, TMO tmout) throw();
extern ER	ref_mbx(ID mbxid, T_RMBX *pk_rmbx) throw();

/*
 *  拡張同期・通信機能
 */
extern ER	cre_mtx(ID mtxid, const T_CMTX *pk_cmtx) throw();
extern ER_ID	acre_mtx(const T_CMTX *pk_cmtx) throw();
extern ER	del_mtx(ID mtxid) throw();
extern ER	loc_mtx(ID mtxid) throw();
extern ER	ploc_mtx(ID mtxid) throw();
extern ER	tloc_mtx(ID mtxid, TMO tmout) throw();
extern ER	unl_mtx(ID mtxid) throw();
extern ER	ref_mtx(ID mtxid, T_RMTX *pk_rmtx) throw();

extern ER	cre_mbf(ID mbfid, const T_CMBF *pk_cmbf) throw();
extern ER_ID	acre_mbf(const T_CMBF *pk_cmbf) throw();
extern ER	del_mbf(ID mbfid) throw();
extern ER	snd_mbf(ID mbfid, VP msg, UINT msgsz) throw();
extern ER	psnd_mbf(ID mbfid, VP msg, UINT msgsz) throw();
extern ER	tsnd_mbf(ID mbfid, VP msg, UINT msgsz, TMO tmout) throw();
extern ER_UINT	rcv_mbf(ID mbfid, VP msg) throw();
extern ER_UINT	prcv_mbf(ID mbfid, VP msg) throw();
extern ER_UINT	trcv_mbf(ID mbfid, VP msg, TMO tmout) throw();
extern ER	ref_mbf(ID mbfid, T_RMBF *pk_rmbf) throw();

extern ER	cre_por(ID porid, const T_CPOR *pk_cpor) throw();
extern ER_ID	acre_por(const T_CPOR *pk_cpor) throw();
extern ER	del_por(ID porid) throw();
extern ER_UINT	cal_por(ID porid, RDVPTN calptn, VP msg, UINT cmsgsz) throw();
extern ER_UINT	tcal_por(ID porid, RDVPTN calptn, VP msg, UINT cmsgsz, TMO tmout) throw();
extern ER_UINT	acp_por(ID porid, RDVPTN acpptn, RDVNO *p_rdvno, VP msg) throw();
extern ER_UINT	pacp_por(ID porid, RDVPTN acpptn, RDVNO *p_rdvno, VP msg) throw();
extern ER_UINT	tacp_por(ID porid, RDVPTN acpptn, RDVNO *p_rdvno, VP msg, TMO tmout) throw();
extern ER	fwd_por(ID porid, RDVPTN calptn, RDVNO rdvno, VP msg, UINT cmsgsz) throw();
extern ER	rpl_rdv(RDVNO rdvno, VP msg, UINT rmsgsz) throw();
extern ER	ref_por(ID porid, T_RPOR *pk_rpor) throw();
extern ER	ref_rdv(RDVNO rdvno, T_RRDV *pk_rrdv) throw();


/*
 *  メモリプール管理機能
 */
extern ER	cre_mpf(ID mpfid, const T_CMPF *pk_cmpf) throw();
extern ER_ID	acre_mpf(const T_CMPF *pk_cmpf) throw();
extern ER	del_mpf(ID mpfid) throw();
extern ER	get_mpf(ID mpfid, VP *p_blk) throw();
extern ER	pget_mpf(ID mpfid, VP *p_blk) throw();
extern ER	tget_mpf(ID mpfid, VP *p_blk, TMO tmout) throw();
extern ER	rel_mpf(ID mpfid, VP blk) throw();
extern ER	ref_mpf(ID mpfid, T_RMPF *pk_rmpf) throw();

extern ER	cre_mpl(ID mplid, const T_CMPL *pk_cmpl) throw();
extern ER_ID	acre_mpl(const T_CMPL *pk_cmpl) throw();
extern ER	del_mpl(ID mplid) throw();
extern ER	get_mpl(ID mplid, UINT blksz, VP *p_blk) throw();
extern ER	pget_mpl(ID mplid, UINT blksz, VP *p_blk) throw();
extern ER	tget_mpl(ID mplid, UINT blksz, VP *p_blk, TMO tmout) throw();
extern ER	rel_mpl(ID mplid, VP blk) throw();
extern ER	ref_mpl(ID mplid, T_RMPL *pk_rmpl) throw();

/*
 *  時間管理機能
 */
extern ER	set_tim(const SYSTIM *p_systim) throw();
extern ER	get_tim(SYSTIM *p_systim) throw();
extern ER	isig_tim(void) throw();

extern ER	cre_cyc(ID cycid, const T_CCYC *pk_ccyc) throw();
extern ER_ID	acre_cyc(const T_CCYC *pk_ccyc) throw();
extern ER	cre_cyc(ID cycid, const T_CCYC *pk_ccyc) throw();
extern ER	del_cyc(ID cycid) throw();
extern ER	sta_cyc(ID cycid) throw();
extern ER	stp_cyc(ID cycid) throw();
extern ER	ref_cyc(ID cycid, T_RCYC *pk_rcyc) throw();

extern ER	cre_alm(ID almid, const T_CALM *pk_calm) throw();
extern ER_ID	acre_alm(const T_CALM *pk_calm) throw();
extern ER	del_alm(ID almid);
extern ER	sta_alm(ID almid, RELTIM almtim) throw();
extern ER	stp_alm(ID almid) throw();
extern ER	ref_alm(ID almid, T_RALM *pk_ralm) throw();

extern ER	def_ovr(const T_DOVR *pk_dovr) throw();
extern ER	sta_ovr(ID tskid, OVRTIM ovrtim) throw();
extern ER	stp_ovr(ID tskid) throw();
extern ER	ref_ovr(ID tskid, T_ROVR *pk_rovr) throw();


/*
 *  システム状態管理機能
 */
extern ER	rot_rdq(PRI tskpri) throw();
extern ER	irot_rdq(PRI tskpri) throw();
extern ER	get_tid(ID *p_tskid) throw();
extern ER	iget_tid(ID *p_tskid) throw();
extern ER	loc_cpu(void) throw();
extern ER	iloc_cpu(void) throw();
extern ER	unl_cpu(void) throw();
extern ER	iunl_cpu(void) throw();
extern ER	dis_dsp(void) throw();
extern ER	ena_dsp(void) throw();
extern BOOL	sns_ctx(void) throw();
extern BOOL	sns_loc(void) throw();
extern BOOL	sns_dsp(void) throw();
extern BOOL	sns_dpn(void) throw();
extern BOOL	vsns_ini(void) throw();
extern ER	ref_sys(T_RSYS *pk_rsys) throw();


/*
 *  割込み管理機能
 */
extern ER	def_inh(INHNO inhno, const T_DINH *pk_dinh) throw();
extern ER	cre_isr(ID isrid, const T_CISR *pk_cisr) throw();
extern ER_ID	acre_isr(const T_CISR *pk_cisr) throw();
extern ER	del_isr(ID isrid) throw();
extern ER	ref_isr(ID isrid, T_RISR *pk_risr) throw();
/* TODO: extern ER chg_ixx(IXXXX ixxxx) throw(); */
/* TODO: extern ER get_ixx(IXXXX *p_ixxxx) throw(); */

/*
 *  サービスコール管理機能
 */
extern ER	def_svc(FN fncd, const T_DSVC *pk_dsvc) throw();
extern ER	cal_svc(FN fncd, VP_INT arg1, VP_INT arg2, VP_INT arg3, VP_INT arg4, VP_INT arg5) throw();

/*
 *  システム構成管理機能
 */
extern ER	def_exc(EXCNO excno, const T_DEXC *pk_dexc) throw();
extern ER	ref_cfg(T_RCFG *pk_rcfg) throw();
extern ER	ref_ver(T_RVER *pk_rver) throw();


/*
 *  実装独自サービスコール
 */
extern BOOL	vxsns_ctx(VP p_excinf) throw();
extern BOOL	vxsns_loc(VP p_excinf) throw();
extern BOOL	vxsns_dsp(VP p_excinf) throw();
extern BOOL	vxsns_dpn(VP p_excinf) throw();
extern BOOL	vxsns_tex(VP p_excinf) throw();


#endif /* _MACRO_ONLY */

/*
 *  オブジェクト属性の定義
 */
#define TA_HLNG		0x00u		/* 高級言語用インタフェース */
#define TA_ASM		0x01u		/* アセンブリ言語用インタフェース */

#define TA_TFIFO	0x00u		/* タスクの待ち行列をFIFO順に */
#define TA_TPRI		0x01u		/* タスクの待ち行列を優先度順に */

#define TA_INHERIT	(0x02u|TA_TPRI)	/* ミューテックスを優先度継承プロトコルに*/
#define TA_CEILING	(0x04u|TA_TPRI) /* ミューテックスを優先度上限プロトコルに*/

#define TA_MFIFO	0x00u		/* メッセージキューをFIFO順に */
#define TA_MPRI		0x02u		/* メッセージキューを優先度順に */

#define TA_ACT		0x02u		/* タスクを起動された状態で生成 */

#define TA_WSGL		0x00u		/* イベントフラグの待ちタスクを1つに */
#define TA_WMUL		0x02u		/* イベントフラグの待ちタスクを複数に */
#define TA_CLR		0x04u		/* イベントフラグのクリア指定 */

#define	TA_STA		0x02u		/* 周期ハンドラを動作状態で生成 */
#define TA_PHS		0x04u		/* 周期ハンドラの位相を保存 */

/*
 *  サービスコールの動作モードの定義
 */
#define	TWF_ANDW	0x00u		/* イベントフラグのAND待ち */
#define	TWF_ORW		0x01u		/* イベントフラグのOR待ち */

/*
 *  タスク状態の定義
 */
#define TTS_RUN		0x01u			/* 実行状態 */
#define TTS_RDY		0x02u			/* 実行可能状態 */
#define TTS_WAI		0x04u			/* 待ち状態 */
#define TTS_SUS		0x08u			/* 強制待ち状態 */
#define TTS_WAS		(TTS_WAI|TTS_SUS)	/* 二重待ち状態 */
#define TTS_DMT		0x10u			/* 休止状態 */

#define TTW_SLP		0x0001u		/* 起床待ち状態 */
#define TTW_DLY		0x0002u		/* 時間経過待ち状態 */
#define TTW_SEM		0x0004u		/* セマフォ資源の獲得待ち状態 */
#define TTW_FLG		0x0008u		/* イベントフラグ待ち状態 */
#define TTW_SDTQ	0x0010u		/* データキューへの送信待ち状態 */
#define TTW_RDTQ	0x0020u		/* データキューからの受信待ち状態 */
#define TTW_MBX		0x0040u		/* メールボックスからの受信待ち状態 */
#define TTW_MTX		0x0080u		/* ミューテックスのロック待ち状態 */
#define TTW_SMBF	0x0100u		/* メッセージバッファへの送信待ち */
#define TTW_RMBF	0x0200u		/* メッセージバッファからの受信待ち */
#define TTW_CAL		0x0400u		/* ランデブの呼出し待ち状態 */
#define TTW_ACP		0x0800u		/* ランデブの受付待ち状態 */
#define TTW_RDV		0x1000u		/* ランデブの終了待ち状態 */
#define TTW_MPF		0x2000u		/* 固定長メモリブロックの獲得待ち */
#define TTW_MPL		0x4000u		/* 可変長メモリブロックの獲得待ち */

/*
 *  オブジェクトの状態
 */
#define TTEX_ENA	0x00u		/* タスク例外許可状態 */
#define TTEX_DIS	0x01u		/* タスク例外禁止状態 */

#define TCYC_STP	0x00u		/* 周期ハンドラが動作していない */
#define TCYC_STA	0x01u		/* 周期ハンドラが動作している */

#define TALM_STP	0x00u		/* アラームハンドラが動作していない */
#define TALM_STA	0x01u		/* アラームハンドラが動作している */

#define TOVR_STP	0x00u		/* 上限プロセッサ時間設定されていない */
#define TOVR_STA	0x01u		/* 上限プロセッサ時間設定されている */

/*
 *  その他の定数の定義
 */
#define	TSK_SELF	0		/* 自タスク指定 */
#define	TSK_NONE	0		/* 該当するタスクがない */
#define	TPRI_SELF	0		/* 自タスクのベース優先度の指定 */
#define	TPRI_INI	0		/* タスクの起動時優先度の指定 */

/*
 *  構成定数とマクロ
 */

/*
 *  優先度の範囲
 */
#define	TMIN_TPRI	1		/* タスク優先度の最小値 */
#define	TMAX_TPRI	16		/* タスク優先度の最大値 */
#define	TMIN_MPRI	1		/* メッセージ優先度の最小値 */
#define	TMAX_MPRI	16		/* メッセージ優先度の最大値 */

/*
 *  バージョン情報
 *  FI4カーネルの場合、TKERNEL_PRVERは対応するJSPの値に合わせる。
 */
#define	TKERNEL_MAKER	0x0118u		/* カーネルのメーカーコード */
#define	TKERNEL_PRID	0x4010u		/* カーネルの識別番号 */
#define	TKERNEL_SPVER	0x5402u		/* ITRON仕様のバージョン番号 */
#define	TKERNEL_PRVER	0x1042u		/* カーネルのバージョン番号 */

/*
 *  キューイング／ネスト回数の最大値
 */
#define	TMAX_ACTCNT	1		/* 起動要求キューイング数の最大値 */
#define	TMAX_WUPCNT	1		/* 起床要求キューイング数の最大値 */
#define	TMAX_SUSCNT	1		/* 強制待ち要求ネスト数の最大値 */

/*
 *  ビットパターンのビット数
 */
#define	TBIT_TEXPTN	(sizeof(TEXPTN) * CHAR_BIT)
					/* タスク例外要因のビット数 */
#define	TBIT_FLGPTN	(sizeof(FLGPTN) * CHAR_BIT)
					/* イベントフラグのビット数 */
#define	TBIT_RDVPTN	(sizeof(RDVPTN) * CHAR_BIT)
					/* ランデブ条件のビット数 */

/*
 *  セマフォの最大資源数
 */
#define TMAX_MAXSEM	((UINT)-1)

/*
 *  dtqcnt個のデータを格納するのに必要なデータキュー領域のサイズ
 *  (バイト数)
 */
#define TSZ_DTQ(dtqcnt) (sizeof(VP_INT) * (dtqcnt))

/*
 *  JSP同様、単一のキューで管理するため、mprihdを使わない。
 *  μITRON4.0仕様書の記述に基づき、定数として定義する。
 */
#define TSZ_MPRIHD(mpricnt) (4)

/*
 * blkszバイトの可変長メモリブロックblkcnt個を格納するために
 * 必要なメモリプール領域のバイト数。
 *
 * やや大きめに取ってある。
 */
#define TSZ_MPL(blkcnt, blksz) ((blkcnt) * (TROUND_VP(blksz) + 16) + 16)

/*
 * msgszバイトのメッセージをmsgcnt個格納するの必要な
 * メッセージバッファ領域のサイズ(目安のバイト数)
 *
 * やや大きめに取ってある。
 */
#define TSZ_MBF(msgcnt, msgsz)	((msgcnt) * TROUND_VP((msgsz) + sizeof(SIZE)))

/*
 *  blkszバイトのメモリブロックをblkcnt個取得できるのに
 *  必要な固定長メモリプール領域のサイズ(バイト数)
 */
#define TSZ_MPF(blkcnt, blksz)	((blkcnt) * TROUND_VP(blksz))

/*
 *  機能コードの定義
 */
#include "fncd.h"

#ifdef __cplusplus
}
#endif

#endif /* TOPPERS_KERNEL_H */
