/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2000-2003 by Masaki Muranaka
 *  Copyright (C) 2003-2004 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/mempvar.c 6101 2006-09-10T08:00:54.989588Z monaka  $
 */

/*
 *	可変長メモリプール機能
 */

#include "fi4_kernel.h"
#include "check.h"
#include "task.h"
#include "wait.h"
#include "kmem.h"
#include "bufmgr.h"
#include "mempvar.h"

/*
 *  可変長メモリプールIDの最大値（kernel_cfg.c）
 */
extern const ID	tmax_mplid;

/*
 *  可変長メモリプール初期化ブロックのエリア（kernel_cfg.c）
 */
extern MPLINIB	mplinib_table[];

/*
 *  可変長メモリプール初期化ブロック連想ペアの数（kernel_cfg.c）
 */
extern const UINT	tnum_mplinia;

/*
 *  可変長メモリプール初期化ブロック連想ペアのエリア（kernel_cfg.c）
 */
extern const MPLINIA	mplinia_table[];

/*
 *  可変長メモリプール予約ID情報のエリア（kernel_cfg.c）
 */
extern const ID		mplrid_table[];

/*
 *  可変長メモリプール予約ID情報の数 (kernel_cfg.c)
 */
extern const UINT	tnum_mplrid;

/*
 *  可変長メモリプール管理ブロックのエリア（kernel_cfg.c）
 */
extern MPLCB	mplcb_table[];

/*
 *  可変長メモリプールの数
 */
#define TNUM_MPL	((UINT)(tmax_mplid - TMIN_MPLID + 1))

/*
 *  可変長メモリプールIDから可変長メモリプール管理ブロックを
 *  取り出すためのマクロ
 */
#define INDEX_MPL(mplid)	((mplid) - TMIN_MPLID)
#define get_mplcb(mplid)	(&(mplcb_table[INDEX_MPL(mplid)]))

/*
 *  ID自動割当て用フリーリスト
 */
extern QUEUE mpl_freelist;

/*
 *  可変長メモリプール待ち情報ブロックの定義
 */
typedef struct variable_memorypool_waiting_information {
	WINFO winfo;	/* 標準の待ち情報ブロック */
	WOBJCB *wobjcb;	/* 待ちオブジェクトのコントロールブロック */
	UINT blksz;
	VP blk;		/* 獲得したメモリブロック */
} WINFO_MPL;

/* 
 *  可変長メモリプール機能の初期化
 */
#ifdef __mplini

QUEUE mpl_freelist;

void
mempvar_initialize(void)
{
	UINT	i;
	MPLCB	*mplcb;
	MPLINIB *mplinib;
	const MPLINIA *mplinia;
	const ID	*mplrid;

	/*
	 *  カーネル起動時の状態に関わらず初期化しなければいけないものを
	 *  まず初期化する。
	 */
	for (mplcb = mplcb_table, i = 0; i < TNUM_MPL; mplcb++, i++) {
		queue_initialize(&(mplcb->wait_queue));
	}
	queue_initialize(&mpl_freelist);

	/*
	 *  mplinia によって示されるものをまず初期化
	 */
	for (mplinia = mplinia_table, i = 0; i < tnum_mplinia; mplinia++, i++) {
		BOOL stat;

		mplinib = &mplinib_table[INDEX_MPL(mplinia->mplid)];
		*mplinib = mplinia->mplinib;
		mplcb = &mplcb_table[INDEX_MPL(mplinia->mplid)];
		mplcb->mplinib = mplinib;
		mplcb->exist = KOBJ_EXIST;
		mplcb->reserved = TRUE;

		/*
		 *  非標準のcfgを使ったとき、mplがNULLになる可能性がある。<BTS:21>
		 *  そのようなcfgでは検証項目が増える。標準のcfgではこのコードに
		 *  入ることはない。
		 */
		if (mplinib->mpl == NULL) {
			bufmgr_allocate(&stat, mplinib->mplsz, &(mplinib->mpl));
			assert(stat);
		}
		stat = kmem_setup(mplinib->mpl, mplinib->mplsz, &(mplcb->free_list));
		assert(stat);
	}

	/*
	 *  予約IDの設定
	 */
	for (mplrid = mplrid_table, i = 0; i < tnum_mplrid; mplrid++, i++) {
		mplcb_table[INDEX_MPL(*mplrid)].reserved = TRUE;
	}

	/*
	 * mplinia によって示されないものを初期化
	 */
	for (mplcb = mplcb_table, i = 0; i < TNUM_MPL; mplcb++, i++) {
		if (mplcb->mplinib == NULL) {
			mplcb->exist = KOBJ_NOEXS;
			mplcb->mplinib = &mplinib_table[i];

			if (!CHECK_OBJECT_RESERVED(mplcb)) {
				queue_insert_prev(&mpl_freelist, (QUEUE*)mplcb);
			}
		}
	}
}

#endif /* __mplini */

/*
 *  初期設定を行う。
 */
#ifdef __mplcbini
ER
mplcb_initialize(MPLCB *mplcb, T_CMPL *pk_cmpl)
{
	BOOL stat;
	VP	mpl;
	MPLINIB	*mplinib;

	mpl = pk_cmpl->mpl;

	/*
	 *  mpl == NULL の時、バッファマネージャからメモリを獲得する。
	 */
	if (mpl == NULL) {
		BOOL stat;
		bufmgr_allocate(&stat, pk_cmpl->mplsz, &mpl);
		if (!stat) {
			return E_NOMEM;
		}
	}

	queue_initialize((QUEUE*)mplcb);
	mplinib = mplcb->mplinib;
	mplinib->mplatr = pk_cmpl->mplatr;
	mplinib->mpl = mpl;
	mplinib->mplsz = pk_cmpl->mplsz;
	stat = kmem_setup(mpl, mplinib->mplsz, &(mplcb->free_list));
	if (stat) {
		return E_OK;
	} else {
		return E_SYS;
	}
}

#endif

/*
 *  可変長メモリプールの生成
 */
#ifdef __cre_mpl

SYSCALL ER
cre_mpl(ID mplid, const T_CMPL *pk_cmpl)
{
	MPLCB	*mplcb;
	ER	ercd;
    
	LOG_CRE_MPL_ENTER(mplid, pk_cmpl);
	CHECK_TSKCTX_UNL();

	CHECK_MPLID(mplid);
	CHECK_ATTRIBUTE(pk_cmpl->mplatr, TA_TFIFO | TA_TPRI);
	CHECK_NOT_ZERO(pk_cmpl->mplsz);

	mplcb = get_mplcb(mplid);
	CHECK_OBJECT_CREATABLE(mplcb);
	UPDATE_OBJECT_FREELIST(mplcb);

	ercd = mplcb_initialize(mplcb, pk_cmpl);

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	mplcb->exist = KOBJ_EXIST;

    exit:
	LOG_CRE_MPL_LEAVE(ercd);
	return(ercd);
}

#endif

/*
 *  可変長メモリプールの生成(ID番号自動割付け）
 */
#ifdef __acre_mpl

SYSCALL ER_ID
acre_mpl(const T_CMPL *pk_cmpl)
{
	MPLCB	*mplcb;
	ER_ID	ercd;
    
	LOG_ACRE_MPL_ENTER(pk_cmpl);
	CHECK_TSKCTX_UNL();

	if (TNUM_MPL == 0) {
		ercd = E_NOID;
		goto exit;
	}
	CHECK_ATTRIBUTE(pk_cmpl->mplatr, TA_TFIFO | TA_TPRI);
	CHECK_NOT_ZERO(pk_cmpl->mplsz);

	t_lock_cpu();
	mplcb = (MPLCB *)t_get_free_id((QUEUE *)&mpl_freelist);
	if (mplcb == NULL) {
		ercd = E_NOID;
		t_unlock_cpu();
		goto exit;
	}
	mplcb->exist = KOBJ_INIT;
	t_unlock_cpu();

	ercd = mplcb_initialize(mplcb, pk_cmpl);
	if (ercd != E_OK) {
		t_lock_cpu();
		queue_insert_prev(&mpl_freelist, (QUEUE *)mplcb);
		t_unlock_cpu();
		goto exit;
	}

	ercd = MPLID(mplcb);

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	mplcb->exist = KOBJ_EXIST;

    exit:
	LOG_ACRE_MPL_LEAVE(ercd);
	return(ercd);
}

#endif

/*
 *  可変長メモリプールの削除
 */
#ifdef __del_mpl
SYSCALL ER
del_mpl(ID mplid)
{
	MPLCB	*mplcb;
	ER	ercd;
	BOOL	stat;

	LOG_DEL_MPL_ENTER(mplid);
	CHECK_TSKCTX_UNL();

	CHECK_MPLID(mplid);
	mplcb = get_mplcb(mplid);

	CHECK_OBJECT_DELETABLE(mplcb);

	/*
	 *  待ち行列の後片付け。削除に伴う待ち解除を行う。
	 */
	while (queue_empty(&(mplcb->wait_queue)) == FALSE) {
		TCB *tcb;
		tcb = (TCB *)queue_delete_next(&(mplcb->wait_queue));
		t_lock_cpu();
		if (wait_deleted(tcb)) {
			dispatch();
		}
		t_unlock_cpu();
	}
	  
	/*
	 *  予約IDでなければフリーリストに入れる。
	 */
	if (!CHECK_OBJECT_RESERVED(mplcb)) {
		t_lock_cpu();
		queue_insert_prev(&mpl_freelist, (QUEUE *)mplcb);
		t_unlock_cpu();
	}

	/*
	 *  メモリプール領域を解放する。
	 */
	stat = bufmgr_release(mplcb->mplinib->mpl);
	if (stat) {
		ercd = E_OK;
	} else {
		ercd = E_SYS;
	}


	/*
	 *  ここから先はacre_/cre_でのアクセスが可能
	 */
	mplcb->exist = KOBJ_NOEXS;

    exit:
	LOG_DEL_MPL_LEAVE(ercd);
	return(ercd);
}

#endif /* __del_mpl */

/*
 *  可変長メモリブロックの獲得
 */
#ifdef __get_mpl

SYSCALL ER
get_mpl(ID mplid, UINT blksz, VP *p_blk)
{
	MPLCB	*mplcb;
	WINFO_MPL winfo;
	ER	ercd;

	LOG_GET_MPL_ENTER(mplid, blksz, p_blk);
	CHECK_DISPATCH();

	CHECK_MPLID(mplid);
	mplcb = get_mplcb(mplid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mplcb);

	if (kmem_allocate(mplcb->free_list, blksz, p_blk)) {
		ercd = E_OK;
	} else {
		winfo.blksz = blksz;
		wobj_make_wait((WOBJCB *) mplcb, (WINFO_WOBJ *) &winfo);
		dispatch();
		ercd = winfo.winfo.wercd;
		if (ercd == E_OK) {
			*p_blk = winfo.blk;
		}
	}

	t_unlock_cpu();

     exit:
	LOG_GET_MPL_LEAVE(ercd, p_blk);
	return(ercd);
}

#endif /* __get_mpl */

/*
 *  可変長メモリブロックの獲得（ポーリング）
 */
#ifdef __pget_mpl

SYSCALL ER
pget_mpl(ID mplid, UINT blksz, VP *p_blk)
{
	MPLCB	*mplcb;
	ER	ercd;

	LOG_PGET_MPL_ENTER(mplid, blksz, p_blk);
	CHECK_TSKCTX_UNL();

	CHECK_MPLID(mplid);
	mplcb = get_mplcb(mplid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mplcb);

	if (kmem_allocate(mplcb->free_list, blksz, p_blk)) {
		ercd = E_OK;
	} else {
		ercd = E_TMOUT;
	}

	t_unlock_cpu();

    exit:
	LOG_PGET_MPL_LEAVE(ercd, p_blk);
	return(ercd);
}

#endif /* __pget_mpl */

/*
 *  可変長メモリブロックの獲得（タイムアウトあり）
 */
#ifdef __tget_mpl

SYSCALL ER
tget_mpl(ID mplid, UINT blksz, VP *p_blk, TMO tmout)
{
	MPLCB	*mplcb;
	WINFO_MPL winfo;
	TMEVTB	tmevtb;
	ER	ercd;

	LOG_TGET_MPL_ENTER(mplid, blksz, p_blk, tmout);
	CHECK_DISPATCH();
	CHECK_TMOUT(tmout);

	CHECK_MPLID(mplid);
	mplcb = get_mplcb(mplid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mplcb);

	if (kmem_allocate(mplcb->free_list, blksz, p_blk)) {
		ercd = E_OK;
	}
	else if (tmout == TMO_POL) {
		ercd = E_TMOUT;
	} else {
		winfo.blksz = blksz;
		wobj_make_wait_tmout((WOBJCB *) mplcb, (WINFO_WOBJ *) &winfo,
				     &tmevtb, tmout);
		dispatch();
		*p_blk = winfo.blk;
		ercd = winfo.winfo.wercd;
	}
	
	t_unlock_cpu();
    exit:
	LOG_TGET_MPL_LEAVE(ercd, p_blk);
	return(ercd);
}

#endif /* __tget_mpl */

/*
 *  可変長メモリブロックの返却
 */
#ifdef __rel_mpl

SYSCALL ER
rel_mpl(ID mplid, VP blk)
{
	MPLCB	*mplcb;
	TCB	*tcb;
	ER	ercd = E_OK;
	BOOL	stat;
	
	LOG_REL_MPL_ENTER(mplid, blk);
	CHECK_TSKCTX_UNL();

	CHECK_MPLID(mplid);
	mplcb = get_mplcb(mplid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mplcb);

	if ((UB *)(mplcb->free_list) + mplcb->mplinib->mplsz <= (UB *)blk ||
	    (UB *)blk < (UB *)(mplcb->free_list)) {
		ercd = E_PAR;
	} else {
		stat = kmem_release(mplcb->free_list, blk);
		if (stat == FALSE) {
			ercd = E_PAR;
		} else while (!queue_empty(&(mplcb->wait_queue)))  {
			VP new_blk;
			UINT blksz;

			/*
			 *  待ち行列の最初にあるタスクが要求している
			 *  可変長メモリのサイズを求める。
			 */
			tcb = (TCB *)(mplcb->wait_queue.next);
			blksz = ((WINFO_MPL *)(tcb->winfo))->blksz;

			if (!kmem_allocate(mplcb->free_list, blksz, &new_blk)) {
				break;
			}

			/*
			 *  取得できたら、待ちを解除してdispatch
			 */
			queue_delete_next(&(mplcb->wait_queue));
			((WINFO_MPL *)(tcb->winfo))->blk = new_blk;
			if (wait_complete(tcb)) {
				dispatch();
			}

			/*
			 *  ロックしているタスクがどうなろうとも、
			 *  今回の呼出しには関係なく、E_OKである。
			 */
			ercd = E_OK;
		}
	}

	t_unlock_cpu();
 exit:
	LOG_REL_MPL_LEAVE(ercd);
	return(ercd);
}

#endif /* __tget_mpl */

/*
 *  可変長メモリプールの状態取得
 */
#ifdef __ref_mpl

SYSCALL ER
ref_mpl(ID mplid, T_RMPL *pk_rmpl)
{
	MPLCB	*mplcb;
	ER	ercd;
	BOOL	stat;

	LOG_REF_MPL_ENTER(mplid);
	CHECK_TSKCTX_UNL();

	CHECK_MPLID(mplid);
	mplcb = get_mplcb(mplid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mplcb);

	if (!(queue_empty(&(mplcb->wait_queue)))) {
		TCB	*tcb;
		tcb = (TCB *)queue_peek_next(&(mplcb->wait_queue));
		pk_rmpl->wtskid = TSKID(tcb);
	}
	else {
		pk_rmpl->wtskid = TSK_NONE;
	}

	stat = kmem_status(mplcb->free_list, &(pk_rmpl->fmplsz), &(pk_rmpl->fblksz));
	if (stat == FALSE) {
		/*
		 *  free_list が壊れている
		 */
		ercd = E_SYS;
	} else {
		ercd = E_OK;
	}

	t_unlock_cpu();

    exit:
	LOG_REF_MPL_LEAVE(ercd);
	return(ercd);
}

#endif
