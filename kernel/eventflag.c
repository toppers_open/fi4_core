/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2003-2004 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/eventflag.c 6186 2006-11-05T04:04:59.527214Z monaka  $
 */

/*
 *	イベントフラグ機能
 */

#include "fi4_kernel.h"
#include "check.h"
#include "task.h"
#include "wait.h"
#include "eventflag.h"

/*
 *  イベントフラグIDの最大値（kernel_cfg.c）
 */
extern const ID	tmax_flgid;

/*
 *  イベントフラグ初期化ブロックのエリア（kernel_cfg.c）
 */
extern FLGINIB	flginib_table[];

/*
 *  イベントフラグ初期化ブロック連想ペアの数（kernel_cfg.c）
 */
extern const UINT	tnum_flginia;

/*
 *  イベントフラグ初期化ブロック連想ペアのエリア（kernel_cfg.c）
 */
extern const FLGINIA	flginia_table[];

/*
 *  イベントフラグ予約ID情報のエリア（kernel_cfg.c）
 */
extern const ID		flgrid_table[];

/*
 *  イベントフラグ予約ID情報の数 (kernel_cfg.c)
 */
extern const UINT	tnum_flgrid;

/*
 *  イベントフラグ管理ブロックのエリア（kernel_cfg.c）
 */
extern FLGCB	flgcb_table[];

/*
 *  イベントフラグの数
 */
#define TNUM_FLG	((UINT)(tmax_flgid - TMIN_FLGID + 1))

/*
 *  イベントフラグIDからイベントフラグ管理ブロックを取り出すためのマクロ
 */
#define INDEX_FLG(flgid)	((UINT)((flgid) - TMIN_FLGID))
#define get_flgcb(flgid)	(&(flgcb_table[INDEX_FLG(flgid)]))

/*
 *  ID自動割当て用フリーリスト
 */
extern QUEUE flg_freelist;

/*
 *  イベントフラグ待ち情報ブロックの定義
 *
 *  flgptn は，waiptn および wfmode と同時に使うことはないため，union 
 *  を使えばメモリを節約することが可能である．
 */
typedef struct eventflag_waiting_information {
	WINFO	winfo;		/* 標準の待ち情報ブロック */
	WOBJCB	*wobjcb;	/* 待ちオブジェクトの管理ブロック */
	FLGPTN	waiptn;		/* 待ちパターン */
	MODE	wfmode;		/* 待ちモード */
	FLGPTN	flgptn;		/* 待ち解除時のパターン */
} WINFO_FLG;

extern QUEUE *chk_flg_cond(FLGCB *flgcb, QUEUE *queue, BOOL *disp);

/*
 *  イベントフラグ機能の初期化
 */
#ifdef __flgini

QUEUE flg_freelist;

void
eventflag_initialize(void)
{
	UINT	i;
	FLGCB	*flgcb;
	FLGINIB *flginib;
	FLGINIA const *flginia;
	ID	const *flgrid;

	/*
	 * カーネル起動時の状態に関わらず初期化しなければいけないものを
	 * まず初期化する。
	 */
	for (flgcb = flgcb_table, i = 0; i < TNUM_FLG; flgcb++, i++) {
		queue_initialize(&(flgcb->wait_queue));
		flgcb->flginib = NULL;
	}
	queue_initialize(&flg_freelist);

	/*
	 * flginia によって示されるものをまず初期化
	 */
	for (flginia = flginia_table, i = 0; i < tnum_flginia; flginia++, i++) {
		flginib = &flginib_table[INDEX_FLG(flginia->flgid)];
		*flginib = flginia->flginib;
		flgcb = &flgcb_table[INDEX_FLG(flginia->flgid)];
		flgcb->flginib = flginib;
		flgcb->exist = KOBJ_EXIST;
		flgcb->reserved = TRUE;

		flgcb->flgptn = flginib->iflgptn;
	}

	/*
	 *  予約IDの設定
	 */
	for (flgrid = flgrid_table, i = 0; i < tnum_flgrid; flgrid++, i++) {
		flgcb_table[INDEX_FLG(*flgrid)].reserved = TRUE;
	}

	/*
	 * flginia によって示されないものを初期化
	 */
	for (flgcb = flgcb_table, i = 0; i < TNUM_FLG; flgcb++, i++) {
		if (flgcb->flginib == NULL) {
			flgcb->exist = KOBJ_NOEXS;
			flgcb->flginib = &flginib_table[i];

			if (!CHECK_OBJECT_RESERVED(flgcb)) {
				queue_insert_prev(&flg_freelist, (QUEUE*)flgcb);
			}
		}
	}
}

#endif /* __flgini */

/*
 *  イベントフラグ待ち解除条件のチェック
 */
#ifdef __flgcnd

BOOL
eventflag_cond(FLGCB *flgcb, FLGPTN waiptn, MODE wfmode, FLGPTN *p_flgptn)
{
	if ((wfmode & TWF_ORW) != 0 ? (flgcb->flgptn & waiptn) != 0
				: (flgcb->flgptn & waiptn) == waiptn) {
		*p_flgptn = flgcb->flgptn;
		if ((flgcb->flginib->flgatr & TA_CLR) != 0) {
			flgcb->flgptn = 0;
		}
		return(TRUE);
	}
	return(FALSE);
}

#endif /* __flgcnd */

/*
 *  イベントフラグの生成
 */
#ifdef __cre_flg

SYSCALL ER
cre_flg(ID flgid, const T_CFLG * pk_cflg)
{
	FLGCB	*flgcb;
	FLGINIB *flginib;
	ER	ercd;
    
	LOG_CRE_FLG_ENTER(flgid, pk_cflg);
	CHECK_TSKCTX_UNL();

	CHECK_FLGID(flgid);
	CHECK_ATTRIBUTE(pk_cflg->flgatr, TA_TFIFO | TA_TPRI | TA_WSGL | TA_WMUL | TA_CLR);
	flgcb = get_flgcb(flgid);
	CHECK_OBJECT_CREATABLE(flgcb);
	UPDATE_OBJECT_FREELIST(flgcb);

	/*
	 * 初期設定を行う。
	 */
	queue_initialize((QUEUE *)flgcb);
	flginib = flgcb->flginib;
	flginib->flgatr = pk_cflg->flgatr;
	flgcb->flgptn = flginib->iflgptn = pk_cflg->iflgptn;

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	flgcb->exist = KOBJ_EXIST;

	ercd = E_OK;

    exit:
	LOG_CRE_FLG_LEAVE(ercd);
	return(ercd);
}

#endif /* __cre_flg */

/*
 *  イベントフラグの生成(ID番号自動割付け)
 */
#ifdef __acre_flg

SYSCALL ER_ID
acre_flg(const T_CFLG *pk_cflg)
{
	FLGCB	*flgcb;
	FLGINIB	*flginib;
	ER_ID	ercd;
    
	LOG_ACRE_FLG_ENTER(pk_cflg);
	CHECK_TSKCTX_UNL();

	if (TNUM_FLG == 0) {
		ercd = E_NOID;
		goto exit;
	}
	CHECK_ATTRIBUTE(pk_cflg->flgatr, TA_TFIFO | TA_TPRI | TA_WSGL | TA_WMUL | TA_CLR);
	t_lock_cpu();
	flgcb = (FLGCB *)t_get_free_id((QUEUE *)&flg_freelist);
	if (flgcb == NULL) {
		ercd = E_NOID;
		t_unlock_cpu();
		goto exit;
	}
	flgcb->exist = KOBJ_INIT;
	t_unlock_cpu();

	/*
	 * 初期設定を行う。
	 */
	queue_initialize((QUEUE *)flgcb);
	flginib = flgcb->flginib;
	flginib->flgatr = pk_cflg->flgatr;
	flgcb->flgptn = flginib->iflgptn = pk_cflg->iflgptn;

	ercd = FLGID(flgcb);

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	flgcb->exist = KOBJ_EXIST;

    exit:
	LOG_ACRE_FLG_LEAVE(ercd);
	return(ercd);
}

#endif /* __acre_flg */

/*
 *  イベントフラグの削除
 */
#ifdef __del_flg
SYSCALL ER
del_flg(ID flgid)
{
	FLGCB	*flgcb;
	ER	ercd;
    
	LOG_DEL_FLG_ENTER(flgid);
	CHECK_TSKCTX_UNL();

	CHECK_FLGID(flgid);
	flgcb = get_flgcb(flgid);

	CHECK_OBJECT_DELETABLE(flgcb);

	/* 待ち行列の後片付け。削除に伴う待ち解除を行う。*/
	while (queue_empty(&(flgcb->wait_queue)) == FALSE) {
		TCB *tcb;
		tcb = (TCB *)queue_delete_next(&(flgcb->wait_queue));
		t_lock_cpu();
		if (wait_deleted(tcb)) {
			dispatch();
		}
		t_unlock_cpu();
	}
	  
	if (!CHECK_OBJECT_RESERVED(flgcb)) {
	  /*
	   * 予約IDでなければフリーリストに入れる。
	   */
		t_lock_cpu();
		queue_insert_prev(&flg_freelist, (QUEUE *)flgcb);
		t_unlock_cpu();
	}

	/* ここから先はacre_/cre_でのアクセスが可能 */
	flgcb->exist = KOBJ_NOEXS;

	ercd = E_OK;

    exit:
	LOG_DEL_FLG_LEAVE(ercd);
	return(ercd);
}

#endif /* __del_flg */

#ifdef __chk_flg_cond

QUEUE *chk_flg_cond(FLGCB *flgcb, QUEUE *queue, BOOL *disp)
{
	TCB	*tcb;
	WINFO_FLG *winfo;
	QUEUE	*ret;

	tcb = (TCB *)queue;
	ret = queue;
	winfo = (WINFO_FLG *)(tcb->winfo);
	if (eventflag_cond(flgcb, winfo->waiptn,
				winfo->wfmode, &(winfo->flgptn))) {
	  //	  	queue_delete(&(tcb->task_queue));
		ret = queue->prev;
	  	queue_delete(queue);
		if (wait_complete(tcb)) {
			*disp = TRUE;
		} else {
			*disp = FALSE;
		}
	} else {
		*disp = FALSE;
	}

	return ret;
}

#endif /* __chk_flg_cond */
/*
 *  イベントフラグのセット
 */
#ifdef __set_flg

SYSCALL ER
set_flg(ID flgid, FLGPTN setptn)
{
	FLGCB	*flgcb;
	QUEUE	*queue = NULL;
	ER	ercd;

	LOG_SET_FLG_ENTER(flgid, setptn);
	CHECK_TSKCTX_UNL();

	CHECK_FLGID(flgid);
	flgcb = get_flgcb(flgid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(flgcb);

	flgcb->flgptn |= setptn;
	if ((flgcb->flginib->flgatr & TA_WMUL) != TA_WMUL ||
	    (flgcb->flginib->flgatr & TA_CLR) == TA_CLR) {
		if (!(queue_empty(&(flgcb->wait_queue)))) {
			BOOL disp;
			queue = flgcb->wait_queue.next;
			chk_flg_cond(flgcb, queue, &disp);
			if (disp) {
				dispatch();
			}
		}
	} else {
		queue = NULL;
		while ((queue = queue_enumerate(&(flgcb->wait_queue), queue)) != NULL) {
			BOOL disp;
			queue = chk_flg_cond(flgcb, queue, &disp);
			if (disp) {
				dispatch();
			}
		}
	}
	ercd = E_OK;

	t_unlock_cpu();

    exit:
	LOG_SET_FLG_LEAVE(ercd);
	return(ercd);
}

#endif /* __set_flg */

/*
 *  イベントフラグのセット（非タスクコンテキスト用）
 */
#ifdef __iset_flg

SYSCALL ER
iset_flg(ID flgid, FLGPTN setptn)
{
	QUEUE	*queue;
	FLGCB	*flgcb;
	ER	ercd;

	LOG_ISET_FLG_ENTER(flgid, setptn);
	CHECK_INTCTX_UNL();

	CHECK_FLGID(flgid);
	flgcb = get_flgcb(flgid);

	i_lock_cpu();
	I_CHECK_OBJECT_EXIST(flgcb);

	flgcb->flgptn |= setptn;
	if ((flgcb->flginib->flgatr & TA_WMUL) != TA_WMUL ||
	    (flgcb->flginib->flgatr & TA_CLR) == TA_CLR) {
		if (!(queue_empty(&(flgcb->wait_queue)))) {
			BOOL disp;
			queue = flgcb->wait_queue.next;
			chk_flg_cond(flgcb, queue, &disp);
			if (disp) {
				reqflg = TRUE;
			}
		}
	} else {
		queue = NULL;
		while ((queue = queue_enumerate(&(flgcb->wait_queue), queue)) != NULL) {
			BOOL disp;
			queue = chk_flg_cond(flgcb, queue, &disp);
			if (disp) {
				reqflg = TRUE;
			}
		}
	}
	ercd = E_OK;

	i_unlock_cpu();

    exit:
	LOG_ISET_FLG_LEAVE(ercd);
	return(ercd);
}

#endif /* __iset_flg */

/*
 *  イベントフラグのクリア
 */
#ifdef __clr_flg

SYSCALL ER
clr_flg(ID flgid, FLGPTN clrptn)
{
	FLGCB	*flgcb;
	ER	ercd;

	LOG_CLR_FLG_ENTER(flgid, clrptn);
	CHECK_TSKCTX_UNL();

	CHECK_FLGID(flgid);
	flgcb = get_flgcb(flgid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(flgcb);

	flgcb->flgptn &= clrptn; 
	ercd = E_OK;

	t_unlock_cpu();

    exit:
	LOG_CLR_FLG_LEAVE(ercd);
	return(ercd);
}

#endif /* __clr_flg */

/*
 *  イベントフラグ待ち
 */
#ifdef __wai_flg

SYSCALL ER
wai_flg(ID flgid, FLGPTN waiptn, MODE wfmode, FLGPTN *p_flgptn)
{
	FLGCB	*flgcb;
	WINFO_FLG winfo;
	ER	ercd;

	LOG_WAI_FLG_ENTER(flgid, waiptn, wfmode, p_flgptn);
	CHECK_DISPATCH();
	CHECK_PAR(waiptn != 0);
	CHECK_PAR((wfmode & ~TWF_ORW) == 0);

	CHECK_FLGID(flgid);
	flgcb = get_flgcb(flgid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(flgcb);

	if (!(queue_empty(&(flgcb->wait_queue))) &&
	    (flgcb->flginib->flgatr & TA_WMUL) == 0) {
		ercd = E_ILUSE;
	}
	else if (eventflag_cond(flgcb, waiptn, wfmode, p_flgptn)) {
		ercd = E_OK;
	}
	else {
		winfo.waiptn = waiptn;
		winfo.wfmode = wfmode;
		wobj_make_wait((WOBJCB *) flgcb, (WINFO_WOBJ *) &winfo);
		dispatch();
		ercd = winfo.winfo.wercd;
		if (ercd == E_OK) {
			*p_flgptn = winfo.flgptn;
		}
	}

	t_unlock_cpu();

    exit:
	LOG_WAI_FLG_LEAVE(ercd, *p_flgptn);
	return(ercd);
}

#endif /* __wai_flg */

/*
 *  イベントフラグ待ち（ポーリング）
 */
#ifdef __pol_flg

SYSCALL ER
pol_flg(ID flgid, FLGPTN waiptn, MODE wfmode, FLGPTN *p_flgptn)
{
	FLGCB	*flgcb;
	ER	ercd;

	LOG_POL_FLG_ENTER(flgid, waiptn, wfmode, p_flgptn);
	CHECK_TSKCTX_UNL();
	CHECK_PAR(waiptn != 0);
	CHECK_PAR((wfmode & ~TWF_ORW) == 0);

	CHECK_FLGID(flgid);
	flgcb = get_flgcb(flgid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(flgcb);

	if (!(queue_empty(&(flgcb->wait_queue))) &&
	    (flgcb->flginib->flgatr & TA_WMUL) == 0) {
		ercd = E_ILUSE;
	}
	else if (eventflag_cond(flgcb, waiptn, wfmode, p_flgptn)) {
		ercd = E_OK;
	}
	else {
		ercd = E_TMOUT;
	}

	t_unlock_cpu();

    exit:
	LOG_POL_FLG_LEAVE(ercd, *p_flgptn);
	return(ercd);
}

#endif /* __pol_flg */

/*
 *  イベントフラグ待ち（タイムアウトあり）
 */
#ifdef __twai_flg

SYSCALL ER
twai_flg(ID flgid, FLGPTN waiptn, MODE wfmode, FLGPTN *p_flgptn, TMO tmout)
{
	FLGCB	*flgcb;
	WINFO_FLG winfo;
	TMEVTB	tmevtb;
	ER	ercd;

	LOG_TWAI_FLG_ENTER(flgid, waiptn, wfmode, p_flgptn, tmout);
	CHECK_DISPATCH();
	CHECK_PAR(waiptn != 0);
	CHECK_PAR((wfmode & ~TWF_ORW) == 0);
	CHECK_TMOUT(tmout);

	CHECK_FLGID(flgid);
	flgcb = get_flgcb(flgid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(flgcb);

	if (!(queue_empty(&(flgcb->wait_queue))) &&
	    (flgcb->flginib->flgatr & TA_WMUL) == 0) {
		ercd = E_ILUSE;
	}
	else if (eventflag_cond(flgcb, waiptn, wfmode, p_flgptn)) {
		ercd = E_OK;
	}
	else if (tmout == TMO_POL) {
		ercd = E_TMOUT;
	}
	else {
		winfo.waiptn = waiptn;
		winfo.wfmode = wfmode;
		wobj_make_wait_tmout((WOBJCB *) flgcb, (WINFO_WOBJ *) &winfo,
						&tmevtb, tmout);
		dispatch();
		ercd = winfo.winfo.wercd;
		if (ercd == E_OK) {
			*p_flgptn = winfo.flgptn;
		}
	}

	t_unlock_cpu();

    exit:
	LOG_TWAI_FLG_LEAVE(ercd, *p_flgptn);
	return(ercd);
}

#endif /* __twai_flg */

/*
 *  イベントフラグの状態参照
 */
#ifdef __ref_flg
SYSCALL ER
ref_flg(ID flgid, T_RFLG *pk_rflg)
{
	FLGCB	*flgcb;
	ER	ercd;
    
	LOG_REF_FLG_ENTER(flgid);
	CHECK_TSKCTX_UNL();

	CHECK_FLGID(flgid);
	flgcb = get_flgcb(flgid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(flgcb);

	if (!queue_empty(&(flgcb->wait_queue))) {
		TCB	*tcb;
		tcb = (TCB *)queue_peek_next(&(flgcb->wait_queue));
		pk_rflg->wtskid = TSKID(tcb);
	} else {
		pk_rflg->wtskid = TSK_NONE;
	}
	pk_rflg->flgptn = flgcb->flgptn;
	ercd = E_OK;

	t_unlock_cpu();

    exit:
	LOG_REF_FLG_LEAVE(ercd);
	return(ercd);
}

#endif /* __ref_flg */

