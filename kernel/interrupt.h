/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2003-2006 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/interrupt.h 6183 2006-11-05T03:43:32.794177Z monaka  $
 */

/*
 *	割込み管理機能
 */

#ifndef TOPPERS_INTERRUPT_H
#define TOPPERS_INTERRUPT_H

#include "queue.h"

/*
 *  割込みハンドラ初期化ブロック
 */
typedef struct interrupt_handler_initialization_block {
	INHNO	inhno;		/* 割込みハンドラ番号 */
	ATR	inhatr;		/* 割込みハンドラ属性 */
	FP	inthdr;		/* 割込みハンドラの起動番地 */
} INHINIB;

/*
 *  割込みサービスルーチン初期化ブロック
 */
typedef struct interrupt_service_routine_initialization_block {
	ATR	isratr;		/* 割込みサービスルーチン属性 */
	VP_INT	exinf;		/* 割込みサービスルーチンの拡張情報 */
	INTNO	intno;		/* 割込みサービスルーチンを付加する割込み番号 */
	FP	isr;		/* 割込みサービスルーチンの起動番地 */
} ISRINIB;

typedef struct interrupt_service_routine_initialization_block ATTACHED_ISRINIB;

/*
 *  割込みサービスルーチン初期化ブロック連想ペア
 */
typedef struct interrupt_service_rourine_initialization_block_apair {
	ID	isrid;
	ISRINIB	isrinib;
} ISRINIA;

/*
 *  割込みサービスルーチン管理ブロック
 */
typedef struct interrupt_service_control_block {
	QUEUE	wait_queue;	/* セマフォ待ちキュー */
	ISRINIB *isrinib;	/* 割込みサービスルーチン初期化ブロックへのポインタ */
	unsigned int	exist:2;	/* 使われているとき、KOBJ_EXIST */
	unsigned int	reserved:1;
} ISRCB;

/*
 *  割込み管理機能の初期化
 */
extern void	interrupt_initialize(void);

/*
 *  静的生成した割込みサービスルーチン(ATT_ISR)の初期化
 */
extern void	attached_isr_initialize(void);

/*
 *  割込みサービスルーチン(cre_,acre_isr)の初期化
 */
extern void	interrupt_service_initialize(void);

#define ISRID(isrcb)	((ID)((isrcb) - isrcb_table) + TMIN_ISRID)

#endif /* TOPPERS_INTERRUPT_H */
