/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2003-2004 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/stdsvc.c 2750 2006-03-07T16:15:24.532712Z monaka  $
 */

#include <stdarg.h>
#include "fi4_kernel.h"

#ifdef __calstdsvc
ER
__cal_cre_tsk(va_list va)
{
	ER ercd;
	ID tskid;
	T_CTSK *pk_ctsk;

	tskid = va_arg(va, ID);
	pk_ctsk = va_arg(va, T_CTSK *);

	ercd = cre_tsk(tskid, pk_ctsk);

	return ercd;
}

ER_UINT
__cal_del_tsk(va_list va)
{
	ER ercd;
	ID tskid;
	
	tskid = va_arg(va, ID);

	ercd = del_tsk(tskid);

	return ercd;
}

ER_UINT
__cal_act_tsk(va_list va)
{
	ER ercd;
	ID tskid;

	tskid = va_arg(va, ID);
	
	ercd = act_tsk(tskid);

	return ercd;
}

ER_UINT
__cal_can_act(va_list va)
{
	ER_UINT actcnt;
	ID tskid;

	tskid = va_arg(va, ID);

	actcnt = can_act(tskid);

	return actcnt;
	
}

ER_UINT
__cal_sta_tsk(va_list va)
{
	ER ercd;
	ID tskid;
	VP_INT stacd;

	tskid = va_arg(va, ID);
	stacd = va_arg(va, VP_INT);

	ercd = sta_tsk(tskid, stacd);

	return ercd;
}

void
__cal_ext_tsk(va_list va)
{
	ext_tsk();
}

void
__cal_exd_tsk(va_list va)
{
	exd_tsk();
}

ER_UINT
__cal_ter_tsk(va_list va)
{
	ER ercd;
	ID tskid;

	tskid = va_arg(va, ID);

	ercd = ter_tsk(tskid);

	return ercd;
}

ER_UINT
__cal_chg_pri(va_list va)
{
	ER ercd;
	ID tskid;
	PRI tskpri;

	tskid = va_arg(va, ID);
	tskpri = va_arg(va, PRI);

	ercd = chg_pri(tskid, tskpri);

	return ercd;
}

ER_UINT
__cal_get_pri(va_list va)
{
	ER ercd;
	ID tskid;
	PRI *p_tskpri;
	
	tskid = va_arg(va, ID);
	p_tskpri = va_arg(va, PRI *);

	ercd = get_pri(tskid, p_tskpri);

	return ercd;
}

ER_UINT
__cal_ref_tsk(va_list va)
{
	ER ercd;
	ID tskid;
	T_RTSK *pk_rtsk;

	tskid = va_arg(va, ID);
	pk_rtsk = va_arg(va, T_RTSK *);

	ercd = ref_tsk(tskid, pk_rtsk);

	return ercd;
}

ER_UINT
__cal_ref_tst(va_list va)
{
	ER ercd;
	ID tskid;
	T_RTST *pk_rtst;

	tskid = va_arg(va, ID);
	pk_rtst = va_arg(va, T_RTST *);

	ercd = ref_tst(tskid, pk_rtst);

	return ercd;
}

ER_UINT
__cal_slp_tsk(va_list va)
{
	ER ercd;
	
	ercd = slp_tsk();

	return ercd;
}

ER_UINT
__cal_tslp_tsk(va_list va)
{
	ER ercd;
	TMO tmout;

	tmout = va_arg(va, TMO);

	ercd = tslp_tsk(tmout);

	return ercd;
}

ER_UINT
__cal_wup_tsk(va_list va)
{
	ER ercd;
	ID tskid;

	tskid = va_arg(va, ID);

	ercd = wup_tsk(tskid);

	return ercd;
}

ER_UINT
__cal_can_wup(va_list va)
{
	ER_UINT wupcnt;
	ID tskid;

	tskid = va_arg(va, ID);

	wupcnt = can_wup(tskid);

	return wupcnt;
}

ER_UINT
__cal_rel_wai(va_list va)
{
	ER ercd;
	ID tskid;

	tskid = va_arg(va, ID);

	ercd = rel_wai(tskid);

	return ercd;
}

ER_UINT
__cal_sus_tsk(va_list va)
{
	ER ercd;
	ID tskid;

	tskid = va_arg(va, ID);

	ercd = sus_tsk(tskid);

	return ercd;
}

ER_UINT
__cal_rsm_tsk(va_list va)
{
	ER ercd;
	ID tskid;

	tskid = va_arg(va, ID);

	ercd = rsm_tsk(tskid);

	return ercd;
}

ER_UINT
__cal_frsm_tsk(va_list va)
{
	ER ercd;
	ID tskid;

	tskid = va_arg(va, ID);

	ercd = frsm_tsk(tskid);

	return ercd;
}

ER_UINT
__cal_dly_tsk(va_list va)
{
	ER ercd;
	RELTIM dlytim;

	dlytim = va_arg(va, RELTIM);

	ercd = dly_tsk(dlytim);

	return ercd;
}

ER_UINT
__cal_def_tex(va_list va)
{
	ER ercd;
	ID tskid;
	T_DTEX *pk_dtex;

	tskid = va_arg(va, ID);
	pk_dtex = va_arg(va, T_DTEX *);

	ercd = def_tex(tskid, pk_dtex);

	return ercd;
}

ER_UINT
__cal_ras_tex(va_list va)
{
	ER ercd;
	ID tskid;
	TEXPTN rasptn;

	tskid = va_arg(va, ID);
	rasptn = va_arg(va, TEXPTN);

	ercd = ras_tex(tskid, rasptn);

	return ercd;
}

ER_UINT
__cal_dis_tex(va_list va)
{
	ER ercd;

	ercd = dis_tex();

	return ercd;
}

ER_UINT
__cal_ena_tex(va_list va)
{
	ER ercd;

	ercd = ena_tex();

	return ercd;
}

ER_UINT
__cal_sns_tex(va_list va)
{
	BOOL state;

	state = sns_tex();

	return state;
}

ER_UINT
__cal_ref_tex(va_list va)
{
	ER ercd;
	ID tskid;
	T_RTEX *pk_rtex;

	tskid = va_arg(va, ID);
	pk_rtex = va_arg(va, T_RTEX *);

	ercd = ref_tex(tskid, pk_rtex);

	return ercd;
}

ER_UINT
__cal_cre_sem(va_list va)
{
	ER ercd;
	ID semid;
	T_CSEM *pk_csem;

	semid = va_arg(va, ID);
	pk_csem = va_arg(va, T_CSEM *);

	ercd = cre_sem(semid, pk_csem);

	return ercd;
}

ER_UINT
__cal_del_sem(va_list va)
{
	ER ercd;
	ID semid;

	semid = va_arg(va, ID);

	ercd = del_sem(semid);

	return ercd;
}

ER_UINT
__cal_sig_sem(va_list va)
{
	ER ercd;
	ID semid;

	semid = va_arg(va, ID);

	ercd = sig_sem(semid);

	return ercd;
}

ER_UINT
__cal_wai_sem(va_list va)
{
	ER ercd;
	ID semid;

	ercd = wai_sem(semid);

	return ercd;
}

ER_UINT
__cal_pol_sem(va_list va)
{
	ER ercd;
	ID semid;

	ercd = pol_sem(semid);

	return ercd;
}

ER_UINT
__cal_twai_sem(va_list va)
{
	ER ercd;
	ID semid;
	TMO tmout;

	ercd = twai_sem(semid, tmout);

	return ercd;
}

ER_UINT
__cal_ref_sem(va_list va)
{
	ER ercd;
	ID semid;
	T_RSEM *pk_rsem;

	semid = va_arg(va, ID);
	pk_rsem = va_arg(va, T_RSEM *);

	ercd = ref_sem(semid, pk_rsem);

	return ercd;
}

ER_UINT
__cal_cre_flg(va_list va)
{
	ER ercd;
	ID flgid;
	T_CFLG *pk_cflg;

	flgid = va_arg(va, ID);
	pk_cflg = va_arg(va, T_CFLG *);
 
	ercd = cre_flg(flgid, pk_cflg);

	return ercd;
}

ER_UINT
__cal_del_flg(va_list va) 
{
	ER ercd;
	ID flgid;

	flgid = va_arg(va, ID);

	ercd = del_flg(flgid);

	return ercd;
}

ER_UINT
__cal_set_flg(va_list va)
{
	ER ercd;
	ID flgid;
	FLGPTN setptn;

	flgid = va_arg(va, ID);
	setptn = va_arg(va, FLGPTN);

	ercd = set_flg(flgid, setptn);

	return ercd;
}

ER_UINT
__cal_clr_flg(va_list va)
{
	ER ercd;
	ID flgid;
	FLGPTN clrptn;

	flgid = va_arg(va, ID);
	clrptn = va_arg(va, FLGPTN);

	ercd = clr_flg(flgid, clrptn);

	return ercd;
}

ER_UINT
__cal_wai_flg(va_list va)
{
	ER ercd;
	ID flgid;
	FLGPTN waiptn;
	MODE wfmode;
	FLGPTN *p_flgptn;

	flgid = va_arg(va, ID);
	waiptn = va_arg(va, FLGPTN);
	wfmode = va_arg(va, MODE);
	p_flgptn = va_arg(va, FLGPTN *);

	ercd = wai_flg(flgid, waiptn, wfmode, p_flgptn);

	return ercd;
}

ER_UINT
__cal_pol_flg(va_list va)
{
	ER ercd;
	ID flgid;
	FLGPTN waiptn;
	MODE wfmode;
	FLGPTN *p_flgptn;

	flgid = va_arg(va, ID);
	waiptn = va_arg(va, FLGPTN);
	wfmode = va_arg(va, MODE);
	p_flgptn = va_arg(va, FLGPTN *);

	ercd = pol_flg(flgid, waiptn, wfmode, p_flgptn);

	return ercd;
}	

ER_UINT
__cal_twai_flg(va_list va)
{
	ER ercd;
	ID flgid;
	FLGPTN waiptn;
	MODE wfmode;
	TMO tmout;
	FLGPTN *p_flgptn;

	flgid = va_arg(va, ID);
	waiptn = va_arg(va, FLGPTN);
	wfmode = va_arg(va, MODE);
	tmout = va_arg(va, TMO);
	p_flgptn = va_arg(va, FLGPTN *);

	ercd = twai_flg(flgid, waiptn, wfmode, p_flgptn, tmout);

	return ercd;
}

ER_UINT
__cal_ref_flg(va_list va)
{
	ER ercd;
	ID flgid;
	T_RFLG *pk_rflg;

	flgid = va_arg(va, ID);
	pk_rflg = va_arg(va, T_RFLG *);

	ercd = ref_flg(flgid, pk_rflg);

	return ercd;
}

ER_UINT
__cal_cre_dtq(va_list va)
{
	ER ercd;
	ID dtqid;
	T_CDTQ *pk_cdtq;
	
	dtqid = va_arg(va, ID);
	pk_cdtq = va_arg(va, T_CDTQ *);

	ercd = cre_dtq(dtqid, pk_cdtq);

	return ercd;
}

ER_UINT
__cal_del_dtq(va_list va)
{
	ER ercd;
	ID dtqid;
	
	dtqid = va_arg(va, ID);

	ercd = del_dtq(dtqid);

	return ercd;
}

ER_UINT
__cal_snd_dtq(va_list va)
{
	ER ercd;
	ID dtqid;
	VP_INT data;

	dtqid = va_arg(va, ID);
	data = va_arg(va, VP_INT);

	ercd = snd_dtq(dtqid, data);

	return ercd;
}

ER_UINT
__cal_psnd_dtq(va_list va)
{
	ER ercd;
	ID dtqid;
	VP_INT data;

	dtqid = va_arg(va, ID);
	data = va_arg(va, VP_INT);

	ercd = psnd_dtq(dtqid, data);

	return ercd;
}

ER_UINT
__cal_tsnd_dtq(va_list va)
{
	ER ercd;
	ID dtqid;
	VP_INT data;
	TMO tmout;

	dtqid = va_arg(va, ID);
	data = va_arg(va, VP_INT);
	tmout = va_arg(va, TMO);

	ercd = tsnd_dtq(dtqid, data, tmout);

	return ercd;
}

ER_UINT
__cal_fsnd_dtq(va_list va)
{
	ER ercd;
	ID dtqid;
	VP_INT data;

	dtqid = va_arg(va, ID);
	data = va_arg(va, VP_INT);

	ercd = fsnd_dtq(dtqid, data);
	
	return ercd;
}

ER_UINT
__cal_rcv_dtq(va_list va)
{
	ER ercd;
	ID dtqid;
	VP_INT *p_data;

	dtqid = va_arg(va, ID);
	p_data = va_arg(va, VP_INT *);

	ercd = rcv_dtq(dtqid, p_data);

	return ercd;
}

ER_UINT
__cal_prcv_dtq(va_list va)
{
	ER ercd;
	ID dtqid;
	VP_INT *p_data;

	dtqid = va_arg(va, ID);
	p_data = va_arg(va, VP_INT *);

	ercd = prcv_dtq(dtqid, p_data);

	return ercd;
}

ER_UINT
__cal_trcv_dtq(va_list va)
{
	ER ercd;
	ID dtqid;
	VP_INT *p_data;
	TMO tmout;

	dtqid = va_arg(va, ID);
	p_data = va_arg(va, VP_INT *);
	tmout = va_arg(va, TMO);

	ercd = trcv_dtq(dtqid, p_data, tmout);

	return ercd;
}

ER_UINT
__cal_ref_dtq(va_list va)
{
	ER ercd;
	ID dtqid;
	T_RDTQ *pk_rdtq;

	dtqid = va_arg(va, ID);
	pk_rdtq = va_arg(va, T_RDTQ *);

	ercd = ref_dtq(dtqid, pk_rdtq);

	return ercd;
}

ER_UINT
__cal_cre_mbx(va_list va)
{
	ER ercd;
	ID mbxid;
	T_CMBX *pk_cmbx;

	mbxid = va_arg(va, ID);
	pk_cmbx = va_arg(va, T_CMBX *);

	ercd = cre_mbx(mbxid, pk_cmbx);

	return ercd;
}

ER_UINT
__cal_del_mbx(va_list va)
{
	ER ercd;
	ID mbxid;
	
	mbxid = va_arg(va, ID);

	ercd = del_mbx(mbxid);

	return ercd;
}

ER_UINT
__cal_snd_mbx(va_list va)
{
	ER ercd;
	ID mbxid;
	T_MSG *pk_msg;

	mbxid =va_arg(va, ID);
	pk_msg = va_arg(va, T_MSG *);

	ercd = snd_mbx(mbxid, pk_msg);

	return ercd;
}

ER_UINT
__cal_rcv_mbx(va_list va) 
{
	ER ercd;
	ID mbxid;
	T_MSG **ppk_msg;

	mbxid = va_arg(va, ID);
	ppk_msg = va_arg(va, T_MSG **);

	ercd = rcv_mbx(mbxid, ppk_msg);

	return ercd;
}

ER_UINT
__cal_prcv_mbx(va_list va)
{
	ER ercd;
	ID mbxid;
	T_MSG **ppk_msg;

	mbxid = va_arg(va, ID);
	ppk_msg = va_arg(va, T_MSG **);

	ercd = prcv_mbx(mbxid, ppk_msg);

	return ercd;
}

ER_UINT
__cal_trcv_mbx(va_list va)
{
	ER ercd;
	ID mbxid;
	T_MSG **ppk_msg;
	TMO tmout;

	mbxid = va_arg(va, ID);
	ppk_msg = va_arg(va, T_MSG **);
	tmout = va_arg(va, TMO);

	ercd = trcv_mbx(mbxid, ppk_msg, tmout);

	return ercd;
}

ER_UINT
__cal_ref_mbx(va_list va)
{
	ER ercd;
	ID mbxid;
	T_RMBX *pk_rmbx;

	mbxid = va_arg(va, ID);
	pk_rmbx = va_arg(va, T_RMBX *);

	ercd = ref_mbx(mbxid, pk_rmbx);

	return ercd;
}

ER_UINT
__cal_cre_mpf(va_list va)
{
	ER ercd;
	ID mpfid;
	T_CMPF *pk_cmpf;

	mpfid = va_arg(va, ID);
	pk_cmpf = va_arg(va, T_CMPF *);

	ercd = cre_mpf(mpfid, pk_cmpf);

	return ercd;
}

ER_UINT
__cal_del_mpf(va_list va)
{
	ER ercd;
	ID mpfid;

	mpfid = va_arg(va, ID);

	ercd = del_mpf(mpfid);

	return ercd;
}

ER_UINT
__cal_ref_mpf(va_list va)
{
	ER ercd;
	ID mpfid;
	T_RMPF *pk_rmpf;

	mpfid = va_arg(va, ID);
	pk_rmpf = va_arg(va, T_RMPF *);

	ercd = ref_mpf(mpfid, pk_rmpf);

	return ercd;
}

ER_UINT
__cal_get_mpf(va_list va)
{
	ER ercd;
	ID mpfid;
	VP *p_blk;

	mpfid = va_arg(va, ID);
	p_blk = va_arg(va, VP *);

	ercd = get_mpf(mpfid, p_blk);

	return ercd;
}

ER_UINT
__cal_pget_mpf(va_list va)
{
	ER ercd;
	ID mpfid;
	VP *p_blk;
	
	mpfid = va_arg(va, ID);
	p_blk = va_arg(va, VP *);

	ercd = pget_mpf(mpfid, p_blk);

	return ercd;
}

ER_UINT
__cal_tget_mpf(va_list va)
{
	ER ercd;
	ID mpfid;
	VP *p_blk;
	TMO tmout;

	mpfid = va_arg(va, ID);
	p_blk = va_arg(va, VP *);
	tmout = va_arg(va, TMO);

	ercd = tget_mpf(mpfid, p_blk, tmout);

	return ercd;
}

ER_UINT
__cal_rel_mpf(va_list va)
{
	ER ercd;
	ID mpfid;
	VP blk;

	mpfid = va_arg(va, ID);
	blk = va_arg(va, VP);

	ercd = ref_mpf(mpfid, blk);

	return ercd;
}

ER_UINT
__cal_ref_mpl(va_list va)
{
	ER ercd;
	ID mpfid;
	T_RMPL *pk_rmpl;

	mpfid = va_arg(va, ID);
	pk_rmpl = va_arg(va, T_RMPL *);

	ercd = ref_mpl(mpfid, pk_rmpl);

	return ercd;
}

ER_UINT
__cal_set_tim(va_list va)
{
	ER ercd;
	SYSTIM *p_systim;

	p_systim = va_arg(va, SYSTIM *);

	ercd = set_tim(p_systim);

	return ercd;
}

ER_UINT
__cal_get_tim(va_list va)
{
	ER ercd;
	SYSTIM *p_systim;

	ercd = get_tim(p_systim);

	return ercd;
}

ER_UINT
__cal_cre_cyc(va_list va)
{
	ER ercd;
	ID cycid;
	T_CCYC *pk_ccyc;

	cycid = va_arg(va, ID);
	pk_ccyc = va_arg(va, T_CCYC *);

	ercd = cre_cyc(cycid, pk_ccyc);

	return ercd;
}

ER_UINT
__cal_del_cyc(va_list va)
{
	ER ercd;
	ID cycid;

	cycid = va_arg(va, ID);
	
	ercd = del_cyc(cycid);

	return ercd;
}

ER_UINT
__cal_sta_cyc(va_list va)
{
	ER ercd;
	ID cycid;

	cycid = va_arg(va, ID);
	ercd = sta_cyc(cycid);
	return ercd;
}

ER_UINT
__cal_stp_cyc(va_list va)
{
	ER ercd;
	ID cycid;

	cycid = va_arg(va, ID);

	ercd = stp_cyc(cycid);
	return ercd;
}

ER_UINT
__cal_ref_cyc(va_list va)
{
	ER ercd;
	ID cycid;
	T_RCYC *pk_rcyc;

	cycid = va_arg(va, ID);
	pk_rcyc = va_arg(va, T_RCYC *);

	return ercd;
}

ER_UINT
__cal_rot_rdq(va_list va)
{
	ER ercd;
	PRI tskpri;

	tskpri = va_arg(va, PRI);

	ercd = rot_rdq(tskpri);

	return ercd;
}

ER_UINT
__cal_get_tid(va_list va)
{
	ER ercd;
	ID *p_tskid;

	p_tskid = va_arg(va, ID *);
	
	ercd = get_tid(p_tskid);

	return ercd;
}

ER_UINT
__cal_loc_cpu(va_list va)
{
	ER ercd;

	ercd = loc_cpu();

	return ercd;
}

ER_UINT
__cal_unl_cpu(va_list va)
{
	ER ercd;
	ercd = unl_cpu();
	return ercd;
}

ER_UINT
__cal_dis_dsp(va_list va)
{
	ER ercd;

	ercd = dis_dsp();

	return ercd;
}

ER_UINT
__cal_ena_dsp(va_list va)
{
	ER ercd;

	ercd = ena_dsp();
	
	return ercd;
}

ER_UINT
__cal_sns_ctx(va_list va)
{
	BOOL state;
	state = sns_ctx();

	return state;
}

ER_UINT
__cal_sns_loc(va_list va)
{
	BOOL state;
	state = sns_loc();
	return state;
}

ER_UINT
__cal_sns_dsp(va_list va)
{
	BOOL state;
	state = sns_dsp();
	return state;
}

ER_UINT
__cal_sns_dpn(va_list va)
{
	BOOL state;
	state = sns_dpn();
	return state;
}

ER_UINT
__cal_ref_sys(va_list va)
{
	ER ercd;
	T_RSYS *pk_rsys;

	pk_rsys = va_arg(va, T_RSYS *);
	ercd = ref_sys(pk_rsys);

	return ercd;
}

ER_UINT
__cal_def_inh(va_list va)
{
	ER ercd;
	INHNO inhno;
	T_DINH *pk_dinh;

	inhno = va_arg(va, INHNO);
	pk_dinh = va_arg(va, T_DINH *);

	ercd = def_inh(inhno, pk_dinh);
	return ercd;
}

ER_UINT
__cal_cre_isr(va_list va)
{
	ER ercd;
	ID isrid;
	T_CISR *pk_cisr;

	isrid = va_arg(va, ID);
	pk_cisr = va_arg(va, T_CISR *);

	ercd = cre_isr(isrid, pk_cisr);

	return ercd;
}

ER_UINT
__cal_del_isr(va_list va)
{
	ER ercd;
	ID isrid;

	isrid = va_arg(va, ID);

	ercd = del_isr(isrid);

	return ercd;
}

ER_UINT
__cal_ref_isr(va_list va) 
{
	ER ercd;
	ID isrid;
	T_RISR *pk_risr;

	isrid = va_arg(va, ID);
	pk_risr = va_arg(va, T_RISR *);

	ercd = ref_isr(isrid, pk_risr);
	return ercd;
}

ER_UINT
__cal_dis_int(va_list va)
{
	ER ercd;
	INTNO intno;

	intno = va_arg(va, INTNO);

	ercd = dis_int(intno);
	return ercd;
}

ER_UINT
__cal_ena_int(va_list va)
{
	ER ercd;
	INTNO intno;
	intno = va_arg(va, INTNO);
	ercd = ena_int(intno);
	return ercd;
}

#if 0
ER_UINT
__cal_chg_ixx(va_list va)
{
	ER ercd;
	IXXXX ixxxx;

	ixxxx = va_arg(va, IXXXX);

	ercd = chg_ixx(ixxxx);
	return ercd;
}

ER_UINT
__cal_get_ixx(va_list va)
{
	ER ercd;
	IXXXX *p_ixxxx;

	p_ixxxx = va_arg(va, IXXXX *);

	ercd = get_ixx(p_ixxxx);

	return ercd;
}
#endif

ER_UINT
__cal_def_svc(va_list va)
{
	ER ercd;
	FN fncd;
	T_DSVC *pk_dsvc;
	
	fncd = va_arg(va, FN);
	pk_dsvc = va_arg(va, T_DSVC *);

	ercd = def_svc(fncd, pk_dsvc);

	return ercd;

}

ER_UINT
__cal_def_exc(va_list va)
{
	ER_UINT ercd;
	EXCNO excno;
	T_DEXC *pk_dexc;

	excno = va_arg(va, EXCNO);
	pk_dexc = va_arg(va, T_DEXC *);

	ercd = def_exc(excno, pk_dexc);
	return ercd;
}

ER_UINT
__cal_ref_cfg(va_list va)
{
	ER ercd;
	T_RCFG *pk_rcfg;

	pk_rcfg = va_arg(va, T_RCFG *);
	
	ercd = ref_cfg(pk_rcfg);

	return ercd;
}

ER_UINT
__cal_ref_ver(va_list va)
{
	ER ercd;
	T_RVER *pk_rver;

	pk_rver = va_arg(va, T_RVER *);

	ercd = ref_ver(pk_rver);

	return ercd;
}

ER_UINT
__cal_iact_tsk(va_list va)
{
	ER ercd;
	ID tskid;

	tskid = va_arg(va, ID);

	ercd = iact_tsk(tskid);

	return ercd;
}

ER_UINT
__cal_iwup_tsk(va_list va)
{
	ER ercd;
	ID tskid;

	tskid = va_arg(va, ID);

	ercd = iwup_tsk(tskid);

	return ercd;
}

ER_UINT
__cal_irel_wai(va_list va)
{
	ER ercd;
	ID tskid;
	
	tskid = va_arg(va, ID);

	ercd = irel_wai(tskid);

	return ercd;
}

ER_UINT
__cal_iras_tex(va_list va)
{
	ER ercd;
	ID tskid;
	TEXPTN rasptn;

	tskid = va_arg(va, ID);
	rasptn = va_arg(va, TEXPTN);

	ercd = iras_tex(tskid, rasptn);

	return ercd;
}

ER_UINT
__cal_isig_sem(va_list va)
{
	ER ercd;
	ID semid;

	semid = va_arg(va, ID);

	ercd = isig_sem(semid);
	return ercd;
}

ER_UINT
__cal_iset_flg(va_list va)
{
	ER ercd;
	ID flgid;
	FLGPTN setptn;

	flgid = va_arg(va, ID);
	setptn = va_arg(va, FLGPTN);
	ercd = iset_flg(flgid, setptn);

	return ercd;
}

ER_UINT
__cal_ipsnd_dtq(va_list va)
{
	ER ercd;
	ID dtqid;
	VP_INT data;
	
	dtqid = va_arg(va, ID);
	data = va_arg(va, VP_INT);

	ercd = ipsnd_dtq(dtqid, data);

	return ercd;
}


ER_UINT
__cal_ifsnd_dtq(va_list va)
{
	ER ercd;
	ID dtqid;
	VP_INT data;

	dtqid = va_arg(va, ID);
	data = va_arg(va, VP_INT);

	ercd = ifsnd_dtq(dtqid, data);
	return ercd;
}

ER_UINT
__cal_irot_rdq(va_list va)
{
	ER ercd;
	PRI tskpri;

	tskpri = va_arg(va, PRI);

	ercd = irot_rdq(tskpri);

	return ercd;
}

ER_UINT
__cal_iget_tid(va_list va)
{
	ER ercd;
	ID *p_tskid;

	p_tskid = va_arg(va, ID *);

	ercd = iget_tid(p_tskid);

	return ercd;
}

ER_UINT
__cal_iloc_cpu(va_list va)
{
	ER ercd;

	ercd = iloc_cpu();

	return ercd;
}

ER_UINT
__cal_iunl_cpu(va_list va)
{
	ER ercd;

	ercd = iunl_cpu();

	return ercd;
}

ER_UINT
__cal_isig_tim(va_list va)
{
	ER ercd;

	ercd = isig_tim();
	return ercd;
}

ER_UINT
__cal_cre_mtx(va_list va)
{
	ER ercd;
	ID mtxid;
	T_CMTX *pk_cmtx;

	mtxid = va_arg(va, ID);
	pk_cmtx = va_arg(va, T_CMTX *);

	ercd = cre_mtx(mtxid, pk_cmtx);
	return ercd;
}

ER_UINT
__cal_del_mtx(va_list va)
{
	ER ercd;
	ID mtxid;

	mtxid = va_arg(va, ID);

	ercd = del_mtx(mtxid);

	return ercd;
}

ER_UINT
__cal_unl_mtx(va_list va)
{
	ER ercd;
	ID mtxid;

	mtxid = va_arg(va, ID);

	ercd = unl_mtx(mtxid);

	return ercd;
}

ER_UINT
__cal_loc_mtx(va_list va)
{
	ER ercd;
	ID mtxid;

	mtxid = va_arg(va, ID);

	ercd = loc_mtx(mtxid);

	return ercd;
}

ER_UINT
__cal_ploc_mtx(va_list va)
{
	ER ercd;
	ID mtxid;
	mtxid = va_arg(va, ID);
	ercd = ploc_mtx(mtxid);
	return ercd;
}

ER_UINT
__cal_tloc_mtx(va_list va)
{
	ER ercd;
	ID mtxid;
	TMO tmout;

	mtxid = va_arg(va, ID);
	tmout = va_arg(va, TMO);

	ercd = tloc_mtx(mtxid, tmout);

	return ercd;
}

ER_UINT
__cal_ref_mtx(va_list va)
{
	ER ercd;
	ID mtxid;
	T_RMTX *pk_rmtx;

	mtxid = va_arg(va, ID);
	pk_rmtx = va_arg(va, T_RMTX *);
	
	ercd = ref_mtx(mtxid, pk_rmtx);

	return ercd;
}

ER_UINT
__cal_cre_mbf(va_list va)
{
	ER ercd;
	ID mbfid;
	T_CMBF *pk_cmbf;

	mbfid = va_arg(va, ID);
	pk_cmbf = va_arg(va, T_CMBF *);

	ercd = cre_mbf(mbfid, pk_cmbf);

	return ercd;
}

ER_UINT
__cal_del_mbf(va_list va)
{
	ER ercd;
	ID mbfid;
	mbfid = va_arg(va, ID);
	ercd = del_mbf(mbfid);

	return ercd;
}

ER_UINT
__cal_snd_mbf(va_list va)
{
	ER ercd;
	ID mbfid;
	VP msg;
	UINT msgsz;

	mbfid = va_arg(va, ID);
	msg = va_arg(va, VP);
	msgsz = va_arg(va, UINT);

	ercd = snd_mbf(mbfid, msg, msgsz);

	return ercd;
}

ER_UINT
__cal_psnd_mbf(va_list va)
{
	ER ercd;
	ID mbfid;
	VP msg;
	UINT msgsz;

	mbfid = va_arg(va, ID);
	msg = va_arg(va, VP);
	msgsz = va_arg(va, UINT);

	ercd = psnd_mbf(mbfid, msg, msgsz);

	return ercd;
}

ER_UINT
__cal_tsnd_mbf(va_list va)
{
	ER ercd;
	ID mbfid;
	VP msg;
	UINT msgsz;
	TMO tmout;

	mbfid = va_arg(va, ID);
	msg = va_arg(va, VP);
	msgsz = va_arg(va, UINT);
	tmout = va_arg(va, TMO);

	ercd = tsnd_mbf(mbfid, msg, msgsz, tmout);

	return ercd;
}

ER_UINT
__cal_rcv_mbf(va_list va)
{
	ER ercd;
	ID mbfid;
	VP msg;
	
	mbfid = va_arg(va, ID);
	msg = va_arg(va, VP);

	ercd = rcv_mbf(mbfid, msg);

	return ercd;
}

ER_UINT
__cal_prcv_mbf(va_list va)
{
	ER ercd;
	ID mbfid;
	VP msg;
	
	mbfid = va_arg(va, ID);
	msg = va_arg(va, VP);

	ercd = prcv_mbf(mbfid, msg);

	return ercd;
}

ER_UINT
__cal_trcv_mbf(va_list va)
{
	ER ercd;
	ID mbfid;
	VP msg;
	TMO tmout;
	
	mbfid = va_arg(va, ID);
	msg = va_arg(va, VP);
	tmout = va_arg(va, TMO);

	ercd = trcv_mbf(mbfid, msg, tmout);

	return ercd;
}

ER_UINT
__cal_ref_mbf(va_list va)
{
	ER ercd;
	ID mbfid;
	T_RMBF *pk_rmbf;

	mbfid = va_arg(va, ID);
	pk_rmbf = va_arg(va, T_RMBF *);

	ercd = ref_mbf(mbfid, pk_rmbf);

	return ercd;
}

ER_UINT
__cal_cre_por(va_list va)
{
	ER ercd;
	ID porid;
	T_CPOR *pk_cpor;

	porid = va_arg(va, ID);
	pk_cpor = va_arg(va, T_CPOR *);

	ercd = cre_por(porid, pk_cpor);

	return ercd;
}

ER_UINT
__cal_del_por(va_list va)
{
	ER ercd;
	ID porid;

	porid = va_arg(va, ID);

	ercd = del_por(porid);

	return ercd;
}

ER_UINT
__cal_cal_por(va_list va)
{
	ER_UINT rmsgsz;
	ID porid;
	RDVPTN calptn;
	VP msg;
	UINT cmsgsz;

	porid = va_arg(va, ID);
	calptn = va_arg(va, RDVPTN);
	msg = va_arg(va, VP);
	cmsgsz = va_arg(va, UINT);

	rmsgsz = cal_por(porid, calptn, msg, cmsgsz);

	return rmsgsz;
}

ER_UINT
__cal_tcal_por(va_list va)
{
	ER_UINT rmsgsz;
	ID porid;
	RDVPTN calptn;
	VP msg;
	UINT cmsgsz;
	TMO tmout;

	porid = va_arg(va, ID);
	calptn = va_arg(va, RDVPTN);
	msg = va_arg(va, VP);
	cmsgsz = va_arg(va, UINT);
	tmout = va_arg(va, TMO);

	rmsgsz = tcal_por(porid, calptn, msg, cmsgsz, tmout);

	return rmsgsz;
}

ER_UINT
__cal_acp_por(va_list va)
{
	ER_UINT cmsgsz;
	ID porid;
	RDVPTN acpptn;
	RDVNO *p_rdvno;
	VP msg;

	porid = va_arg(va, ID);
	acpptn = va_arg(va, RDVPTN);
	p_rdvno = va_arg(va, RDVNO *);
	msg = va_arg(va, VP);

	cmsgsz = acp_por(porid, acpptn, p_rdvno, msg);

	return cmsgsz;
}

ER_UINT
__cal_pacp_por(va_list va)
{
	ER_UINT cmsgsz;
	ID porid;
	RDVPTN acpptn;
	RDVNO *p_rdvno;
	VP msg;

	porid = va_arg(va, ID);
	acpptn = va_arg(va, RDVPTN);
	p_rdvno = va_arg(va, RDVNO *);
	msg = va_arg(va, VP);

	cmsgsz = pacp_por(porid, acpptn, p_rdvno, msg);

	return cmsgsz;
}

ER_UINT
__cal_tacp_por(va_list va)
{
	ER_UINT cmsgsz;
	ID porid;
	RDVPTN acpptn;
	RDVNO *p_rdvno;
	VP msg;
	TMO tmout;

	porid = va_arg(va, ID);
	acpptn = va_arg(va, RDVPTN);
	p_rdvno = va_arg(va, RDVNO *);
	msg = va_arg(va, VP);
	tmout = va_arg(va, TMO);

	cmsgsz = tacp_por(porid, acpptn, p_rdvno, msg, tmout);

	return cmsgsz;
}

ER_UINT
__cal_fwd_por(va_list va)
{
	ER ercd;
	ID porid;
	RDVPTN calptn;
	RDVNO rdvno;
	VP msg;
	UINT cmsgsz;

	porid = va_arg(va, ID);
	calptn = va_arg(va, RDVPTN);
	rdvno = va_arg(va, RDVNO);
	msg = va_arg(va, VP);
	cmsgsz = va_arg(va, UINT);

	ercd = fwd_por(porid, calptn, rdvno, msg, cmsgsz);

	return ercd;
}

ER_UINT
__cal_rpl_rdv(va_list va)
{
	ER ercd;
	RDVNO rdvno;
	VP msg;
	UINT rmsgsz;
	
	rdvno = va_arg(va, RDVNO);
	msg = va_arg(va, VP);
	rmsgsz = va_arg(va, UINT);

	ercd = rpl_rdv(rdvno, msg, rmsgsz);
	return ercd;
}

ER_UINT
__cal_ref_por(va_list va)
{
	ER ercd;
	ID porid;
	T_RPOR *pk_rpor;
	porid = va_arg(va, ID);
	pk_rpor = va_arg(va, T_RPOR *);
	ercd = ref_por(porid, pk_rpor);

	return ercd;
}

ER_UINT
__cal_ref_rdv(va_list va)
{
	ER ercd;
	RDVNO rdvno;
	T_RRDV *pk_rrdv;

	rdvno = va_arg(va, RDVNO);
	pk_rrdv = va_arg(va, T_RRDV *);

	ercd = ref_rdv(rdvno, pk_rrdv);

	return ercd;
}

ER_UINT
__cal_cre_mpl(va_list va)
{
	ER ercd;
	ID mplid;
	T_CMPL *pk_cmpl;
	
	mplid = va_arg(va, ID);
	pk_cmpl = va_arg(va, T_CMPL *);

	ercd = cre_mpl(mplid, pk_cmpl);

	return ercd;
}

ER_UINT
__cal_del_mpl(va_list va)
{
	ER ercd;
	ID mplid;

	mplid = va_arg(va, ID);

	ercd = del_mpl(mplid);

	return ercd;
}

#if 0
ER_UINT
__cal_rel_mpl(va_list va)
{
	ER ercd;
	ID mplid;
	VP blk;

	mplid = va_arg(va, ID);
	blk = va_arg(va, VP);

	ercd = rel_mpl(mplid, blk);

	return ercd;
}
#endif

ER_UINT
__cal_get_mpl(va_list va)
{
	ER ercd;
	ID mplid;
	UINT blksz;
	VP *p_blk;

	mplid = va_arg(va, ID);
	blksz = va_arg(va, UINT);
	p_blk = va_arg(va, VP *);

	ercd = get_mpl(mplid, blksz, p_blk);
	return ercd;
}

ER_UINT
__cal_pget_mpl(va_list va)
{
	ER ercd;
	ID mplid;
	UINT blksz;
	VP *p_blk;

	mplid = va_arg(va, ID);
	blksz = va_arg(va, UINT);
	p_blk = va_arg(va, VP *);

	ercd = pget_mpl(mplid, blksz, p_blk);
	return ercd;
}
	
ER_UINT
__cal_tget_mpl(va_list va)
{
	ER ercd;
	ID mplid;
	UINT blksz;
	VP *p_blk;
	TMO tmout;

	mplid = va_arg(va, ID);
	blksz = va_arg(va, UINT);
	p_blk = va_arg(va, VP *);
	tmout = va_arg(va, TMO);

	ercd = tget_mpl(mplid, blksz, p_blk, tmout);
	return ercd;
}

ER_UINT
__cal_rel_mpl(va_list va)
{
	ER ercd;
	ID mplid;
	VP blk;

	mplid = va_arg(va, ID);
	blk = va_arg(va, VP);

	ercd = rel_mpl(mplid, blk);
	return ercd;
}

ER_UINT
__cal_cre_alm(va_list va)
{
	ER ercd;
	ID almid;
	T_CALM *pk_calm;

	almid = va_arg(va, ID);
	pk_calm = va_arg(va, T_CALM *);

	ercd = cre_alm(almid, pk_calm);

	return ercd;
}

ER_UINT
__cal_del_alm(va_list va)
{
	ER ercd;
	ID almid;
	
	almid = va_arg(va, ID);
	ercd = del_alm(almid);
	return ercd;
}

ER_UINT
__cal_sta_alm(va_list va)
{
	ER ercd;
	ID almid;
	RELTIM almtim;

	almid = va_arg(va, ID);
	almtim = va_arg(va, RELTIM);
	
	ercd = sta_alm(almid, almtim);
	return ercd;
}

ER_UINT
__cal_stp_alm(va_list va)
{
	ER ercd;
	ID almid;
	almid = va_arg(va, ID);
	ercd = stp_alm(almid);
	return ercd;
}

ER_UINT
__cal_ref_alm(va_list va)
{
	ER ercd;
	ID almid;
	T_RALM *pk_ralm;
	
	almid = va_arg(va, ID);
	pk_ralm = va_arg(va, T_RALM *);
	ercd = ref_alm(almid, pk_ralm);
	return ercd;
}

ER_UINT
__cal_def_ovr(va_list va)
{
	ER ercd;
	T_DOVR *pk_dovr;

	pk_dovr = va_arg(va, T_DOVR *);

	ercd = def_ovr(pk_dovr);
	return ercd;
}

ER_UINT
__cal_sta_ovr(va_list va)
{
	ER ercd;
	ID tskid;
	OVRTIM ovrtim;

	tskid = va_arg(va, ID);
	ovrtim = va_arg(va, OVRTIM);

	ercd = sta_ovr(tskid, ovrtim);
	return ercd;
}

ER_UINT
__cal_stp_ovr(va_list va)
{
	ER ercd;
	ID tskid;

	tskid = va_arg(va, ID);

	ercd = stp_ovr(tskid);
	return ercd;
}

ER_UINT
__cal_ref_ovr(va_list va)
{
	ER ercd;
	ID tskid;
	T_ROVR *pk_rovr;

	tskid = va_arg(va, ID);
	pk_rovr = va_arg(va, T_ROVR *);

	ercd = ref_ovr(tskid, pk_rovr);

	return ercd;
}

ER_ID
__cal_acre_tsk(va_list va)
{
	ER_ID tskid;
	T_CTSK *pk_ctsk;

	pk_ctsk = va_arg(va, T_CTSK *);

	tskid = acre_tsk(pk_ctsk);
	return tskid;
}

ER_ID
__cal_acre_sem(va_list va)
{
	ER_ID semid;
	T_CSEM *pk_csem;

	pk_csem = va_arg(va, T_CSEM *);

	semid = acre_sem(pk_csem);
	return semid;
}

ER_ID
__cal_acre_flg(va_list va)
{
	ER_ID flgid;
	T_CFLG *pk_cflg;

	pk_cflg = va_arg(va, T_CFLG *);

	flgid = acre_flg(pk_cflg);
	return flgid;
}

ER_ID
__cal_acre_dtq(va_list va)
{
	ER_ID dtqid;
	T_CDTQ *pk_cdtq;

	pk_cdtq = va_arg(va, T_CDTQ *);
	
	dtqid = acre_dtq(pk_cdtq);
	return dtqid;
}

ER_ID
__cal_acre_mbx(va_list va)
{
	ER_ID mbxid;
	T_CMBX *pk_cmbx;
	pk_cmbx = va_arg(va, T_CMBX *);
	mbxid = acre_mbx(pk_cmbx);
	return mbxid;
}

ER_ID
__cal_acre_mtx(va_list va)
{
	ER_ID mtxid;
	T_CMTX *pk_cmtx;

	pk_cmtx = va_arg(va, T_CMTX *);
	mtxid = acre_mtx(pk_cmtx);
	return mtxid;
}

ER_ID
__cal_acre_mbf(va_list va)
{
	ER_ID mbfid;
	T_CMBF *pk_cmbf;

	pk_cmbf =va_arg(va, T_CMBF *);
	mbfid = acre_mbf(pk_cmbf);
	return mbfid;
}

ER_ID
__cal_acre_por(va_list va)
{
	ER_ID porid;
	T_CPOR *pk_cpor;
	pk_cpor = va_arg(va, T_CPOR *);
	porid = acre_por(pk_cpor);
	return porid;
}

ER_ID
__cal_acre_mpf(va_list va)
{
	ER_ID mpfid;
	T_CMPF *pk_cmpf;
	pk_cmpf = va_arg(va, T_CMPF *);
	mpfid = acre_mpf(pk_cmpf);
	return mpfid;
}

ER_ID
__cal_acre_mpl(va_list va)
{
	ER_ID mplid;
	T_CMPL *pk_cmpl;
	pk_cmpl = va_arg(va, T_CMPL *);
	mplid = acre_mpl(pk_cmpl);
	return mplid;
}

ER_ID
__cal_acre_cyc(va_list va)
{
	ER_ID cycid;
	T_CCYC *pk_ccyc;
	pk_ccyc = va_arg(va, T_CCYC *);
	cycid = acre_cyc(pk_ccyc);
	return cycid;
}

ER_ID
__cal_acre_alm(va_list va)
{
	ER_ID almid;
	T_CALM *pk_calm;
	pk_calm = va_arg(va, T_CALM *);
	almid = acre_alm(pk_calm);
	return almid;
}

ER_ID
__cal_acre_isr(va_list va)
{
	ER_ID isrid;
	T_CISR *pk_cisr;
	pk_cisr = va_arg(va, T_CISR *);
	isrid = acre_isr(pk_cisr);
	return isrid;
}

#endif /* __calstdsvc */
