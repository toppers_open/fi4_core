/* This file is generated from fi4_rename.def by genrename. */

#ifndef TOPPERS_FI4_RENAME_H
#define TOPPERS_FI4_RENAME_H

/*
 *  kmem.c
 */
#define kmem_setup		_kernel_kmem_setup
#define kmem_allocate		_kernel_kmem_allocate
#define kmem_allocatable	_kernel_kmem_allocatable
#define kmem_release		_kernel_kmem_release
#define kmem_status		_kernel_kmem_status

/*
 *  startup.c
 */
#define iniflg			_kernel_iniflg

/*
 *  banner.c
 */
#define print_banner		_kernel_print_banner

/*
 *  cpu_config.c, cpu_support.S, sys_config.c, sys_support.S
 */
#define dispatch		_kernel_dispatch
#define exit_and_dispatch	_kernel_exit_and_dispatch
#define cpu_initialize		_kernel_cpu_initialize
#define cpu_terminate		_kernel_cpu_terminate
#define sys_initialize		_kernel_sys_initialize
#define sys_exit		_kernel_sys_exit
#define sys_putc		_kernel_sys_putc

/*
 *  bufmgr.c
 */
#define bufmgr_initialize	_kernel_bufmgr_initialize
#define bufmgr_allocate		_kernel_bufmgr_allocate
#define bufmgr_release		_kernel_bufmgr_release

/*
 *  task.c
 */
#define runtsk			_kernel_runtsk
#define schedtsk		_kernel_schedtsk
#define reqflg			_kernel_reqflg
#define enadsp			_kernel_enadsp
#define ready_queue		_kernel_ready_queue
#define ready_primap		_kernel_ready_primap
#define task_initialize		_kernel_task_initialize
#define search_schedtsk		_kernel_search_schedtsk
#define make_runnable		_kernel_make_runnable
#define make_non_runnable	_kernel_make_non_runnable
#define make_dormant		_kernel_make_dormant
#define make_active		_kernel_make_active
#define exit_task		_kernel_exit_task
#define change_priority		_kernel_change_priority
#define rotate_ready_queue	_kernel_rotate_ready_queue
#define call_texrtn		_kernel_call_texrtn
#define calltex			_kernel_calltex
#define unlock_all_mutex	_kernel_unlock_all_mutex

/*
 *  task_overrun.c
 */
#define overrun_initialize	_kernel_overrun_initialize
#define call_ovrhdr		_kernel_call_ovrhdr
#define overrun_handler_function	_kernel_overrun_handler_function

/*
 *  wait.c
 */
#define make_wait_tmout		_kernel_make_wait_tmout
#define wait_complete		_kernel_wait_complete
#define wait_tmout		_kernel_wait_tmout
#define wait_tmout_ok		_kernel_wait_tmout_ok
#define wait_cancel		_kernel_wait_cancel
#define wait_release		_kernel_wait_release
#define wobj_make_wait		_kernel_wobj_make_wait
#define wobj_make_wait_tmout	_kernel_wobj_make_wait_tmout
#define wobj_make_wait_mutexi	_kernel_wobj_make_wait_mutexi
#define wobj_make_wait_mutexi_tmout	_kernel_wobj_make_wait_mutexi_tmout
#define wobj_move_wait		_kernel_wobj_move_wait
#define wobj_change_priority	_kernel_wobj_change_priority
#define scan_new_priority	_kernel_scan_new_priority

/*
 *  time_event.c
 */
#define systim_offset		_kernel_systim_offset
#define current_time		_kernel_current_time
#define next_time		_kernel_next_time
#define next_subtime		_kernel_next_subtime
#define last_index		_kernel_last_index
#define tmevt_initialize	_kernel_tmevt_initialize
#define tmevt_up		_kernel_tmevt_up
#define tmevt_down		_kernel_tmevt_down
#define tmevtb_insert		_kernel_tmevtb_insert
#define tmevtb_delete		_kernel_tmevtb_delete

/*
 *  syslog.c
 */
#define syslog_buffer		_kernel_syslog_buffer
#define syslog_count		_kernel_syslog_count
#define syslog_head		_kernel_syslog_head
#define syslog_tail		_kernel_syslog_tail
#define syslog_lost		_kernel_syslog_lost
#define syslog_logmask		_kernel_syslog_logmask
#define syslog_lowmask		_kernel_syslog_lowmask
#define syslog_initialize	_kernel_syslog_initialize
#define syslog_terminate	_kernel_syslog_terminate

/*
 *  semaphore.c
 */
#define semaphore_initialize	_kernel_semaphore_initialize

/*
 *  eventflag.c
 */
#define eventflag_initialize	_kernel_eventflag_initialize
#define eventflag_cond		_kernel_eventflag_cond

/*
 *  dataqueue.c
 */
#define dataqueue_initialize	_kernel_dataqueue_initialize
#define enqueue_data		_kernel_enqueue_data
#define force_enqueue_data	_kernel_force_enqueue_data
#define dequeue_data		_kernel_dequeue_data
#define send_data_rwait		_kernel_send_data_rwait
#define receive_data_swait	_kernel_receive_data_swait

/*
 *  mailbox.c
 */
#define mailbox_initialize	_kernel_mailbox_initialize
#define enqueue_msg_pri		_kernel_enqueue_msg_pri

/*
 *  mutex.c
 */
#define mutex_initialize	_kernel_mutex_initialize
#define search_mtx_priority	_kernel_search_mtx_priority
#define search_max_ceilpri	_kernel_search_max_ceilpri

/*
 *  messagebuffer.c
 */
#define messagebuffer_initialize	_kernel_messagebuffer_initialize
#define send_mbf_rwait		_kernel_send_mbf_rwait
#define read_mbf_buffer		_kernel_read_mbf_buffer
#define write_mbf_buffer	_kernel_write_mbf_buffer

/*
 *  rendezvous.c
 */
#define rendezvous_initialize	_kernel_rendezvous_initialize

/*
 *  mempfix.c
 */
#define mempfix_initialize	_kernel_mempfix_initialize
#define mempfix_get_block	_kernel_mempfix_get_block
#define mempfix_count_free	_kernel_mempfix_count_free
#define init_mpfcb		_kernel_init_mpfcb

/*
 *  mempvar.c
 */
#define mempvar_initialize	_kernel_mempvar_initialize
#define mblcb_initialize	_kernel_mblcb_initialize

/*
 *  cyclic.c
 */
#define cyclic_initialize	_kernel_cyclic_initialize
#define tmevtb_enqueue_cyc	_kernel_tmevtb_enqueue_cyc
#define call_cychdr		_kernel_call_cychdr

/*
 *  alarm.c
 */
#define alarm_initialize	_kernel_alarm_initialize
#define call_almhdr		_kernel_call_almhdr

/*
 *  interrupt.c
 */
#define interrupt_initialize	_kernel_interrupt_initialize
#define interrupt_service_initialize	_kernel_interrupt_service_initialize
#define attached_isr_initialize	_kernel_attached_isr_initialize

/*
 *  exception.c
 */
#define exception_initialize	_kernel_exception_initialize

/*
 *  kernel_cfg.c
 */
#define object_initialize	_kernel_object_initialize
#define call_inirtn		_kernel_call_inirtn
#define call_terrtn		_kernel_call_terrtn
#define tmax_tskid		_kernel_tmax_tskid
#define tnum_tinia		_kernel_tnum_tinia
#define tinia_table		_kernel_tinia_table
#define tinib_table		_kernel_tinib_table
#define torder_table		_kernel_torder_table
#define tcb_table		_kernel_tcb_table
#define tnum_trid		_kernel_tnum_trid
#define trid_table		_kernel_trid_table
#define tmax_semid		_kernel_tmax_semid
#define tnum_seminia		_kernel_tnum_seminia
#define seminia_table		_kernel_seminia_table
#define seminib_table		_kernel_seminib_table
#define semcb_table		_kernel_semcb_table
#define tnum_semrid		_kernel_tnum_semrid
#define semrid_table		_kernel_semrid_table
#define tmax_flgid		_kernel_tmax_flgid
#define tnum_flginia		_kernel_tnum_flginia
#define flginia_table		_kernel_flginia_table
#define flginib_table		_kernel_flginib_table
#define flgcb_table		_kernel_flgcb_table
#define tnum_flgrid		_kernel_tnum_flgrid
#define flgrid_table		_kernel_flgrid_table
#define tmax_dtqid		_kernel_tmax_dtqid
#define tnum_dtqinia		_kernel_tnum_dtqinia
#define dtqinia_table		_kernel_dtqinia_table
#define dtqinib_table		_kernel_dtqinib_table
#define dtqcb_table		_kernel_dtqcb_table
#define tnum_dtqrid		_kernel_tnum_dtqrid
#define dtqrid_table		_kernel_dtqrid_table
#define tmax_mbxid		_kernel_tmax_mbxid
#define mbxcb_table		_kernel_mbxcb_table
#define tnum_mbxinia		_kernel_tnum_mbxinia
#define mbxinia_table		_kernel_mbxinia_table
#define mbxinib_table		_kernel_mbxinib_table
#define tnum_mbxrid		_kernel_tnum_mbxrid
#define mbxrid_table		_kernel_mbxrid_table
#define tmax_mpfid		_kernel_tmax_mpfid
#define tnum_mpfinia		_kernel_tnum_mpfinia
#define mpfinia_table		_kernel_mpfinia_table
#define mpfinib_table		_kernel_mpfinib_table
#define mpfcb_table		_kernel_mpfcb_table
#define tnum_mpfrid		_kernel_tnum_mpfrid
#define mpfrid_table		_kernel_mpfrid_table
#define tmax_mplid		_kernel_tmax_mplid
#define tnum_mplinia		_kernel_tnum_mplinia
#define mplinia_table		_kernel_mplinia_table
#define mplinib_table		_kernel_mplinib_table
#define mplcb_table		_kernel_mplcb_table
#define tnum_mplrid		_kernel_tnum_mplrid
#define mplrid_table		_kernel_mplrid_table
#define tmax_mtxid		_kernel_tmax_mtxid
#define tnum_mtxinia		_kernel_tnum_mtxinia
#define mtxinia_table		_kernel_mtxinia_table
#define mtxinib_table		_kernel_mtxinib_table
#define mtxcb_table		_kernel_mtxcb_table
#define tnum_mtxrid		_kernel_tnum_mtxrid
#define mtxrid_table		_kernel_mtxrid_table
#define tmax_mbfid		_kernel_tmax_mbfid
#define tnum_mbfinia		_kernel_tnum_mbfinia
#define mbfinia_table		_kernel_mbfinia_table
#define mbfinib_table		_kernel_mbfinib_table
#define mbfcb_table		_kernel_mbfcb_table
#define tnum_mbfrid		_kernel_tnum_mbfrid
#define mbfrid_table		_kernel_mbfrid_table
#define tmax_porid		_kernel_tmax_porid
#define tnum_porinia		_kernel_tnum_porinia
#define porinia_table		_kernel_porinia_table
#define porinib_table		_kernel_porinib_table
#define porcb_table		_kernel_porcb_table
#define tnum_porrid		_kernel_tnum_porrid
#define porrid_table		_kernel_porrid_table
#define tmax_cycid		_kernel_tmax_cycid
#define tnum_cycinia		_kernel_tnum_cycinia
#define cycinia_table		_kernel_cycinia_table
#define cycinib_table		_kernel_cycinib_table
#define cyccb_table		_kernel_cyccb_table
#define tnum_cycrid		_kernel_tnum_cycrid
#define cycrid_table		_kernel_cycrid_table
#define tnum_cycrid		_kernel_tnum_cycrid
#define cycrid_table		_kernel_cycrid_table
#define tmax_almid		_kernel_tmax_almid
#define tnum_alminia		_kernel_tnum_alminia
#define alminia_table		_kernel_alminia_table
#define alminib_table		_kernel_alminib_table
#define almcb_table		_kernel_almcb_table
#define tnum_almrid		_kernel_tnum_almrid
#define almrid_table		_kernel_almrid_table
#define tnum_inhno		_kernel_tnum_inhno
#define inhinib_table		_kernel_inhinib_table
#define tmax_isrid		_kernel_tmax_isrid
#define tnum_isrinia		_kernel_tnum_isrinia
#define isrinia_table		_kernel_isrinia_table
#define isrinib_table		_kernel_isrinib_table
#define isrcb_table		_kernel_isrcb_table
#define tnum_isrrid		_kernel_tnum_isrrid
#define isrrid_table		_kernel_isrrid_table
#define tnum_attached_isrno	_kernel_tnum_attached_isrno
#define attached_isrinib_table	_kernel_attached_isrinib_table
#define tnum_excno		_kernel_tnum_excno
#define excinib_table		_kernel_excinib_table
#define tmevt_heap		_kernel_tmevt_heap
#define tnum_svcno		_kernel_tnum_svcno
#define svccb_table		_kernel_svccb_table
#define svcinib_table		_kernel_svcinib_table
#define svcinib_hash_table	_kernel_svcinib_hash_table

/*
 * servicecall
 */
#define exservicecall_initialize	_kernel_exservicecall_initialize


#define alm_freelist		_kernel_alm_freelist
#define cyc_freelist		_kernel_cyc_freelist
#define dtq_freelist		_kernel_dtq_freelist
#define flg_freelist		_kernel_flg_freelist
#define isr_freelist		_kernel_isr_freelist
#define mbx_freelist		_kernel_mbx_freelist
#define mpf_freelist		_kernel_mpf_freelist
#define mpl_freelist		_kernel_mpl_freelist
#define mbf_freelist		_kernel_mbf_freelist
#define mtx_freelist		_kernel_mtx_freelist
#define por_freelist		_kernel_por_freelist
#define sem_freelist		_kernel_sem_freelist
#define tsk_freelist		_kernel_tsk_freelist

#ifdef LABEL_ASM

/*
 *  kmem.c
 */
#define _kmem_setup		__kernel_kmem_setup
#define _kmem_allocate		__kernel_kmem_allocate
#define _kmem_allocatable	__kernel_kmem_allocatable
#define _kmem_release		__kernel_kmem_release
#define _kmem_status		__kernel_kmem_status

/*
 *  startup.c
 */
#define _iniflg			__kernel_iniflg

/*
 *  banner.c
 */
#define _print_banner		__kernel_print_banner

/*
 *  cpu_config.c, cpu_support.S, sys_config.c, sys_support.S
 */
#define _dispatch		__kernel_dispatch
#define _exit_and_dispatch	__kernel_exit_and_dispatch
#define _cpu_initialize		__kernel_cpu_initialize
#define _cpu_terminate		__kernel_cpu_terminate
#define _sys_initialize		__kernel_sys_initialize
#define _sys_exit		__kernel_sys_exit
#define _sys_putc		__kernel_sys_putc

/*
 *  bufmgr.c
 */
#define _bufmgr_initialize	__kernel_bufmgr_initialize
#define _bufmgr_allocate	__kernel_bufmgr_allocate
#define _bufmgr_release		__kernel_bufmgr_release

/*
 *  task.c
 */
#define _runtsk			__kernel_runtsk
#define _schedtsk		__kernel_schedtsk
#define _reqflg			__kernel_reqflg
#define _enadsp			__kernel_enadsp
#define _ready_queue		__kernel_ready_queue
#define _ready_primap		__kernel_ready_primap
#define _task_initialize	__kernel_task_initialize
#define _search_schedtsk	__kernel_search_schedtsk
#define _make_runnable		__kernel_make_runnable
#define _make_non_runnable	__kernel_make_non_runnable
#define _make_dormant		__kernel_make_dormant
#define _make_active		__kernel_make_active
#define _exit_task		__kernel_exit_task
#define _change_priority	__kernel_change_priority
#define _rotate_ready_queue	__kernel_rotate_ready_queue
#define _call_texrtn		__kernel_call_texrtn
#define _calltex		__kernel_calltex

/*
 *  task_overrun.c
 */
#define _overrun_initialize	__kernel_overrun_initialize
#define _call_ovrhdr		__kernel_call_ovrhdr
#define _overrun_handler_function	__kernel_overrun_handler_function

/*
 *  wait.c
 */
#define _make_wait_tmout	__kernel_make_wait_tmout
#define _wait_complete		__kernel_wait_complete
#define _wait_tmout		__kernel_wait_tmout
#define _wait_tmout_ok		__kernel_wait_tmout_ok
#define _wait_cancel		__kernel_wait_cancel
#define _wait_release		__kernel_wait_release
#define _wobj_make_wait		__kernel_wobj_make_wait
#define _wobj_make_wait_tmout	__kernel_wobj_make_wait_tmout
#define _wobj_make_wait_mutexi	__kernel_wobj_make_wait_mutexi
#define _wobj_make_wait_mutexi_tmout	__kernel_wobj_make_wait_mutexi_tmout
#define _wobj_move_wait		__kernel_wobj_move_wait
#define _wobj_change_priority	__kernel_wobj_change_priority

/*
 *  time_event.c
 */
#define _systim_offset		__kernel_systim_offset
#define _current_time		__kernel_current_time
#define _next_time		__kernel_next_time
#define _next_subtime		__kernel_next_subtime
#define _last_index		__kernel_last_index
#define _tmevt_initialize	__kernel_tmevt_initialize
#define _tmevt_up		__kernel_tmevt_up
#define _tmevt_down		__kernel_tmevt_down
#define _tmevtb_insert		__kernel_tmevtb_insert
#define _tmevtb_delete		__kernel_tmevtb_delete

/*
 *  syslog.c
 */
#define _syslog_buffer		__kernel_syslog_buffer
#define _syslog_count		__kernel_syslog_count
#define _syslog_head		__kernel_syslog_head
#define _syslog_tail		__kernel_syslog_tail
#define _syslog_lost		__kernel_syslog_lost
#define _syslog_logmask		__kernel_syslog_logmask
#define _syslog_lowmask		__kernel_syslog_lowmask
#define _syslog_initialize	__kernel_syslog_initialize
#define _syslog_terminate	__kernel_syslog_terminate

/*
 *  semaphore.c
 */
#define _semaphore_initialize	__kernel_semaphore_initialize

/*
 *  eventflag.c
 */
#define _eventflag_initialize	__kernel_eventflag_initialize
#define _eventflag_cond		__kernel_eventflag_cond

/*
 *  dataqueue.c
 */
#define _dataqueue_initialize	__kernel_dataqueue_initialize
#define _enqueue_data		__kernel_enqueue_data
#define _force_enqueue_data	__kernel_force_enqueue_data
#define _dequeue_data		__kernel_dequeue_data
#define _send_data_rwait	__kernel_send_data_rwait
#define _receive_data_swait	__kernel_receive_data_swait

/*
 *  mailbox.c
 */
#define _mailbox_initialize	__kernel_mailbox_initialize
#define _enqueue_msg_pri	__kernel_enqueue_msg_pri

/*
 *  mutex.c
 */
#define _mutex_initialize	__kernel_mutex_initialize
#define _search_mtx_priority	__kernel_search_mtx_priority
#define _search_max_ceilpri	__kernel_search_max_ceilpri

/*
 *  messagebuffer.c
 */
#define _messagebuffer_initialize	__kernel_messagebuffer_initialize
#define _send_mbf_rwait		__kernel_send_mbf_rwait
#define _read_mbf_buffer	__kernel_read_mbf_buffer
#define _write_mbf_buffer	__kernel_write_mbf_buffer

/*
 *  rendezvous.c
 */
#define _rendezvous_initialize	__kernel_rendezvous_initialize

/*
 *  mempfix.c
 */
#define _mempfix_initialize	__kernel_mempfix_initialize
#define _mempfix_get_block	__kernel_mempfix_get_block
#define _mempfix_count_free	__kernel_mempfix_count_free
#define _init_mpfcb		__kernel_init_mpfcb

/*
 *  mempvar.c
 */
#define _mempvar_initialize	__kernel_mempvar_initialize
#define _mblcb_initialize	__kernel_mblcb_initialize

/*
 *  cyclic.c
 */
#define _cyclic_initialize	__kernel_cyclic_initialize
#define _tmevtb_enqueue_cyc	__kernel_tmevtb_enqueue_cyc
#define _call_cychdr		__kernel_call_cychdr

/*
 *  alarm.c
 */
#define _alarm_initialize	__kernel_alarm_initialize
#define _call_almhdr		__kernel_call_almhdr

/*
 *  interrupt.c
 */
#define _interrupt_initialize	__kernel_interrupt_initialize
#define _interrupt_service_initialize	__kernel_interrupt_service_initialize
#define _attached_isr_initialize	__kernel_attached_isr_initialize

/*
 *  exception.c
 */
#define _exception_initialize	__kernel_exception_initialize

/*
 *  kernel_cfg.c
 */
#define _object_initialize	__kernel_object_initialize
#define _call_inirtn		__kernel_call_inirtn
#define _call_terrtn		__kernel_call_terrtn
#define _tmax_tskid		__kernel_tmax_tskid
#define _tnum_tinia		__kernel_tnum_tinia
#define _tinia_table		__kernel_tinia_table
#define _tinib_table		__kernel_tinib_table
#define _torder_table		__kernel_torder_table
#define _tcb_table		__kernel_tcb_table
#define _tnum_trid		__kernel_tnum_trid
#define _trid_table		__kernel_trid_table
#define _tmax_semid		__kernel_tmax_semid
#define _tnum_seminia		__kernel_tnum_seminia
#define _seminia_table		__kernel_seminia_table
#define _seminib_table		__kernel_seminib_table
#define _semcb_table		__kernel_semcb_table
#define _tnum_semrid		__kernel_tnum_semrid
#define _semrid_table		__kernel_semrid_table
#define _tmax_flgid		__kernel_tmax_flgid
#define _tnum_flginia		__kernel_tnum_flginia
#define _flginia_table		__kernel_flginia_table
#define _flginib_table		__kernel_flginib_table
#define _flgcb_table		__kernel_flgcb_table
#define _tnum_flgrid		__kernel_tnum_flgrid
#define _flgrid_table		__kernel_flgrid_table
#define _tmax_dtqid		__kernel_tmax_dtqid
#define _tnum_dtqinia		__kernel_tnum_dtqinia
#define _dtqinia_table		__kernel_dtqinia_table
#define _dtqinib_table		__kernel_dtqinib_table
#define _dtqcb_table		__kernel_dtqcb_table
#define _tnum_dtqrid		__kernel_tnum_dtqrid
#define _dtqrid_table		__kernel_dtqrid_table
#define _tmax_mbxid		__kernel_tmax_mbxid
#define _mbxcb_table		__kernel_mbxcb_table
#define _tnum_mbxinia		__kernel_tnum_mbxinia
#define _mbxinia_table		__kernel_mbxinia_table
#define _mbxinib_table		__kernel_mbxinib_table
#define _tnum_mbxrid		__kernel_tnum_mbxrid
#define _mbxrid_table		__kernel_mbxrid_table
#define _tmax_mpfid		__kernel_tmax_mpfid
#define _tnum_mpfinia		__kernel_tnum_mpfinia
#define _mpfinia_table		__kernel_mpfinia_table
#define _mpfinib_table		__kernel_mpfinib_table
#define _mpfcb_table		__kernel_mpfcb_table
#define _tnum_mpfrid		__kernel_tnum_mpfrid
#define _mpfrid_table		__kernel_mpfrid_table
#define _tmax_mplid		__kernel_tmax_mplid
#define _tnum_mplinia		__kernel_tnum_mplinia
#define _mplinia_table		__kernel_mplinia_table
#define _mplinib_table		__kernel_mplinib_table
#define _mplcb_table		__kernel_mplcb_table
#define _tnum_mplrid		__kernel_tnum_mplrid
#define _mplrid_table		__kernel_mplrid_table
#define _tmax_mtxid		__kernel_tmax_mtxid
#define _tnum_mtxinia		__kernel_tnum_mtxinia
#define _mtxinia_table		__kernel_mtxinia_table
#define _mtxinib_table		__kernel_mtxinib_table
#define _mtxcb_table		__kernel_mtxcb_table
#define _tnum_mtxrid		__kernel_tnum_mtxrid
#define _mtxrid_table		__kernel_mtxrid_table
#define _tmax_mbfid		__kernel_tmax_mbfid
#define _tnum_mbfinia		__kernel_tnum_mbfinia
#define _mbfinia_table		__kernel_mbfinia_table
#define _mbfinib_table		__kernel_mbfinib_table
#define _mbfcb_table		__kernel_mbfcb_table
#define _tnum_mbfrid		__kernel_tnum_mbfrid
#define _mbfrid_table		__kernel_mbfrid_table
#define _tmax_porid		__kernel_tmax_porid
#define _tnum_porinia		__kernel_tnum_porinia
#define _porinia_table		__kernel_porinia_table
#define _porinib_table		__kernel_porinib_table
#define _porcb_table		__kernel_porcb_table
#define _tnum_porrid		__kernel_tnum_porrid
#define _porrid_table		__kernel_porrid_table
#define _tmax_cycid		__kernel_tmax_cycid
#define _tnum_cycinia		__kernel_tnum_cycinia
#define _cycinia_table		__kernel_cycinia_table
#define _cycinib_table		__kernel_cycinib_table
#define _cyccb_table		__kernel_cyccb_table
#define _tnum_cycrid		__kernel_tnum_cycrid
#define _cycrid_table		__kernel_cycrid_table
#define _tnum_cycrid		__kernel_tnum_cycrid
#define _cycrid_table		__kernel_cycrid_table
#define _tmax_almid		__kernel_tmax_almid
#define _tnum_alminia		__kernel_tnum_alminia
#define _alminia_table		__kernel_alminia_table
#define _alminib_table		__kernel_alminib_table
#define _almcb_table		__kernel_almcb_table
#define _tnum_almrid		__kernel_tnum_almrid
#define _almrid_table		__kernel_almrid_table
#define _tnum_inhno		__kernel_tnum_inhno
#define _inhinib_table		__kernel_inhinib_table
#define _tmax_isrid		__kernel_tmax_isrid
#define _tnum_isrinia		__kernel_tnum_isrinia
#define _isrinia_table		__kernel_isrinia_table
#define _isrinib_table		__kernel_isrinib_table
#define _isrcb_table		__kernel_isrcb_table
#define _tnum_isrrid		__kernel_tnum_isrrid
#define _isrrid_table		__kernel_isrrid_table
#define _tnum_attached_isrno	__kernel_tnum_attached_isrno
#define _attached_isrinib_table	__kernel_attached_isrinib_table
#define _tnum_excno		__kernel_tnum_excno
#define _excinib_table		__kernel_excinib_table
#define _tmevt_heap		__kernel_tmevt_heap
#define _tnum_svcno		__kernel_tnum_svcno
#define _svccb_table		__kernel_svccb_table
#define _svcinib_table		__kernel_svcinib_table
#define _svcinib_hash_table	__kernel_svcinib_hash_table

/*
 * servicecall
 */
#define _exservicecall_initialize	__kernel_exservicecall_initialize


#define _alm_freelist		__kernel_alm_freelist
#define _cyc_freelist		__kernel_cyc_freelist
#define _dtq_freelist		__kernel_dtq_freelist
#define _flg_freelist		__kernel_flg_freelist
#define _isr_freelist		__kernel_isr_freelist
#define _mbx_freelist		__kernel_mbx_freelist
#define _mpf_freelist		__kernel_mpf_freelist
#define _mpl_freelist		__kernel_mpl_freelist
#define _mbf_freelist		__kernel_mbf_freelist
#define _mtx_freelist		__kernel_mtx_freelist
#define _por_freelist		__kernel_por_freelist
#define _sem_freelist		__kernel_sem_freelist
#define _tsk_freelist		__kernel_tsk_freelist

#endif /* LABEL_ASM */
#endif /* TOPPERS_FI4_RENAME_H */
