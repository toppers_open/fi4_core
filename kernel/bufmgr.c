/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2003-2004 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/bufmgr.c 2750 2006-03-07T16:15:24.532712Z monaka  $
 */

/*
 *  カーネルバッファマネージャ
 */
#include "fi4_kernel.h"
#include "bufmgr.h"

extern VP __kernel_bufmgr_buffer[];
extern const SIZE __kernel_bufmgr_buffer_size;

extern KMEMB *__kernel_bufmgr_kmemb;

/*
 *  カーネルバッファマネージャの初期化
 */
#ifdef __bufmgrini

KMEMB *__kernel_bufmgr_kmemb;

void
bufmgr_initialize(void)
{
	kmem_setup((VP)&__kernel_bufmgr_buffer, __kernel_bufmgr_buffer_size, &__kernel_bufmgr_kmemb);
}

#endif /* __bufmgrini */

/*
 *  カーネルバッファの開放
 *
 */
#ifdef __bufmgrrelease

BOOL
bufmgr_release(VP ptr)
{
	BOOL ret;
	UB *buf;

	buf = (UB *)&__kernel_bufmgr_buffer;

	/*
	 *  もしカーネルバッファ領域でなかったとしたら、
	 *  bufmgr_allocateで取得したのではないことになるので、
	 *  何もせずTRUEを返す。
	 */
	if (buf <= (UB *)ptr && (UB *)ptr < (buf + __kernel_bufmgr_buffer_size)) {
		ret = kmem_release(__kernel_bufmgr_kmemb, ptr);

		return ret;
	}

	return (ptr != 0);
}

#endif /* __bufmgrrelease */
	
