/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2003-2006 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/alarm.h 6183 2006-11-05T03:43:32.794177Z monaka  $
 */

/*
 *	アラームハンドラ機能
 */

#ifndef TOPPERS_ALARM_H
#define TOPPERS_ALARM_H

#include "queue.h"
#include "time_event.h"

/*
 *  アラームハンドラ初期化ブロック
 */
typedef struct alarm_handler_initialization_block {
	ATR	almatr;		/* アラームハンドラ属性 */
	VP_INT	exinf;		/* アラームハンドラの拡張情報 */
	FP	almhdr;		/* アラームハンドラの起動番地 */
} ALMINIB;

/*
 *  アラームハンドラ初期化ブロック連想ペア
 */
typedef struct alarm_handler_initialization_block_apair {
	ID	almid;
	ALMINIB	alminib;
} ALMINIA;

/*
 *  アラームハンドラ管理ブロック
 */
typedef struct alarm_handler_control_block {
	QUEUE free_queue;	/* アラームハンドラ空きキュー */
	ALMINIB *alminib;
	BOOL	almsta;		/* アラームハンドラの動作状態 */
	EVTTIM	evttim;		/* アラームハンドラを起動する時刻 */
	TMEVTB	tmevtb;		/* タイムイベントブロック */
	unsigned int	exist:2;	/* 使われているとき、KOBJ_EXIST */
	unsigned int	reserved:1;
} ALMCB;

/*
 *  アラームハンドラ機能の初期化
 */
extern void	alarm_initialize(void);

/*
 *  アラームハンドラ起動ルーチン
 */
extern void	call_almhdr(ALMCB *cyccb);

#define ALMID(almcb)	((ID)(almcb - almcb_table) + TMIN_ALMID)

#endif /* TOPPERS_ALARM_H */
