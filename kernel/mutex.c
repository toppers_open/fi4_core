/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 * 
 *  Copyright (C) 2002 by Masao Higuchi
 *  Copyright (C) 2003-2004 by Monami software Limited Partnership, JAPAN
 *
 *
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/mutex.c 6101 2006-09-10T08:00:54.989588Z monaka  $
 */

/*
 *	ミューテックス機能
 *	(厳密な優先度制御規則)
 *
 *  The original code was written by Masao Higuchi.
 *  Extended for uITRON4.0 fullset was written by Masaki Muranaka.
 */

#include "fi4_kernel.h"
#include "check.h"
#include "task.h"
#include "wait.h"
#include "mutex.h"
#include "multiqueue.h"

/*
 *  ミューテックスIDの最大値（kernel_cfg.c）
 */
extern const ID	tmax_mtxid;

/*
 *  ミューテックス初期化ブロックのエリア（kernel_cfg.c）
 */
extern MTXINIB	mtxinib_table[];

/*
 *  ミューテックス初期化ブロック連想ペアの数（kernel_cfg.c）
 */
extern const UINT	tnum_mtxinia;

/*
 *  ミューテックス初期化ブロック連想ペアのエリア（kernel_cfg.c）
 */
extern const MTXINIA	mtxinia_table[];

/*
 *  ミューテックス予約ID情報のエリア（kernel_cfg.c）
 */
extern const ID		mtxrid_table[];

/*
 *  ミューテックス予約ID情報の数 (kernel_cfg.c)
 */
extern const UINT	tnum_mtxrid;

/*
 *  ミューテックス管理ブロックのエリア（kernel_cfg.c）
 */
extern MTXCB	mtxcb_table[];

/*
 *  ミューテックスの数
 */
#define TNUM_MTX	((UINT)(tmax_mtxid - TMIN_MTXID + 1))

/*
 *  ミューテックスIDからミューテックス管理ブロックを取り出すためのマクロ
 */
#define INDEX_MTX(mtxid)	((UINT)((mtxid) - TMIN_MTXID))
#define get_mtxcb(mtxid)	(&(mtxcb_table[INDEX_MTX(mtxid)]))

/*
 *  ID自動割当て用フリーリスト
 */
extern QUEUE mtx_freelist;

/* 
 *  ミューテックス機能の初期化
 */
#ifdef __mtxini

QUEUE mtx_freelist;

void
mutex_initialize(void)
{
	UINT	i;
	MTXCB	*mtxcb;
	MTXINIB *mtxinib;
	const MTXINIA *mtxinia;
	const ID	*mtxrid;

	/*
	 * カーネル起動時の状態に関わらず初期化しなければいけないものを
	 * まず初期化する。
	 */
	for (mtxcb = mtxcb_table, i = 0; i < TNUM_MTX; mtxcb++, i++) {
		queue_initialize(&(mtxcb->wait_queue));
		queue_initialize(&(mtxcb->mtxlist));
		mtxcb->mtxinib = NULL;
	}
	queue_initialize(&mtx_freelist);

	/*
	 * mtxinia によって示されるものをまず初期化
	 */
	for (mtxinia = mtxinia_table, i = 0; i < tnum_mtxinia; mtxinia++, i++) {
		UINT idx;
		idx = INDEX_MTX(mtxinia->mtxid);
		mtxinib = &mtxinib_table[idx];
		*mtxinib = mtxinia->mtxinib;
		mtxcb = &mtxcb_table[idx];
		mtxcb->mtxinib = mtxinib;
		mtxcb->mtxtsk = NULL;
		mtxcb->exist = KOBJ_EXIST;
		mtxcb->reserved = TRUE;
	}

	/*
	 *  予約IDの設定
	 */
	for (mtxrid = mtxrid_table, i = 0; i < tnum_mtxrid; mtxrid++, i++) {
		mtxcb_table[INDEX_MTX(*mtxrid)].reserved = TRUE;
	}

	/*
	 * mtxinia によって示されないものを初期化
	 */
	for (mtxcb = mtxcb_table, i = 0; i < TNUM_MTX; mtxcb++, i++) {
		if (mtxcb->mtxinib == NULL) {
			mtxcb->exist = KOBJ_NOEXS;
			mtxcb->mtxinib = &mtxinib_table[i];

			if (!CHECK_OBJECT_RESERVED(mtxcb)) {
				queue_insert_prev(&mtx_freelist, (QUEUE*)mtxcb);
			}
		}
	}
}

#endif /* __mtxini */

/*
 *  ミューテックスの生成
 */
#ifdef __cre_mtx

SYSCALL ER
cre_mtx(ID mtxid, const T_CMTX * pk_cmtx)
{
	MTXCB	*mtxcb;
	MTXINIB *mtxinib;
	ER	ercd;
    
	LOG_CRE_MTX_ENTER(mtxid, pk_cmtx);
	CHECK_TSKCTX_UNL();

	CHECK_MTXID(mtxid);
	CHECK_ATTRIBUTE(pk_cmtx->mtxatr, TA_TFIFO | TA_TPRI | TA_INHERIT | TA_CEILING);
	if ((pk_cmtx->mtxatr & TA_CEILING) == TA_CEILING) {
		if (pk_cmtx->ceilpri < TMIN_TPRI ||
		    pk_cmtx->ceilpri > TMAX_TPRI) {
			return E_PAR;
		}
	}
	    
	mtxcb = get_mtxcb(mtxid);
	CHECK_OBJECT_CREATABLE(mtxcb);
	UPDATE_OBJECT_FREELIST(mtxcb);

	/*
	 * 初期設定を行う。
	 */
	queue_initialize(&mtxcb->wait_queue);
	queue_initialize(&mtxcb->mtxlist);
	mtxcb->mtxtsk = NULL;

	mtxinib = mtxcb->mtxinib;
	mtxinib->mtxatr = pk_cmtx->mtxatr;
	mtxinib->ceilpri = pk_cmtx->ceilpri;

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	mtxcb->exist = KOBJ_EXIST;

	ercd = E_OK;

    exit:
	LOG_CRE_MTX_LEAVE(ercd);
	return(ercd);
}

#endif /* __cre_mtx */

/*
 *  ミューテックスの生成(ID番号自動割付け)
 */
#ifdef __acre_mtx

SYSCALL ER_ID
acre_mtx(const T_CMTX *pk_cmtx)
{
	MTXCB	*mtxcb;
	MTXINIB *mtxinib;
	ER_ID	ercd;
    
	LOG_ACRE_MTX_ENTER(pk_cmtx);

	if (TNUM_MTX == 0) {
		ercd = E_NOID;
		goto exit;
	}
	CHECK_ATTRIBUTE(pk_cmtx->mtxatr, TA_TFIFO | TA_TPRI | TA_INHERIT | TA_CEILING);
	if ((pk_cmtx->mtxatr & TA_CEILING) == TA_CEILING) {
		if (pk_cmtx->ceilpri < TMIN_TPRI ||
		    pk_cmtx->ceilpri > TMAX_TPRI) {
			return E_PAR;
		}
	}

	t_lock_cpu();
	mtxcb = (MTXCB *)t_get_free_id((QUEUE *)&mtx_freelist);
	if (mtxcb == NULL) {
		ercd = E_NOID;
		t_unlock_cpu();
		goto exit;
	}
	mtxcb->exist = KOBJ_INIT;
	t_unlock_cpu();

	/*
	 * 初期設定を行う。
	 */
	queue_initialize(&mtxcb->wait_queue);
	queue_initialize(&mtxcb->mtxlist);
	mtxcb->mtxtsk = NULL;

	mtxinib = mtxcb->mtxinib;
	mtxinib->mtxatr = pk_cmtx->mtxatr;
	mtxinib->ceilpri = pk_cmtx->ceilpri;

	ercd = MTXID(mtxcb);

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	mtxcb->exist = KOBJ_EXIST;

    exit:
	LOG_ACRE_MTX_LEAVE(ercd);
	return(ercd);
}

#endif /* __acre_mtx */

/*
 *  ミューテックスの削除
 */
#ifdef __del_mtx
SYSCALL ER
del_mtx(ID mtxid)
{
	MTXCB	*mtxcb;
	ER	ercd;
	BOOL	dispflg = FALSE;

	LOG_DEL_MTX_ENTER(mtxid);
	CHECK_TSKCTX_UNL();

	CHECK_MTXID(mtxid);
	mtxcb = get_mtxcb(mtxid);

	CHECK_OBJECT_DELETABLE(mtxcb);

	t_lock_cpu();
	if (mtxcb->mtxtsk != NULL) {
		/* 
		 *  ミューテックスがロックされていた場合。
		 *  厳密な優先度制御規則を採用しているので、
		 *  直ちに現在優先度の変更をチェックする。
		 */
		
		TCB *mtxtsk;
		PRI pri;

		/*
		 *  開放するミューテックスを
		 *  ロックリストから削除
		 */
		multiqueue_delete((MULTIOBJCB *)mtxcb, 0);

		mtxtsk = mtxcb->mtxtsk;
		if (mtxcb->mtxinib->mtxatr == TA_INHERIT)  {
			/*現在優先度の変更(厳密な優先度制御規則に従い下げる)*/
			if (multiqueue_empty((MULTIOBJCB *)mtxtsk, 0)) {
				/*
				 *  ロックしているミューテックスが
				 *  無くなったときにはベース優先度に戻す
				 */
				dispflg = set_current_priority(mtxtsk,mtxtsk->base_priority);
			}else if (!queue_empty(&(mtxcb->wait_queue))) {
				if (mtxtsk->priority == ((TCB *)(mtxcb->wait_queue.next))->priority ) {
				/*
				 *  現在優先度が先頭の待ちタスクの現在優先度と
				 * 一致しているときはサーチして変更
				 */
					pri = search_mtx_priority(mtxtsk);
					if (pri != mtxtsk->priority){
						dispflg |= set_current_priority(mtxtsk, pri);
					}
				}
			}
		} else if ( mtxcb->mtxinib->mtxatr == TA_CEILING) {
			/*現在優先度の変更(厳密な優先度制御規則に従い下げる)*/
			if (multiqueue_empty((MULTIOBJCB *)mtxtsk, 0)) {
				/*
				 *  ロックしているミューテックスが
				 *  無くなったときにはベース優先度に戻す
				 */
				dispflg = set_current_priority(mtxtsk, mtxtsk->base_priority);
			} else if (mtxtsk->priority != mtxtsk->base_priority &&
				   mtxtsk->priority == INT_PRIORITY(mtxcb->mtxinib->ceilpri)) {
				/*
				 *  現在優先度が上限優先度と一致して
				 *  いるときはサーチして変更
				 */
				pri = search_mtx_priority(mtxtsk);
				if (pri != mtxtsk->priority) {
					dispflg |= set_current_priority(mtxtsk, pri);
				}
			}
		}
	}
	if (dispflg) {
		dispatch();
	}

	/* 待ち行列の後片付け。削除に伴う待ち解除を行う。*/
	while (queue_empty(&(mtxcb->wait_queue)) == FALSE) {
		TCB *tcb;
		tcb = (TCB *)queue_delete_next(&(mtxcb->wait_queue));
		if (wait_deleted(tcb)) {
			dispatch();
		}
	}

	t_unlock_cpu();

	if (!CHECK_OBJECT_RESERVED(mtxcb)) {
	  /*
	   * 予約IDでなければフリーリストに入れる。
	   */
		t_lock_cpu();
		queue_insert_prev(&mtx_freelist, (QUEUE *)mtxcb);
		t_unlock_cpu();
	}

	/* ここから先はacre_/cre_でのアクセスが可能 */
	mtxcb->exist = KOBJ_NOEXS;

	ercd = E_OK;

    exit:
	LOG_DEL_MTX_LEAVE(ercd);
	return(ercd);
}

#endif /* __del_mtx */

/*
 *  ミューテックスのロック
 */
#ifdef __loc_mtx
SYSCALL ER
loc_mtx(ID mtxid)
{
	MTXCB	*mtxcb;
	WINFO_WOBJ winfo;
	ER	ercd;
    
	LOG_LOC_MTX_ENTER(mtxid);
	CHECK_DISPATCH();

	CHECK_MTXID(mtxid);
	mtxcb = get_mtxcb(mtxid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mtxcb);

	if (runtsk == mtxcb->mtxtsk){
		/*
		 *  多重ロック
		 */
		ercd = E_ILUSE;
		goto error;
	}

	if ( ( mtxcb->mtxinib->mtxatr == TA_CEILING ) 
	     && (runtsk->base_priority < INT_PRIORITY(mtxcb->mtxinib->ceilpri)) ) {
		/*
		 *  自タスクのベース優先度が対象ミューテックスの上限優先度よりも
		 *  高い場合にはE_ILUSEを返す。(μITRON4.0仕様書)
		 */
		ercd = E_ILUSE;
		goto error;
	}

	if (mtxcb->mtxtsk == NULL){
		/*
		 *  空いていて獲得できる場合
		 */
		mtxcb->mtxtsk = runtsk;
		multiqueue_insert_prev((MULTIOBJCB *)runtsk, (MULTIOBJCB *)mtxcb, 0);
		if ((mtxcb->mtxinib->mtxatr == TA_CEILING) &&
		    (INT_PRIORITY(mtxcb->mtxinib->ceilpri) < runtsk->priority)) {
			/*
			 *  TA_CEILING属性のミューテックスを自タスクをロックした場合で、
			 *  対象ミューテックスの上限優先度が自タスクの現在優先度よりも高い
			 *  場合には、自タスクの現在優先度を、ミューテックスの上限優先度に変更する。
			 * (μITRON4.0仕様書)
			 */
			change_priority(runtsk, INT_PRIORITY(mtxcb->mtxinib->ceilpri));
		}
		ercd = E_OK;
	} else {
		/*
		 *  待つ場合
		 */
		UINT pri;
		pri = runtsk->priority;
		if (mtxcb->mtxinib->mtxatr == TA_INHERIT) {
			if (mtxcb->mtxtsk->priority > pri) { 
				/*
				 *  (TA_INHERIT属性の場合 && 自タスクをロック待ちに移行させた場合)
				 *  自タスクの現在優先度(A)が対象ミューテックスをロックしているタスクの
				 *  現在優先度よりも高い場合には、ミューテックスをロックしているタスクの
				 *  現在優先度を、(A)に変更する。(μITRON4.0仕様書要約)
				 */
				set_current_priority(mtxcb->mtxtsk, pri);
			}
			wobj_make_wait_mutexi((WOBJCB *) mtxcb, &winfo);
		} else {
			wobj_make_wait((WOBJCB *) mtxcb, &winfo);
		}
		/*
		 *  待ちに入ったタスクがあるので、
		 *  必ずディスパッチが必要
		 */
		dispatch();
		ercd = winfo.winfo.wercd;
	}

    error:
	t_unlock_cpu();

    exit:
	LOG_LOC_MTX_LEAVE(ercd);
	return(ercd);
}

#endif /* __loc_mtx */

/*
 *  ミューテックスのロック（ポーリング）
 */
#ifdef __ploc_mtx
SYSCALL ER
ploc_mtx(ID mtxid)
{
	MTXCB	*mtxcb;
	ER	ercd;
    
	LOG_LOC_MTX_ENTER(mtxid);
	CHECK_TSKCTX_UNL();

	CHECK_MTXID(mtxid);
	mtxcb = get_mtxcb(mtxid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mtxcb);

	if (runtsk == mtxcb->mtxtsk){
		/*
		 *  多重ロック
		 */
		ercd = E_ILUSE;
		goto error;
	}

	if ( ( mtxcb->mtxinib->mtxatr == TA_CEILING ) 
	     && (runtsk->base_priority < INT_PRIORITY(mtxcb->mtxinib->ceilpri)) ) {
		/*
		 *  自タスクのベース優先度が対象ミューテックスの上限優先度よりも
		 *  高い場合にはE_ILUSEを返す。(μITRON4.0仕様書)
		 */
		ercd = E_ILUSE;
		goto error;
	}

	if (mtxcb->mtxtsk == NULL){
		/*
		 *  空いていて獲得できる場合
		 */
		mtxcb->mtxtsk = runtsk;
		multiqueue_insert_prev((MULTIOBJCB *)runtsk, (MULTIOBJCB *)mtxcb, 0);
		if ((mtxcb->mtxinib->mtxatr == TA_CEILING) &&
		    (INT_PRIORITY(mtxcb->mtxinib->ceilpri) < runtsk->priority)) {
			/*
			 *  TA_CEILING属性のミューテックスを自タスクをロックした場合で、
			 *  対象ミューテックスの上限優先度が自タスクの現在優先度よりも高い
			 *  場合には、自タスクの現在優先度を、ミューテックスの上限優先度に変更する。
			 * (μITRON4.0仕様書)
			 */
			change_priority(runtsk, INT_PRIORITY(mtxcb->mtxinib->ceilpri));
		}
		ercd = E_OK;
	} else {
		/*
		 *  ポーリングの場合、待たずにタイムアウトする。
		 */
		ercd = E_TMOUT;
	}

    error:
	t_unlock_cpu();

    exit:
	LOG_LOC_MTX_LEAVE(ercd);
	return(ercd);
}

#endif /* __ploc_mtx */

/*
 *  ミューテックスのロック（タイムアウトあり）
 */
#ifdef __tloc_mtx
SYSCALL ER
tloc_mtx(ID mtxid, TMO tmout)
{
	MTXCB	*mtxcb;
	WINFO_WOBJ winfo;
	TMEVTB tmevtb;
	ER	ercd;

	CHECK_MTXID(mtxid);
	CHECK_TMOUT(tmout);

	mtxcb = get_mtxcb(mtxid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mtxcb);

	if (runtsk == mtxcb->mtxtsk){
		/*
		 *  多重ロック
		 */
		ercd = E_ILUSE;
		goto error;
	}

	if ( ( mtxcb->mtxinib->mtxatr == TA_CEILING ) 
	     && (runtsk->base_priority < INT_PRIORITY(mtxcb->mtxinib->ceilpri)) ) {
		/*
		 *  自タスクのベース優先度が対象ミューテックスの上限優先度よりも
		 *  高い場合にはE_ILUSEを返す。(μITRON4.0仕様書)
		 */
		ercd = E_ILUSE;
		goto error;
	}

	if (mtxcb->mtxtsk == NULL){
		/*
		 *  空いていて獲得できる場合
		 */
		mtxcb->mtxtsk = runtsk;
		multiqueue_insert_prev((MULTIOBJCB *)runtsk, (MULTIOBJCB *)mtxcb, 0);
		if ((mtxcb->mtxinib->mtxatr == TA_CEILING) &&
		    (INT_PRIORITY(mtxcb->mtxinib->ceilpri) < runtsk->priority)) {
			/*
			 *  TA_CEILING属性のミューテックスを自タスクをロックした場合で、
			 *  対象ミューテックスの上限優先度が自タスクの現在優先度よりも高い
			 *  場合には、自タスクの現在優先度を、ミューテックスの上限優先度に変更する。
			 * (μITRON4.0仕様書)
			 */
			change_priority(runtsk, INT_PRIORITY(mtxcb->mtxinib->ceilpri));
		}
		ercd = E_OK;
	} else {
		UINT pri;
		if (tmout == TMO_POL) {
			ercd = E_TMOUT;
			goto error;
		}
		/*
		 *  待つ場合
		 */
		pri = runtsk->priority;
		if (mtxcb->mtxinib->mtxatr == TA_INHERIT) {
			if (mtxcb->mtxtsk->priority > pri) { 
				/*
				 *  (TA_INHERIT属性の場合 && 自タスクをロック待ちに移行させた場合)
				 *  自タスクの現在優先度(A)が対象ミューテックスをロックしているタスクの
				 *  現在優先度よりも高い場合には、ミューテックスをロックしているタスクの
				 *  現在優先度を、(A)に変更する。(μITRON4.0仕様書要約)
				 */
				set_current_priority(mtxcb->mtxtsk, pri);
			}
			wobj_make_wait_mutexi_tmout((WOBJCB *) mtxcb, &winfo, &tmevtb, tmout);
		} else {
			wobj_make_wait_tmout((WOBJCB *) mtxcb, &winfo, &tmevtb, tmout);
		}
		/*
		 *  待ちに入ったタスクがあるので、
		 *  必ずディスパッチが必要
		 */
		dispatch();
		ercd = winfo.winfo.wercd;
	}

    error:
	t_unlock_cpu();

    exit:
	LOG_LOC_MTX_LEAVE(ercd);
	return(ercd);
}

#endif /* __tloc_mtx */

/*
 *  ミューテックスのロック解除
 */
#ifdef __unl_mtx
SYSCALL ER
unl_mtx(ID mtxid)
{
	MTXCB	*mtxcb;
	TCB	*tcb;
	ER	ercd;
    
	UINT	pri;
	BOOL	dispflg = FALSE;

	LOG_UNL_MTX_ENTER(mtxid);
	CHECK_DISPATCH();

	CHECK_MTXID(mtxid);
	mtxcb = get_mtxcb(mtxid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mtxcb);

	if (mtxcb->mtxtsk != runtsk){
		/*
		 *  不正使用:  対象ミューテックスをロックしていない。
		 */
		ercd = E_ILUSE;
		goto error;
	}

	multiqueue_delete((MULTIOBJCB *)mtxcb, 0);/*開放するミューテックスをロックリストから削除*/

	/* 
	 *  厳密な優先度制御規則を採用しているので、
	 *  常に現在優先度の変更を検討する必要がある。
	 */
	if (mtxcb->mtxinib->mtxatr == TA_INHERIT)  {
		/*現在優先度の変更(厳密な優先度制御規則に従い下げる)*/
		if (multiqueue_empty((MULTIOBJCB *)runtsk, 0)) {
			/*
			 *  ロックしているミューテックスが
			 *  無くなったときにはベース優先度に戻す
			 * 
			 *  対象となっているタスクの状態はrunningなのが自明なので、
			 *  別のTA_INHERIT属性ミューテックスを待っているはずが無い。
			 *  よって、set_current_priority()を用いる必要は無い。
			 */
			runtsk->priority = runtsk->base_priority;
			dispflg = FALSE;
		}else if (!queue_empty(&(mtxcb->wait_queue))) {
			if (runtsk->priority == ((TCB *)(mtxcb->wait_queue.next))->priority ) {
				/*
				 *  現在優先度が先頭の待ちタスクの現在優先度と
				 * 一致しているときはサーチして変更
				 */
				pri = search_mtx_priority(runtsk);
				if (pri != runtsk->priority){
					dispflg = set_current_priority(runtsk, pri);
				}
			}
		}
	} else if ( mtxcb->mtxinib->mtxatr == TA_CEILING) {
		/*現在優先度の変更(厳密な優先度制御規則に従い下げる)*/
		if (multiqueue_empty((MULTIOBJCB *)runtsk, 0)) {
			/*
			 *  ロックしているミューテックスが
			 *  無くなったときにはベース優先度に戻す。
			 * 
			 *  対象となっているタスクの状態はrunningなのが自明なので、
			 *  別のTA_INHERIT属性ミューテックスを待っているはずが無い。
			 *  よって、set_current_priority()を用いる必要は無い。
			 */
			runtsk->priority = runtsk->base_priority;
			dispflg = FALSE;
		} else if (runtsk->priority != runtsk->base_priority &&
			   runtsk->priority == INT_PRIORITY(mtxcb->mtxinib->ceilpri)) {
			/*
			 *  現在優先度が上限優先度と一致して
			 *  いるときはサーチして変更
			 */
			pri = search_mtx_priority(runtsk);
			if (pri != runtsk->priority) {
				dispflg = set_current_priority(runtsk, pri);
			}
		}
	}

	if (!queue_empty(&(mtxcb->wait_queue))) {
		tcb = (TCB *) queue_delete_next(&(mtxcb->wait_queue));
		dispflg |= wait_complete(tcb);
		mtxcb->mtxtsk = tcb;
		multiqueue_insert_prev((MULTIOBJCB *)tcb, (MULTIOBJCB *)mtxcb, 0);
		/*TA_CEILINGであれば優先度を優先度上限まで上げる*/
		if (mtxcb->mtxinib->mtxatr == TA_CEILING
		    && (INT_PRIORITY(mtxcb->mtxinib->ceilpri) < tcb->priority) ){
			dispflg |= change_priority(tcb, INT_PRIORITY(mtxcb->mtxinib->ceilpri));
		}
	} else {
		mtxcb->mtxtsk = NULL;
	}

	if (dispflg) {
		dispatch();
	}
	ercd = E_OK;

    error:
	t_unlock_cpu();

    exit:
	LOG_UNL_MTX_LEAVE(ercd);
	return(ercd);
}

#endif /* __unl_mtx */

/*
 *  ミューテックスの状態参照
 */
#ifdef __ref_mtx
SYSCALL ER
ref_mtx(ID mtxid, T_RMTX *pk_rmtx)
{
	MTXCB	*mtxcb;
	TCB	*tcb;
	ER	ercd;
    
	LOG_REF_MTX_ENTER(mtxid);
	CHECK_TSKCTX_UNL();

	CHECK_MTXID(mtxid);
	mtxcb = get_mtxcb(mtxid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mtxcb);

	if (mtxcb->mtxtsk == NULL) {
		/* 対象タスクがロックされていない場合 */
		pk_rmtx->htskid = TSK_NONE;
	} else {
		pk_rmtx->htskid = TSKID(mtxcb->mtxtsk);
	}

	if (queue_empty(&(mtxcb->wait_queue))) {
		/* ロックを待っているタスクが無い場合 */
		pk_rmtx->wtskid = TSK_NONE;
	} else {
		tcb = (TCB *)queue_peek_next(&(mtxcb->wait_queue));
		pk_rmtx->wtskid = TSKID(tcb);
	}


	/*
	 *  【補足説明】 htskid == TSK_NONE と wtskid != TSK_NONEが
	 *  同時に成立することはない。
	 */
	assert(!((pk_rmtx->htskid == TSK_NONE) && (pk_rmtx->wtskid != TSK_NONE)));

	
	ercd = E_OK;
   
	t_unlock_cpu();

    exit:
	LOG_REF_MTX_LEAVE(ercd);
	return(ercd);
}

#endif /* __ref_mtx */


/*
 *  ロック中ミューテックス内の最高優先度のサーチ
 *  - TA_INHERITのミューテックスの待ちタスクの現在優先度
 *  - TA_CEILINGのミューテックスの優先度上限
 *  - ベース優先度
 *  のうち、最高の優先度の値を検索
 */
#ifdef __schmtxpri

UINT 
search_mtx_priority(TCB *tcb)
{
	UINT pri;
	MULTIOBJCB *cb;

	/*
	 *  ベース優先度を初期値として検索
	 */
	pri = tcb->base_priority;

	cb = NULL;
	while ((cb = multiqueue_enumerate((MULTIOBJCB *)tcb, cb, 0)) != NULL) {
		MTXCB *mtxcb;
		mtxcb = (MTXCB *)cb;
		if (mtxcb->mtxinib->mtxatr == TA_INHERIT) {
			/*
			 *  最高優先度の待ちタスクの現在優先度と比較
			 */
			if (!queue_empty(&(mtxcb->wait_queue))) {
				TCB *tcb;
				tcb = (TCB *)(mtxcb->wait_queue.next);
				if(tcb->priority < pri){
					pri = tcb->priority;
				}
			}
		} if (mtxcb->mtxinib->mtxatr == TA_CEILING) {
			/*
			 *  優先度上限と比較
			 */
			if (INT_PRIORITY(mtxcb->mtxinib->ceilpri) < pri){
				pri = INT_PRIORITY(mtxcb->mtxinib->ceilpri);
			}
		}
	}
	return pri;
}

#endif /* __schmtxpri */


/*
 *  ロックしているTA_CEILINGミューテックスの上限優先度のうち
 *  最低の値の検索
 */
#ifdef __schmaxceilpri

UINT
search_max_ceilpri(TCB *tcb)
{
	UINT pri = 0;/*ダミーの高い値*/
	MULTIOBJCB *cb;

	cb = NULL;
	while ((cb = multiqueue_enumerate((MULTIOBJCB *)tcb, cb, 0)) != NULL) {
		MTXCB *mtxcb;
		mtxcb = (MTXCB *)cb;

		if(mtxcb->mtxinib->mtxatr == TA_CEILING) {
			/*
			 *  TA_CEILINGなら優先度上限と比較
			 */
			if (INT_PRIORITY(mtxcb->mtxinib->ceilpri) > pri) {
				pri = INT_PRIORITY(mtxcb->mtxinib->ceilpri);
			}
		}
	}

	return pri;
}

#endif /* __schmaxceilpri */

/*
 *  指定されたベース優先度をタスクに与えてよいかどうかをチェックする。
 *  ベース優先度設定の制限を受けるのは、対象タスクがTA_CEILINGをロックしているか
 *  ロックを待っている場合で、tskpriで指定されたベース優先度が、それらのミューテックスの
 *  いずれかの上限優先度よりも高い場合。
 *  ベース優先度設定の制限を受ける場合には、FALSEを返す。
 */
#ifdef __checkceilpri

BOOL
check_ceiling_priority(TCB *tcb, PRI base_priority)
{
 	if (search_max_ceilpri(tcb) > base_priority) {
		 return FALSE;
	}
	
	if ((tcb->tstat & TS_WAIT_MTX_I))  {
			MTXINIB *mtxinib = ((MTXCB *)( ((WINFO_WOBJ *)(tcb->winfo))->wobjcb))->mtxinib;
			if ((mtxinib->mtxatr & TA_CEILING) && mtxinib->ceilpri > base_priority) {
				return FALSE;
			}
	}
	
	return TRUE;
}

#endif /* __checkceilpri */

