/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2003-2004 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/alarm.c 6101 2006-09-10T08:00:54.989588Z monaka  $
 */

/*
 *	アラームハンドラ機能
 */

#include "fi4_kernel.h"
#include "check.h"
#include "alarm.h"

/*
 *  アラームハンドラIDの最大値（kernel_cfg.c）
 */
extern const ID	tmax_almid;

/*
 *  アラームハンドラ初期化ブロックのエリア（kernel_cfg.c）
 */
extern ALMINIB	alminib_table[];

/*
 *  アラームハンドラ初期化ブロック連想ペアの数（kernel_cfg.c）
 */
extern const UINT	tnum_alminia;

/*
 *  アラームハンドラ初期化ブロック連想ペアのエリア（kernel_cfg.c）
 */
extern const ALMINIA	alminia_table[];

/*
 *  アラームハンドラ予約ID情報のエリア（kernel_cfg.c）
 */
extern const ID		almrid_table[];

/*
 *  アラームハンドラ予約ID情報の数 (kernel_cfg.c)
 */
extern const UINT	tnum_almrid;

/*
 *  アラームハンドラ管理ブロックのエリア（kernel_cfg.c）
 */
extern ALMCB	almcb_table[];

/*
 *  アラームハンドラの数
 */
#define TNUM_ALM	((UINT)(tmax_almid - TMIN_ALMID + 1))

/*
 *  アラームハンドラIDからアラームハンドラ管理ブロックを取り出すためのマクロ
 */
#define INDEX_ALM(almid)	((UINT)((almid) - TMIN_ALMID))
#define get_almcb(almid)	(&(almcb_table[INDEX_ALM(almid)]))

/*
 *  ID自動割当て用フリーリスト
 */
extern QUEUE alm_freelist;

/*
 *  引数まで定義したアラームハンドラの型
 */
typedef void	(*ALMHDR)(VP_INT exinf);

/*
 *  アラームハンドラ機能の初期化
 */
#ifdef __almini

QUEUE alm_freelist;

void
alarm_initialize(void)
{
	UINT	i;
	ALMCB	*almcb;
	ALMINIB *alminib;
	const ALMINIA *alminia;
	const ID	*almrid;

	/*
	 * カーネル起動時の状態に関わらず初期化しなければいけないものを
	 * まず初期化する。
	 */
	for (almcb = almcb_table, i = 0; i < TNUM_ALM; almcb++, i++) {
		queue_initialize(&(almcb->free_queue));
		almcb->alminib = NULL;
	}
	queue_initialize(&alm_freelist);

	/*
	 * alminia によって示されるものをまず初期化
	 */
	for (alminia = alminia_table, i = 0; i < tnum_alminia; alminia++, i++) {
		UINT idx;
		idx = INDEX_ALM(alminia->almid);
		alminib = &alminib_table[idx];
		*alminib = alminia->alminib;
		almcb = &almcb_table[idx];
		almcb->alminib = alminib;

		almcb->almsta = FALSE;
		almcb->evttim = 0;

		almcb->exist = KOBJ_EXIST;
		almcb->reserved = TRUE;
	}

	/*
	 *  予約IDの設定
	 */
	for (almrid = almrid_table, i = 0; i < tnum_almrid; almrid++, i++) {
		almcb_table[INDEX_ALM(*almrid)].reserved = TRUE;
	}

	/*
	 * alminia によって示されないものを初期化
	 */
	for (almcb = almcb_table, i = 0; i < TNUM_ALM; almcb++, i++) {
		if (almcb->alminib == NULL) {
			almcb->exist = KOBJ_NOEXS;
			almcb->alminib = &alminib_table[i];

			if (!CHECK_OBJECT_RESERVED(almcb)) {
				queue_insert_prev(&alm_freelist, (QUEUE*)almcb);
			}
		}
	}
}

#endif /* __almini */

/*
 *  アラームハンドラの生成
 */
#ifdef __cre_alm

SYSCALL ER
cre_alm(ID almid, const T_CALM * pk_calm)
{
	ALMCB	*almcb;
	ALMINIB *alminib;
	ER	ercd;
    
	LOG_CRE_ALM_ENTER(almid, pk_calm);
	CHECK_TSKCTX_UNL();

	CHECK_ALMID(almid);
	CHECK_ATTRIBUTE(pk_calm->almatr, TA_HLNG | TA_ASM);

	almcb = get_almcb(almid);
	CHECK_OBJECT_CREATABLE(almcb);
	UPDATE_OBJECT_FREELIST(almcb);

	/*
	 * 初期設定を行う。
	 */
	queue_initialize(&(almcb->free_queue));
	alminib = almcb->alminib;
	alminib->almatr = pk_calm->almatr;
	alminib->exinf  = pk_calm->exinf;
	alminib->almhdr = pk_calm->almhdr;
	almcb->almsta = FALSE;

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	almcb->exist = KOBJ_EXIST;

	ercd = E_OK;

    exit:
	LOG_CRE_ALM_LEAVE(ercd);
	return(ercd);
}

#endif /* __cre_alm */

/*
 *  アラームハンドラの生成(ID番号自動割付け)
 */
#ifdef __acre_alm

SYSCALL ER_ID
acre_alm(const T_CALM *pk_calm)
{
	ALMCB	*almcb;
	ALMINIB *alminib;
	ER_ID	ercd;
    
	LOG_ACRE_ALM_ENTER(pk_calm);
	CHECK_TSKCTX_UNL();

	if (TNUM_ALM == 0) {
		ercd = E_NOID;
		goto exit;
	}
	CHECK_ATTRIBUTE(pk_calm->almatr, TA_HLNG | TA_ASM);

	t_lock_cpu();
	almcb = (ALMCB *)t_get_free_id((QUEUE *)&alm_freelist);
	if (almcb == NULL) {
		ercd = E_NOID;
		t_unlock_cpu();
		goto exit;
	}
	almcb->exist = KOBJ_INIT;
	t_unlock_cpu();

	/*
	 * 初期設定を行う。
	 */
	queue_initialize(&(almcb->free_queue));
	alminib = almcb->alminib;
	alminib->almatr = pk_calm->almatr;
	alminib->exinf  = pk_calm->exinf;
	alminib->almhdr = pk_calm->almhdr;
	almcb->almsta = FALSE;

	ercd = ALMID(almcb);

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	almcb->exist = KOBJ_EXIST;

    exit:
	LOG_ACRE_ALM_LEAVE(ercd);
	return(ercd);
}

#endif /* __acre_alm */

/*
 *  アラームハンドラの削除
 */
#ifdef __del_alm
SYSCALL ER
del_alm(ID almid)
{
	ALMCB	*almcb;
	ER	ercd;
    
	LOG_DEL_ALM_ENTER(almid);
	CHECK_TSKCTX_UNL();

	CHECK_ALMID(almid);
	almcb = get_almcb(almid);

	CHECK_OBJECT_DELETABLE(almcb);

	/*
	 *  アラームハンドラが動作していたら、停止する。
	 */
	if (almcb->almsta) {
		t_lock_cpu();
		almcb->almsta = FALSE;
		tmevtb_dequeue(&(almcb->tmevtb));
		t_unlock_cpu();
	}

	/*
	 *  予約IDでなければフリーリストに入れる。
	 */
	if (!CHECK_OBJECT_RESERVED(almcb)) {
		t_lock_cpu();
		queue_insert_prev(&alm_freelist, (QUEUE *)almcb);
		t_unlock_cpu();
	}

	/*
	 *  ここから先はacre_/cre_でのアクセスが可能
	 */
	almcb->exist = KOBJ_NOEXS;

	ercd = E_OK;

    exit:
	LOG_DEL_ALM_LEAVE(ercd);
	return(ercd);
}

#endif /* __del_alm */

/*
 *  アラームハンドラの動作開始
 */
#ifdef __sta_alm

SYSCALL ER
sta_alm(ID almid, RELTIM almtim)
{
	ALMCB	*almcb;
	ER	ercd;
	EVTTIM	evttim;

	LOG_STA_ALM_ENTER(almid);
	CHECK_TSKCTX_UNL();

	CHECK_ALMID(almid);
	almcb = get_almcb(almid);

	evttim = base_time + almtim;
	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(almcb);

	if (almcb->almsta) {
		tmevtb_dequeue(&(almcb->tmevtb));
	} else {
		almcb->almsta = TRUE;
	}
	tmevtb_enqueue_evttim(&(almcb->tmevtb), evttim,
				(CBACK) call_almhdr, (VP) almcb);
	almcb->evttim = evttim;

	t_unlock_cpu();

	ercd = E_OK;
    exit:
	LOG_STA_ALM_LEAVE(ercd);
	return(ercd);
}

#endif /* __sta_alm */

/*
 *  アラームハンドラの動作停止
 */
#ifdef __stp_alm

SYSCALL ER
stp_alm(ID almid)
{
	ALMCB	*almcb;
	ER	ercd;

	LOG_STP_ALM_ENTER(almid);
	CHECK_TSKCTX_UNL();

	CHECK_ALMID(almid);
	almcb = get_almcb(almid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(almcb);

	/*
	 * 対象アラームハンドラに,動作していないアラームハンドラが指定された
	 * 場合には,何もしない. (μITRON4.0仕様書)
	 */
	if (almcb->almsta) {
		almcb->almsta = FALSE;
		tmevtb_dequeue(&(almcb->tmevtb));
	}
	ercd = E_OK;

	t_unlock_cpu();
     exit:
	LOG_STP_ALM_LEAVE(ercd);
	return(ercd);
}

#endif /* __stp_alm */

/*
 *  アラームハンドラの状態参照
 */
#ifdef __ref_alm
SYSCALL ER
ref_alm(ID almid, T_RALM *pk_ralm)
{
	ALMCB	*almcb;
	ER	ercd;
    
	LOG_REF_ALM_ENTER(almid);
	CHECK_TSKCTX_UNL();

	CHECK_ALMID(almid);
	almcb = get_almcb(almid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(almcb);

	if (almcb->almsta) {
		pk_ralm->almstat = TALM_STA;
	} else {
		pk_ralm->almstat = TALM_STP;
	}

	pk_ralm->lefttim = almcb->evttim - base_time;

	ercd = E_OK;

	t_unlock_cpu();

    exit:
	LOG_REF_ALM_LEAVE(ercd);
	return(ercd);
}

#endif /* __ref_alm */

/*
 *  周期ハンドラ起動ルーチン
 */
#ifdef __almcal

void
call_almhdr(ALMCB *almcb)
{
	/*
	 *  アラームハンドラを，CPUロック解除状態で呼び出す．
	 */
	i_unlock_cpu();
	LOG_ALM_ENTER(almcb);
	((ALMHDR)(*almcb->alminib->almhdr))(almcb->alminib->exinf);
	almcb->almsta = FALSE;
	LOG_ALM_LEAVE(almcb);
	i_lock_cpu();
}

#endif /* __almcal */
