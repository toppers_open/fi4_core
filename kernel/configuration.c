/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2003-2004 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/configuration.c 2750 2006-03-07T16:15:24.532712Z monaka  $
 */

/*
 *	システム構成管理機能
 */

#include "fi4_kernel.h"
#include "configuration.h"
#include "check.h"

/*
 *  CPU例外ハンドラの定義
 */
#ifdef __def_exc

SYSCALL
ER def_exc(EXCNO excno, const T_DEXC *pk_dexc)
{
	ER ercd = E_OK;

	LOG_DEF_EXC_ENTER(excno);

	if (pk_dexc == NULL) {
		undef_exc(excno);
	} else {
		CHECK_ATTRIBUTE(pk_dexc->excatr, TA_HLNG | TA_ASM);
		define_exc(excno, pk_dexc->exchdr);
	}
exit:
	LOG_DEF_EXC_LEAVE(ercd);
	return ercd;
}

#endif /* __def_exc */

/*
 *  コンフィギュレーション情報の参照
 *
 * pk_rcfgの中身は実装依存。
 * 本バージョンでは、pk_rcfg は空。
 * よって、何もすることがない。
 */
#ifdef __ref_cfg

SYSCALL
ER ref_cfg(T_RCFG *pk_rcfg)
{
	return E_OK;
}
#endif /* __ref_cfg */

/*
 *  バージョン情報の参照
 */
#ifdef __ref_ver

SYSCALL
ER ref_ver(T_RVER *pk_rver)
{
	pk_rver->maker = TKERNEL_MAKER;
	pk_rver->prid = TKERNEL_PRID;
	pk_rver->spver = TKERNEL_SPVER;
	pk_rver->prver = TKERNEL_PRVER;

	/*
	 *  prno の内容はメーカに任されている。
	 *  将来、この項目を使うようになった時に
	 *  混乱しないよう、本バージョンでは
	 *  配列を0クリアする。
	 */
	pk_rver->prno[0] = 0;
	pk_rver->prno[1] = 0;
	pk_rver->prno[2] = 0;
	pk_rver->prno[3] = 0;

	return E_OK;
}

#endif /* __ref_ver */


