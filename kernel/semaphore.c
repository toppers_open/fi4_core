/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2003-2004 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/semaphore.c 6101 2006-09-10T08:00:54.989588Z monaka  $
 */

/*
 *	セマフォ機能
 */

#include "fi4_kernel.h"
#include "check.h"
#include "task.h"
#include "wait.h"
#include "semaphore.h"

/*
 *  セマフォIDの最大値（kernel_cfg.c）
 */
extern const ID	tmax_semid;

/*
 *  セマフォ初期化ブロックのエリア（kernel_cfg.c）
 */
extern SEMINIB	seminib_table[];

/*
 *  セマフォ初期化ブロック連想ペアの数（kernel_cfg.c）
 */
extern const UINT	tnum_seminia;

/*
 *  セマフォ初期化ブロック連想ペアのエリア（kernel_cfg.c）
 */
extern const SEMINIA	seminia_table[];

/*
 *  セマフォ予約ID情報のエリア（kernel_cfg.c）
 */
extern const ID		semrid_table[];

/*
 *  セマフォ予約ID情報の数 (kernel_cfg.c)
 */
extern const UINT	tnum_semrid;

/*
 *  セマフォ管理ブロックのエリア（kernel_cfg.c）
 */
extern SEMCB	semcb_table[];

/*
 *  セマフォの数
 */
#define TNUM_SEM	((UINT)(tmax_semid - TMIN_SEMID + 1))

/*
 *  セマフォIDからセマフォ管理ブロックを取り出すためのマクロ
 */
#define INDEX_SEM(semid)	((UINT)((semid) - TMIN_SEMID))
#define get_semcb(semid)	(&(semcb_table[INDEX_SEM(semid)]))

/*
 *  ID自動割当て用フリーリスト
 */
extern QUEUE sem_freelist;

/* 
 *  セマフォ機能の初期化
 */
#ifdef __semini

/*
 *  ID自動割当て用フリーリスト
 */
QUEUE sem_freelist;

void
semaphore_initialize(void)
{
	UINT	i;
	SEMCB	*semcb;
	SEMINIB *seminib;
	const SEMINIA	*seminia;
	const ID	*semrid;

	/*
	 * カーネル起動時の状態に関わらず初期化しなければいけないものを
	 * まず初期化する。
	 */
	for (semcb = semcb_table, i = 0; i < TNUM_SEM; semcb++, i++) {
		queue_initialize(&(semcb->wait_queue));
		semcb->seminib = NULL;
	}
	queue_initialize(&sem_freelist);

	/*
	 * seminia によって示されるものをまず初期化
	 */
	for (seminia = seminia_table, i = 0; i < tnum_seminia; seminia++, i++) {
		seminib = &seminib_table[INDEX_SEM(seminia->semid)];
		*seminib = seminia->seminib;
		semcb = &semcb_table[INDEX_SEM(seminia->semid)];
		semcb->seminib = seminib;
		semcb->exist = KOBJ_EXIST;
		semcb->reserved = TRUE;

		semcb->semcnt = seminib->isemcnt;
	}

	/*
	 *  予約IDの設定
	 */
	for (semrid = semrid_table, i = 0; i < tnum_semrid; semrid++, i++) {
		semcb_table[INDEX_SEM(*semrid)].reserved = TRUE;
	}

	/*
	 * seminia によって示されないものを初期化
	 */
	for (semcb = semcb_table, i = 0; i < TNUM_SEM; semcb++, i++) {
		if (semcb->seminib == NULL) {
			semcb->exist = KOBJ_NOEXS;
			semcb->seminib = &seminib_table[i];

			if (!CHECK_OBJECT_RESERVED(semcb)) {
				queue_insert_prev(&sem_freelist, (QUEUE*)semcb);
			}
		}
	}
}

#endif /* __semini */

/*
 *  セマフォの初期化
 */
#ifdef __semcbini

ER init_semcb(SEMCB *semcb, T_CSEM *pk_csem)
{
	SEMINIB *seminib;

	/*
	 * 初期設定を行う。
	 */
	queue_initialize(&(semcb->wait_queue));
	seminib = semcb->seminib;
	seminib->sematr = pk_csem->sematr;
	semcb->semcnt = seminib->isemcnt = pk_csem->isemcnt;
	seminib->maxsem = pk_csem->maxsem;


	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	semcb->exist = KOBJ_EXIST;

	return E_OK;
}

#endif /* __semcbini */

/*
 *  セマフォの生成
 */
#ifdef __cre_sem

SYSCALL ER
cre_sem(ID semid, const T_CSEM * pk_csem)
{
	SEMCB	*semcb;
	ER	ercd;
    
	LOG_CRE_SEM_ENTER(semid, pk_csem);
	CHECK_TSKCTX_UNL();

	CHECK_SEMID(semid);
	CHECK_ATTRIBUTE(pk_csem->sematr, TA_TFIFO | TA_TPRI);
	if (pk_csem->isemcnt > pk_csem->maxsem) {
		ercd = E_PAR;
	} else {
		semcb = get_semcb(semid);
		CHECK_OBJECT_CREATABLE(semcb);
		UPDATE_OBJECT_FREELIST(semcb);

		ercd = init_semcb(semcb, pk_csem);
	}
    exit:
	LOG_CRE_SEM_LEAVE(ercd);
	return(ercd);
}

#endif /* __cre_sem */

/*
 *  セマフォの生成(ID番号自動割付け)
 */
#ifdef __acre_sem

SYSCALL ER_ID
acre_sem(const T_CSEM *pk_csem)
{
	SEMCB	*semcb;
	ER_ID	ercd;
    
	LOG_ACRE_SEM_ENTER(pk_csem);

	if (TNUM_SEM == 0) {
		ercd = E_NOID;
		goto exit;
	}
	CHECK_ATTRIBUTE(pk_csem->sematr, TA_TFIFO | TA_TPRI);
	if (pk_csem->isemcnt > pk_csem->maxsem) {
		ercd = E_PAR;
		goto exit;
	}

	t_lock_cpu();
	semcb = (SEMCB *)t_get_free_id((QUEUE *)&sem_freelist);
	if (semcb == NULL) {
		ercd = E_NOID;
		t_unlock_cpu();
		goto exit;
	}
	semcb->exist = KOBJ_EXIST;
	t_unlock_cpu();

	ercd = init_semcb(semcb, pk_csem);

	if (ercd == E_OK) {
		ercd = SEMID(semcb);
	}

    exit:
	LOG_ACRE_SEM_LEAVE(ercd);
	return(ercd);
}

#endif /* __acre_sem */

/*
 *  セマフォの削除
 */
#ifdef __del_sem
SYSCALL ER
del_sem(ID semid)
{
	SEMCB	*semcb;
	ER	ercd;
    
	LOG_DEL_SEM_ENTER(semid);
	CHECK_TSKCTX_UNL();

	CHECK_SEMID(semid);
	semcb = get_semcb(semid);

	CHECK_OBJECT_DELETABLE(semcb);

	/* 待ち行列の後片付け。削除に伴う待ち解除を行う。*/
	while (queue_empty(&(semcb->wait_queue)) == FALSE) {
		TCB *tcb;
		tcb = (TCB *)queue_delete_next(&(semcb->wait_queue));
		t_lock_cpu();
		if (wait_deleted(tcb)) {
			dispatch();
		}
		t_unlock_cpu();	
	}

	if (!CHECK_OBJECT_RESERVED(semcb)) {
	  /*
	   * 予約IDでなければフリーリストに入れる。
	   */
		t_lock_cpu();
		queue_insert_prev(&sem_freelist, (QUEUE *)semcb);
		t_unlock_cpu();
	}

	/* ここから先はacre_/cre_でのアクセスが可能 */
	semcb->exist = KOBJ_NOEXS;

	ercd = E_OK;

    exit:
	LOG_DEL_SEM_LEAVE(ercd);
	return(ercd);
}

#endif /* __del_sem */

/*
 *  セマフォ資源の返却
 */
#ifdef __sig_sem

SYSCALL ER
sig_sem(ID semid)
{
	SEMCB	*semcb;
	TCB	*tcb;
	ER	ercd;
    
	LOG_SIG_SEM_ENTER(semid);

	CHECK_TSKCTX_UNL();

	CHECK_SEMID(semid);
	semcb = get_semcb(semid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(semcb);

	if (!(queue_empty(&(semcb->wait_queue)))) {
		tcb = (TCB *) queue_delete_next(&(semcb->wait_queue));
		if (wait_complete(tcb)) {
			dispatch();
		}
		ercd = E_OK;
	}
	else if (semcb->semcnt < semcb->seminib->maxsem) {
		semcb->semcnt += 1;
		ercd = E_OK;
	}
	else {
		ercd = E_QOVR;
	}

	t_unlock_cpu();

    exit:
	LOG_SIG_SEM_LEAVE(ercd);
	return(ercd);
}

#endif /* __sig_sem */

/*
 *  セマフォ資源の返却（非タスクコンテキスト用）
 */
#ifdef __isig_sem

SYSCALL ER
isig_sem(ID semid)
{
	SEMCB	*semcb;
	TCB	*tcb;
	ER	ercd;
    
	LOG_ISIG_SEM_ENTER(semid);
	CHECK_INTCTX_UNL();

	CHECK_SEMID(semid);
	semcb = get_semcb(semid);

	i_lock_cpu();
	I_CHECK_OBJECT_EXIST(semcb);

	if (!queue_empty(&(semcb->wait_queue))) {
		tcb = (TCB *) queue_delete_next(&(semcb->wait_queue));
		if (wait_complete(tcb)) {
			reqflg = TRUE;
		}
		ercd = E_OK;
	}
	else if (semcb->semcnt < semcb->seminib->maxsem) {
		semcb->semcnt += 1;
		ercd = E_OK;
	}
	else {
		ercd = E_QOVR;
	}

	i_unlock_cpu();

    exit:
	LOG_ISIG_SEM_LEAVE(ercd);
	return(ercd);
}

#endif /* __isig_sem */

/*
 *  セマフォ資源の獲得
 */
#ifdef __wai_sem

SYSCALL ER
wai_sem(ID semid)
{
	SEMCB	*semcb;
	WINFO_WOBJ winfo;
	ER	ercd;

	LOG_WAI_SEM_ENTER(semid);
	CHECK_DISPATCH();

	CHECK_SEMID(semid);
	semcb = get_semcb(semid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(semcb);

	if (semcb->semcnt >= 1) {
		semcb->semcnt -= 1;
		ercd = E_OK;
	}
	else {
		wobj_make_wait((WOBJCB *) semcb, &winfo);
		dispatch();
		ercd = winfo.winfo.wercd;
	}

	t_unlock_cpu();

    exit:
	LOG_WAI_SEM_LEAVE(ercd);
	return(ercd);
}

#endif /* __wai_sem */

/*
 *  セマフォ資源の獲得（ポーリング）
 */
#ifdef __pol_sem

SYSCALL ER
pol_sem(ID semid)
{
	SEMCB	*semcb;
	ER	ercd;

	LOG_POL_SEM_ENTER(semid);
	CHECK_TSKCTX_UNL();

	CHECK_SEMID(semid);
	semcb = get_semcb(semid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(semcb);

	if (semcb->semcnt >= 1) {
		semcb->semcnt -= 1;
		ercd = E_OK;
	}
	else {
		ercd = E_TMOUT;
	}

	t_unlock_cpu();

    exit:
	LOG_POL_SEM_LEAVE(ercd);
	return(ercd);
}

#endif /* __pol_sem */

/*
 *  セマフォ資源の獲得（タイムアウトあり）
 */
#ifdef __twai_sem

SYSCALL ER
twai_sem(ID semid, TMO tmout)
{
	SEMCB	*semcb;
	WINFO_WOBJ winfo;
	TMEVTB	tmevtb;
	ER	ercd;

	LOG_TWAI_SEM_ENTER(semid, tmout);
	CHECK_DISPATCH();
	CHECK_TMOUT(tmout);

	CHECK_SEMID(semid);
	semcb = get_semcb(semid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(semcb);

	if (semcb->semcnt >= 1) {
		semcb->semcnt -= 1;
		ercd = E_OK;
	}
	else if (tmout == TMO_POL) {
		ercd = E_TMOUT;
	}
	else {
		wobj_make_wait_tmout((WOBJCB *) semcb, &winfo, &tmevtb, tmout);
		dispatch();
		ercd = winfo.winfo.wercd;
	}

	t_unlock_cpu();

    exit:
	LOG_TWAI_SEM_LEAVE(ercd);
	return(ercd);
}

#endif /* __twai_sem */

/*
 *  セマフォの状態参照
 */
#ifdef __ref_sem
SYSCALL ER
ref_sem(ID semid, T_RSEM *pk_rsem)
{
	SEMCB	*semcb;
	ER	ercd;
    
	LOG_REF_SEM_ENTER(semid);
	CHECK_TSKCTX_UNL();

	CHECK_SEMID(semid);
	semcb = get_semcb(semid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(semcb);

	if (!queue_empty(&(semcb->wait_queue))) {
		TCB *tcb;
		tcb = (TCB *)queue_peek_next(&(semcb->wait_queue));
		pk_rsem->wtskid = TSKID(tcb);
	} else {
		pk_rsem->wtskid = TSK_NONE;
	}
	pk_rsem->semcnt = semcb->semcnt;
	ercd = E_OK;
   
	t_unlock_cpu();

    exit:
	LOG_REF_SEM_LEAVE(ercd);
	return(ercd);
}

#endif /* __ref_sem */

