/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2003-2004 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/task_overrun.c 2750 2006-03-07T16:15:24.532712Z monaka  $
 */

/*
 *	オーバーランハンドラ機能
 */

#include "fi4_kernel.h"
#include "check.h"
#include "task.h"
#include "task_overrun.h"

/*
 *  オーバーランハンドラIDの最大値（kernel_cfg.c）
 */
extern const ID	tmax_ovrid;

/*
 *  オーバーランハンドラ初期化ブロックのエリア（kernel_cfg.c）
 */
extern const OVRINIB	ovrinib_table[];

/*
 *  オーバーランハンドラ管理ブロックのエリア（kernel_cfg.c）
 */
extern OVRCB	ovrcb_table[];

/*
 *  引数まで定義したオーバーランハンドラの型
 */
typedef void	(*OVRHDR)(ID tskid, VP_INT exinf);

/*
 *  オーバランハンドラの存在チェック
 *  オーバランハンドラ未定義のとき、E_OBJ が返る。
 */
#define CHECK_OVERRUN_HADNLER_DEFINED() \
	if (overrun_handler_function == NULL) { \
		ercd = E_OBJ; \
		goto exit; \
	}

extern FP overrun_handler_function;

/*
 *  オーバーランハンドラ機能の初期化
 */
#ifdef __ovrini

void
overrun_initialize(void)
{
	/* TODO: 未実装　*/
}

#endif /* __ovrini */

/*
 *  オーバランハンドラの定義
 */
#ifdef __def_ovr

SYSCALL ER
def_ovr(const T_DOVR *pk_dovr)
{
	ER	ercd;
    
	LOG_DEF_OVR_ENTER();
	CHECK_TSKCTX_UNL();

	if (pk_dovr == NULL) {
		int i;

		t_lock_cpu();

		/*
		 *  全てのタスク上限時間を解除する
		 */
		for (i = 0; i < tmax_tskid; i++) {
			tcb_table[i].staovr = FALSE;
		}
		/*
		 *  オーバランハンドラタイマを停止する。
		 */
		hw_overrun_timer_stop();

		overrun_handler_function = NULL;

		t_unlock_cpu();
	} else {
		
		CHECK_ATTRIBUTE(pk_dovr->ovratr, TA_HLNG | TA_ASM);
		
		/*
		 *  ロックは不要と思うが、コンパイラが
		 *  どんなコードを生成するか判らないので念のため。
		 */
		t_lock_cpu();
		overrun_handler_function = pk_dovr->ovrhdr;
		t_unlock_cpu();

	}

	ercd = E_OK;
 exit:
	LOG_DEF_OVR_LEAVE(ercd);
	return(ercd);
}

#endif /* __def_ovr */

/*
 *  オーバランハンドラの動作開始
 */
#ifdef __sta_ovr

SYSCALL ER
sta_ovr(ID tskid, OVRTIM ovrtim)
{
	TCB	*tcb;
	ER	ercd;

	LOG_STA_OVR_ENTER(tskid);
	CHECK_TSKCTX_UNL();

	CHECK_OVERRUN_HADNLER_DEFINED();

	CHECK_TSKID_SELF(tskid);
	tcb = get_tcb_self(tskid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(tcb);

	tcb->staovr = TRUE;
	tcb->leftotm = ovrtim;

	if (tcb == runtsk) {
	    hw_overrun_timer_start();
	}

	t_unlock_cpu();

	ercd = E_OK;

    exit:
	LOG_STA_OVR_LEAVE(ercd);
	return(ercd);
}

#endif /* __sta_ovr */

/*
 *  オーバランハンドラの動作停止
 */
#ifdef __stp_ovr

SYSCALL ER
stp_ovr(ID tskid)
{
	TCB	*tcb;
	ER	ercd;

	LOG_STP_OVR_ENTER(tskid);
	CHECK_TSKCTX_UNL();

	CHECK_OVERRUN_HADNLER_DEFINED();

	CHECK_TSKID_SELF(tskid);
	tcb = get_tcb_self(tskid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(tcb);

	tcb->staovr = FALSE;

	if (tcb == runtsk) {
	    hw_overrun_timer_stop();
	}

	t_unlock_cpu();

	ercd = E_OK;
    exit:
	LOG_STP_OVR_LEAVE(ercd);
	return(ercd);
}

#endif /* __stp_ovr */

/*
 *  オーバランハンドラの状態参照
 */
#ifdef __ref_ovr
SYSCALL ER
ref_ovr(ID tskid, T_ROVR *pk_rovr)
{
	TCB	*tcb;
	ER	ercd;
    
	LOG_REF_OVR_ENTER(tskid);
	CHECK_TSKCTX_UNL();

	CHECK_OVERRUN_HADNLER_DEFINED();

	CHECK_TSKID_SELF(tskid);
	tcb = get_tcb_self(tskid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(tcb);

	if (tcb->staovr) {
		pk_rovr->ovrstat = TOVR_STA;
	} else {
		pk_rovr->ovrstat = TOVR_STP;
	}

	pk_rovr->leftotm = tcb->leftotm;

	t_unlock_cpu();

	ercd = E_OK;

    exit:
	LOG_REF_OVR_LEAVE(ercd);
	return(ercd);
}

#endif /* __ref_ovr */

/*
 *  オーバランハンドラ呼出し
 *  オーバランハンドラ用タイマ割込みハンドラから
 *  呼び出される。呼出し元が非タスクコンテキスト
 *  であることに注意。
 */
#ifdef __ovrcal

extern FP overrun_handler_function;

void
call_ovrhdr()
{
	typedef void (*OVRHDR)(ID,VP_INT);

	/*
	 *  あり得ないはずだが、念のため。
	 *  assertマクロにしたほうがよいかもしれない。
	 */
	if (overrun_handler_function == NULL) {
		return;
	}
	
	/*
	 *  タスクの上限プロセッサ時間の設定を解除
	 */
	runtsk->staovr = FALSE;

	/*
	 *  オーバランハンドラの呼出し
	 */
	((OVRHDR)overrun_handler_function)(runtsk->tskid, runtsk->tinib->exinf);
}

#endif
