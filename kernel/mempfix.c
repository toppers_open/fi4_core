/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2005 by Embedded and Real-Time Systems Laboratory
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2003-2006 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/mempfix.c 6101 2006-09-10T08:00:54.989588Z monaka  $
 */

/*
 *	固定長メモリプール機能
 */

#include "fi4_kernel.h"
#include "check.h"
#include "task.h"
#include "wait.h"
#include "mempfix.h"
#include "bufmgr.h"

/*
 *  固定長メモリプールIDの最大値（kernel_cfg.c）
 */
extern const ID	tmax_mpfid;

/*
 *  固定長メモリプール初期化ブロックのエリア（kernel_cfg.c）
 */
extern MPFINIB	mpfinib_table[];

/*
 *  固定長メモリプール初期化ブロック連想ペアの数（kernel_cfg.c）
 */
extern const UINT	tnum_mpfinia;

/*
 *  固定長メモリプール初期化ブロック連想ペアのエリア（kernel_cfg.c）
 */
extern const MPFINIA	mpfinia_table[];

/*
 *  固定長メモリプール予約ID情報のエリア（kernel_cfg.c）
 */
extern const ID		mpfrid_table[];

/*
 *  固定長メモリプール予約ID情報の数 (kernel_cfg.c)
 */
extern const UINT	tnum_mpfrid;

/*
 *  固定長メモリプール管理ブロックのエリア（kernel_cfg.c）
 */
extern MPFCB	mpfcb_table[];

/*
 *  固定長メモリプールの数
 */
#define TNUM_MPF	((UINT)(tmax_mpfid - TMIN_MPFID + 1))

/*
 *  固定長メモリプールIDから固定長メモリプール管理ブロックを取り出すた
 *  めのマクロ
 */
#define INDEX_MPF(mpfid)	((UINT)((mpfid) - TMIN_MPFID))
#define get_mpfcb(mpfid)	(&(mpfcb_table[INDEX_MPF(mpfid)]))

/*
 *  ID自動割当て用フリーリスト
 */
extern QUEUE mpf_freelist;

/*
 *  固定長メモリプール待ち情報ブロックの定義
 */
typedef struct fixed_memorypool_waiting_information {
	WINFO	winfo;		/* 標準の待ち情報ブロック */
	WOBJCB	*wobjcb;	/* 待ちオブジェクトの管理ブロック */
	VP	blk;		/* 獲得したメモリブロック */
} WINFO_MPF;

/* 
 *  固定長メモリプール機能の初期化
 */
#ifdef __mpfini

QUEUE mpf_freelist;

void
mempfix_initialize(void)
{
	UINT	i;
	MPFCB	*mpfcb;
	MPFINIB *mpfinib;
	const MPFINIA *mpfinia;
	const ID	*mpfrid;

	/*
	 * カーネル起動時の状態に関わらず初期化しなければいけないものを
	 * まず初期化する。
	 */
	for (mpfcb = mpfcb_table, i = 0; i < TNUM_MPF; mpfcb++, i++) {
		queue_initialize(&(mpfcb->wait_queue));
		mpfcb->mpfinib = NULL;
	}
	queue_initialize(&mpf_freelist);

	/*
	 * mpfinia によって示されるものをまず初期化
	 */
	for (mpfinia = mpfinia_table, i = 0; i < tnum_mpfinia; mpfinia++, i++) {
		mpfinib = &mpfinib_table[INDEX_MPF(mpfinia->mpfid)];
		*mpfinib = mpfinia->mpfinib;
		mpfcb = &mpfcb_table[INDEX_MPF(mpfinia->mpfid)];
		mpfcb->mpfinib = mpfinib;
		mpfcb->exist = KOBJ_EXIST;
		mpfcb->reserved = TRUE;

		/*
		 *  非標準のcfgを使ったとき、mpfがNULLになる可能性がある。<BTS:21>
		 *  そのようなcfgでは検証項目が増える。標準のcfgではこのコードに
		 *  入ることはない。
		 */
		if (mpfinib->mpf == NULL) {
			BOOL stat;
			mpfinib->limit = (VP)((SIZE)mpfinib->mpf + mpfinib->blksz);
			bufmgr_allocate(&stat, mpfinib->blksz, &(mpfinib->mpf));
			assert(stat);
		}

		mpfcb->unused = mpfinib->mpf;
		mpfcb->freelist = NULL;
	}

	/*
	 *  予約IDの設定
	 */
	for (mpfrid = mpfrid_table, i = 0; i < tnum_mpfrid; mpfrid++, i++) {
		mpfcb_table[INDEX_MPF(*mpfrid)].reserved = TRUE;
	}

	/*
	 * mpfinia によって示されないものを初期化
	 */
	for (mpfcb = mpfcb_table, i = 0; i < TNUM_MPF; mpfcb++, i++) {
		if (mpfcb->mpfinib == NULL) {
			mpfcb->exist = KOBJ_NOEXS;
			mpfcb->mpfinib = &mpfinib_table[i];

			if (!CHECK_OBJECT_RESERVED(mpfcb)) {
				queue_insert_prev(&mpf_freelist, (QUEUE*)mpfcb);
			}
		}
	}
}

#endif /* __mpfini */

/*
 *  固定長メモリプールからブロックを獲得
 */
#ifdef __mpfget

BOOL
mempfix_get_block(MPFCB *mpfcb, VP *p_blk)
{
	FREEL	*free;

	if (mpfcb->freelist != NULL) {
		free = mpfcb->freelist;
		mpfcb->freelist = free->next;
		*p_blk = (VP) free;
		return(TRUE);
	}
	else if (mpfcb->unused < mpfcb->mpfinib->limit) {
		*p_blk = mpfcb->unused;
		mpfcb->unused = (VP)((char *)(mpfcb->unused)
						+ mpfcb->mpfinib->blksz);
		return(TRUE);
	}
	return(FALSE);
}

#endif /* __mpfget */

/*
 *  固定長メモリプールの空きメモリブロック数を数える。
 */
#ifdef __mpfcnt

UINT
mempfix_count_free(MPFCB *mpfcb)
{
	FREEL	*free;
	UINT count = 0;
	free = mpfcb->freelist;
	while(free != NULL) {
		free = free->next;
		++count;
	}

	return count;
}

#endif /* __mpfcnt */

/*
 *  固定長メモリプールコントロールブロックの初期化
 */
#ifdef __mpfcbini

ER
init_mpfcb(MPFCB *mpfcb, T_CMPF *pk_cmpf)
{
	MPFINIB *mpfinib;
	VP mpf;
	UINT blksz;
	UINT blkcnt;

	mpfinib = mpfcb->mpfinib;
	mpf = pk_cmpf->mpf;
	blksz = TROUND_VP(pk_cmpf->blksz);
	blkcnt = pk_cmpf->blkcnt;

	if (mpf == NULL) {
		BOOL stat;
		bufmgr_allocate(&stat, blksz * blkcnt, &mpf);
		if (!stat) {
			return E_NOMEM;
		}
	}
	/*
	 * 初期設定を行う。
	 */
	queue_initialize((QUEUE *)mpfcb);
	mpfinib = mpfcb->mpfinib;
	mpfinib->mpfatr = pk_cmpf->mpfatr;
	mpfinib->blksz = blksz;
	mpfinib->mpf = mpf;
	mpfinib->limit = (VP)((SIZE)mpf + blksz * blkcnt);

	mpfcb->unused = mpf;
	mpfcb->freelist = NULL;

	return E_OK;
}

#endif /* __mpfcbini */

/*
 *  固定長メモリプールの生成
 */
#ifdef __cre_mpf

SYSCALL ER
cre_mpf(ID mpfid, const T_CMPF *pk_cmpf)
{	
	MPFCB	*mpfcb;
	ER	ercd;
    
	LOG_CRE_MPF_ENTER(mpfid, pk_cmpf);
	CHECK_TSKCTX_UNL();

	CHECK_MPFID(mpfid);
	CHECK_ATTRIBUTE(pk_cmpf->mpfatr, TA_TFIFO | TA_TPRI);
	CHECK_NOT_ZERO(pk_cmpf->blkcnt);
	CHECK_NOT_ZERO(pk_cmpf->blksz);

	mpfcb = get_mpfcb(mpfid);
	CHECK_OBJECT_CREATABLE(mpfcb);
	UPDATE_OBJECT_FREELIST(mpfcb);

	ercd = init_mpfcb(mpfcb, pk_cmpf);

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	mpfcb->exist = KOBJ_EXIST;

    exit:
	LOG_CRE_MPF_LEAVE(ercd);
	return(ercd);
}

#endif

/*
 *  固定長メモリプールの生成(ID番号自動割付け）
 */
#ifdef __acre_mpf

SYSCALL ER_ID
acre_mpf(const T_CMPF *pk_cmpf)
{
	static MPFCB	*mpfcb = NULL;
	ER_ID	ercd;

	LOG_ACRE_MPF_ENTER(pk_cmpf);
	CHECK_TSKCTX_UNL();

	if (TNUM_MPF == 0) {
		ercd = E_NOID;
		goto exit;
	}
	CHECK_ATTRIBUTE(pk_cmpf->mpfatr, TA_TFIFO | TA_TPRI);
	CHECK_NOT_ZERO(pk_cmpf->blkcnt);
	CHECK_NOT_ZERO(pk_cmpf->blksz);

	t_lock_cpu();
	mpfcb = (MPFCB *)t_get_free_id((QUEUE *)&mpf_freelist);
	if (mpfcb == NULL) {
		ercd = E_NOID;
		t_unlock_cpu();
		goto exit;
	}
	mpfcb->exist = KOBJ_INIT;
	t_unlock_cpu();

	ercd = init_mpfcb(mpfcb, pk_cmpf);
	if (ercd == E_OK) {
		ercd = MPFID(mpfcb);
	}

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	mpfcb->exist = KOBJ_EXIST;

    exit:
	LOG_ACRE_MPF_LEAVE(ercd);
	return(ercd);
}

#endif

/*
 *  固定長メモリプールの削除
 */
#ifdef __del_mpf
SYSCALL ER
del_mpf(ID mpfid)
{
	MPFCB	*mpfcb;
	ER	ercd;
	BOOL	stat;

	LOG_DEL_MPF_ENTER(mpfid);
	CHECK_TSKCTX_UNL();

	CHECK_MPFID(mpfid);
	mpfcb = get_mpfcb(mpfid);

	CHECK_OBJECT_DELETABLE(mpfcb);

	/* 待ち行列の後片付け。削除に伴う待ち解除を行う。*/
	while (queue_empty(&(mpfcb->wait_queue)) == FALSE) {
		TCB *tcb;
		tcb = (TCB *)queue_delete_next(&(mpfcb->wait_queue));
		t_lock_cpu();
		if (wait_deleted(tcb)) {
			dispatch();
		}
		t_unlock_cpu();
	}
	  
	/* メモリプール領域を解放する。*/
	stat = bufmgr_release(mpfcb->mpfinib->mpf);
	if (stat) {
		ercd = E_OK;
	} else {
		ercd = E_SYS;
	}

	if (!CHECK_OBJECT_RESERVED(mpfcb)) {
	  /*
	   * 予約IDでなければフリーリストに入れる。
	   */
		t_lock_cpu();
		queue_insert_prev(&mpf_freelist, (QUEUE *)mpfcb);
		t_unlock_cpu();
	}

	/* ここから先はacre_/cre_でのアクセスが可能 */
	mpfcb->exist = KOBJ_NOEXS;

    exit:
	LOG_DEL_MPF_LEAVE(ercd);
	return(ercd);
}

#endif /* __del_mpf */

/*
 *  固定長メモリブロックの獲得
 */
#ifdef __get_mpf

SYSCALL ER
get_mpf(ID mpfid, VP *p_blk)
{
	MPFCB	*mpfcb;
	WINFO_MPF winfo;
	ER	ercd;

	LOG_GET_MPF_ENTER(mpfid, p_blk);
	CHECK_DISPATCH();

	CHECK_MPFID(mpfid);
	mpfcb = get_mpfcb(mpfid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mpfcb);

	if (mempfix_get_block(mpfcb, p_blk)) {
		ercd = E_OK;
	}
	else {
		wobj_make_wait((WOBJCB *) mpfcb, (WINFO_WOBJ *) &winfo);
		dispatch();
		ercd = winfo.winfo.wercd;
		if (ercd == E_OK) {
			*p_blk = winfo.blk;
		}
	}

	t_unlock_cpu();

    exit:
	LOG_GET_MPF_LEAVE(ercd, *p_blk);
	return(ercd);
}

#endif /* __get_mpf */

/*
 *  固定長メモリブロックの獲得（ポーリング）
 */
#ifdef __pget_mpf

SYSCALL ER
pget_mpf(ID mpfid, VP *p_blk)
{
	MPFCB	*mpfcb;
	ER	ercd;

	LOG_PGET_MPF_ENTER(mpfid, p_blk);
	CHECK_TSKCTX_UNL();

	CHECK_MPFID(mpfid);
	mpfcb = get_mpfcb(mpfid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mpfcb);

	if (mempfix_get_block(mpfcb, p_blk)) {
		ercd = E_OK;
	}
	else {
		ercd = E_TMOUT;
	}

	t_unlock_cpu();

    exit:
	LOG_PGET_MPF_LEAVE(ercd, *p_blk);
	return(ercd);
}

#endif /* __pget_mpf */

/*
 *  固定長メモリブロックの獲得（タイムアウトあり）
 */
#ifdef __tget_mpf

SYSCALL ER
tget_mpf(ID mpfid, VP *p_blk, TMO tmout)
{
	MPFCB	*mpfcb;
	WINFO_MPF winfo;
	TMEVTB	tmevtb;
	ER	ercd;

	LOG_TGET_MPF_ENTER(mpfid, p_blk, tmout);
	CHECK_DISPATCH();
	CHECK_TMOUT(tmout);

	CHECK_MPFID(mpfid);
	mpfcb = get_mpfcb(mpfid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mpfcb);

	if (mempfix_get_block(mpfcb, p_blk)) {
		ercd = E_OK;
	}
	else if (tmout == TMO_POL) {
		ercd = E_TMOUT;
	}
	else {
		wobj_make_wait_tmout((WOBJCB *) mpfcb, (WINFO_WOBJ *) &winfo,
						&tmevtb, tmout);
		dispatch();
		ercd = winfo.winfo.wercd;
		if (ercd == E_OK) {
			*p_blk = winfo.blk;
		}
	}

	t_unlock_cpu();

    exit:
	LOG_TGET_MPF_LEAVE(ercd, *p_blk);
	return(ercd);
}

#endif /* __tget_mpf */

/*
 *  固定長メモリブロックの返却
 */
#ifdef __rel_mpf

SYSCALL ER
rel_mpf(ID mpfid, VP blk)
{
	MPFCB	*mpfcb;
	TCB	*tcb;
	FREEL	*free;
	ER	ercd;
    
	LOG_REL_MPF_ENTER(mpfid, blk);
	CHECK_TSKCTX_UNL();

	CHECK_MPFID(mpfid);
	mpfcb = get_mpfcb(mpfid);

	CHECK_PAR(mpfcb->mpfinib->mpf <= blk
			&& blk < mpfcb->mpfinib->limit
			&& ((char *)(blk) - (char *)(mpfcb->mpfinib->mpf))
					% mpfcb->mpfinib->blksz == 0);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mpfcb);

	if (!(queue_empty(&(mpfcb->wait_queue)))) {
		tcb = (TCB *) queue_delete_next(&(mpfcb->wait_queue));
		((WINFO_MPF *)(tcb->winfo))->blk = blk;
		if (wait_complete(tcb)) {
			dispatch();
		}
		ercd = E_OK;
	}
	else {
		free = (FREEL *) blk;
		free->next = mpfcb->freelist;
		mpfcb->freelist = free;
		ercd = E_OK;
	}

	t_unlock_cpu();

    exit:
	LOG_REL_MPF_LEAVE(ercd);
	return(ercd);
}

#endif /* __rel_mpf */


/*
 *  固定長メモリブロックの状態参照
 */
#ifdef __ref_mpf

SYSCALL ER
ref_mpf(ID mpfid, T_RMPF *pk_rmpf)
{
	MPFCB	*mpfcb;
	ER	ercd;
    
	LOG_REF_MPF_ENTER(mpfid);
	CHECK_TSKCTX_UNL();

	CHECK_MPFID(mpfid);
	mpfcb = get_mpfcb(mpfid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mpfcb);

	if (!(queue_empty(&(mpfcb->wait_queue)))) {
		TCB	*tcb;
		tcb = (TCB *)queue_peek_next(&(mpfcb->wait_queue));
		pk_rmpf->wtskid = TSKID(tcb);
	}
	else {
		pk_rmpf->wtskid = TSK_NONE;
	}
	pk_rmpf->fblkcnt = mempfix_count_free(mpfcb) +
	  ((SIZE)(mpfcb->mpfinib->limit) - (SIZE)(mpfcb->unused)) /
	  mpfcb->mpfinib->blksz;

	ercd = E_OK;

	t_unlock_cpu();

    exit:
	LOG_REF_MPF_LEAVE(ercd);
	return(ercd);
}

#endif /* __ref_mpf */


