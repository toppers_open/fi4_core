/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2000-2003 by Masaki Muranaka
 *  Copyright (C) 2003-2004 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/rendezvous.c 6101 2006-09-10T08:00:54.989588Z monaka  $
 */

/*
 *	ランデブ機能
 */

#include "fi4_kernel.h"
#include "check.h"
#include "task.h"
#include "wait.h"
#include "rendezvous.h"

/*
 *  ランデブポートIDの最大値（kernel_cfg.c）
 */
extern const ID	tmax_porid;

/*
 *  ランデブポート初期化ブロックのエリア（kernel_cfg.c）
 */
extern PORINIB	porinib_table[];

/*
 *  ランデブポート初期化ブロック連想ペアの数（kernel_cfg.c）
 */
extern const UINT	tnum_porinia;

/*
 *  ランデブポート初期化ブロック連想ペアのエリア（kernel_cfg.c）
 */
extern const PORINIA	porinia_table[];

/*
 *  ランデブポート予約ID情報のエリア（kernel_cfg.c）
 */
extern const ID		porrid_table[];

/*
 *  ランデブポート予約ID情報の数 (kernel_cfg.c)
 */
extern const UINT	tnum_porrid;

/*
 *  ランデブポート管理ブロックのエリア（kernel_cfg.c）
 */
extern PORCB	porcb_table[];

/*
 *  ID自動割当て用フリーリスト
 */
extern QUEUE sem_freelist;

/*
 *  ランデブポートIDからランデブポート管理ブロックを取り出すためのマクロ
 */
#define INDEX_POR(porid)	((UINT)((porid) - TMIN_PORID))
#define get_porcb(porid)	(&(porcb_table[INDEX_POR(porid)]))

/*
 *  ランデブポートの数
 */
#define TNUM_POR	((UINT)(tmax_porid - TMIN_PORID + 1))

#define CHECK_RDVPTN(x) { if ((x) == 0) { return E_PAR; } }

#define TNUM_RDVNO_HASH (10)

extern RDVCB	rdvcb_table[TNUM_RDVNO_HASH];
extern RDVINIB	rdvinib_table[TNUM_RDVNO_HASH];

extern UINT rdvidx;

/*
 *  ランデブ待ち情報ブロックの定義
 */
typedef struct rdvport_waiting_information {
	WINFO	winfo;		/* 標準の待ち情報ブロック */
	WOBJCB *wobjcb;	/* 待ちオブジェクトのコントロールブロック */
	RDVPTN ptn;		/* 呼出/受付のパターン */
	RDVNO rdvno;
	VP *msg;
	UINT msgsz;
	UINT maxrmsz;
	TCB *caller_tcb;
} WINFO_POR;

extern QUEUE por_freelist;

/* 
 *  ランデブポート機能の初期化
 */
#ifdef __inipor

UINT rdvidx = 0;
QUEUE por_freelist;

/*
 *  rdvcb_table のindexはrdvnoそのものではなく
 *  rdvnoのハッシュ値であることに注意。
 */
RDVCB rdvcb_table[TNUM_RDVNO_HASH];
RDVINIB rdvinib_table[TNUM_RDVNO_HASH];

void
rendezvous_initialize(void)
{
	UINT	i;
	PORCB	*porcb;
	PORINIB *porinib;
	const PORINIA	*porinia;
	const ID	*porrid;

	/*
	 * カーネル起動時の状態に関わらず初期化しなければいけないものを
	 * まず初期化する。
	 */
	for (porcb = porcb_table, i = 0; i < tmax_porid; porcb++, i++) {
		queue_initialize(&(porcb->wait_queue));
		porcb->porinib = NULL;
	}
	queue_initialize(&por_freelist);

	/*
	 *  porinia によって示されるものをまず初期化
	 */
	for (porinia = porinia_table, i = 0; i < tnum_porinia; porinia++, i++) {
		UINT idx = INDEX_POR(porinia->porid);
		queue_initialize(&(porcb_table[idx].wait_queue));
		queue_initialize(&(porcb_table[idx].await_queue));
		porinib = &porinib_table[idx];
		*porinib = porinia->porinib;
		porcb = &porcb_table[idx];
		porcb->porinib = porinib;
		porcb->exist = KOBJ_EXIST;
		porcb->reserved = TRUE;
	}

	/*
	 *  予約IDの設定
	 */
	for (porrid = porrid_table, i = 0; i < tnum_porrid; porrid++, i++) {
		porcb_table[INDEX_POR(*porrid)].reserved = TRUE;
	}

	/*
	 *  porinia によって示されないものを初期化
	 */
	for (porcb = porcb_table, i = 0; i < TNUM_POR; porcb++, i++) {
		if (porcb->porinib == NULL) {
			porcb->exist = KOBJ_NOEXS;
			porcb->porinib = &porinib_table[i];

			if (!CHECK_OBJECT_RESERVED(porcb)) {
				queue_insert_prev(&por_freelist, (QUEUE*)porcb);
			}
		}
	}

	for(i = 0; i < TNUM_RDVNO_HASH; i++) {
		queue_initialize(&(rdvcb_table[i].wait_queue));
		rdvcb_table[i].rdvinib = &rdvinib_table[i];
		rdvinib_table[i].atr = TA_TFIFO;
	}
}

#endif /* __inipor */

/*
 *  ランデブポートの生成
 */
#ifdef __cre_por

SYSCALL ER
cre_por(ID porid, const T_CPOR * pk_cpor)
{
	PORCB	*porcb;
	PORINIB *porinib;
	ER	ercd;
    
	LOG_CRE_POR_ENTER(porid, pk_cpor);
	CHECK_TSKCTX_UNL();

	CHECK_PORID(porid);
	CHECK_ATTRIBUTE(pk_cpor->poratr, TA_TFIFO | TA_TPRI);
	porcb = get_porcb(porid);
	CHECK_OBJECT_CREATABLE(porcb);
	UPDATE_OBJECT_FREELIST(porcb);

	/*
	 * 初期設定を行う。
	 */
	queue_initialize(&(porcb->wait_queue));
	queue_initialize(&(porcb->await_queue));
	porinib = porcb->porinib;
	porinib->poratr = pk_cpor->poratr;
	porinib->maxcmsz = pk_cpor->maxcmsz;
	porinib->maxrmsz = pk_cpor->maxrmsz;

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	porcb->exist = KOBJ_EXIST;

	ercd = E_OK;

    exit:
	LOG_CRE_POR_LEAVE(ercd);
	return(ercd);
}

#endif /* __cre_por */

/*
 *  ランデブポートの生成(ID番号自動割付け)
 */
#ifdef __acre_por

SYSCALL ER_ID
acre_por(const T_CPOR *pk_cpor)
{
	PORCB	*porcb;
	PORINIB *porinib;
	ER_ID	ercd;
    
	LOG_ACRE_POR_ENTER(pk_cpor);
	CHECK_TSKCTX_UNL();

	if (TNUM_POR == 0) {
		ercd = E_NOID;
		goto exit;
	}
	CHECK_ATTRIBUTE(pk_cpor->poratr, TA_TFIFO | TA_TPRI);
	
	t_lock_cpu();
	porcb = (PORCB *)t_get_free_id((QUEUE *)&por_freelist);
	if (porcb == NULL) {
		ercd = E_NOID;
		t_unlock_cpu();
		goto exit;
	}
	porcb->exist = KOBJ_INIT;
	t_unlock_cpu();

	/*
	 *  初期設定を行う。
	 */
	queue_initialize(&(porcb->wait_queue));
	queue_initialize(&(porcb->await_queue));
	porinib = porcb->porinib;
	porinib->poratr = pk_cpor->poratr;
	porinib->maxcmsz = pk_cpor->maxcmsz;
	porinib->maxrmsz = pk_cpor->maxrmsz;

	ercd = PORID(porcb);

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	porcb->exist = KOBJ_EXIST;

    exit:
	LOG_ACRE_POR_LEAVE(ercd);
	return(ercd);
}

#endif /* __acre_por */


/*
 *  ランデブポートの削除
 */
#ifdef __del_por
SYSCALL ER
del_por(ID porid)
{
	PORCB	*porcb;
	ER	ercd;
    
	LOG_DEL_POR_ENTER(porid);
	CHECK_TSKCTX_UNL();

	CHECK_PORID(porid);
	porcb = get_porcb(porid);

	CHECK_OBJECT_DELETABLE(porcb);

	/*
	 *  待ち行列の後片付け。削除に伴う待ち解除を行う。
	 */
	while (queue_empty(&(porcb->wait_queue)) == FALSE) {
		TCB *tcb;
		tcb = (TCB *)queue_delete_next(&(porcb->wait_queue));
		t_lock_cpu();
		if (wait_deleted(tcb)) {
			dispatch();
		}
		t_unlock_cpu();
	}

	/*
	 *  待ち行列の後片付け。削除に伴う待ち解除を行う。
	 */
	while (queue_empty(&(porcb->await_queue)) == FALSE) {
		TCB *tcb;
		tcb = (TCB *)queue_delete_next(&(porcb->await_queue));
		t_lock_cpu();
		if (wait_deleted(tcb)) {
			dispatch();
		}
		t_unlock_cpu();
	}

	if (!CHECK_OBJECT_RESERVED(porcb)) {
		/*
		 *  予約IDでなければフリーリストに入れる。
		 */
		t_lock_cpu();
		queue_insert_prev(&por_freelist, (QUEUE *)porcb);
		t_unlock_cpu();
	}

	/*
	 *  ここから先はacre_/cre_でのアクセスが可能
	 */
	porcb->exist = KOBJ_NOEXS;

	ercd = E_OK;

    exit:
	LOG_DEL_POR_LEAVE(ercd);
	return(ercd);
}

#endif /* __del_por */

/*
 *  ランデブの呼出し
 *
 *  poridで指定されるランデプポートに対して、
 *  calptnで指定されるランデブ条件により、ランデブを呼び出す。
 */
#ifdef __cal_por

SYSCALL ER_UINT
cal_por(ID porid, RDVPTN calptn, VP msg, UINT cmsgsz)
{
	PORCB	*porcb = NULL;
	ER	ercd;
	QUEUE	*queue = NULL;
	RDVNO	rdvno;
	TCB *tcb;

	CHECK_DISPATCH();
	CHECK_RDVPTN(calptn);

	CHECK_PORID(porid);
	porcb = get_porcb(porid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(porcb);

	if (cmsgsz > porcb->porinib->maxcmsz) {
		/*
		 *  cmsgsz が、対象ランデブポートの呼出しメッセージの
		 *  最大サイズよりも大きい場合には、E_PARのエラーを返す。
		 */
		ercd = E_PAR;
		goto error;
	}

	rdvno = ((TSKID(runtsk) & 0xffff) << 16) | (++rdvidx & 0xffff);

	/*
	 *  ランデブ受付待ち行列を走査し、ビットパターンが一致したら
	 *  ランデブ受付待ちを解除する。
	 */
	while ((queue = queue_enumerate(&(porcb->await_queue), queue)) != NULL) {
		WINFO_POR *pk_winfo;
		tcb = (TCB *)queue;
		pk_winfo = (WINFO_POR *)(tcb->winfo);
		if ((pk_winfo->ptn & calptn)) {
			memcpy(pk_winfo->msg, msg, cmsgsz);
			pk_winfo->msgsz = cmsgsz;
			pk_winfo->rdvno = rdvno;
			queue_delete(queue);
			break;
		}
	}

	/*
	 *  ランデブ受付待ちが解除されていたら(即ち、上の処理でビットパターンが
	 *  一致していたら)、自タスクをランデブ終了待ちにする。
	 *  そうでなければ、自タスクをランデブ呼出し待ち状態にする。
	 */
	if (queue != NULL) {
		WINFO_POR winfo;
		RDVCB *rdvcb;
		winfo.msg = msg;
		winfo.msgsz = cmsgsz;
		winfo.rdvno = rdvno;
		winfo.maxrmsz = porcb->porinib->maxrmsz;
		rdvcb = &rdvcb_table[winfo.rdvno % TNUM_RDVNO_HASH];
		wobj_make_wait((WOBJCB *)rdvcb, (WINFO_WOBJ *)&winfo);
		dispatch();
		if (winfo.winfo.wercd == E_OK) {
			ercd = winfo.msgsz;
		} else {
			ercd = winfo.winfo.wercd;
		}
	} else {
		WINFO_POR winfo;
		winfo.ptn = calptn;
		winfo.msg = msg;
		winfo.msgsz = cmsgsz;
		winfo.rdvno = rdvno;
		winfo.caller_tcb = runtsk;
		winfo.maxrmsz = porcb->porinib->maxrmsz;
		wobj_make_wait((WOBJCB *) porcb, (WINFO_WOBJ *)&winfo);
		dispatch();
		if (winfo.winfo.wercd == E_OK) {
			ercd = winfo.msgsz;
		} else {
			ercd = winfo.winfo.wercd;
		}
	}

    error:
	t_unlock_cpu();
    exit:
	return ercd;
}

#endif /* __cal_por */

/*
 *  ランデブの呼出し
 *
 * poridで指定されるランデプポートに対して、
 * calptnで指定されるランデブ条件により、ランデブを呼び出す。
 */
#ifdef __tcal_por

SYSCALL ER_UINT
tcal_por(ID porid, RDVPTN calptn, VP msg, UINT cmsgsz, TMO tmout)
{
	PORCB	*porcb = NULL;
	ER	ercd;
	TCB *tcb;
	QUEUE	*queue = NULL;
	RDVNO	rdvno;
	TMEVTB	tmevtb;

	CHECK_DISPATCH();
	CHECK_RDVPTN(calptn);
	CHECK_TMOUT(tmout);
	if (tmout == TMO_POL) {
		ercd = E_PAR;
		goto exit;
	}

	CHECK_PORID(porid);
	porcb = get_porcb(porid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(porcb);

	if (cmsgsz > porcb->porinib->maxcmsz) {
		/*
		 *  cmsgsz が、対象ランデブポートの呼出しメッセージの
		 *  最大サイズよりも大きい場合には、E_PARのエラーを返す。
		 */
		ercd = E_PAR;
		goto error;
	}

	rdvno = ((TSKID(runtsk) & 0xffff) << 16) | (++rdvidx & 0xffff);

	/*
	 *  ランデブ受付待ち行列を走査し、ビットパターンが一致したら
	 *  ランデブ受付待ちを解除する。
	 */
	while ((queue = queue_enumerate(&(porcb->await_queue), queue)) != NULL) {
		WINFO_POR *pk_winfo;
		tcb = (TCB *)queue;
		pk_winfo = (WINFO_POR *)(tcb->winfo);
		if ((pk_winfo->ptn & calptn)) {
			memcpy(pk_winfo->msg, msg, cmsgsz);
			pk_winfo->msgsz = cmsgsz;
			pk_winfo->rdvno = rdvno;
			queue_delete(queue);
			break;
		}
	}

	/*
	 *  ランデブ受付待ちが解除されていたら(即ち、上の処理でビットパターンが
	 *  一致していたら)、自タスクをランデブ終了待ちにする。
	 *  そうでなければ、自タスクをランデブ呼出し待ち状態にする。
	 */
	if (queue != NULL) {
		WINFO_POR winfo;
		RDVCB *rdvcb;
		winfo.msg = msg;
		winfo.msgsz = cmsgsz;
		winfo.rdvno = rdvno;
		winfo.maxrmsz = porcb->porinib->maxrmsz;
		rdvcb = &rdvcb_table[winfo.rdvno % TNUM_RDVNO_HASH];
		wobj_make_wait_tmout((WOBJCB *)rdvcb, (WINFO_WOBJ *)&winfo, &tmevtb, tmout);
		dispatch();
		if (winfo.winfo.wercd == E_OK) {
			ercd = winfo.msgsz;
		} else {
			ercd = winfo.winfo.wercd;
		}
	} else {
		WINFO_POR winfo;
		winfo.ptn = calptn;
		winfo.msg = msg;
		winfo.msgsz = cmsgsz;
		winfo.rdvno = rdvno;
		winfo.maxrmsz = porcb->porinib->maxrmsz;
		winfo.caller_tcb = runtsk;
		wobj_make_wait_tmout((WOBJCB *) porcb, (WINFO_WOBJ *)&winfo, &tmevtb, tmout);
		dispatch();
		if (winfo.winfo.wercd == E_OK) {
			ercd = winfo.msgsz;
		} else {
			ercd = winfo.winfo.wercd;
		}
	}

    error:
	t_unlock_cpu();
    exit:
	return ercd;
}

#endif /* __tcal_por */

/*
 *  ランデブの受付
 *
 * porid で指定されるランデブポートに対して、
 * acpptnで指定されるランデブ条件により、ランデブを受け付ける。
 */
#ifdef __acp_por

SYSCALL ER
acp_por(ID porid, RDVPTN acpptn, RDVNO *p_rdvno, VP msg)
{
	PORCB	*porcb = NULL;
	WINFO_POR winfo;
	ER_UINT	ercd = E_SYS;
	QUEUE	*queue = NULL;

	CHECK_DISPATCH();
	
	CHECK_PORID(porid);
	CHECK_RDVPTN(acpptn);
	porcb = get_porcb(porid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(porcb);

	/*
	 *  呼出し待ち行列を走査し、ビットパターンが一致するならば、
	 *  待ちタスクをランデブ終了待ちに移行する。
	 */
	while ((queue = queue_enumerate(&(porcb->wait_queue), queue)) != NULL) {
		WINFO_POR *pk_winfo;
		TCB *tcb;

		tcb = (TCB *)queue;
		pk_winfo = (WINFO_POR *)(tcb->winfo);
		if ((pk_winfo->ptn & acpptn)) {
			memcpy(msg, pk_winfo->msg, pk_winfo->msgsz);
			ercd = pk_winfo->msgsz;
			*p_rdvno = pk_winfo->rdvno;

			queue_delete(queue);
			queue_insert_prev(&rdvcb_table[pk_winfo->rdvno % TNUM_RDVNO_HASH].wait_queue, queue);

			break;
		}
	}

	if (queue == NULL) {
		/*
		 *  受付側条件に一致する呼出し待ちが存在しない時は
		 *  ランデブ受付待ち状態にする。
		 */
		winfo.ptn = acpptn;
		winfo.msg = msg;
		winfo.msgsz = porcb->porinib->maxcmsz;
		winfo.maxrmsz = porcb->porinib->maxrmsz;
		runtsk->tstat = (TS_WAITING | TS_WAIT_WOBJ);
		make_wait(&(winfo.winfo));
		queue_insert_prev(&(porcb->await_queue),
				  &(runtsk->task_queue));
		winfo.wobjcb = (WOBJCB *)porcb;
		LOG_TSKSTAT(runtsk);
		dispatch();
		ercd = winfo.winfo.wercd;
		if (MERCD(ercd) == E_OK)
		{
			ercd = winfo.msgsz;
			*p_rdvno = winfo.rdvno;
		}
	}

	t_unlock_cpu();
    exit:
	return ercd;
}

#endif /* __acp_por */

/*
 *  ランデブの受付 (ポーリング)
 *
 * porid で指定されるランデブポートに対して、
 * acpptnで指定されるランデブ条件により、ランデブを受け付ける。
 */
#ifdef __pacp_por

ER_UINT
pacp_por(ID porid, RDVPTN acpptn, RDVNO *p_rdvno, VP msg)
{
	PORCB	*porcb = NULL;
	ER_UINT	ercd = E_SYS;
	QUEUE	*queue = NULL;

	CHECK_TSKCTX_UNL();
	
	CHECK_PORID(porid);
	CHECK_RDVPTN(acpptn);
	porcb = get_porcb(porid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(porcb);

	/*
	 *  呼出し待ち行列を走査し、ビットパターンが一致するならば、
	 *  待ちタスクをランデブ終了待ちに移行する。
	 */
	while ((queue = queue_enumerate(&(porcb->wait_queue), queue)) != NULL) {
		WINFO_POR *pk_winfo;
		TCB *tcb;

		tcb = (TCB *)queue;
		pk_winfo = (WINFO_POR *)(tcb->winfo);
		if ((pk_winfo->ptn & acpptn)) {
			memcpy(msg, pk_winfo->msg, pk_winfo->msgsz);
			ercd = pk_winfo->msgsz;
			*p_rdvno = pk_winfo->rdvno;

			queue_delete(queue);
			queue_insert_prev(&rdvcb_table[pk_winfo->rdvno % TNUM_RDVNO_HASH].wait_queue, queue);

			break;
		}
	}

	/*
	 * 受付側条件に一致する呼出し待ちが存在しない時は
	 * タイムアウトエラーにする。
	 */
	if (queue == NULL) {
		ercd = E_TMOUT;
	}

	t_unlock_cpu();
    exit:
	return ercd;
}

#endif /* __pacp_por */

/*
 *  ランデブの受付 (タイムアウト)
 *
 * porid で指定されるランデブポートに対して、
 * acpptnで指定されるランデブ条件により、ランデブを受け付ける。
 */
#ifdef __tacp_por

ER_UINT
tacp_por(ID porid, RDVPTN acpptn, RDVNO *p_rdvno, VP msg, TMO tmout)
{
	PORCB	*porcb = NULL;
	WINFO_POR winfo;
	ER_UINT	ercd = E_SYS;
	QUEUE	*queue = NULL;

	CHECK_DISPATCH();
	CHECK_TMOUT(tmout);
	
	CHECK_PORID(porid);
	CHECK_RDVPTN(acpptn);
	porcb = get_porcb(porid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(porcb);

	/*
	 *  呼出し待ち行列を走査し、ビットパターンが一致するならば、
	 *  待ちタスクをランデブ終了待ちに移行する。
	 */
	while ((queue = queue_enumerate(&(porcb->wait_queue), queue)) != NULL) {
		WINFO_POR *pk_winfo;
		TCB *tcb;

		tcb = (TCB *)queue;
		pk_winfo = (WINFO_POR *)(tcb->winfo);
		if ((pk_winfo->ptn & acpptn)) {
			memcpy(msg, pk_winfo->msg, pk_winfo->msgsz);
			ercd = pk_winfo->msgsz;
			*p_rdvno = pk_winfo->rdvno;

			queue_delete(queue);
			queue_insert_prev(&rdvcb_table[pk_winfo->rdvno % TNUM_RDVNO_HASH].wait_queue, queue);

			break;
		}
	}

	/*
	 *  受付側条件に一致する呼出し待ちが存在しない
	 */
	if (queue == NULL) {
		/*
		 *  ポーリング指定のときは、
		 *  待ってはいけない。(E_TMOUT)
		 *  ポーリング指定以外のときは、
		 *  ランデブ受付待ち状態にする。
		 */
		if (tmout == TMO_POL) {
			ercd = E_TMOUT;
		} else {
			TMEVTB tmevtb;

			winfo.ptn = acpptn;
			winfo.msg = msg;
			winfo.msgsz = porcb->porinib->maxcmsz;
			runtsk->tstat = (TS_WAITING | TS_WAIT_WOBJ);
			make_wait_tmout(&(winfo.winfo), &tmevtb, tmout);
			queue_insert_prev(&(porcb->await_queue),
					  &(runtsk->task_queue));
			winfo.wobjcb = (WOBJCB *)porcb;
			LOG_TSKSTAT(runtsk);
			dispatch();
			ercd = winfo.winfo.wercd;
			if (MERCD(ercd) == E_OK)
			{
				ercd = winfo.msgsz;
				*p_rdvno = winfo.rdvno;
			}
		}
	}

	t_unlock_cpu();
    exit:
	return ercd;
}

#endif /* __tacp_por */

/*
 *  ランデブの終了
 */
#ifdef __rpl_rdv

SYSCALL ER
rpl_rdv(RDVNO rdvno, VP msg, UINT rmsgsz)
{
	ER	ercd = E_OBJ;
	QUEUE	*queue = NULL;
	RDVCB	*rdvcb;
	TCB	*tcb;
	WINFO_POR *pk_winfo;

	CHECK_TSKCTX_UNL();

	rdvcb = &rdvcb_table[rdvno % TNUM_RDVNO_HASH];

	t_lock_cpu();

	/*
	 *  ランデブ終了待ち行列を走査し、ランデブ番号が一致するならば、
	 *  待ち解除する。
	 */
	while ((queue = queue_enumerate(&(rdvcb->wait_queue), queue)) != NULL) {
		tcb = (TCB *)queue;
		pk_winfo = (WINFO_POR *)(tcb->winfo);
		
		if (pk_winfo->rdvno != rdvno) {
			continue;
		}

		if (pk_winfo->maxrmsz < rmsgsz) {
			ercd = E_PAR;
		} else {
			memcpy(pk_winfo->msg, msg, rmsgsz);
			pk_winfo->msgsz = rmsgsz;
			queue_delete(queue);
			if (wait_complete(tcb)) {
				dispatch();
			}
			ercd = E_OK;
		}
		break;
	}

	/*
	 *  該当するランデブ番号がないので、変数ercdは
	 *  初期設定値であるE_OBJとなる。
	 *  rdvnoに指定された値が、ランデブ番号として解釈できない
	 *  ケースは本バージョンでは存在しない。
	 */
	t_unlock_cpu();
    exit:
	return ercd;
}
#endif /* __rpl_rdv */

/*
 *  ランデブの回送
 */
#ifdef __fwd_por

SYSCALL ER
fwd_por(ID porid, RDVPTN calptn, RDVNO rdvno, VP msg, UINT cmsgsz)
{
	PORCB *porcb;
	RDVCB *rdvcb;
	QUEUE *queue = NULL;
	TCB	*tcb;
	TCB *await_tcb;
	WINFO_POR	*pk_winfo;

	ER	ercd = E_OBJ;

	CHECK_DISPATCH();

	CHECK_PORID(porid);
	CHECK_RDVPTN(calptn);
	porcb = get_porcb(porid);

	rdvcb = &rdvcb_table[rdvno % TNUM_RDVNO_HASH];

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(porcb);

	if (cmsgsz > porcb->porinib->maxcmsz) {
		/*
		 *  cmsgsz が、対象ランデブポートの呼出しメッセージの
		 *  最大サイズよりも大きい場合には、E_PARのエラーを返す。
		 */
		ercd = E_PAR;
		goto error;
	}

	/*
	 *  ランデブ終了待ちのタスクを、走査する。
	 */
	while ((queue = queue_enumerate(&(rdvcb->wait_queue), queue)) != NULL) {
		tcb = (TCB *)queue;
		pk_winfo = (WINFO_POR *)(tcb->winfo);
		
		if (rdvno == pk_winfo->rdvno) {
			QUEUE	*await_queue = NULL;

			/*
			 *  回送するランデブが成立したランデブポートの返送メッセージ
			 *  よりもcmsgszが大きければ、E_PARエラーを返す。
			 */
			if (pk_winfo->maxrmsz < cmsgsz) {
				ercd = E_PAR;
				break;
			}
			queue_delete(queue);

			/*
			 *  ランデブ受付待ち行列のビットパターンを走査し、
			 *  条件が合えば、そのタスクの待ちを解除する。
			 */
			while ((await_queue = queue_enumerate(&(porcb->await_queue), await_queue)) != NULL) {
				await_tcb = (TCB *)await_queue;
				pk_winfo = (WINFO_POR *)(await_tcb->winfo);
				if ((pk_winfo->ptn & calptn)) {
					/*
					 *  回送先のランデブポートの返答メッセージの最大サイズが
					 *  回送するランデブが成立したランデブポートの返答メッセージの
					 *  最大サイズより大きいとき、E_ILUSEを返す。
					 */
					if (pk_winfo->maxrmsz < porcb->porinib->maxrmsz) {
						ercd = E_ILUSE;
						goto error;
					}
					memcpy(pk_winfo->msg, msg, cmsgsz);
					pk_winfo->msgsz = cmsgsz;
					pk_winfo->rdvno = rdvno;
					queue_delete(await_queue);

					queue_insert_prev(&rdvcb_table[rdvno % TNUM_RDVNO_HASH].wait_queue, queue);

					break;
				}
			}

			/*
			 *  ランデブ受付待ち行列に該当するビットパターンがなければ、
			 *  poridが示すポートに対してランデブ呼出し待ちの状態にする。
			 */
			if (await_queue == NULL) {
				memcpy(pk_winfo->msg, msg, cmsgsz);
				pk_winfo->msgsz = cmsgsz;
				pk_winfo->ptn = calptn;
				wobj_move_wait(porcb, pk_winfo, tcb);
			} else {
				if (wait_complete(await_tcb)) {
					dispatch();
				}
			}

			ercd = E_OK;
			break;
		}
	}

	/*
	 *  該当するランデブ番号がないので、変数ercdは
	 *  初期設定値であるE_OBJとなる。
	 *  rdvnoに指定された値が、ランデブ番号として解釈できない
	 *  ケースは本バージョンでは存在しない。
	 */
    error:
	t_unlock_cpu();
    exit:
	return ercd;
}

#endif /* __fwd_por */

/*
 *  ランデブポートの状態参照
 */
#ifdef __ref_por

SYSCALL ER
ref_por(ID porid, T_RPOR *pk_rpor)
{
	ER ercd;
	TCB *tcb;
	PORCB *porcb;

	LOG_REF_POR_ENTER(porid);
	CHECK_TSKCTX_UNL();

	CHECK_PORID(porid);
	porcb = get_porcb(porid); 

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(porcb);

	if (!queue_empty(&(porcb->wait_queue))) {
		tcb = (TCB *)queue_peek_next(&(porcb->wait_queue));
		pk_rpor->ctskid = TSKID(tcb);
	} else {
		pk_rpor->ctskid = TSK_NONE;
	}

	if (!queue_empty(&(porcb->await_queue))) {
		tcb = (TCB *)queue_peek_next(&(porcb->await_queue));
		pk_rpor->atskid = TSKID(tcb);
	} else {
		pk_rpor->atskid = TSK_NONE;
	}

	ercd = E_OK;

	t_unlock_cpu();

    exit:
	LOG_REF_POR_LEAVE(ercd);

	return(ercd);
}

#endif

/*
 *  ランデブの状態参照
 */
#ifdef __ref_rdv

SYSCALL ER
ref_rdv(RDVNO rdvno, T_RRDV *pk_rrdv)
{
	ER ercd;
	RDVCB *rdvcb;
	QUEUE *queue = NULL;

	LOG_REF_RDV_ENTER(rdvno);

	rdvcb = &rdvcb_table[rdvno % TNUM_RDVNO_HASH];

	t_lock_cpu();

	/*
	 *  該当するランデブ番号が無くても、E_OBJにはならず、TSK_NONEになる。
	 */
	pk_rrdv->wtskid = TSK_NONE;
	while((queue = queue_enumerate(&(rdvcb->wait_queue), queue)) != NULL) {
		WINFO_POR *pk_winfo;
		TCB *tcb;
		tcb = (TCB*)queue;
		pk_winfo = (WINFO_POR *)(tcb->winfo);
		if (pk_winfo->rdvno == rdvno) {
			pk_rrdv->wtskid = TSKID(tcb);
			break;
		}
	}

	ercd = E_OK;

	t_unlock_cpu();

	LOG_REF_RDV_LEAVE(ercd);

	return(ercd);
}

#endif
