/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2003-2004 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/cyclic.c 6101 2006-09-10T08:00:54.989588Z monaka  $
 */

/*
 *	周期ハンドラ機能
 */

#include "fi4_kernel.h"
#include "check.h"
#include "cyclic.h"

/*
 *  周期ハンドラIDの最大値（kernel_cfg.c）
 */
extern const ID	tmax_cycid;

/*
 *  周期ハンドラ初期化ブロックのエリア（kernel_cfg.c）
 */
extern CYCINIB	cycinib_table[];

/*
 *  周期ハンドラ初期化ブロック連想ペアの数（kernel_cfg.c）
 */
extern const UINT	tnum_cycinia;

/*
 *  周期ハンドラ初期化ブロック連想ペアのエリア（kernel_cfg.c）
 */
extern const CYCINIA	cycinia_table[];

/*
 *  周期ハンドラ予約ID情報のエリア（kernel_cfg.c）
 */
extern const ID		cycrid_table[];

/*
 *  周期ハンドラ予約ID情報の数 (kernel_cfg.c)
 */
extern const UINT	tnum_cycrid;

/*
 *  周期ハンドラ管理ブロックのエリア（kernel_cfg.c）
 */
extern CYCCB	cyccb_table[];

/*
 *  周期ハンドラの数
 */
#define TNUM_CYC	((UINT)(tmax_cycid - TMIN_CYCID + 1))

/*
 *  周期ハンドラIDから周期ハンドラ管理ブロックを取り出すためのマクロ
 */
#define INDEX_CYC(cycid)	((UINT)((cycid) - TMIN_CYCID))
#define get_cyccb(cycid)	(&(cyccb_table[INDEX_CYC(cycid)]))

/*
 *  ID自動割当て用フリーリスト
 */
extern QUEUE cyc_freelist;

/*
 *  引数まで定義した周期ハンドラの型
 */
typedef void	(*CYCHDR)(VP_INT exinf);

/*
 *  周期ハンドラ機能の初期化
 */
#ifdef __cycini

QUEUE cyc_freelist;

void
cyclic_initialize(void)
{
	UINT	i;
	CYCCB	*cyccb;
	CYCINIB *cycinib;
	const CYCINIA *cycinia;
	const ID	*cycrid;

	/*
	 * カーネル起動時の状態に関わらず初期化しなければいけないものを
	 * まず初期化する。
	 */
	for (cyccb = cyccb_table, i = 0; i < TNUM_CYC; cyccb++, i++) {
		queue_initialize(&(cyccb->free_queue));
		cyccb->cycinib = NULL;
	}
	queue_initialize(&cyc_freelist);

	/*
	 * cycinia によって示されるものをまず初期化
	 */
	for (cycinia = cycinia_table, i = 0; i < tnum_cycinia; cycinia++, i++) {
		UINT idx;
		idx = INDEX_CYC(cycinia->cycid);
		cycinib = &cycinib_table[idx];
		*cycinib = cycinia->cycinib;
		cyccb = &cyccb_table[idx];
		cyccb->cycinib = cycinib;
		cyccb->exist = KOBJ_EXIST;
		cyccb->reserved = TRUE;

		if ((cyccb->cycinib->cycatr & TA_STA) != 0) {
			cyccb->cycsta = TRUE;
			tmevtb_enqueue_cyc(cyccb, cyccb->cycinib->cycphs);
		}
		else {
			if ((cyccb->cycinib->cycatr & TA_PHS) != 0) {
				tmevtb_enqueue_cyc(cyccb, cyccb->cycinib->cycphs);
			}
			cyccb->cycsta = FALSE;
		}
	}

	/*
	 *  予約IDの設定
	 */
	for (cycrid = cycrid_table, i = 0; i < tnum_cycrid; cycrid++, i++) {
		cyccb_table[INDEX_CYC(*cycrid)].reserved = TRUE;
	}

	/*
	 * cycinia によって示されないものを初期化
	 */
	for (cyccb = cyccb_table, i = 0; i < TNUM_CYC; cyccb++, i++) {
		if (cyccb->cycinib == NULL) {
			cyccb->exist = KOBJ_NOEXS;
			cyccb->cycinib = &cycinib_table[i];

			if (!CHECK_OBJECT_RESERVED(cyccb)) {
				queue_insert_prev(&cyc_freelist, (QUEUE*)cyccb);
			}
		}
	}
}

#endif /* __cycini */

/*
 *  周期ハンドラ起動のためのタイムイベントブロックの登録
 */
#ifdef __cycenq

void
tmevtb_enqueue_cyc(CYCCB *cyccb, EVTTIM evttim)
{
	tmevtb_enqueue_evttim(&(cyccb->tmevtb), evttim,
				(CBACK) call_cychdr, (VP) cyccb);
	cyccb->evttim = evttim;
}

#endif /* __cycenq */

/*
 *  周期ハンドラの生成
 */
#ifdef __cre_cyc

SYSCALL ER
cre_cyc(ID cycid, const T_CCYC * pk_ccyc)
{
	CYCCB	*cyccb;
	CYCINIB *cycinib;
	ER	ercd;
    
	LOG_CRE_CYC_ENTER(cycid, pk_ccyc);
	CHECK_TSKCTX_UNL();

	CHECK_CYCID(cycid);
	CHECK_ATTRIBUTE(pk_ccyc->cycatr, TA_HLNG | TA_ASM | TA_PHS | TA_STA);
	CHECK_NOT_ZERO(pk_ccyc->cyctim);

	cyccb = get_cyccb(cycid);
	CHECK_OBJECT_CREATABLE(cyccb);
	UPDATE_OBJECT_FREELIST(cyccb);

	/*
	 * 初期設定を行う。
	 */
	queue_initialize(&(cyccb->free_queue));
	cycinib = cyccb->cycinib;
	cycinib->cycatr = pk_ccyc->cycatr;
	cycinib->exinf  = pk_ccyc->exinf;
	cycinib->cychdr = pk_ccyc->cychdr;
	cycinib->cyctim = pk_ccyc->cyctim;
	cycinib->cycphs = pk_ccyc->cycphs;

	if ((cyccb->cycinib->cycatr & TA_STA) != 0) {
		cyccb->cycsta = TRUE;
		tmevtb_enqueue_cyc(cyccb, base_time + cyccb->cycinib->cycphs);
	}
	else {
		if ((cyccb->cycinib->cycatr & TA_PHS) != 0) {
			tmevtb_enqueue_cyc(cyccb, base_time + cyccb->cycinib->cycphs);
		}
		cyccb->cycsta = FALSE;
	}

	ercd = E_OK;

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	cyccb->exist = KOBJ_EXIST;

    exit:
	LOG_CRE_CYC_LEAVE(ercd);
	return(ercd);
}

#endif /* __cre_cyc */

/*
 *  周期ハンドラの生成(ID番号自動割付け)
 */
#ifdef __acre_cyc

SYSCALL ER_ID
acre_cyc(const T_CCYC *pk_ccyc)
{
	CYCCB	*cyccb;
	CYCINIB *cycinib;
	ER_ID	ercd;
    
	LOG_ACRE_CYC_ENTER(pk_ccyc);
	CHECK_TSKCTX_UNL();

	if (TNUM_CYC == 0) {
		ercd = E_NOID;
		goto exit;
	}
	CHECK_ATTRIBUTE(pk_ccyc->cycatr, TA_HLNG | TA_ASM | TA_PHS | TA_STA);
	CHECK_NOT_ZERO(pk_ccyc->cyctim);

	t_lock_cpu();
	cyccb = (CYCCB *)t_get_free_id((QUEUE *)&cyc_freelist);
	if (cyccb == NULL) {
		ercd = E_NOID;
		t_unlock_cpu();
		goto exit;
	}
	cyccb->exist = KOBJ_INIT;
	t_unlock_cpu();

	/*
	 * 初期設定を行う。
	 */
	queue_initialize(&(cyccb->free_queue));
	cycinib = cyccb->cycinib;
	cycinib->cycatr = pk_ccyc->cycatr;
	cycinib->exinf  = pk_ccyc->exinf;
	cycinib->cychdr = pk_ccyc->cychdr;
	cycinib->cyctim = pk_ccyc->cyctim;
	cycinib->cycphs = pk_ccyc->cycphs;

	if ((cyccb->cycinib->cycatr & TA_STA) != 0) {
		cyccb->cycsta = TRUE;
		tmevtb_enqueue_cyc(cyccb, base_time + cyccb->cycinib->cycphs);
	}
	else {	
		if ((cyccb->cycinib->cycatr & TA_PHS) != 0) {
			tmevtb_enqueue_cyc(cyccb, base_time + cyccb->cycinib->cycphs);
		}
		cyccb->cycsta = FALSE;
	}

	ercd = CYCID(cyccb);

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	cyccb->exist = KOBJ_EXIST;

    exit:
	LOG_ACRE_CYC_LEAVE(ercd);
	return(ercd);
}

#endif /* __acre_cyc */

/*
 *  周期ハンドラの削除
 */
#ifdef __del_cyc
SYSCALL ER
del_cyc(ID cycid)
{
	CYCCB	*cyccb;
	ER	ercd;
    
	LOG_DEL_CYC_ENTER(cycid);
	CHECK_TSKCTX_UNL();

	CHECK_CYCID(cycid);
	cyccb = get_cyccb(cycid);

	CHECK_OBJECT_DELETABLE(cyccb);

	/*
	 * 周期ハンドラが動作していたら、停止する。
	 * 時間イベントキューに入っている最後のハンドラを
	 * このように即時で止めて良いのかどうかは、
	 * 仕様書からは読めないが、このような実装にしないと
	 * 改変が大規模になるので、実装依存と理解することにする。
	 */
	if (cyccb->cycsta || (cyccb->cycinib->cycatr & TA_PHS)) {
		cyccb->cycsta = FALSE;
		t_lock_cpu();
		tmevtb_dequeue(&(cyccb->tmevtb));
		t_unlock_cpu();
	}

	if (!CHECK_OBJECT_RESERVED(cyccb)) {
	  /*
	   * 予約IDでなければフリーリストに入れる。
	   */
		t_lock_cpu();
		queue_insert_prev(&cyc_freelist, (QUEUE *)cyccb);
		t_unlock_cpu();
	}

	/* ここから先はacre_/cre_でのアクセスが可能 */
	cyccb->exist = KOBJ_NOEXS;

	ercd = E_OK;

    exit:
	LOG_DEL_CYC_LEAVE(ercd);
	return(ercd);
}

#endif /* __del_cyc */

/*
 *  周期ハンドラの動作開始
 */
#ifdef __sta_cyc

SYSCALL ER
sta_cyc(ID cycid)
{
	CYCCB	*cyccb;
	ER	ercd;

	LOG_STA_CYC_ENTER(cycid);
	CHECK_TSKCTX_UNL();

	CHECK_CYCID(cycid);
	cyccb = get_cyccb(cycid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(cyccb);

	if ((cyccb->cycinib->cycatr & TA_PHS) != 0) {
		/*
		 *  TA_PHS のときは、tmevtb にキューイングされているので
		 *  単に cycsta を TRUE にするだけでよい。
		 */
		cyccb->cycsta = TRUE;
	} else {
		if (cyccb->cycsta) {
			tmevtb_dequeue(&(cyccb->tmevtb));
		}
		else {
			cyccb->cycsta = TRUE;
		}
		tmevtb_enqueue_cyc(cyccb, base_time + cyccb->cycinib->cyctim);
	}
	ercd = E_OK;

	t_unlock_cpu();

    exit:
	LOG_STA_CYC_LEAVE(ercd);
	return(ercd);
}

#endif /* __sta_cyc */

/*
 *  周期ハンドラの動作停止
 */
#ifdef __stp_cyc

SYSCALL ER
stp_cyc(ID cycid)
{
	CYCCB	*cyccb;
	ER	ercd;

	LOG_STP_CYC_ENTER(cycid);
	CHECK_TSKCTX_UNL();

	CHECK_CYCID(cycid);
	cyccb = get_cyccb(cycid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(cyccb);

	/*
	 * 対象周期ハンドラに,動作していない状態の周期ハンドラが指定された
	 * 場合には,何もしない. (μITRON4.0仕様書)
	 */
	if (cyccb->cycsta) {
		if ((cyccb->cycinib->cycatr & TA_PHS) != 0) {
			/*
			 *  TA_PHS のときは、tmevtb からデキューしては
			 *  いけない。(位相が保存できなくなる)
			 */
			cyccb->cycsta = FALSE;
		} else {
			cyccb->cycsta = FALSE;
			tmevtb_dequeue(&(cyccb->tmevtb));
		}
	}
	ercd = E_OK;

	t_unlock_cpu();

    exit:
	LOG_STP_CYC_LEAVE(ercd);
	return(ercd);
}

#endif /* __stp_cyc */

/*
 *  周期ハンドラの状態参照
 */
#ifdef __ref_cyc

SYSCALL ER
ref_cyc(ID cycid, T_RCYC *pk_rcyc)
{
	CYCCB	*cyccb;
	ER	ercd;

	LOG_STP_CYC_ENTER(cycid);
	CHECK_TSKCTX_UNL();

	CHECK_CYCID(cycid);
	cyccb = get_cyccb(cycid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(cyccb);

	if (cyccb->cycsta) {
		pk_rcyc->cycstat = TCYC_STA;
	} else {
		pk_rcyc->cycstat = TCYC_STP;
	}

	pk_rcyc->lefttim = cyccb->evttim - base_time;

	ercd = E_OK;

	t_unlock_cpu();

    exit:
	LOG_STP_CYC_LEAVE(ercd);
	return(ercd);
}

#endif /* __ref_cyc */

/*
 *  周期ハンドラ起動ルーチン
 */
#ifdef __cyccal

void
call_cychdr(CYCCB *cyccb)
{
	/*
	 *  次回の起動のためのタイムイベントブロックを登録する．
	 *
	 *  同じタイムティックで周期ハンドラを再度起動すべき場合には，
	 *  この関数から isig_tim に戻った後に，再度この関数が呼ばれる
	 *  ことになる．
	 */
	tmevtb_enqueue_cyc(cyccb, cyccb->evttim + cyccb->cycinib->cyctim);

	if (cyccb->cycsta) {
		/*
		 *  周期ハンドラを，CPUロック解除状態で呼び出す．
		 */
		i_unlock_cpu();
		LOG_CYC_ENTER(cyccb);
		((CYCHDR)(*cyccb->cycinib->cychdr))(cyccb->cycinib->exinf);
		LOG_CYC_LEAVE(cyccb);
		i_lock_cpu();
	} else {
		if ((cyccb->cycinib->cycatr & TA_PHS) != 0) {
			/*
			 *  起動位相の保存をしたいだけなので,
			 *  周期ハンドラは呼び出さない.
			 *  ログを残す必要があるならば、
			 *  ここに挿入すればよい.
			 */
			return;
		} else {
			/*
			 *  周期ハンドラの動作が停止され,かつ起動位相の保存も
			 *  しない状態で、この関数に入ってくることはあり得ない.
			 */
			assert("Internal error: called call_cychdr in stopped state.");
		}
	}
}

#endif /* __cyccal */
