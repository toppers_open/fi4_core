/* This file is generated from fi4_rename.def by genrename. */

#ifdef _FI4_UNRENAME_H_
#undef _FI4_UNRENAME_H_

/*
 *  kmem.c
 */
#undef kmem_setup
#undef kmem_allocate
#undef kmem_allocatable
#undef kmem_release
#undef kmem_status

/*
 *  startup.c
 */
#undef iniflg

/*
 *  banner.c
 */
#undef print_banner

/*
 *  cpu_config.c, cpu_support.S, sys_config.c, sys_support.S
 */
#undef dispatch
#undef exit_and_dispatch
#undef cpu_initialize
#undef cpu_terminate
#undef sys_initialize
#undef sys_exit
#undef sys_putc

/*
 *  bufmgr.c
 */
#undef bufmgr_initialize
#undef bufmgr_allocate
#undef bufmgr_release

/*
 *  task.c
 */
#undef runtsk
#undef schedtsk
#undef reqflg
#undef enadsp
#undef ready_queue
#undef ready_primap
#undef task_initialize
#undef search_schedtsk
#undef make_runnable
#undef make_non_runnable
#undef make_dormant
#undef make_active
#undef exit_task
#undef change_priority
#undef rotate_ready_queue
#undef call_texrtn
#undef calltex

/*
 *  task_overrun.c
 */
#undef overrun_initialize
#undef call_ovrhdr
#undef overrun_handler_function

/*
 *  wait.c
 */
#undef make_wait_tmout
#undef wait_complete
#undef wait_tmout
#undef wait_tmout_ok
#undef wait_cancel
#undef wait_release
#undef wobj_make_wait
#undef wobj_make_wait_tmout
#undef wobj_make_wait_mutexi
#undef wobj_make_wait_mutexi_tmout
#undef wobj_move_wait
#undef wobj_change_priority
#undef scan_new_priority

/*
 *  time_event.c
 */
#undef systim_offset
#undef current_time
#undef next_time
#undef next_subtime
#undef last_index
#undef tmevt_initialize
#undef tmevt_up
#undef tmevt_down
#undef tmevtb_insert
#undef tmevtb_delete

/*
 *  syslog.c
 */
#undef syslog_buffer
#undef syslog_count
#undef syslog_head
#undef syslog_tail
#undef syslog_lost
#undef syslog_logmask
#undef syslog_lowmask
#undef syslog_initialize
#undef syslog_terminate

/*
 *  semaphore.c
 */
#undef semaphore_initialize

/*
 *  eventflag.c
 */
#undef eventflag_initialize
#undef eventflag_cond

/*
 *  dataqueue.c
 */
#undef dataqueue_initialize
#undef enqueue_data
#undef force_enqueue_data
#undef dequeue_data
#undef send_data_rwait
#undef receive_data_swait

/*
 *  mailbox.c
 */
#undef mailbox_initialize
#undef enqueue_msg_pri

/*
 *  mutex.c
 */
#undef mutex_initialize
#undef search_mtx_priority
#undef search_max_ceilpri

/*
 *  messagebuffer.c
 */
#undef messagebuffer_initialize
#undef send_mbf_rwait
#undef read_mbf_buffer
#undef write_mbf_buffer

/*
 *  rendezvous.c
 */
#undef rendezvous_initialize

/*
 *  mempfix.c
 */
#undef mempfix_initialize
#undef mempfix_get_block
#undef mempfix_count_free
#undef init_mpfcb

/*
 *  mempvar.c
 */
#undef mempvar_initialize
#undef mblcb_initialize

/*
 *  cyclic.c
 */
#undef cyclic_initialize
#undef tmevtb_enqueue_cyc
#undef call_cychdr

/*
 *  alarm.c
 */
#undef alarm_initialize
#undef call_almhdr

/*
 *  interrupt.c
 */
#undef interrupt_initialize
#undef interrupt_service_initialize
#undef attached_isr_initialize

/*
 *  exception.c
 */
#undef exception_initialize

/*
 *  kernel_cfg.c
 */
#undef object_initialize
#undef call_inirtn
#undef call_terrtn
#undef tmax_tskid
#undef tnum_tinia
#undef tinia_table
#undef tinib_table
#undef torder_table
#undef tcb_table
#undef tnum_trid
#undef trid_table
#undef tmax_semid
#undef tnum_seminia
#undef seminia_table
#undef seminib_table
#undef semcb_table
#undef tnum_semrid
#undef semrid_table
#undef tmax_flgid
#undef tnum_flginia
#undef flginia_table
#undef flginib_table
#undef flgcb_table
#undef tnum_flgrid
#undef flgrid_table
#undef tmax_dtqid
#undef tnum_dtqinia
#undef dtqinia_table
#undef dtqinib_table
#undef dtqcb_table
#undef tnum_dtqrid
#undef dtqrid_table
#undef tmax_mbxid
#undef mbxcb_table
#undef tnum_mbxinia
#undef mbxinia_table
#undef mbxinib_table
#undef tnum_mbxrid
#undef mbxrid_table
#undef tmax_mpfid
#undef tnum_mpfinia
#undef mpfinia_table
#undef mpfinib_table
#undef mpfcb_table
#undef tnum_mpfrid
#undef mpfrid_table
#undef tmax_mplid
#undef tnum_mplinia
#undef mplinia_table
#undef mplinib_table
#undef mplcb_table
#undef tnum_mplrid
#undef mplrid_table
#undef tmax_mtxid
#undef tnum_mtxinia
#undef mtxinia_table
#undef mtxinib_table
#undef mtxcb_table
#undef tnum_mtxrid
#undef mtxrid_table
#undef tmax_mbfid
#undef tnum_mbfinia
#undef mbfinia_table
#undef mbfinib_table
#undef mbfcb_table
#undef tnum_mbfrid
#undef mbfrid_table
#undef tmax_porid
#undef tnum_porinia
#undef porinia_table
#undef porinib_table
#undef porcb_table
#undef tnum_porrid
#undef porrid_table
#undef tmax_cycid
#undef tnum_cycinia
#undef cycinia_table
#undef cycinib_table
#undef cyccb_table
#undef tnum_cycrid
#undef cycrid_table
#undef tnum_cycrid
#undef cycrid_table
#undef tmax_almid
#undef tnum_alminia
#undef alminia_table
#undef alminib_table
#undef almcb_table
#undef tnum_almrid
#undef almrid_table
#undef tnum_inhno
#undef inhinib_table
#undef tmax_isrid
#undef tnum_isrinia
#undef isrinia_table
#undef isrinib_table
#undef isrcb_table
#undef tnum_isrrid
#undef isrrid_table
#undef tnum_attached_isrno
#undef attached_isrinib_table
#undef tnum_excno
#undef excinib_table
#undef tmevt_heap
#undef tnum_svcno
#undef svccb_table
#undef svcinib_table
#undef svcinib_hash_table

/*
 * servicecall
 */
#undef exservicecall_initialize


#undef alm_freelist
#undef cyc_freelist
#undef dtq_freelist
#undef flg_freelist
#undef isr_freelist
#undef mbx_freelist
#undef mpf_freelist
#undef mpl_freelist
#undef mbf_freelist
#undef mtx_freelist
#undef por_freelist
#undef sem_freelist
#undef tsk_freelist

#ifdef LABEL_ASM

/*
 *  kmem.c
 */
#undef _kmem_setup
#undef _kmem_allocate
#undef _kmem_allocatable
#undef _kmem_release
#undef _kmem_status

/*
 *  startup.c
 */
#undef _iniflg

/*
 *  banner.c
 */
#undef _print_banner

/*
 *  cpu_config.c, cpu_support.S, sys_config.c, sys_support.S
 */
#undef _dispatch
#undef _exit_and_dispatch
#undef _cpu_initialize
#undef _cpu_terminate
#undef _sys_initialize
#undef _sys_exit
#undef _sys_putc

/*
 *  bufmgr.c
 */
#undef _bufmgr_initialize
#undef _bufmgr_allocate
#undef _bufmgr_release

/*
 *  task.c
 */
#undef _runtsk
#undef _schedtsk
#undef _reqflg
#undef _enadsp
#undef _ready_queue
#undef _ready_primap
#undef _task_initialize
#undef _search_schedtsk
#undef _make_runnable
#undef _make_non_runnable
#undef _make_dormant
#undef _make_active
#undef _exit_task
#undef _change_priority
#undef _rotate_ready_queue
#undef _call_texrtn
#undef _calltex
#undef _unlock_all_mutex

/*
 *  task_overrun.c
 */
#undef _overrun_initialize
#undef _call_ovrhdr
#undef _overrun_handler_function

/*
 *  wait.c
 */
#undef _make_wait_tmout
#undef _wait_complete
#undef _wait_tmout
#undef _wait_tmout_ok
#undef _wait_cancel
#undef _wait_release
#undef _wobj_make_wait
#undef _wobj_make_wait_tmout
#undef _wobj_make_wait_mutexi
#undef _wobj_make_wait_mutexi_tmout
#undef _wobj_move_wait
#undef _wobj_change_priority

/*
 *  time_event.c
 */
#undef _systim_offset
#undef _current_time
#undef _next_time
#undef _next_subtime
#undef _last_index
#undef _tmevt_initialize
#undef _tmevt_up
#undef _tmevt_down
#undef _tmevtb_insert
#undef _tmevtb_delete

/*
 *  syslog.c
 */
#undef _syslog_buffer
#undef _syslog_count
#undef _syslog_head
#undef _syslog_tail
#undef _syslog_lost
#undef _syslog_logmask
#undef _syslog_lowmask
#undef _syslog_initialize
#undef _syslog_terminate

/*
 *  semaphore.c
 */
#undef _semaphore_initialize

/*
 *  eventflag.c
 */
#undef _eventflag_initialize
#undef _eventflag_cond

/*
 *  dataqueue.c
 */
#undef _dataqueue_initialize
#undef _enqueue_data
#undef _force_enqueue_data
#undef _dequeue_data
#undef _send_data_rwait
#undef _receive_data_swait

/*
 *  mailbox.c
 */
#undef _mailbox_initialize
#undef _enqueue_msg_pri

/*
 *  mutex.c
 */
#undef _mutex_initialize
#undef _search_mtx_priority
#undef _search_max_ceilpri

/*
 *  messagebuffer.c
 */
#undef _messagebuffer_initialize
#undef _send_mbf_rwait
#undef _read_mbf_buffer
#undef _write_mbf_buffer

/*
 *  rendezvous.c
 */
#undef _rendezvous_initialize

/*
 *  mempfix.c
 */
#undef _mempfix_initialize
#undef _mempfix_get_block
#undef _mempfix_count_free
#undef _init_mpfcb

/*
 *  mempvar.c
 */
#undef _mempvar_initialize
#undef _mblcb_initialize

/*
 *  cyclic.c
 */
#undef _cyclic_initialize
#undef _tmevtb_enqueue_cyc
#undef _call_cychdr

/*
 *  alarm.c
 */
#undef _alarm_initialize
#undef _call_almhdr

/*
 *  interrupt.c
 */
#undef _interrupt_initialize
#undef _interrupt_service_initialize
#undef _attached_isr_initialize

/*
 *  exception.c
 */
#undef _exception_initialize

/*
 *  kernel_cfg.c
 */
#undef _object_initialize
#undef _call_inirtn
#undef _call_terrtn
#undef _tmax_tskid
#undef _tnum_tinia
#undef _tinia_table
#undef _tinib_table
#undef _torder_table
#undef _tcb_table
#undef _tnum_trid
#undef _trid_table
#undef _tmax_semid
#undef _tnum_seminia
#undef _seminia_table
#undef _seminib_table
#undef _semcb_table
#undef _tnum_semrid
#undef _semrid_table
#undef _tmax_flgid
#undef _tnum_flginia
#undef _flginia_table
#undef _flginib_table
#undef _flgcb_table
#undef _tnum_flgrid
#undef _flgrid_table
#undef _tmax_dtqid
#undef _tnum_dtqinia
#undef _dtqinia_table
#undef _dtqinib_table
#undef _dtqcb_table
#undef _tnum_dtqrid
#undef _dtqrid_table
#undef _tmax_mbxid
#undef _mbxcb_table
#undef _tnum_mbxinia
#undef _mbxinia_table
#undef _mbxinib_table
#undef _tnum_mbxrid
#undef _mbxrid_table
#undef _tmax_mpfid
#undef _tnum_mpfinia
#undef _mpfinia_table
#undef _mpfinib_table
#undef _mpfcb_table
#undef _tnum_mpfrid
#undef _mpfrid_table
#undef _tmax_mplid
#undef _tnum_mplinia
#undef _mplinia_table
#undef _mplinib_table
#undef _mplcb_table
#undef _tnum_mplrid
#undef _mplrid_table
#undef _tmax_mtxid
#undef _tnum_mtxinia
#undef _mtxinia_table
#undef _mtxinib_table
#undef _mtxcb_table
#undef _tnum_mtxrid
#undef _mtxrid_table
#undef _tmax_mbfid
#undef _tnum_mbfinia
#undef _mbfinia_table
#undef _mbfinib_table
#undef _mbfcb_table
#undef _tnum_mbfrid
#undef _mbfrid_table
#undef _tmax_porid
#undef _tnum_porinia
#undef _porinia_table
#undef _porinib_table
#undef _porcb_table
#undef _tnum_porrid
#undef _porrid_table
#undef _tmax_cycid
#undef _tnum_cycinia
#undef _cycinia_table
#undef _cycinib_table
#undef _cyccb_table
#undef _tnum_cycrid
#undef _cycrid_table
#undef _tnum_cycrid
#undef _cycrid_table
#undef _tmax_almid
#undef _tnum_alminia
#undef _alminia_table
#undef _alminib_table
#undef _almcb_table
#undef _tnum_almrid
#undef _almrid_table
#undef _tnum_inhno
#undef _inhinib_table
#undef _tmax_isrid
#undef _tnum_isrinia
#undef _isrinia_table
#undef _isrinib_table
#undef _isrcb_table
#undef _tnum_isrrid
#undef _isrrid_table
#undef _tnum_attached_isrno
#undef _attached_isrinib_table
#undef _tnum_excno
#undef _excinib_table
#undef _tmevt_heap
#undef _tnum_svcno
#undef _svccb_table
#undef _svcinib_table
#undef _svcinib_hash_table

/*
 * servicecall
 */
#undef _exservicecall_initialize


#undef _alm_freelist
#undef _cyc_freelist
#undef _dtq_freelist
#undef _flg_freelist
#undef _isr_freelist
#undef _mbx_freelist
#undef _mpf_freelist
#undef _mpl_freelist
#undef _mbf_freelist
#undef _mtx_freelist
#undef _por_freelist
#undef _sem_freelist
#undef _tsk_freelist

#endif /* LABEL_ASM */
#endif /* TOPPERS_FI4_UNRENAME_H */
