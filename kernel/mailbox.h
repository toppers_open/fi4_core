/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2003-2006 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/mailbox.h 6183 2006-11-05T03:43:32.794177Z monaka  $
 */

/*
 *	メールボックス機能
 */

#ifndef TOPPERS_MAILBOX_H
#define TOPPERS_MAILBOX_H

#include "queue.h"

/*
 *  メールボックス初期化ブロック
 */
typedef struct mailbox_initialization_block {
	ATR	mbxatr;		/* メールボックス属性 */
	PRI	maxmpri;	/* メッセージ優先度の最大値 */
	VP	mprihd;		/* 優先度別メッセージキューヘッダ領域の先頭番地 */
} MBXINIB;

/*
 *  メールボックス初期化ブロック連想ペア
 */
typedef struct mailbox_initialization_block_apair {
	ID	mbxid;
	MBXINIB	mbxinib;
} MBXINIA;

/*
 *  メールボックス管理ブロック
 *
 *  メッセージキューがメッセージの優先度順の場合には，last は使わない．
 */
typedef struct mailbox_control_block {
	QUEUE	wait_queue;	/* メールボックス待ちキュー */
	MBXINIB *mbxinib;	/* メールボックス初期化ブロックへのポインタ */
	T_MSG	*head;		/* 先頭のメッセージ */
	T_MSG	*last;		/* 末尾のメッセージ */
	unsigned int	exist:2;	/* 使われているとき、KOBJ_EXIST */
	unsigned int	reserved:1;
} MBXCB;

/*
 *  メールボックス機能の初期化
 */
extern void	mailbox_initialize(void);

#define MBXID(mbxcb)	((ID)((mbxcb) - mbxcb_table) + TMIN_MBXID)

#endif /* TOPPERS_MAILBOX_H */
