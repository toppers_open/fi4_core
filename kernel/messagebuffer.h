/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2003-2006 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/messagebuffer.h 6183 2006-11-05T03:43:32.794177Z monaka  $
 */

/*
 *	メッセージバッファ機能
 */

#ifndef TOPPERS_MESSAGEBUFFER_H
#define TOPPERS_MESSAGEBUFFER_H

#include "queue.h"
#include "kmem.h"

/*
 *  メッセージバッファ初期化ブロック
 */
typedef struct messagebuffer_initialization_block {
	ATR mbfatr;
	UINT maxmsz;
	UINT mbfsz;
	VP mbf;
} MBFINIB;

/*
 *  メッセージバッファ初期化ブロック連想ペア
 */
typedef struct messagebuffer_initialization_block_apair {
	ID	mbfid;
	MBFINIB	mbfinib;
} MBFINIA;

#define MBF_SWAIT_QUEUE_NUM	(TMAX_TPRI - TMIN_TPRI + 1)

/*
 *  メッセージバッファ管理ブロック
 */
typedef struct messagebuffer_control_block {
	QUEUE	wait_queue;				/* 受信用待ちキュー */
	MBFINIB	*mbfinib;
	QUEUE	rwait_queue;				/* 受信用待ちキュー */
	UINT	head;
	UINT	tail;
	unsigned int	exist:2;				/* 使われているとき、KOBJ_EXIST */
	unsigned int	reserved:1;
} MBFCB;

/*
 *  メッセージバッファ機能の初期化
 */
extern void	messagebuffer_initialize(void);

extern BOOL send_mbf_rwait(ID mbfid, VP msg, UINT msgsz);
extern BOOL recv_mbuffer_swait(ID mbfid, VP msg, UINT *msgsz);

#define MBFID(mbfcb)	((ID)((mbfcb) - mbfcb_table) + TMIN_MBFID)

#endif /* TOPPERS_MESSAGEBUFFER_H */
