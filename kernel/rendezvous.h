/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2000-2006 by Masaki Muranaka
 *  Copyright (C) 2003-2006 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/rendezvous.h 6183 2006-11-05T03:43:32.794177Z monaka  $
 */

/*
 *	ランデブポート機能
 */

#ifndef TOPPERS_RENDEZVOUS_H
#define TOPPERS_RENDEZVOUS_H

#include "queue.h"

/*
 *  ランデブポート初期化ブロック
 */
typedef struct rendezvous_port_initialization_block {
  ATR	poratr;		/* Rendezvous port attribute. */
  UINT	maxcmsz;	/* 呼出しメッセージの最大サイズ(byte count) */
  UINT	maxrmsz;	/* 返答メッセージの最大サイズ(byte count) */
} PORINIB;

/*
 *  ランデブポート初期化ブロック連想ペア
 */
typedef struct rendezvous_port_initialization_block_apair {
	ID	porid;
	PORINIB	porinib;
} PORINIA;

/*
 *  ランデブポート管理ブロック
 */
typedef struct rendezvous_port_control_block {
  QUEUE	wait_queue;		/* ランデブ呼出し待ちキュー */
  PORINIB *porinib;		/* ランデブポート初期化ブロックへのポインタ */
  QUEUE	await_queue;		/* ランデブ受付待ちキュー */
  unsigned int	reserved:1;
  unsigned int	exist:2;
} PORCB;


/*
 *  ランデブ番号初期化ブロック
 *  他の管理ブロックとは違う(ハッシュ構造である)ことに注意。
 */
typedef struct rendezvous_number_initialization_block {
	ATR	atr;			/* dummy */
} RDVINIB;

/*
 *  ランデブ番号管理ブロック
 *  他の管理ブロックとは違う(ハッシュ構造である)ことに注意。
 */
typedef struct rendezvous_number_control_block {
	QUEUE	wait_queue;		/* ランデブ終了待ちキュー */
	RDVINIB *rdvinib;  
} RDVCB;


/*
 *  ランデブポート機能の初期化
 */
extern void rendezvous_initialize(void);

#define PORID(porcb)	((ID)((porcb) - porcb_table) + TMIN_PORID)

#endif /* TOPPERS_RENDEZVOUS_H */
