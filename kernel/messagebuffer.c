/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2003-2004 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/messagebuffer.c 6101 2006-09-10T08:00:54.989588Z monaka  $
 */

/*
 *	メッセージバッファ機能
 */

#include "fi4_kernel.h"
#include "check.h"
#include "task.h"
#include "wait.h"
#include "messagebuffer.h"
#include "bufmgr.h"

/*
 *  メッセージバッファIDの最大値（kernel_cfg.c）
 */
extern const ID	tmax_mbfid;

/*
 *  メッセージバッファ初期化ブロックのエリア（kernel_cfg.c）
 */
extern MBFINIB	mbfinib_table[];

/*
 *  メッセージバッファ初期化ブロック連想ペアの数（kernel_cfg.c）
 */
extern const UINT	tnum_mbfinia;

/*
 *  メッセージバッファ初期化ブロック連想ペアのエリア（kernel_cfg.c）
 */
extern const MBFINIA	mbfinia_table[];

/*
 *  メッセージバッファ予約ID情報のエリア（kernel_cfg.c）
 */
extern const ID		mbfrid_table[];

/*
 *  メッセージバッファ予約ID情報の数 (kernel_cfg.c)
 */
extern const UINT	tnum_mbfrid;

/*
 *  メッセージバッファ管理ブロックのエリア（kernel_cfg.c）
 */
extern MBFCB	mbfcb_table[];
/*
 *  メッセージバッファの数
 */
#define TNUM_MBF	((UINT)(tmax_mbfid - TMIN_MBFID + 1))

/*
 *  メッセージサイズの上限
 */
#define MBF_MAXMSZ_MAX		((UINT)65536)

/*
 *  メッセージバッファIDからメッセージバッファ管理ブロックを取り出すためのマクロ
 */
#define INDEX_MBF(mbfid)	((UINT)((mbfid) - TMIN_MBFID))
#define get_mbfcb(mbfid)	(&(mbfcb_table[INDEX_MBF(mbfid)]))

/*
 *  ID自動割当て用フリーリスト
 */
extern QUEUE mbf_freelist;

/*
 *  メッセージバッファ待ち情報ブロックの定義
 *
 *  メッセージバッファへの送信待ちとメッセージバッファからの受信待ちで，同じ待ち情
 *  報ブロックを使う．
 */
typedef struct dataqueue_waiting_information {
	WINFO	winfo;		/* 標準の待ち情報ブロック */
	WOBJCB	*wobjcb;	/* 待ちオブジェクトの管理ブロック */
	VP	msg;		/* 送受信メッセージ  */
	UINT	msgsz;		/* 送受信メッセージのサイズ */
} WINFO_MBF;


/*
 *  (送信待ちの)行列を走査し、自タスクの優先度より
 *  高い優先度のタスクが含まれるとき、TRUEを返す。
 *  TRUEのときかつTA_TPRIのときは、待ちに入ることが
 *  期待される。
 *
 *  本関数はCPUロック状態で呼ぶこと。
 */
Inline BOOL
check_queue_priority(QUEUE *wait_queue)
{
	TCB	*tcb;
	QUEUE	*queue = NULL;

	while((queue = queue_enumerate(wait_queue, queue)) != NULL) {
		tcb = (TCB *)queue;
		if (tcb->priority < runtsk->priority) {
			return TRUE;
		}
	}
	return FALSE;
}


/*
 *
 */
#ifdef __mbfrwait

BOOL
send_mbf_rwait(ID mbfid, VP msg, UINT msgsz)
{
	TCB		*tcb;
	MBFCB		*mbfcb;
	WINFO_MBF	*winfo_mbf;

	mbfcb = get_mbfcb(mbfid);
	if (queue_empty(&(mbfcb->rwait_queue))) {
		return FALSE;
	}

	tcb = (TCB *) queue_delete_next(&(mbfcb->rwait_queue));
	winfo_mbf = (WINFO_MBF *)(tcb->winfo);
	memcpy(winfo_mbf->msg, msg, msgsz);
	winfo_mbf->msgsz = msgsz;
	if (wait_complete(tcb)) {
		dispatch();
	}

	return TRUE;
}

#endif /* __mbfrwait */

/*
 *
 */
#ifdef __mbfrbuf

BOOL
read_mbf_buffer(ID mbfid, VP msg, UINT *msgsz)
{
	MBFCB *mbfcb;
	UB *m;
	UB *buf;
	UINT mbfsz;
	UINT head;
	UINT tail;
	UINT i;
	UINT size;

	mbfcb = get_mbfcb(mbfid);
	m = (UB*)msg;
	buf = mbfcb->mbfinib->mbf;
	mbfsz = mbfcb->mbfinib->mbfsz;
	head = mbfcb->head;
	tail = mbfcb->tail;

	if (head == tail) {
		return FALSE;
	}

	size = (buf[tail++ % mbfsz] << 8);
	size |= buf[tail++ % mbfsz];
	tail %= mbfsz;
	for(i = 0; i < size; i++, tail = (tail + 1) % mbfsz) {
		m[i] = buf[tail];
	}
	mbfcb->tail = tail;
	*msgsz = size;

	return TRUE;
}
#endif /* __mbfrbuf */

/*
 *
 */
#ifdef __mbfwbuf

BOOL
write_mbf_buffer(ID mbfid, VP msg, UINT msgsz)
{
	MBFCB *mbfcb;
	UB *m;
	UB *buf;
	UINT mbfsz;
	UINT head;
	UINT tail;
	UINT i;
	UINT limit;

	mbfcb = get_mbfcb(mbfid);
	m = (UB*)msg;
	buf = mbfcb->mbfinib->mbf;
	mbfsz = mbfcb->mbfinib->mbfsz;
	head = mbfcb->head;
	tail = mbfcb->tail;

	if (tail <= head) {
		limit = mbfsz + tail;
	} else {
		limit = tail;
	}

	if (head + 2 + msgsz >= limit) {
		return FALSE;
	}

	buf[head++ % mbfsz] = (msgsz >> 8) & 0xff;
	buf[head++ % mbfsz] = msgsz & 0xff;
	head %= mbfsz;

	for(i = 0; i < msgsz; i++, head = (head + 1) % mbfsz) {
		buf[head] = m[i];
	}
	mbfcb->head = head;

	return TRUE;
}
#endif /* __mbfrbuf */

/* 
 *  メッセージバッファ機能の初期化
 */
#ifdef __mbfini

QUEUE mbf_freelist;

void
messagebuffer_initialize(void)
{
	UINT	i;
	MBFCB	*mbfcb;
	MBFINIB *mbfinib;
	const MBFINIA *mbfinia;
	const ID	*mbfrid;

	/*
	 * カーネル起動時の状態に関わらず初期化しなければいけないものを
	 * まず初期化する。
	 */
	for (mbfcb = mbfcb_table, i = 0; i < TNUM_MBF; mbfcb++, i++) {
		queue_initialize(&(mbfcb->wait_queue));
		queue_initialize(&(mbfcb->rwait_queue));
		mbfcb->mbfinib = NULL;
	}
	queue_initialize(&mbf_freelist);

	/*
	 * mbfinia によって示されるものをまず初期化
	 */
	for (mbfinia = mbfinia_table, i = 0; i < tnum_mbfinia; mbfinia++, i++) {
		UINT idx = INDEX_MBF(mbfinia->mbfid);
		mbfinib = &mbfinib_table[idx];
		*mbfinib = mbfinia->mbfinib;
		mbfcb = &mbfcb_table[idx];
		mbfcb->mbfinib = mbfinib;
		mbfcb->exist = KOBJ_EXIST;
		mbfcb->reserved = TRUE;
		mbfcb->head = 0;
		mbfcb->tail = 0;

		/*
		 *  非標準のcfgを使ったとき、mbfがNULLになる可能性がある。<BTS:21>
		 *  そのようなcfgでは検証項目が増える。標準のcfgではこのコードに
		 *  入ることはない。
		 */
		if (mbfinib->mbf == NULL) {
			BOOL stat;
			bufmgr_allocate(&stat, mbfinib->mbfsz, &(mbfinib->mbf));
			assert(stat);
		}
	}

	/*
	 *  予約IDの設定
	 */
	for (mbfrid = mbfrid_table, i = 0; i < tnum_mbfrid; mbfrid++, i++) {
		mbfcb_table[INDEX_MBF(*mbfrid)].reserved = TRUE;
	}

	/*
	 * mbfinia によって示されないものを初期化
	 */
	for (mbfcb = mbfcb_table, i = 0; i < TNUM_MBF; mbfcb++, i++) {
		if (mbfcb->mbfinib == NULL) {
			mbfcb->exist = KOBJ_NOEXS;
			mbfcb->mbfinib = &mbfinib_table[i];

			if (!CHECK_OBJECT_RESERVED(mbfcb)) {
				queue_insert_prev(&mbf_freelist, (QUEUE*)mbfcb);
			}
		}
	}
}

#endif /* __mbfini */

/*
 *  メッセージバッファの生成
 */
#ifdef __cre_mbf

SYSCALL ER
cre_mbf(ID mbfid, const T_CMBF * pk_cmbf)
{
	MBFCB	*mbfcb;
	MBFINIB *mbfinib;
	ER	ercd;
	VP	mbf;
	LOG_CRE_MBF_ENTER(mbfid, pk_cmbf);
	CHECK_TSKCTX_UNL();

	CHECK_MBFID(mbfid);
	CHECK_ATTRIBUTE(pk_cmbf->mbfatr, TA_TFIFO | TA_TPRI);
	CHECK_NOT_ZERO(pk_cmbf->maxmsz);
	CHECK_PAR(pk_cmbf->maxmsz < MBF_MAXMSZ_MAX);

	mbfcb = get_mbfcb(mbfid);
	CHECK_OBJECT_CREATABLE(mbfcb);
	UPDATE_OBJECT_FREELIST(mbfcb);

	mbf = pk_cmbf->mbf;
	if (mbf == NULL) {
		BOOL stat;
		t_lock_cpu();
		bufmgr_allocate(&stat, pk_cmbf->mbfsz, &mbf);
		t_unlock_cpu();
		if (!stat) {
			ercd = E_NOMEM;
			goto exit;
		}
	}
	
	/*
	 * 初期設定を行う。
	 * wait_queue は、 messagebuffer_initialize()で初期化されるし
	 * del_mbfの時にも掃除される。よって、ここでは初期化の必要がない。
	 */
	queue_initialize((QUEUE *)mbfcb);
	queue_initialize(&(mbfcb->rwait_queue));
	mbfinib = mbfcb->mbfinib;
	mbfinib->mbfatr = pk_cmbf->mbfatr;
	mbfinib->maxmsz = pk_cmbf->maxmsz;
	mbfinib->mbfsz  = pk_cmbf->mbfsz;
	mbfinib->mbf = mbf;
	mbfcb->head = 0;
	mbfcb->tail = 0;

	ercd = E_OK;

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	mbfcb->exist = KOBJ_EXIST;

    exit:
	LOG_CRE_MBF_LEAVE(ercd);
	return(ercd);
}

#endif /* __cre_mbf */

/*
 *  メッセージバッファの生成(ID番号自動割付け)
 */
#ifdef __acre_mbf

SYSCALL ER_ID
acre_mbf(const T_CMBF *pk_cmbf)
{
	VP	mbf;
	MBFCB	*mbfcb;
	MBFINIB *mbfinib;
	ER_ID	ercd;
    
	LOG_ACRE_MBF_ENTER(pk_cmbf);

	if (TNUM_MBF == 0) {
		ercd = E_NOID;
		goto exit;
	}
	CHECK_ATTRIBUTE(pk_cmbf->mbfatr, TA_TFIFO | TA_TPRI);
	CHECK_NOT_ZERO(pk_cmbf->maxmsz);
	CHECK_PAR(pk_cmbf->maxmsz < MBF_MAXMSZ_MAX);

	t_lock_cpu();

	mbfcb = (MBFCB *)t_get_free_id((QUEUE *)&mbf_freelist);
	if (mbfcb == NULL) {
		ercd = E_NOID;
		t_unlock_cpu();
		goto exit;
	}

	mbf = pk_cmbf->mbf;
	if (mbf == NULL) {
		BOOL stat;
		bufmgr_allocate(&stat, pk_cmbf->mbfsz, &mbf);
		if (!stat) {
			if (mbfcb) {
				queue_insert_prev(&mbf_freelist, (QUEUE *)mbfcb);		
			}
			t_unlock_cpu();
			ercd = E_NOMEM;
			goto exit;
		}
	}
	
	t_unlock_cpu();
	
	ercd = MBFID(mbfcb);
	mbfcb = get_mbfcb(ercd);

	mbfcb->exist = KOBJ_INIT;
	t_unlock_cpu();

	/*
	 * 初期設定を行う。
	 * swait_queue は、 messagebuffer_initialize()で初期化されるし
	 * del_mbfの時にも掃除される。よって、ここでは初期化の必要がない。
	 */
	queue_initialize((QUEUE *)mbfcb);
	queue_initialize(&(mbfcb->rwait_queue));
	mbfinib = mbfcb->mbfinib;
	mbfinib->mbfatr = pk_cmbf->mbfatr;
	mbfinib->maxmsz = pk_cmbf->maxmsz;
	mbfinib->mbfsz  = pk_cmbf->mbfsz;
	mbfinib->mbf = mbf;
	mbfcb->head = 0;
	mbfcb->tail = 0;

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	mbfcb->exist = KOBJ_EXIST;

    exit:
	LOG_ACRE_MBF_LEAVE(ercd);
	return(ercd);
}

#endif /* __acre_mbf */

/*
 *  メッセージバッファの削除
 */
#ifdef __del_mbf
SYSCALL ER
del_mbf(ID mbfid)
{
	MBFCB	*mbfcb;
	ER	ercd;
   	BOOL	stat;
 
	LOG_DEL_MBF_ENTER(mbfid);
	CHECK_TSKCTX_UNL();

	CHECK_MBFID(mbfid);
	mbfcb = get_mbfcb(mbfid);

	CHECK_OBJECT_DELETABLE(mbfcb);

	/* 送信待ち行列の後片付け。削除に伴う待ち解除を行う。*/
	while (queue_empty(&(mbfcb->wait_queue)) == FALSE) {
		TCB *tcb;
		tcb = (TCB *)queue_delete_next(&(mbfcb->wait_queue));
		t_lock_cpu();
		if (wait_deleted(tcb)) {
			dispatch();
		}
		t_unlock_cpu();
	}

	/* 受信待ち行列の後片付け。削除に伴う待ち解除を行う。*/
	while (queue_empty(&(mbfcb->rwait_queue)) == FALSE) {
		TCB *tcb;
		tcb = (TCB *)queue_delete_next(&(mbfcb->rwait_queue));
		t_lock_cpu();
		if (wait_deleted(tcb)) {
			dispatch();
		}
		t_unlock_cpu();
	}

	if (!CHECK_OBJECT_RESERVED(mbfcb)) {
	  /*
	   * 予約IDでなければフリーリストに入れる。
	   */
		t_lock_cpu();
		queue_insert_prev(&mbf_freelist, (QUEUE *)mbfcb);
		t_unlock_cpu();
	}

	/*
	 *  メッセージバッファ領域をカーネルで確保した場合には、
	 *  その領域を返却する。
	 */
	stat = bufmgr_release(mbfcb->mbfinib->mbf);

	/* ここから先はacre_/cre_でのアクセスが可能 */
	mbfcb->exist = KOBJ_NOEXS;

	ercd = E_OK;

    exit:
	LOG_DEL_MBF_LEAVE(ercd);
	return(ercd);
}

#endif /* __del_mbf */

/*
 *  メッセージバッファへの送信
 */
#ifdef __snd_mbf
SYSCALL ER
snd_mbf(ID mbfid, VP msg, UINT msgsz)
{
	MBFCB	*mbfcb;
	QUEUE	*wait_queue;
	ATR	mbfatr;
	ER	ercd;
	BOOL	stat;

	LOG_SND_MBF_ENTER(mbfid);
	CHECK_DISPATCH();

	CHECK_MBFID(mbfid);
	CHECK_NOT_ZERO(msgsz);

	mbfcb = get_mbfcb(mbfid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mbfcb);
	if (msgsz > mbfcb->mbfinib->maxmsz) {
		ercd = E_PAR;
		goto escape;
	}

	/*
	 * 受信待ちがあったなら、即座にリターン。
	 * コピー処理はsend_mbf_rwaitで内すでに終わっている。
	 */
	stat = send_mbf_rwait(mbfid, msg, msgsz);
	if (stat) {
		ercd = E_OK;
		goto escape;
	}


	/*
	 *  TA_TPRIで自分の優先度以上の優先度を持つタスクが送信待ちに
	 *  入っているか、TA_TFIFOですでに送信待ちタスクがあるならば、
	 *  自分も待ちへ。
	 */
	mbfatr = mbfcb->mbfinib->mbfatr;
	wait_queue = &(mbfcb->wait_queue);
	if ((((mbfatr & TA_TPRI) == TA_TPRI) &&
	     check_queue_priority(wait_queue)) ||
	    (((mbfatr & TA_TFIFO) == TA_TFIFO) &&
	     !queue_empty(wait_queue))) {
		/* make waiting */
		WINFO_MBF winfo;
		winfo.msg = msg;
		winfo.msgsz = msgsz;
		wobj_make_wait((WOBJCB *) mbfcb, (WINFO_WOBJ *) &winfo);
		dispatch();
		ercd = winfo.winfo.wercd;
		goto escape;
	}

	/*
	 *  メッセージバッファへの書込みを試みる。
	 *  メッセージバッファに書けない場合、
	 *  送信待ちへ。
	 */
	stat = write_mbf_buffer(mbfid, msg, msgsz);
	if (stat) {
		ercd = E_OK;
	} else {
		/* make waiting */
		WINFO_MBF winfo;
		winfo.msg = msg;
		winfo.msgsz = msgsz;
		wobj_make_wait((WOBJCB *) mbfcb, (WINFO_WOBJ *) &winfo);
		dispatch();
		ercd = winfo.winfo.wercd;
	}

    escape:
	t_unlock_cpu();

    exit:
	LOG_SND_MBF_LEAVE(ercd);
	return(ercd);
}

#endif /* __snd_mbf */

/*
 *  メッセージバッファへの送信（ポーリング）
 */
#ifdef __psnd_mbf
SYSCALL ER
psnd_mbf(ID mbfid, VP msg, UINT msgsz)
{
	MBFCB	*mbfcb;
	QUEUE	*wait_queue;
	ATR	mbfatr;
	ER	ercd;
	BOOL	stat;

	LOG_SND_MBF_ENTER(mbfid);
	CHECK_TSKCTX_UNL();

	CHECK_MBFID(mbfid);
	CHECK_NOT_ZERO(msgsz);

	mbfcb = get_mbfcb(mbfid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mbfcb);
	if (msgsz > mbfcb->mbfinib->maxmsz) {
		ercd = E_PAR;
		goto escape;
	}

	/*
	 * 受信待ちがあったなら、即座にリターン。
	 * コピー処理はsend_mbf_rwaitで内すでに終わっている。
	 */
	stat = send_mbf_rwait(mbfid, msg, msgsz);
	if (stat) {
		ercd = E_OK;
		goto escape;
	}

	/*
	 *  TA_TPRIで自分の優先度以上の優先度を持つタスクが送信待ちに
	 *  入っているか、TA_TFIFOですでに送信待ちタスクがあるならば、
	 *  タイムアウトエラーで戻る。
	 */
	mbfatr = mbfcb->mbfinib->mbfatr;
	wait_queue = &(mbfcb->wait_queue);
	if ((((mbfatr & TA_TPRI) == TA_TPRI) &&
	     check_queue_priority(wait_queue)) ||
	    (((mbfatr & TA_TFIFO) == TA_TFIFO) &&
	     !queue_empty(wait_queue))) {
		ercd = E_TMOUT;
		goto escape;
	}

	/*
	 *  メッセージバッファへの書込みを試みる。
	 *  メッセージバッファに書けない場合、
	 *  タイムアウトエラーで戻る。
	 */
	stat = write_mbf_buffer(mbfid, msg, msgsz);
	if (stat) {
		ercd = E_OK;
	} else {
		ercd = E_TMOUT;
	}

    escape:
	t_unlock_cpu();

    exit:
	LOG_SND_MBF_LEAVE(ercd);
	return(ercd);
}

#endif /* __psnd_mbf */

/*
 *  メッセージバッファへの送信（タイムアウトあり）
 */
#ifdef __tsnd_mbf
SYSCALL ER
tsnd_mbf(ID mbfid, VP msg, UINT msgsz, TMO tmout)
{
	MBFCB	*mbfcb;
	QUEUE	*wait_queue;
	ATR	mbfatr;
	ER	ercd;
	BOOL	stat;

	LOG_SND_MBF_ENTER(mbfid);
	CHECK_DISPATCH();

	CHECK_MBFID(mbfid);
	CHECK_NOT_ZERO(msgsz);
	CHECK_TMOUT(tmout);

	mbfcb = get_mbfcb(mbfid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mbfcb);
	if (msgsz > mbfcb->mbfinib->maxmsz) {
		ercd = E_PAR;
		goto escape;
	}

	/*
	 * 受信待ちがあったなら、即座にリターン。
	 * コピー処理はsend_mbf_rwaitで内すでに終わっている。
	 */
	stat = send_mbf_rwait(mbfid, msg, msgsz);
	if (stat) {
		ercd = E_OK;
		goto escape;
	}

	/*
	 *  TA_TPRIで自分の優先度以上の優先度を持つタスクが送信待ちに
	 *  入っているか、TA_TFIFOですでに送信待ちタスクがあるならば、
	 *  自分も待ちへ。
	 */
	mbfatr = mbfcb->mbfinib->mbfatr;
	wait_queue = &(mbfcb->wait_queue);
	if ((((mbfatr & TA_TPRI) == TA_TPRI) &&
	     check_queue_priority(wait_queue)) ||
	    (((mbfatr & TA_TFIFO) == TA_TFIFO) &&
	     !queue_empty(wait_queue))) {
		/* make waiting */
		WINFO_MBF winfo;
		winfo.msg = msg;
		winfo.msgsz = msgsz;
		wobj_make_wait((WOBJCB *) mbfcb, (WINFO_WOBJ *) &winfo);
		dispatch();
		ercd = winfo.winfo.wercd;
		goto escape;
	}

	/*
	 *  メッセージバッファへの書込みを試みる。
	 *  メッセージバッファに書けない場合、
	 *  送信待ちへ。
	 */
	stat = write_mbf_buffer(mbfid, msg, msgsz);
	if (stat) {
		ercd = E_OK;
	} else {
		if (tmout == TMO_POL) {
			ercd = E_TMOUT;
		} else {
			/* make waiting */
			WINFO_MBF winfo;
			winfo.msg = msg;
			winfo.msgsz = msgsz;
			wobj_make_wait((WOBJCB *) mbfcb, (WINFO_WOBJ *) &winfo);
			dispatch();
			ercd = winfo.winfo.wercd;
		}
	}

    escape:
	t_unlock_cpu();

    exit:
	LOG_SND_MBF_LEAVE(ercd);
	return(ercd);
}

#endif /* __tsnd_mbf */

/*
 *  メッセージバッファからの受信
 */
#ifdef __rcv_mbf
SYSCALL ER_UINT
rcv_mbf(ID mbfid, VP msg)
{
	MBFCB	*mbfcb;
	TCB	*tcb;
	ER_UINT	ercd;
	UINT	msgsz;
	BOOL	stat;

	LOG_RCV_MBF_ENTER(mbfid);
	CHECK_DISPATCH();

	CHECK_MBFID(mbfid);
	mbfcb = get_mbfcb(mbfid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mbfcb);

	/*
	 * メッセージバッファにデータがあったなら、即座にリターン。
	 * コピー処理はread_mbf_bufferで内すでに終わっている。
	 * データがなければ、送信待ち行列を検索。
	 * 送信待ち行列も空なら、受信待ちへ。
	 * 送信待ちタスクがあるなら、コピーして待ち解除。
	 */
	stat = read_mbf_buffer(mbfid, msg, &msgsz);
	if (stat) {
		ercd = msgsz;
	} else {
		if (queue_empty(&(mbfcb->wait_queue))) {
			/*
			 *  make waiting.
			 */
			WINFO_MBF winfo;
			winfo.msg = msg;
			winfo.msgsz = msgsz;
			runtsk->tstat = (TS_WAITING | TS_WAIT_WOBJ);
			make_wait(&(winfo.winfo));
			queue_insert_prev(&(mbfcb->rwait_queue),
						  &(runtsk->task_queue));
			winfo.wobjcb = (WOBJCB *)mbfcb;
			LOG_TSKSTAT(runtsk);
			dispatch();
			ercd = winfo.winfo.wercd;
			if (ercd == E_OK) {
				ercd = winfo.msgsz;
			}
		} else {
			WINFO_MBF *winfo;
			tcb = (TCB *) queue_delete_next(&(mbfcb->wait_queue));
			winfo = (WINFO_MBF *)(tcb->winfo);
			memcpy(msg, winfo->msg, winfo->msgsz);
			ercd = winfo->msgsz;
			if (wait_complete(tcb)) {
				dispatch();
			}
		}
	}

	/* 
	 * 　これまでの処理が正常、かつ送信待ちタスクがあるならば、
	 * 　メッセージバッファへの書き込みに挑戦。受信によってメッセージバッファに
	 * 　余裕ができているかもしれないので。書込めれば待ち解除。
	 *  これは送信待ち行列が空になるか、メッセージバッファが一杯になるまで
	 * 　続く。
	 */
	if (ercd >= 0) {
		while (!queue_empty(&(mbfcb->wait_queue))) {
			BOOL stat;
			WINFO_MBF *winfo;

			tcb = (TCB *)(mbfcb->wait_queue.next);
			winfo = (WINFO_MBF *)(tcb->winfo);
			stat = write_mbf_buffer(mbfid, winfo->msg, winfo->msgsz);
			if (stat) {
				queue_delete_next(&(mbfcb->wait_queue));
				if (wait_complete(tcb)) {
					dispatch();
				}
			} else {
				break;
			}
		}
	}

	t_unlock_cpu();

    exit:
	LOG_RCV_MBF_LEAVE(ercd);
	return(ercd);
}

#endif /* __rcv_mbf */

/*
 *  メッセージバッファからの受信（ポーリング）
 */
#ifdef __prcv_mbf
SYSCALL ER_UINT
prcv_mbf(ID mbfid, VP msg)
{
	MBFCB	*mbfcb;
	TCB	*tcb;
	ER_UINT	ercd;
	UINT	msgsz;
	BOOL	stat;

	LOG_RCV_MBF_ENTER(mbfid);
	CHECK_DISPATCH();

	CHECK_MBFID(mbfid);
	mbfcb = get_mbfcb(mbfid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mbfcb);

	/*
	 * メッセージバッファにデータがあったなら、即座にリターン。
	 * コピー処理はread_mbf_bufferで内すでに終わっている。
	 * データがなければ、タイムアウトエラー。
	 */
	stat = read_mbf_buffer(mbfid, msg, &msgsz);
	if (stat) {
		ercd = msgsz;
	} else {
		ercd = E_TMOUT;
		goto escape;
	}

	/* 
	 * 　これまでの処理が正常、かつ送信待ちタスクがあるならば、
	 * 　メッセージバッファへの書き込みに挑戦。受信によってメッセージバッファに
	 * 　余裕ができているかもしれないので。書込めれば待ち解除。
	 *  これは送信待ち行列が空になるか、メッセージバッファが一杯になるまで
	 * 　続く。
	 */
	if (ercd >= E_OK) {
		while (!queue_empty(&(mbfcb->wait_queue))) {
			BOOL stat;
			WINFO_MBF *winfo;

			tcb = (TCB *)(mbfcb->wait_queue.next);
			winfo = (WINFO_MBF *)(tcb->winfo);
			stat = write_mbf_buffer(mbfid, winfo->msg, winfo->msgsz);
			if (stat) {
				queue_delete_next(&(mbfcb->wait_queue));
				if (wait_complete(tcb)) {
					dispatch();
				}
			} else {
				break;
			}
		}
	}

    escape:
	t_unlock_cpu();

    exit:
	LOG_RCV_MBF_LEAVE(ercd);
	return(ercd);
}

#endif /* __prcv_mbf */

/*
 *  メッセージバッファからの受信（タイムアウトあり）
 */
#ifdef __trcv_mbf
SYSCALL ER_UINT
trcv_mbf(ID mbfid, VP msg, TMO tmout)
{
	MBFCB	*mbfcb;
	TCB	*tcb;
	TMEVTB	tmevtb;
	ER_UINT	ercd;
	UINT	msgsz;
	BOOL	stat;

	LOG_RCV_MBF_ENTER(mbfid);
	CHECK_DISPATCH();
	CHECK_TMOUT(tmout);

	CHECK_MBFID(mbfid);
	mbfcb = get_mbfcb(mbfid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mbfcb);

	/*
	 * メッセージバッファにデータがあったなら、即座にリターン。
	 * コピー処理はread_mbf_bufferで内すでに終わっている。
	 * データがなければ、送信待ち行列を検索。
	 * 送信待ち行列も空なら、受信待ちへ。
	 * 送信待ちタスクがあるなら、コピーして待ち解除。
	 */
	stat = read_mbf_buffer(mbfid, msg, &msgsz);
	if (stat) {
		ercd = msgsz;
	} else {
		if (queue_empty(&(mbfcb->wait_queue))) {
			if (tmout == TMO_POL) {
				ercd = E_TMOUT;
			} else {
				WINFO_MBF winfo;
				winfo.msg = msg;
				winfo.msgsz = msgsz;
				runtsk->tstat = (TS_WAITING | TS_WAIT_WOBJ);
				make_wait_tmout(&(winfo.winfo), &tmevtb, tmout);
				queue_insert_prev(&(mbfcb->rwait_queue),
						  &(runtsk->task_queue));
				winfo.wobjcb = (WOBJCB *)mbfcb;
				LOG_TSKSTAT(runtsk);
				dispatch();
				ercd = winfo.winfo.wercd;
				if (ercd == E_OK) {
					ercd = winfo.msgsz;
				}
			}
		} else {
			WINFO_MBF *winfo;
			tcb = (TCB *) queue_delete_next(&(mbfcb->wait_queue));
			winfo = (WINFO_MBF *)(tcb->winfo);
			memcpy(msg, winfo->msg, winfo->msgsz);
			ercd = winfo->msgsz;
			if (wait_complete(tcb)) {
				dispatch();
			}
		}
	}

	/* 
	 * 　これまでの処理が正常、かつ送信待ちタスクがあるならば、
	 * 　メッセージバッファへの書き込みに挑戦。受信によってメッセージバッファに
	 * 　余裕ができているかもしれないので。書込めれば待ち解除。
	 *  これは送信待ち行列が空になるか、メッセージバッファが一杯になるまで
	 * 　続く。
	 */
	if (ercd > E_OK) {
		while (!queue_empty(&(mbfcb->wait_queue))) {
			BOOL stat;
			WINFO_MBF *winfo;

			tcb = (TCB *)(mbfcb->wait_queue.next);
			winfo = (WINFO_MBF *)(tcb->winfo);
			stat = write_mbf_buffer(mbfid, winfo->msg, winfo->msgsz);
			if (stat) {
				queue_delete_next(&(mbfcb->wait_queue));
				if (wait_complete(tcb)) {
					dispatch();
				}
			} else {
				break;
			}
		}
	}

	t_unlock_cpu();

    exit:
	LOG_RCV_MBF_LEAVE(ercd);
	return(ercd);
}

#endif /* __trcv_mbf */

/*
 *  メッセージバッファの状態参照
 */
#ifdef __ref_mbf
SYSCALL ER
ref_mbf(ID mbfid, T_RMBF *pk_rmbf)
{
	TCB *tcb;
	MBFCB	*mbfcb;
	ER	ercd;
	UINT	head;
	UINT	tail;
	UB	*mbf;
	UINT	mbfsz;
	UINT	i;
	UINT	free;

	LOG_REF_MBF_ENTER(mbfid);
	CHECK_TSKCTX_UNL();

	CHECK_MBFID(mbfid);
	mbfcb = get_mbfcb(mbfid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mbfcb);

	if (!queue_empty(&(mbfcb->wait_queue))) {
		tcb = (TCB *)queue_peek_next(&(mbfcb->wait_queue));
		pk_rmbf->stskid = TSKID(tcb);
	} else {
		pk_rmbf->stskid = TSK_NONE;
	}
	if (!queue_empty(&(mbfcb->rwait_queue))) {
		tcb = (TCB *)queue_peek_next(&(mbfcb->rwait_queue));
		pk_rmbf->rtskid = TSKID(tcb);
	} else {
		pk_rmbf->rtskid = TSK_NONE;
	}

	mbf = mbfcb->mbfinib->mbf;
	mbfsz = mbfcb->mbfinib->mbfsz;
	free = mbfsz;
	head = mbfcb->head;
	tail = mbfcb->tail;

	pk_rmbf->smsgcnt = 0;
	for (i = tail; i != head; pk_rmbf->smsgcnt++) {
		UINT size;
		size = mbf[i] << 8;
		size |= mbf[i + 1];
		i = (i + size + 2) % mbfsz; /* +2 はヘッダの分 */
		free -= size + 2;
	}

	if (free < 2) {
		free = 0;
	} else {
		free -= 2;
	}
	pk_rmbf->fmbfsz = free;

	ercd = E_OK;

	t_unlock_cpu();

    exit:
	LOG_REF_MBF_LEAVE(ercd);
	return(ercd);
}

#endif /* __ref_mbf */


