/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2003-2004 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/task_manage.c 6101 2006-09-10T08:00:54.989588Z monaka  $
 */

/*
 *	タスク管理機能
 */

#include "fi4_kernel.h"
#include "check.h"
#include "task.h"
#include "wait.h"
#include "bufmgr.h"

/*
 *  ID自動割当て用フリーリスト
 */
extern QUEUE tsk_freelist;


/*
 *  タスクの生成
 */
#ifdef __cre_tsk

SYSCALL ER
cre_tsk(ID tskid, const T_CTSK *pk_ctsk)
{
	TCB	*tcb;
	TINIB *tinib;
	ER	ercd;
	VP	stk = NULL;
	BOOL	disp = FALSE;

	LOG_CRE_TSK_ENTER(tskid, pk_ctsk);
	CHECK_TSKCTX_UNL();

	CHECK_TSKID(tskid);
	CHECK_ATTRIBUTE(pk_ctsk->tskatr, TA_HLNG | TA_ASM | TA_ACT);
	if (TMIN_TPRI > pk_ctsk->itskpri || pk_ctsk->itskpri > TMAX_TPRI) {
		return E_PAR;
	}

	tcb = get_tcb(tskid);
	CHECK_TASK_CREATABLE(tcb);
	stk = pk_ctsk->stk;
	if (stk == NULL) {
		BOOL stat;
		bufmgr_allocate(&stat, pk_ctsk->stksz, &stk);
		if (!stat) {
			tcb->exist = KOBJ_NOEXS;
			ercd = E_NOMEM;
			goto exit;
		}
	}
	
	UPDATE_OBJECT_FREELIST(tcb);
	
	/*
	 * 初期設定を行う。
	 * tcbより先にtinibの初期化が必要。
	 */
	tinib = tcb->tinib;
	tinib->tskatr = pk_ctsk->tskatr;
	tinib->exinf = pk_ctsk->exinf;
	tinib->task = pk_ctsk->task;
	tinib->stksz = pk_ctsk->stksz;
	tinib->stk = stk;
	tinib->ipriority = INT_PRIORITY(pk_ctsk->itskpri);

	tcb->actcnt = FALSE;

	/*
	 *  もしかしたら make_dormant()も
	 *  t_lock_cpu()の中に入れるのは
	 *  冗長かもしれないけれど、念のため。
	 */
	t_lock_cpu();
	make_dormant(tcb);
	if ((tcb->tinib->tskatr & TA_ACT) != 0) {
		/*
		 *  make_active内部にqueueへのアクセスが
		 *  あるため、CPUロックは必須。
		 */
		disp = make_active(tcb);
	}
	t_unlock_cpu();

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	tcb->exist = KOBJ_EXIST;

	ercd = E_OK;

	t_lock_cpu();
	/*
	 *  disp == TRUE になるのは TA_ACT 指定のときのみ
	 *  であるため、ここで改めてチェックする必要は無い。
	 */  
	if (disp) {
		dispatch();
	}
	t_unlock_cpu();

    exit:
	LOG_CRE_TSK_LEAVE(ercd);
	return(ercd);
}

#endif /* __cre_tsk */

/*
 *  タスクの生成 (ID番号自動割付け)
 */
#ifdef __acre_tsk

SYSCALL ER_ID
acre_tsk(const T_CTSK *pk_ctsk)
{
	TCB	*tcb;
	TINIB	*tinib;
	ER_ID	ercd;
	VP stk;
	BOOL	disp = FALSE;

	LOG_ACRE_TSK_ENTER(pk_ctsk);
	CHECK_TSKCTX_UNL();

	if (TNUM_TSK == 0) {
		ercd = E_NOID;
		goto exit;
	}
	CHECK_ATTRIBUTE(pk_ctsk->tskatr, TA_HLNG | TA_ASM | TA_ACT);
	if (TMIN_TPRI > pk_ctsk->itskpri || pk_ctsk->itskpri > TMAX_TPRI) {
		return E_PAR;
	}

	if (pk_ctsk->stk == NULL) {
		BOOL stat;
		bufmgr_allocate(&stat, pk_ctsk->stksz, &stk);
		if (stat == FALSE) {
			ercd = E_NOMEM;
			goto exit;
		}
	} else {
		stk = pk_ctsk->stk;
	}
	
	t_lock_cpu();
	tcb = (TCB *)t_get_free_id((QUEUE *)&tsk_freelist);
	if (tcb == NULL) {
		ercd = E_NOID;
		t_unlock_cpu();
		goto exit;
	}
	tcb->exist = KOBJ_INIT;
	t_unlock_cpu();

	/*
	 * 初期設定を行う。
	 * tcbより先にtinibの初期化が必要。
	 */
	tinib = tcb->tinib;
	tinib->tskatr = pk_ctsk->tskatr;
	tinib->exinf = pk_ctsk->exinf;
	tinib->task = pk_ctsk->task;
	tinib->stk = stk;
	tinib->stksz = pk_ctsk->stksz;
	tinib->ipriority = INT_PRIORITY(pk_ctsk->itskpri);

	tcb->actcnt = FALSE;

	/*
	 * もしかしたら t_lock_cpu()の呼出しは
	 * 冗長かもしれないけれど、念のため。
	 */
	t_lock_cpu();
	make_dormant(tcb);
	if ((tcb->tinib->tskatr & TA_ACT) != 0) {
		disp = make_active(tcb);
	}
	t_unlock_cpu();

	ercd = TSKID(tcb);

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	tcb->exist = KOBJ_EXIST;

	t_lock_cpu();
	/*
	 *  disp == TRUE になるのは TA_ACT 指定のときのみ
	 *  であるため、ここで改めてチェックする必要は無い。
	 */  
	if (disp) {
		dispatch();
	}
	t_unlock_cpu();

    exit:
	if (ercd < E_OK) {
		BOOL stat;
		stat = bufmgr_release(stk);
		if (stat == FALSE) {
			ercd = E_SYS;
		}
	}
	LOG_ACRE_TSK_LEAVE(ercd);
	return(ercd);
}

#endif /* __acre_tsk */

/*
 *  タスクの削除
 */
#ifdef __del_tsk

SYSCALL ER
del_tsk(ID tskid)
{
	TCB	*tcb;
	ER	ercd;
	BOOL	stat;
    
	LOG_DEL_TSK_ENTER(tskid);
	CHECK_TSKCTX_UNL();

	/*
	 * ここでは、自タスクの指定を許す。
	 * しかし、休止状態ではないために
	 * E_OBJエラーを返す。
	 */
	CHECK_TSKID_SELF(tskid);
	tcb = get_tcb_self(tskid);

	CHECK_OBJECT_DELETABLE(tcb);

	t_lock_cpu();
	if (!TSTAT_DORMANT(tcb->tstat)) {
		tcb->exist = KOBJ_EXIST;
		ercd = E_OBJ;
		t_unlock_cpu();
		goto exit;
	}
	t_unlock_cpu();

	/*
	 *  スタック領域を解放する。
	 */
	stat = bufmgr_release(tcb->tinib->stk);
	if (stat) {
		ercd = E_OK;
	} else {
		ercd = E_SYS;
	}

	/*
	 * 未登録状態へ。
	 */
	if (!CHECK_OBJECT_RESERVED(tcb)) {
	  /*
	   * 予約IDでなければフリーリストに入れる。
	   */
		t_lock_cpu();
		queue_insert_prev(&tsk_freelist, (QUEUE *)tcb);
		t_unlock_cpu();
	}

	/* ここから先はacre_/cre_でのアクセスが可能 */
	tcb->exist = KOBJ_NOEXS;

    exit:
	LOG_DEL_TSK_LEAVE(ercd);
	return(ercd);
}

#endif /* __del_tsk */

/*
 *  タスクの起動
 */
#ifdef __act_tsk

SYSCALL ER
act_tsk(ID tskid)
{
	TCB	*tcb;
	ER	ercd;

	LOG_ACT_TSK_ENTER(tskid);
	CHECK_TSKCTX_UNL();

	CHECK_TSKID_SELF(tskid);
	tcb = get_tcb_self(tskid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(tcb);

	if (TSTAT_DORMANT(tcb->tstat)) {
		if (make_active(tcb)) {
			dispatch();
		}
		ercd = E_OK;
	}
	else if (!(tcb->actcnt)) {
		tcb->actcnt = TRUE;
		ercd = E_OK;
	}
	else {
		ercd = E_QOVR;
	}

	t_unlock_cpu();

    exit:
	LOG_ACT_TSK_LEAVE(ercd);
	return(ercd);
}

#endif /* __act_tsk */

/*
 *  タスクの起動（非タスクコンテキスト用）
 */
#ifdef __iact_tsk

SYSCALL ER
iact_tsk(ID tskid)
{
	TCB	*tcb;
	ER	ercd;

	LOG_IACT_TSK_ENTER(tskid);
	CHECK_INTCTX_UNL();

	CHECK_TSKID(tskid);
	tcb = get_tcb(tskid);

	i_lock_cpu();
	I_CHECK_OBJECT_EXIST(tcb);

	if (TSTAT_DORMANT(tcb->tstat)) {
		if (make_active(tcb)) {
			reqflg = TRUE;
		}
		ercd = E_OK;
	}
	else if (!(tcb->actcnt)) {
		tcb->actcnt = TRUE;
		ercd = E_OK;
	}
	else {
		ercd = E_QOVR;
	}

	i_unlock_cpu();

    exit:
	LOG_IACT_TSK_LEAVE(ercd);
	return(ercd);
}

#endif /* __iact_tsk */

/*
 *  タスク起動要求のキャンセル
 */
#ifdef __can_act

SYSCALL ER_UINT
can_act(ID tskid)
{
	TCB	*tcb;
	ER_UINT	ercd;

	LOG_CAN_ACT_ENTER(tskid);
	CHECK_TSKCTX_UNL();

	CHECK_TSKID_SELF(tskid);
	tcb = get_tcb_self(tskid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(tcb);

	ercd = tcb->actcnt ? 1 : 0;
	tcb->actcnt = FALSE;

	t_unlock_cpu();

    exit:
	LOG_CAN_ACT_LEAVE(ercd);
	return(ercd);
}

#endif /* __can_act */

/*
 *  タスクの起動（起動コード指定）
 */
#ifdef __sta_tsk

#include "cpu_context.h"

SYSCALL ER
sta_tsk(ID tskid, VP_INT stacd)
{
	ER	ercd;
	TCB	*tcb;
	LOG_STA_TSK_ENTER(tskid);
	CHECK_TSKCTX_UNL();

	/*
	 *  ここではTSK_SELFを許す。
	 *  しかし、この後のDORMANT状態チェックで
	 *  E_OBJとなる。
	 */
	CHECK_TSKID_SELF(tskid);
	tcb = get_tcb_self(tskid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(tcb);

	if (TSTAT_DORMANT(tcb->tstat)) {
		/*
		 * タスクのベース優先度は、タスクの起動時にタスクの起動時優先度に初期化する。
		 * (ITRON4.0仕様書 4.1節)
		 */ 
		tcb->priority = tcb->tinib->ipriority;
		
		activate_context_with_stacd(tcb, stacd);
		if (make_runnable(tcb)) {
//			reqflg = TRUE;
			dispatch();                 /* [04/Aug/2004 AIC] 変更 */
		}
		ercd = E_OK;
	} else {
		ercd = E_OBJ;
	}

	t_unlock_cpu();
    exit:
	LOG_STA_TSK_LEAVE(ercd);
	return(ercd);
}

#endif /* __sta_tsk */

/*
 *  自タスクの終了の内部処理ルーチン
 *  ext_tskとexd_tskで共有する。
 */
#ifdef __prepexttsk
void
prepare_exit_task(void)
{
#ifdef ACTIVATED_STACK_SIZE
	/*
	 *  create_context と activate_context で，使用中のスタック領
	 *  域を破壊しないように，スタック上にダミー領域を確保する．
	 */
	(void) alloca(ACTIVATED_STACK_SIZE);
#endif /* ACTIVATED_STACK_SIZE */

	if (sense_context()) {
		/*
		 *  非タスクコンテキストから ext_tsk/exd_tsk が
		 *  呼ばれた場合，
		 *  システムログにエラーを記録し，そのまま実行を続ける．
		 *  その結果，強制的にタスクコンテキストに切り換えて，
		 *  実行状態のタスクを終了させることになる．カーネルは
		 *  そのまま実行を継続するが，ターゲットによっては，非
		 *  タスクコンテキスト用のスタックにゴミが残ったり，割
		 *  込みハンドラのネスト数の管理に矛盾が生じたりする場
		 *  合がある．
		 */
		syslog_0(LOG_ALERT,
			"ext_tsk is called from non-task contexts.");
	}
	if (sense_lock()) {
		/*
		 *  CPUロック状態で ext_tsk が呼ばれた場合は，CPUロック
		 *  を解除してからタスクを終了する．実装上は，サービス
		 *  コール内でのCPUロックを省略すればよいだけ．
		 */
		syslog_0(LOG_WARNING,
			"ext_tsk is called from CPU locked state.");
	}
	else {
		if (sense_context()) {
			i_lock_cpu();
		}
		else  {
			t_lock_cpu();
		}
	}
	if (!(enadsp)) {
		/*
		 *  ディスパッチ禁止状態で ext_tsk が呼ばれた場合は，
		 *  ディスパッチ許可状態にしてからタスクを終了する．
		 */
		syslog_0(LOG_WARNING,
			"ext_tsk is called from dispatch disabled state.");
		enadsp = TRUE;
	}
}
#endif /* __prepexttsk */

/*
 *  自タスクの終了
 */
#ifdef __ext_tsk

SYSCALL void
ext_tsk(void)
{
	LOG_EXT_TSK_ENTER();

	prepare_exit_task();
	exit_task();
}

#endif /* __ext_tsk */

/*
 *  自タスクの終了と削除
 */
#ifdef __exd_tsk

#include "mutex.h"
#include "multiqueue.h"

SYSCALL void
exd_tsk(void)
{
	TCB	*tcb;
	MTXCB *mtxcb;

	LOG_EXD_TSK_ENTER();

	prepare_exit_task();

	/*
	 *  以下は、ほぼexit_task()の処理と同等。
	 *  でも、exit_taskは、呼出すと戻ってこないので使えない。
	 *  (See also <BTS:228>) 
	 */

	unlock_all_mutex(runtsk);
	make_non_runnable(runtsk);
	make_dormant(runtsk);
	if (runtsk->actcnt) {
		runtsk->actcnt = FALSE;
		make_active(runtsk);
	} else {
		/*
		 *  actcnt == 0 のときは、回収
		 */
/* Toppers porting F.Okazaki A start　タスクスタック解放漏れ */
		bufmgr_release(runtsk->tinib->stk);
		runtsk->tinib->stk = NULL;
/* Toppers porting F.Okazaki A end */

		if (!CHECK_OBJECT_RESERVED(runtsk)) {
			/*
			 *  予約IDでなければフリーリストに入れる。
			 */
			queue_insert_prev(&tsk_freelist, (QUEUE *)runtsk);
		}
		/*
		 * 未登録状態へ。
		 */
		runtsk->exist = KOBJ_NOEXS;
	}
	exit_and_dispatch();
}

#endif /* __exd_tsk */

/*
 *  タスクの強制終了
 */
#ifdef __ter_tsk

SYSCALL ER
ter_tsk(ID tskid)
{
	TCB	*tcb;
	UINT	tstat;
	ER	ercd;
	BOOL	dspflg = FALSE;     /* [03/Aug/2004 AIC] 追加 */

	LOG_TER_TSK_ENTER(tskid);
	CHECK_TSKCTX_UNL();

	CHECK_TSKID(tskid);
	tcb = get_tcb(tskid);
	CHECK_NONSELF(tcb);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(tcb);

	if (TSTAT_DORMANT(tstat = tcb->tstat)) {
		ercd = E_OBJ;
	}
	else {
		unlock_all_mutex(tcb);
		if (TSTAT_RUNNABLE(tstat)) {
			make_non_runnable(tcb);
		}
		else if (TSTAT_WAITING(tstat)) {
			wait_cancel(tcb);
			if ((tcb->tstat & TS_WAIT_MTX_I) != 0) {
				scan_new_priority(tcb); /* 優先度継承処理 */
			}
		}
		make_dormant(tcb);
		if (tcb->actcnt) {
			tcb->actcnt = FALSE;
			dspflg |= make_active(tcb); /* [03/Aug/2004 AIC] 変更 */
		}
		ercd = E_OK;
		if (dspflg) {           /* [03/Aug/2004 AIC] 追加 */
			dispatch();
		}
	}


	t_unlock_cpu();

    exit:
	LOG_TER_TSK_LEAVE(ercd);
	return(ercd);
}

#endif /* __ter_tsk */

/*
 *  タスク優先度の変更
 */
#ifdef __chg_pri

SYSCALL ER
chg_pri(ID tskid, PRI tskpri)
{
	TCB	*tcb;
	UINT	newpri; /* タスクの新しいベース優先度 */
	UINT	tstat;
	ER	ercd = E_SYS;
	BOOL dispflg = FALSE;
	
	LOG_CHG_PRI_ENTER(tskid, tskpri);
	CHECK_TSKCTX_UNL();
	CHECK_TPRI_INI(tskpri);

	CHECK_TSKID_SELF(tskid);
	tcb = get_tcb_self(tskid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(tcb);

	if (TSTAT_DORMANT(tstat = tcb->tstat)) {
		ercd = E_OBJ;
		goto error;
	}

	if (!check_ceiling_priority(tcb, tskpri)) {
		ercd = E_ILUSE;
		goto error;
	}
	
	if (tskpri == TPRI_INI) {
		/*
		 *  tskpriにTSK_SELF (= 0)が指定されると、
		 *  タスクのベース優先度をタスクの起動時優先度に
		 *  変更する。
		 */
		newpri = tcb->tinib->ipriority;
	} else {
		newpri = INT_PRIORITY(tskpri);
	}

	if (tcb->priority != newpri &&
		tcb->priority == tcb->base_priority) {
		/*
		 *  対象タスクの現在優先度が変化した場合および現在優先度が
		 *  ベース優先度に一致している場合。
		 *  JSPとは違い、上記は常に成り立つとは限らない。
		 */
		if (TSTAT_RUNNABLE(tstat)) {
			if (change_priority(tcb, newpri)) {
				dispflg = TRUE;
			}
			ercd = E_OK;
		}
		else {
			tcb->priority = newpri;
			if ((tstat & TS_WAIT_WOBJCB) != 0) {
				wobj_change_priority(((WINFO_WOBJ *)(tcb->winfo))
								->wobjcb, tcb);
			}
			ercd = E_OK;
		}
	}
	
	dispflg |= inherit_priority(tcb, newpri);
	if (dispflg) {
		dispatch();
	}
	
	error:
	t_unlock_cpu();

    exit:
	LOG_CHG_PRI_LEAVE(ercd);
	return(ercd);
}

#endif /* __chg_pri */

/*
 *  タスク優先度の参照
 */
#ifdef __get_pri

SYSCALL ER
get_pri(ID tskid, PRI *p_tskpri)
{
	TCB	*tcb;
	ER	ercd;

	LOG_GET_PRI_ENTER(tskid, p_tskpri);
	CHECK_TSKCTX_UNL();

	CHECK_TSKID_SELF(tskid);
	tcb = get_tcb_self(tskid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(tcb);

	if (TSTAT_DORMANT(tcb->tstat)) {
		ercd = E_OBJ;
	}
	else {
		*p_tskpri = EXT_TSKPRI(tcb->priority);
		ercd = E_OK;
	}

	t_unlock_cpu();

    exit:
	LOG_GET_PRI_LEAVE(ercd, *p_tskpri);
	return(ercd);
}

#endif /* __get_pri */

/*
 *  タスクの状態参照
 */
#ifdef __ref_tsk

SYSCALL ER
ref_tsk(ID tskid, T_RTSK *pk_rtsk)
{
	TCB	*tcb;
	ER	ercd;
	UINT	tstat;

	LOG_REF_TSK_ENTER(tskid);

	CHECK_TSKID_SELF(tskid);
	tcb = get_tcb_self(tskid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(tcb);

	tstat = tcb->tstat;
	if (TSTAT_RUNNABLE(tstat)) {
		if (tcb == runtsk) {
			pk_rtsk->tskstat = TTS_RUN;
		} else pk_rtsk->tskstat = TTS_RDY;
	} else {
		if (TSTAT_WAIT_SUSPENDED(tstat)) {
			pk_rtsk->tskstat = TTS_WAS;
		} else if (TSTAT_WAITING(tstat)) {
			pk_rtsk->tskstat = TTS_WAI;
		} else if (TSTAT_SUSPENDED(tstat)) {
			pk_rtsk->tskstat = TTS_SUS;
		} else if (TSTAT_DORMANT(tstat)) {
			pk_rtsk->tskstat = TTS_DMT;
		}
	}

	pk_rtsk->tskpri = EXT_TSKPRI(tcb->priority);
	pk_rtsk->tskbpri = EXT_TSKPRI(tcb->base_priority);
	/* TODO: tskwait, wobjid */
	/* pk_rtsk->lefttmo = tmevt_heap[tcb->winfo->tmevtb->index] - current_time; */
	pk_rtsk->actcnt = tcb->actcnt;
	pk_rtsk->wupcnt = tcb->wupcnt;
	if (TSTAT_SUSPENDED(tstat)) {
		pk_rtsk->suscnt = 1;
	} else {
		pk_rtsk->suscnt = 0;
	}

	ercd = E_OK;

	t_unlock_cpu();

    exit:
	LOG_REF_TSK_LEAVE(ercd);
	return(ercd);
}

#endif /* __ref_tsk */

/*
 *  タスクの状態参照（簡易版）
 */
#ifdef __ref_tst

SYSCALL ER
ref_tst(ID tskid, T_RTST *pk_rtst)
{
	TCB	*tcb;
	ER	ercd;
	UINT	tstat;

	LOG_REF_TST_ENTER(tskid);
	CHECK_TSKCTX_UNL();

	CHECK_TSKID_SELF(tskid);
	tcb = get_tcb_self(tskid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(tcb);

	tstat = tcb->tstat;
	if (TSTAT_RUNNABLE(tstat)) {
		if (tcb == runtsk) {
			pk_rtst->tskstat = TTS_RUN;
		} else pk_rtst->tskstat = TTS_RDY;
	} else {
		if (TSTAT_WAIT_SUSPENDED(tstat)) {
			pk_rtst->tskstat = TTS_WAS;
		} else if (TSTAT_WAITING(tstat)) {
			pk_rtst->tskstat = TTS_WAI;
		} else if (TSTAT_SUSPENDED(tstat)) {
			pk_rtst->tskstat = TTS_SUS;
		} else if (TSTAT_DORMANT(tstat)) {
			pk_rtst->tskstat = TTS_DMT;
		}
	}

	/* TODO: pk_rtst->tskwait = tcb-> */

	ercd = E_OK;

	t_unlock_cpu();

    exit:
	LOG_REF_TST_LEAVE(ercd);
	return(ercd);
}

#endif /* __ref_tsk */

