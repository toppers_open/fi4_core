/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2000-2006 by Muranaka Masaki
 *  Copyright (C) 2003-2006 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/queue.h 6183 2006-11-05T03:43:32.794177Z monaka  $
 */

/*
 *	キュー操作ライブラリ
 *
 *  このキュー操作ライブラリでは，キューヘッダを含むリング構造のダブル
 *  リンクキューを扱う．具体的には，キューヘッダの次エントリはキューの
 *  先頭のエントリ，前エントリはキューの末尾のエントリとする．また，キ
 *  ューの先頭のエントリの前エントリと，キューの末尾のエントリの次エン
 *  トリは，キューヘッダとする．空のキューは，次エントリ，前エントリと
 *  も自分自身を指すキューヘッダであらわす．
 */

#ifndef TOPPERS_QUEUE_H
#define TOPPERS_QUEUE_H

/*
 *  キューのデータ構造の定義
 */
typedef struct queue {
	struct queue *next;		/* 次エントリへのポインタ */
	struct queue *prev;		/* 前エントリへのポインタ */
} QUEUE;

/*
 *  キューの初期化
 *
 *  queue にはキューヘッダを指定する．
 */
Inline void
queue_initialize(QUEUE *queue)
{
	queue->prev = queue->next = queue;
}

/*
 *  キューの前エントリへの挿入
 *
 *  queue の前に entry を挿入する．queue にキューヘッダを指定した場合
 *  には，キューの末尾に entry を挿入することになる．
 */
Inline void
queue_insert_prev(QUEUE *queue, QUEUE *entry)
{
	entry->prev = queue->prev;
	entry->next = queue;
	queue->prev->next = entry;
	queue->prev = entry;
}

/*
 *  エントリの削除
 *
 *  entry をキューから削除する．
 */
Inline void
queue_delete(QUEUE *entry)
{
	entry->prev->next = entry->next;
	entry->next->prev = entry->prev;
}

/*
 *  キューの列挙
 *
 *  引数topをキューとするエントリを先頭から列挙する。
 *  最初の呼出しでは、entryにNULLを渡す。返値はtopの次のエントリ(つまり先頭)である。
 *  次回以降は前回に得たエントリをentryに渡す。返値はentryの次のエントリとなる。
 *  entryの次のエントリがtopだったとき、NULLを返値として、キューの終端をあらわす。
 *  topのNULLチェックやentryの妥当性検証は行っていない。呼出し側の責任で行うこと。
 *  また、言うまでもないが、スレッドセーフにはなりえない。列挙ループ中は排他は
 *  呼出し側の責任で行うこと。
 */
Inline QUEUE *
queue_enumerate(QUEUE *top, QUEUE *entry)
{
	QUEUE *result;
	if (entry == NULL) {
		result = top->next;
	} else {
		result = entry->next;
	}
	if (result == top) {
		return NULL;
	}
	return result;
}

/*
 *  キューの次エントリの取出し
 *
 *  queue の次エントリをキューから削除し，削除したエントリを返す．queue
 *  にキューヘッダを指定した場合には，キューの先頭のエントリを取り出す
 *  ことになる．queue に空のキューを指定して呼び出してはならない．
 */
Inline QUEUE *
queue_delete_next(QUEUE *queue)
{
	QUEUE	*entry;

	assert(queue->next != queue);
	entry = queue->next;
	queue->next = entry->next;
	entry->next->prev = queue;
	return(entry);
}

/*
 *  キューの次エントリのポインタを取得
 *
 *  queue の次エントリを返す．queue にキューヘッダを指定した場合には，
 *  キューの先頭のエントリを取り出すことになる．queue に空のキューを
 *  指定して呼び出してはならない．
 */
Inline QUEUE *
queue_peek_next(QUEUE *queue)
{
	assert(queue->next != queue);

	return(queue->next);
}

/*
 *  キューが空かどうかのチェック
 *
 *  queue にはキューヘッダを指定する．
 */
Inline BOOL
queue_empty(QUEUE *queue)
{
	if (queue->next == queue) {
		assert(queue->prev == queue);
		return(TRUE);
	}
	return(FALSE);
}

#endif /* TOPPERS_QUEUE_H */
