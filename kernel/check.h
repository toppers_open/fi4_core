/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2003-2006 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/check.h 6183 2006-11-05T03:43:32.794177Z monaka  $
 */

/*
 *	エラーチェック用マクロ
 */

#ifndef TOPPERS_CHECK_H
#define TOPPERS_CHECK_H

/*
 *  優先度の範囲の判定
 */
#define VALID_TPRI(tpri) \
	(TMIN_TPRI <= (tpri) && (tpri) <= TMAX_TPRI)

/*
 *  値が0でないことをチェック (E_PAR)
 */
#define CHECK_NOT_ZERO(value) { \
	if ((value) == 0) { \
		ercd = E_PAR; \
		goto exit; \
	} \
}


/*
 *  メッセージ優先度のチェック（E_PAR）
 */
#define VALID_MPRI(mpri) \
((mpri) != 0 && (TMIN_MPRI <= (mpri) && (mpri) <= TMAX_MPRI))

#define CHECK_MPRI(mpri) { \
	if (!VALID_MPRI(mpri)) { \
		ercd = E_PAR; \
		goto exit; \
	} \
}

/*
 *  タスク優先度のチェック（E_PAR）
 */
#define CHECK_TPRI(tpri) {					\
	if (!VALID_TPRI(tpri)) {				\
		ercd = E_PAR;					\
		goto exit;					\
	}							\
}

#define CHECK_TPRI_INI(tpri) {					\
	if (!(VALID_TPRI(tpri) || (tpri) == TPRI_INI)) {	\
		ercd = E_PAR;					\
		goto exit;					\
	}							\
}

#define CHECK_TPRI_SELF(tpri) {					\
	if (!(VALID_TPRI(tpri) || (tpri) == TPRI_SELF)) {	\
		ercd = E_PAR;					\
		goto exit;					\
	}							\
}

/*
 *  タイムアウト指定値のチェック（E_PAR）
 */
#define CHECK_TMOUT(tmout) {					\
	if (!(TMO_FEVR <= (tmout))) {				\
		ercd = E_PAR;					\
		goto exit;					\
	}							\
}

/*
 *  その他のパラメータエラーのチェック（E_PAR）
 */
#define CHECK_PAR(exp) {					\
	if (!(exp)) {						\
		ercd = E_PAR;					\
		goto exit;					\
	}							\
}

#define CHECK_ATTRIBUTE(atr, flg) {				\
	if (((atr) & ~(flg)) != 0) {				\
		ercd = E_RSATR;					\
		goto exit;					\
	}							\
}

/*
 *  フリーリストに割当て可能IDがあるかどうかのチェック
 *  割当可能なコントロールブロックがフリーリストに無ければ、NULLを返す。
 *  返値がNULLのとき、サービスコールは E_NOID で抜けることが期待される。
 *  CPUのロックは行わないので、サービスコールの責任で行うこと。
 */
#define t_get_free_id(list) \
	(queue_empty((list)) ?					\
	 NULL : queue_delete_next((list)))			\

/*
 *  オブジェクトIDの範囲の判定
 */
#define VALID_TSKID(tskid) \
	(TMIN_TSKID <= (tskid) && (tskid) <= tmax_tskid)

#define VALID_SEMID(semid) \
	(TMIN_SEMID <= (semid) && (semid) <= tmax_semid)

#define VALID_FLGID(flgid) \
	(TMIN_FLGID <= (flgid) && (flgid) <= tmax_flgid)

#define VALID_DTQID(dtqid) \
	(TMIN_DTQID <= (dtqid) && (dtqid) <= tmax_dtqid)

#define VALID_MBXID(mbxid) \
	(TMIN_MBXID <= (mbxid) && (mbxid) <= tmax_mbxid)

#define VALID_MPFID(mpfid) \
	(TMIN_MPFID <= (mpfid) && (mpfid) <= tmax_mpfid)

#define VALID_MPLID(mplid) \
	(TMIN_MPLID <= (mplid) && (mplid) <= tmax_mplid)

#define VALID_MTXID(mtxid) \
	(TMIN_MTXID <= (mtxid) && (mtxid) <= tmax_mtxid)

#define VALID_MBFID(mbfid) \
	(TMIN_MBFID <= (mbfid) && (mbfid) <= tmax_mbfid)

#define VALID_PORID(porid) \
	(TMIN_PORID <= (porid) && (porid) <= tmax_porid)

#define VALID_CYCID(cycid) \
	(TMIN_CYCID <= (cycid) && (cycid) <= tmax_cycid)

#define VALID_ALMID(almid) \
	(TMIN_ALMID <= (almid) && (almid) <= tmax_almid)

#define VALID_ISRID(almid) \
	(TMIN_ISRID <= (isrid) && (isrid) <= tmax_isrid)

/*
 *  オブジェクトIDのチェック（E_ID）
 */
#define CHECK_TSKID(tskid) {					\
	if (!VALID_TSKID(tskid)) {				\
		ercd = E_ID;					\
		goto exit;					\
	}							\
}

#define CHECK_TSKID_SELF(tskid) {				\
	if (!(VALID_TSKID(tskid) || (tskid) == TSK_SELF)) {	\
		ercd = E_ID;					\
		goto exit;					\
	}							\
}

#define CHECK_SEMID(semid) {					\
	if (!VALID_SEMID(semid)) {				\
		ercd = E_ID;					\
		goto exit;					\
	}							\
}

#define CHECK_FLGID(flgid) {					\
	if (!VALID_FLGID(flgid)) {				\
		ercd = E_ID;					\
		goto exit;					\
	}							\
}

#define CHECK_DTQID(dtqid) {					\
	if (!VALID_DTQID(dtqid)) {				\
		ercd = E_ID;					\
		goto exit;					\
	}							\
}

#define CHECK_MBXID(mbxid) {					\
	if (!VALID_MBXID(mbxid)) {				\
		ercd = E_ID;					\
		goto exit;					\
	}							\
}

#define CHECK_MPFID(mpfid) {					\
	if (!VALID_MPFID(mpfid)) {				\
		ercd = E_ID;					\
		goto exit;					\
	}							\
}

#define CHECK_MPLID(mplid) {					\
	if (!VALID_MPLID(mplid)) {				\
		ercd = E_ID;					\
		goto exit;					\
	}							\
}

#define CHECK_MTXID(mtxid) {					\
	if (!VALID_MTXID(mtxid)) {				\
		ercd = E_ID;					\
		goto exit;					\
	}							\
}

#define CHECK_MBFID(mbfid) {					\
	if (!VALID_MBFID(mbfid)) {				\
		ercd = E_ID;					\
		goto exit;					\
	}							\
}

#define CHECK_PORID(porid) {					\
	if (!VALID_PORID(porid)) {				\
		ercd = E_ID;					\
		goto exit;					\
	}							\
}

#define CHECK_CYCID(cycid) {					\
	if (!VALID_CYCID(cycid)) {				\
		ercd = E_ID;					\
		goto exit;					\
	}							\
}

#define CHECK_ALMID(almid) {					\
	if (!VALID_ALMID(almid)) {				\
		ercd = E_ID;					\
		goto exit;					\
	}							\
}

#define CHECK_ISRID(isrid) {					\
	if (!VALID_ISRID(isrid)) {				\
		ercd = E_ID;					\
		goto exit;					\
	}							\
}

/*
 *  呼出しコンテキストのチェック（E_CTX）
 */
#define CHECK_TSKCTX() {					\
	if (sense_context()) {					\
		ercd = E_CTX;					\
		goto exit;					\
	}							\
}

#define CHECK_INTCTX() {					\
	if (!sense_context()) {					\
		ercd = E_CTX;					\
		goto exit;					\
	}							\
}

/*
 *  呼出しコンテキストとCPUロック状態のチェック（E_CTX）
 */
#define CHECK_TSKCTX_UNL() {					\
	if (sense_context() || t_sense_lock()) {		\
		ercd = E_CTX;					\
		goto exit;					\
	}							\
}

#define CHECK_INTCTX_UNL() {					\
	if (!sense_context() || i_sense_lock()) {		\
		ercd = E_CTX;					\
		goto exit;					\
	}							\
}

/*
 *  ディスパッチ保留状態でないかのチェック（E_CTX）
 */
#define CHECK_DISPATCH() {					\
	if (sense_context() || t_sense_lock() || !(enadsp)) {	\
		ercd = E_CTX;					\
		goto exit;					\
	}							\
}

/*
 *  その他のコンテキストエラーのチェック（E_CTX）
 */
#define CHECK_CTX(exp) {					\
	if (!(exp)) {						\
		ercd = E_CTX;					\
		goto exit;					\
	}							\
}

/*
 *  自タスクを指定していないかのチェック（E_ILUSE）
 */
#define CHECK_NONSELF(tcb) {					\
	if ((tcb) == runtsk) {					\
		ercd = E_ILUSE;					\
		goto exit;					\
	}							\
}

/*
 *  その他の不正使用エラーのチェック（E_ILUSE）
 */
#define CHECK_ILUSE(exp) {					\
	if (!(exp)) {						\
		ercd = E_ILUSE;					\
		goto exit;					\
	}							\
}


/*
 *  cre_系で用いる。
 *  存在する/初期化中のIDに対してはE_OBJを返すが、存在しない場合は
 *  オブジェクトの存在状態を初期化中に移行する。
 */
#define CHECK_OBJECT_CREATABLE(exp) {				\
	t_lock_cpu();						\
	if ((exp)->exist != KOBJ_NOEXS) {			\
		t_unlock_cpu();					\
		ercd = E_OBJ;					\
		goto exit;					\
	}							\
	(exp)->exist = KOBJ_INIT;				\
	t_unlock_cpu();						\
}
#define CHECK_TASK_CREATABLE(exp) {				\
	t_lock_cpu();						\
	if ((exp)->exist != KOBJ_NOEXS) {			\
		t_unlock_cpu();					\
		ercd = E_OBJ;					\
		goto exit;					\
	}							\
	(exp)->exist = KOBJ_INIT;				\
	t_unlock_cpu();						\
}

/*
 *  del_系で用いる。
 *  存在しないIDに対してはE_NOEXSを返すが、存在する場合は
 *  オブジェクトの存在状態を削除処理中に移行する。
 */
#define CHECK_OBJECT_DELETABLE(exp) {				\
	t_lock_cpu();						\
	if ((exp)->exist != KOBJ_EXIST) {			\
		t_unlock_cpu();					\
		ercd = E_NOEXS;					\
		goto exit;					\
	}							\
	(exp)->exist = KOBJ_INIT;				\
	t_unlock_cpu();						\
}

#define T_CHECK_OBJECT_EXIST(exp) {				\
	if ((exp)->exist != KOBJ_EXIST) {			\
		t_unlock_cpu();					\
		ercd = E_NOEXS;					\
		goto exit;					\
	}							\
}
#define I_CHECK_OBJECT_EXIST(exp) {				\
	if ((exp)->exist != KOBJ_EXIST) {			\
		i_unlock_cpu();					\
		ercd = E_NOEXS;					\
		goto exit;					\
	}							\
}

#define CHECK_OBJECT_RESERVED(exp) ((exp)->reserved)

/*
 * コントロールブロックがフリーリスト中にある場合、フリーリストから削除。
 * 削除しないとacre_で破綻。
 */
#define UPDATE_OBJECT_FREELIST(exp) {				\
	if (!(CHECK_OBJECT_RESERVED(exp))) {			\
		queue_delete((QUEUE*)(exp));			\
	}							\
 }

#endif /* TOPPERS_CHECK_H */
