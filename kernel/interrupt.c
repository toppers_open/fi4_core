/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2003-2004 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/interrupt.c 6101 2006-09-10T08:00:54.989588Z monaka  $
 */

/*
 *	割込み管理機能
 */

#include "fi4_kernel.h"
#include "check.h"
#include "interrupt.h"

/*
 *  割込みサービスルーチンIDの最大値（kernel_cfg.c）
 */
extern const ID	tmax_isrid;
extern const ID	tnum_attached_isrno;

/*
 *  割込みサービスルーチン初期化ブロックのエリア（kernel_cfg.c）
 */
extern ISRINIB	isrinib_table[];
extern ATTACHED_ISRINIB	attached_isrinib_table[];

/*
 *  割込みサービスルーチン初期化ブロック連想ペアの数（kernel_cfg.c）
 */
extern const UINT	tnum_isrinia;

/*
 *  割込みサービスルーチン初期化ブロック連想ペアのエリア（kernel_cfg.c）
 */
extern const ISRINIA	isrinia_table[];

/*
 *  割込みサービスルーチン予約ID情報のエリア（kernel_cfg.c）
 */
extern const ID		isrrid_table[];

/*
 *  割込みサービスルーチン予約ID情報の数 (kernel_cfg.c)
 */
extern const UINT	tnum_isrrid;

/*
 *  割込みサービスルーチン管理ブロックのエリア（kernel_cfg.c）
 */
extern ISRCB	isrcb_table[];

/*
 *  割込みサービスルーチンの数
 */
#define TNUM_ISR	((UINT)(tmax_isrid - TMIN_ISRID + 1))

/*
 *  割込みサービスルーチンIDから
 *  割込みサービスルーチン管理ブロックを取り出すためのマクロ
 */
#define INDEX_ISR(isrid)	((UINT)((isrid) - TMIN_ISRID))
#define get_isrcb(isrid)	(&(isrcb_table[INDEX_ISR(isrid)]))

/*
 *  ID自動割当て用フリーリスト
 */
extern QUEUE isr_freelist;

/*
 *  割込みハンドラ番号の数（kernel_cfg.c）
 */
extern const UINT	tnum_inhno;

/*
 *  割込みハンドラ初期化ブロックのエリア（kernel_cfg.c）
 */
extern const INHINIB	inhinib_table[];

/* 
 *  割込み管理機能の初期化
 */
#ifdef __inhini

void
interrupt_initialize(void)
{
	UINT		i;
	const INHINIB	*inhinib;

	/* 割込みハンドラの定義 */
	for (inhinib = inhinib_table, i = 0; i < tnum_inhno; inhinib++, i++) {
		if (check_undefined_inh(inhinib->inhno)) {
			define_inh(inhinib->inhno, inhinib->inthdr);
		} else {
			assert("Duplicated inhno.");
		}
	}

}

#endif /* __inhini */


#ifdef __att_isrini

void
attached_isr_initialize()
{
	UINT		i;

	ATTACHED_ISRINIB	*attached_isrinib;

	/*
	 *  静的生成した割込みサービスルーチンの初期化。
	 *  他のサービスコールと異なり、ATT_でcre_,acre_の組になっていることに注意。
	 */
	for (attached_isrinib = attached_isrinib_table, i = 0;
	     i < tnum_attached_isrno;
	     attached_isrinib++, i++) {
		if (check_undefined_inh((INHNO)attached_isrinib->intno)) {
			define_inh((INHNO)attached_isrinib->intno, isr_interrupt);
		} else {
			assert("Duplicated intno/inhno.");
		}
	}
}

#endif /* __att_isrini */


#ifdef __isrini

QUEUE isr_freelist;

void
interrupt_service_initialize()
{
	UINT		i;

	ISRCB		*isrcb;
	ISRINIB		*isrinib;
	const ISRINIA	*isrinia;
	const ID	*isrrid;


	/*
	 * カーネル起動時の状態に関わらず初期化しなければいけないものを
	 * まず初期化する。
	 */
	for (isrcb = isrcb_table, i = 0; i < TNUM_ISR; isrcb++, i++) {
		queue_initialize(&(isrcb->wait_queue));
		isrcb->isrinib = NULL;
	}
	queue_initialize(&isr_freelist);

	/*
	 * isrinia によって示されるものをまず初期化
	 */
	for (isrinia = isrinia_table, i = 0; i < tnum_isrinia; isrinia++, i++) {
		isrinib = &isrinib_table[INDEX_ISR(isrinia->isrid)];
		*isrinib = isrinia->isrinib;
		isrcb = &isrcb_table[INDEX_ISR(isrinia->isrid)];
		isrcb->isrinib = isrinib;
		isrcb->exist = KOBJ_EXIST;
		isrcb->reserved = TRUE;
	}
	/*
	 *  予約IDの設定
	 */
	for (isrrid = isrrid_table, i = 0; i < tnum_isrrid; isrrid++, i++) {
		isrcb_table[INDEX_ISR(*isrrid)].reserved = TRUE;
	}

	/*
	 * isrinia によって示されないものを初期化
	 */
	for (isrcb = isrcb_table, i = 0; i < TNUM_ISR; isrcb++, i++) {
		if (isrcb->isrinib == NULL) {
			isrcb->exist = KOBJ_NOEXS;
			isrcb->isrinib = &isrinib_table[i];

			if (!CHECK_OBJECT_RESERVED(isrcb)) {
				queue_insert_prev(&isr_freelist, (QUEUE*)isrcb);
			}
		}
	}
}

#endif /* __isrini */

/*
 *  割込みハンドラの定義
 */
#ifdef __def_inh

SYSCALL ER
def_inh(INHNO inhno, const T_DINH *pk_dinh)
{
	ER ercd;

	LOG_DEF_INH_ENTER(inhno);

	/*
	 *  割込みサービスルーチンを
	 *  def_inhで操作することはできない。
	 */
	if (check_defined_isr((INTNO)inhno)) {
		return E_PAR;
	}

	/*
	 *  inhno の正当性チェックは行わない。
	 */
	if (pk_dinh == NULL) {
		t_lock_cpu();
		undef_inh(inhno);
	} else {
		CHECK_ATTRIBUTE(pk_dinh->inhatr, TA_HLNG | TA_ASM);
		t_lock_cpu();
		define_inh(inhno, pk_dinh->inthdr);
	}
	t_unlock_cpu();

	ercd = E_OK;
    exit:
	LOG_DEF_INH_LEAVE(ercd);

	return(ercd);
}

#endif /* __def_inh */

/*
 *  割込みサービスルーチンの生成
 */
#ifdef __cre_isr

SYSCALL ER
cre_isr(ID isrid, const T_CISR *pk_cisr)
{
	ISRCB	*isrcb;
	ISRINIB *isrinib;
	ER	ercd;
    
	LOG_CRE_ISR_ENTER(isrid, pk_cisr);
	CHECK_TSKCTX_UNL();

	/*
	 *  割込みサービスルーチン用以外の割込みハンドラが
	 *  定義されている割込みベクタに割込みサービスルーチンを
	 *  登録することはできない。
	 */
	if (!check_undefined_inh((INHNO)pk_cisr->intno) &&
	    !check_defined_isr(pk_cisr->intno)) {
		return E_PAR;
	}

	CHECK_ISRID(isrid);
	CHECK_ATTRIBUTE(pk_cisr->isratr, TA_HLNG | TA_ASM);
	isrcb = get_isrcb(isrid);
	CHECK_OBJECT_CREATABLE(isrcb);
	UPDATE_OBJECT_FREELIST(isrcb);

	/*
	 * 初期設定を行う。
	 * wait_queue は、 interrupt_initialize()で初期化されるし
	 * del_isrの時にも掃除される。よって、ここでは初期化の必要がない。
	 */
	isrinib = isrcb->isrinib;
	isrinib->isratr = pk_cisr->isratr;
	isrinib->exinf  = pk_cisr->exinf;
	isrinib->intno  = pk_cisr->intno;
	isrinib->isr	= pk_cisr->isr;

	/* TODO: さらなる初期化が要るはず。*/

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	isrcb->exist = KOBJ_EXIST;

	ercd = E_OK;

	t_unlock_cpu();

    exit:
	LOG_CRE_ISR_LEAVE(ercd);
	return(ercd);
}

#endif /* __cre_isr */

/*
 *  割込みサービスルーチンの生成（ID番号自動割付け）
 */
#ifdef __acre_isr

SYSCALL ER
acre_isr(const T_CISR *pk_cisr)
{
	ISRCB	*isrcb;
	ISRINIB	*isrinib;
	ER_ID	ercd;
    
	LOG_ACRE_ISR_ENTER(pk_cisr);
	CHECK_TSKCTX_UNL();

	if (TNUM_ISR == 0) {
		ercd = E_NOID;
		goto exit;
	}
	CHECK_ATTRIBUTE(pk_cisr->isratr, TA_HLNG | TA_ASM);

	/*
	 *  割込みサービスルーチン用以外の割込みハンドラが
	 *  定義されている割込みベクタに割込みサービスルーチンを
	 *  登録することはできない。
	 */
	if (!check_undefined_inh((INHNO)pk_cisr->intno) &&
	    !check_defined_isr(pk_cisr->intno)) {
		return E_PAR;
	}

	t_lock_cpu();
	isrcb = (ISRCB *)t_get_free_id((QUEUE *)&isr_freelist);
	if (isrcb == NULL) {
		ercd = E_NOID;
		t_unlock_cpu();
		goto exit;
	}
	isrcb->exist = KOBJ_INIT;
	t_unlock_cpu();

	/*
	 * 初期設定を行う。
	 */
	queue_initialize(&(isrcb->wait_queue));
	isrinib = isrcb->isrinib;
	isrinib->isratr = pk_cisr->isratr;
	isrinib->exinf  = pk_cisr->exinf;
	isrinib->intno  = pk_cisr->intno;
	isrinib->isr	= pk_cisr->isr;

	ercd = ISRID(isrcb);

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	isrcb->exist = KOBJ_EXIST;

    exit:
	LOG_ACRE_ISR_LEAVE(ercd);
	return(ercd);
}

#endif /* __acre_isr */

/*
 *  割込みサービスルーチンの削除
 */
#ifdef __del_isr

SYSCALL ER
del_isr(ID isrid)
{
	ISRCB	*isrcb;
	ER	ercd;
    
	LOG_DEL_ISR_ENTER(isrid);
	CHECK_TSKCTX_UNL();

	CHECK_ISRID(isrid);
	isrcb = get_isrcb(isrid);

	CHECK_OBJECT_DELETABLE(isrcb);

	/* TODO: 後片付け。*/
	  
	if (!CHECK_OBJECT_RESERVED(isrcb)) {
		/*
		 * 予約IDでなければフリーリストに入れる。
		 */
		t_lock_cpu();
		queue_insert_prev(&isr_freelist, (QUEUE *)isrcb);
		t_unlock_cpu();
	}

	/* ここから先はacre_/cre_でのアクセスが可能 */
	isrcb->exist = KOBJ_NOEXS;

	ercd = E_OK;

    exit:
	LOG_DEL_ISR_LEAVE(ercd);
	return(ercd);
}

#endif /* __del_isr */

/*
 *  割込みサービスルーチンの本体
 */
#ifdef __isr_handler

SYSCALL void
isr_handler(INTNO intno)
{
	UINT		i;
	ISRCB		*isrcb;
	ISRINIB		*isrinib;
	ATTACHED_ISRINIB	*attached_isrinib;

	for (attached_isrinib = attached_isrinib_table, i = 0; i < tnum_attached_isrno; attached_isrinib++, i++) {
		if (attached_isrinib->intno == intno) {
			(void)(attached_isrinib->isr)(attached_isrinib->exinf);
		}
	}
	
	for (isrcb = isrcb_table, i = 0; i < TNUM_ISR; isrcb++, i++) {
		isrinib = isrcb->isrinib;
		if ((isrinib->intno == intno) && (isrcb->exist == KOBJ_EXIST)) {
			(void)(isrinib->isr)(isrinib->exinf);
		}
	}
}

#endif /* __isr_handler */

/*
 *  割込みサービスルーチンの状態参照
 */
#ifdef __ref_isr

SYSCALL ER
ref_isr(ID isrid, T_RISR *pk_risr)
{
	ISRCB	*isrcb;
	ER	ercd;
    
	LOG_REF_ISR_ENTER(isrid);
	CHECK_TSKCTX_UNL();

	CHECK_ISRID(isrid);
	isrcb = get_isrcb(isrid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(isrcb);

	/*
	 * pk_risrの内容は実装定義。
	 * 本バージョンではpk_risrは空なので、何もすることがない。
	 */
	ercd = E_OK;

	t_unlock_cpu();

    exit:
	LOG_REF_ISR_LEAVE(ercd);
	return(ercd);
}

#endif /* __ref_isr */

/*
 *  割込みの禁止
 */
#ifdef __dis_int

SYSCALL ER
dis_int(INTNO intno)
{
	return E_NOSPT;
}

#endif /* __dis_int */

/*
 *  割込みの許可
 */
#ifdef __ena_int

SYSCALL ER
ena_int(INTNO intno)
{
	return E_NOSPT;
}

#endif /* __ena_int */

