/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2003-2004 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/mailbox.c 6101 2006-09-10T08:00:54.989588Z monaka  $
 */

/*
 *	メールボックス機能
 */

#include "fi4_kernel.h"
#include "check.h"
#include "task.h"
#include "wait.h"
#include "mailbox.h"

/*
 *  メールボックスIDの最大値（kernel_cfg.c）
 */
extern const ID	tmax_mbxid;

/*
 *  メールボックス初期化ブロックのエリア（kernel_cfg.c）
 */
extern MBXINIB	mbxinib_table[];

/*
 *  メールボックス初期化ブロック連想ペアの数（kernel_cfg.c）
 */
extern const UINT	tnum_mbxinia;

/*
 *  メールボックス初期化ブロック連想ペアのエリア（kernel_cfg.c）
 */
extern const MBXINIA	mbxinia_table[];

/*
 *  メールボックス予約ID情報のエリア（kernel_cfg.c）
 */
extern const ID		mbxrid_table[];

/*
 *  メールボックス予約ID情報の数 (kernel_cfg.c)
 */
extern const UINT	tnum_mbxrid;

/*
 *  メールボックス管理ブロックのエリア（kernel_cfg.c）
 */
extern MBXCB	mbxcb_table[];

/*
 *  メールボックスの数
 */
#define TNUM_MBX	((UINT)(tmax_mbxid - TMIN_MBXID + 1))

/*
 *  メールボックスIDからメールボックス管理ブロックを取り出すためのマクロ
 */
#define INDEX_MBX(mbxid)	((UINT)((mbxid) - TMIN_MBXID))
#define get_mbxcb(mbxid)	(&(mbxcb_table[INDEX_MBX(mbxid)]))

/*
 *  ID自動割当て用フリーリスト
 */
extern QUEUE mbx_freelist;

/*
 *  メールボックス待ち情報ブロックの定義
 */
typedef struct mailbox_waiting_information {
	WINFO	winfo;		/* 標準の待ち情報ブロック */
	WOBJCB	*wobjcb;	/* 待ちオブジェクトの管理ブロック */
	T_MSG	*pk_msg;	/* 受信したメッセージ */
} WINFO_MBX;

/* 
 *  メールボックス機能の初期化
 */
#ifdef __mbxini

QUEUE mbx_freelist;

void
mailbox_initialize(void)
{
	UINT	i;
	MBXCB	*mbxcb;
	MBXINIB *mbxinib;
	const MBXINIA	*mbxinia;
	const ID	*mbxrid;

	/*
	 * カーネル起動時の状態に関わらず初期化しなければいけないものを
	 * まず初期化する。
	 */
	for (mbxcb = mbxcb_table, i = 0; i < TNUM_MBX; mbxcb++, i++) {
		queue_initialize(&(mbxcb->wait_queue));
		mbxcb->mbxinib = NULL;
	}
	queue_initialize(&mbx_freelist);

	/*
	 * mbxinia によって示されるものをまず初期化
	 */
	for (mbxinia = mbxinia_table, i = 0; i < tnum_mbxinia; mbxinia++, i++) {
		mbxinib = &mbxinib_table[INDEX_MBX(mbxinia->mbxid)];
		*mbxinib = mbxinia->mbxinib;
		mbxcb = &mbxcb_table[INDEX_MBX(mbxinia->mbxid)];
		mbxcb->mbxinib = mbxinib;
		mbxcb->exist = KOBJ_EXIST;
		mbxcb->reserved = TRUE;
		mbxcb->head = NULL;
	}

	/*
	 *  予約IDの設定
	 */
	for (mbxrid = mbxrid_table, i = 0; i < tnum_mbxrid; mbxrid++, i++) {
		mbxcb_table[INDEX_MBX(*mbxrid)].reserved = TRUE;
	}

	/*
	 * mbxinia によって示されないものを初期化
	 */
	for (mbxcb = mbxcb_table, i = 0; i < TNUM_MBX; mbxcb++, i++) {
		if (mbxcb->mbxinib == NULL) {
			mbxcb->exist = KOBJ_NOEXS;
			mbxcb->mbxinib = &mbxinib_table[i];

			if (!CHECK_OBJECT_RESERVED(mbxcb)) {
				queue_insert_prev(&mbx_freelist, (QUEUE*)mbxcb);
			}
		}
	}
}

#endif /* __mbxini */

/*
 *  メッセージ優先度の取出し
 */
#define	MSGPRI(pk_msg)	(((T_MSG_PRI *) pk_msg)->msgpri)

/*
 *  優先度順メッセージキューへの挿入
 */
Inline void
enqueue_msg_pri(T_MSG **p_prevmsg_next, T_MSG *pk_msg)
{
	T_MSG	*pk_nextmsg;

	while ((pk_nextmsg = *p_prevmsg_next) != NULL) {
		if (MSGPRI(pk_nextmsg) > MSGPRI(pk_msg)) {
			break;
		}
		p_prevmsg_next = &(pk_nextmsg->next);
	}
	pk_msg->next = pk_nextmsg;
	*p_prevmsg_next = pk_msg;
}

/*
 *  メールボックスの生成
 */
#ifdef __cre_mbx

SYSCALL ER
cre_mbx(ID mbxid, const T_CMBX * pk_cmbx)
{
	MBXCB	*mbxcb;
	MBXINIB *mbxinib;
	ER	ercd;

	LOG_CRE_MBX_ENTER(mbxid, pk_cmbx);
	CHECK_TSKCTX_UNL();

	CHECK_MBXID(mbxid);
	CHECK_ATTRIBUTE(pk_cmbx->mbxatr, TA_TFIFO | TA_TPRI | TA_MFIFO | TA_MPRI);
	if (pk_cmbx->mbxatr & TA_MPRI) {
		CHECK_MPRI(pk_cmbx->maxmpri);
	}

	mbxcb = get_mbxcb(mbxid);
	CHECK_OBJECT_CREATABLE(mbxcb);
	UPDATE_OBJECT_FREELIST(mbxcb);

	/*
	 * 初期設定を行う。
	 *
	 *  JSP同様、単一のキューで管理する。
	 *  mprihdは保持するだけで使わない。
	 */
	queue_initialize((QUEUE*)mbxcb);
	mbxinib = mbxcb->mbxinib;
	mbxinib->mbxatr = pk_cmbx->mbxatr;
	mbxinib->maxmpri = pk_cmbx->maxmpri;
	mbxinib->mprihd = pk_cmbx->mprihd;
	mbxcb->head = NULL;

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	mbxcb->exist = KOBJ_EXIST;

	ercd = E_OK;

    exit:
	LOG_CRE_MBX_LEAVE(ercd);
	return(ercd);
}

#endif /* __cre_mbx */

/*
 *  メールボックスの生成(ID番号自動割付け)
 */
#ifdef __acre_mbx

SYSCALL ER_ID
acre_mbx(const T_CMBX *pk_cmbx)
{
	MBXCB	*mbxcb;
	MBXINIB *mbxinib;
	ER_ID	ercd;

	LOG_ACRE_MBX_ENTER(pk_cmbx);
	CHECK_TSKCTX_UNL();

	if (TNUM_MBX == 0) {
		ercd = E_NOID;
		goto exit;
	}
	CHECK_ATTRIBUTE(pk_cmbx->mbxatr, TA_TFIFO | TA_TPRI | TA_MFIFO | TA_MPRI);
	if (pk_cmbx->mbxatr & TA_MPRI) {
		CHECK_MPRI(pk_cmbx->maxmpri);
	}

	t_lock_cpu();
	mbxcb = (MBXCB *)t_get_free_id((QUEUE *)&mbx_freelist);
	if (mbxcb == NULL) {
		ercd = E_NOID;
		t_unlock_cpu();
		goto exit;
	}
	mbxcb->exist = KOBJ_INIT;
	t_unlock_cpu();

	/*
	 * 初期設定を行う。
	 *
	 *  JSP同様、単一のキューで管理する。
	 *  mprihdは保持するだけで使わない。
	 */
	queue_initialize((QUEUE*)mbxcb);
	mbxinib = mbxcb->mbxinib;
	mbxinib->mbxatr = pk_cmbx->mbxatr;
	mbxinib->maxmpri = pk_cmbx->maxmpri;
	mbxinib->mprihd = pk_cmbx->mprihd;
	mbxcb->head = NULL;

	ercd = MBXID(mbxcb);

	/*
	 *  ここで初めて他のサービスコールから使用可能になる。
	 */
	mbxcb->exist = KOBJ_EXIST;

    exit:
	LOG_ACRE_MBX_LEAVE(ercd);
	return(ercd);
}

#endif /* __acre_mbx */

/*
 *  メールボックスの削除
 */
#ifdef __del_mbx
SYSCALL ER
del_mbx(ID mbxid)
{
	MBXCB	*mbxcb;
	ER	ercd;

	LOG_DEL_MBX_ENTER(mbxid);
	CHECK_TSKCTX_UNL();

	CHECK_MBXID(mbxid);
	mbxcb = get_mbxcb(mbxid);

	CHECK_OBJECT_DELETABLE(mbxcb);

	/* 待ち行列の後片付け。削除に伴う待ち解除を行う。*/
	while (queue_empty(&(mbxcb->wait_queue)) == FALSE) {
		TCB *tcb;
		tcb = (TCB *)queue_delete_next(&(mbxcb->wait_queue));
		t_lock_cpu();
		if (wait_deleted(tcb)) {
			dispatch();
		}
		t_unlock_cpu();
	}

	if (!CHECK_OBJECT_RESERVED(mbxcb)) {
	  /*
	   * 予約IDでなければフリーリストに入れる。
	   */
		t_lock_cpu();
		queue_insert_prev(&mbx_freelist, (QUEUE *)mbxcb);
		t_unlock_cpu();
	}

	/* ここから先はacre_/cre_でのアクセスが可能 */
	mbxcb->exist = KOBJ_NOEXS;

	ercd = E_OK;

    exit:
	LOG_DEL_MBX_LEAVE(ercd);
	return(ercd);
}

#endif /* __del_mbx */

/*
 *  メールボックスへの送信
 */
#ifdef __snd_mbx

SYSCALL ER
snd_mbx(ID mbxid, T_MSG *pk_msg)
{
	MBXCB	*mbxcb;
	TCB	*tcb;
	ER	ercd;
    
	LOG_SND_MBX_ENTER(mbxid, pk_msg);
	CHECK_TSKCTX_UNL();

	CHECK_MBXID(mbxid);
	mbxcb = get_mbxcb(mbxid);

	CHECK_PAR((mbxcb->mbxinib->mbxatr & TA_MPRI) == 0
		  || (TMIN_MPRI <= MSGPRI(pk_msg)
		      && MSGPRI(pk_msg) <= mbxcb->mbxinib->maxmpri));

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mbxcb);

	if (!(queue_empty(&(mbxcb->wait_queue)))) {
		tcb = (TCB *) queue_delete_next(&(mbxcb->wait_queue));
		((WINFO_MBX *)(tcb->winfo))->pk_msg = pk_msg;
		if (wait_complete(tcb)) {
			dispatch();
		}
		ercd = E_OK;
	}
	else if ((mbxcb->mbxinib->mbxatr & TA_MPRI) != 0) {
		enqueue_msg_pri(&(mbxcb->head), pk_msg);
		ercd = E_OK;
	}
	else {
		pk_msg->next = NULL;
		if (mbxcb->head != NULL) {
			mbxcb->last->next = pk_msg;
		}
		else {
			mbxcb->head = pk_msg;
		}
		mbxcb->last = pk_msg;
		ercd = E_OK;
	}

	t_unlock_cpu();

    exit:
	LOG_SND_MBX_LEAVE(ercd);
	return(ercd);
}

#endif /* __snd_mbx */

/*
 *  メールボックスからの受信
 */
#ifdef __rcv_mbx

SYSCALL ER
rcv_mbx(ID mbxid, T_MSG **ppk_msg)
{
	MBXCB	*mbxcb;
	WINFO_MBX winfo;
	ER	ercd;
    
	LOG_RCV_MBX_ENTER(mbxid, ppk_msg);
	CHECK_DISPATCH();
    
	CHECK_MBXID(mbxid);
	mbxcb = get_mbxcb(mbxid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mbxcb);

	if (mbxcb->head != NULL) {
		*ppk_msg = mbxcb->head;
		mbxcb->head = (*ppk_msg)->next;
		ercd = E_OK;
	}
	else {
		wobj_make_wait((WOBJCB *) mbxcb, (WINFO_WOBJ *) &winfo);
		dispatch();
		ercd = winfo.winfo.wercd;
		if (ercd == E_OK) {
			*ppk_msg = winfo.pk_msg;
		}
	}

	t_unlock_cpu();

    exit:
	LOG_RCV_MBX_LEAVE(ercd, *ppk_msg);
	return(ercd);
}

#endif /* __rcv_mbx */

/*
 *  メールボックスからの受信（ポーリング）
 */
#ifdef __prcv_mbx

SYSCALL ER
prcv_mbx(ID mbxid, T_MSG **ppk_msg)
{
	MBXCB	*mbxcb;
	ER	ercd;
    
	LOG_PRCV_MBX_ENTER(mbxid, ppk_msg);
	CHECK_TSKCTX_UNL();
    
	CHECK_MBXID(mbxid);
	mbxcb = get_mbxcb(mbxid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mbxcb);

	if (mbxcb->head != NULL) {
		*ppk_msg = mbxcb->head;
		mbxcb->head = (*ppk_msg)->next;
		ercd = E_OK;
	}
	else {
		ercd = E_TMOUT;
	}

	t_unlock_cpu();

    exit:
	LOG_PRCV_MBX_LEAVE(ercd, *ppk_msg);
	return(ercd);
}

#endif /* __prcv_mbx */

/*
 *  メールボックスからの受信（タイムアウトあり）
 */
#ifdef __trcv_mbx

SYSCALL ER
trcv_mbx(ID mbxid, T_MSG **ppk_msg, TMO tmout)
{
	MBXCB	*mbxcb;
	WINFO_MBX winfo;
	TMEVTB	tmevtb;
	ER	ercd;
    
	LOG_TRCV_MBX_ENTER(mbxid, ppk_msg, tmout);
	CHECK_DISPATCH();
	CHECK_TMOUT(tmout);
    
	CHECK_MBXID(mbxid);
	mbxcb = get_mbxcb(mbxid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mbxcb);

	if (mbxcb->head != NULL) {
		*ppk_msg = mbxcb->head;
		mbxcb->head = (*ppk_msg)->next;
		ercd = E_OK;
	}
	else if (tmout == TMO_POL) {
		ercd = E_TMOUT;
	}
	else {
		wobj_make_wait_tmout((WOBJCB *) mbxcb, (WINFO_WOBJ *) &winfo,
						&tmevtb, tmout);
		dispatch();
		ercd = winfo.winfo.wercd;
		if (ercd == E_OK) {
			*ppk_msg = winfo.pk_msg;
		}
	}

	t_unlock_cpu();

    exit:
	LOG_TRCV_MBX_LEAVE(ercd, *ppk_msg);
	return(ercd);
}

#endif /* __trcv_mbx */

/*
 *  メールボックスの状態参照
 */
#ifdef __ref_mbx
SYSCALL ER
ref_mbx(ID mbxid, T_RMBX *pk_rmbx)
{
	MBXCB	*mbxcb;
	TCB	*tcb;
	ER	ercd;
    
	LOG_REF_MBX_ENTER(mbxid);
	CHECK_TSKCTX_UNL();

	CHECK_MBXID(mbxid);
	mbxcb = get_mbxcb(mbxid);

	t_lock_cpu();
	T_CHECK_OBJECT_EXIST(mbxcb);

	if (!queue_empty(&(mbxcb->wait_queue))) {
		tcb = (TCB *)(mbxcb->wait_queue.next);
		pk_rmbx->wtskid = TSKID(tcb);
	} else {
		pk_rmbx->wtskid = TSK_NONE;
	}
	pk_rmbx->pk_msg = mbxcb->head;
	ercd = E_OK;

	t_unlock_cpu();

    exit:
	LOG_REF_MBX_LEAVE(ercd);
	return(ercd);
}

#endif /* __ref_mbx */

