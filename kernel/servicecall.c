/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uItron4 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2003-2004 by Monami software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/kernel/servicecall.c 2750 2006-03-07T16:15:24.532712Z monaka  $
 */

/*
 *	拡張サービスコール管理機能
 */

#include "fi4_kernel.h"
#include "servicecall.h"
#include "bufmgr.h"
#include "check.h"

/*
 *  拡張サービスコールの数（kernel_cfg.c）
 */
extern const UINT	tnum_svcno;

/*
 *  拡張サービスコールのエリア（kernel_cfg.c）
 */
extern SVCCB	svccb_table[];

/*
 *  拡張サービスコール初期化ブロックのエリア（kernel_cfg.c）
 */
extern SVCINIB	svcinib_table[];

#define SVCINIB_HASH_SIZE	(8)
extern QUEUE	svcinib_hash_table[];

extern const UINT _kernel_tnum_svcno;

/*
 *  拡張サービスコール管理用ハッシュテーブルの初期化
 *
 *  exservicecall_initialize()を呼び出す前に本関数を必ず呼び出すこと。
 *  通常はコンフィギュレータがkernel_cfg.cを自動生成してくれるので心配する
 *  必要は無いが、古いコンフィギュレータや古いkernel_cfg.cを用いると問題が
 *  出る可能性がある。svcinib_hash_table付近で奇妙な挙動をするときは、これらの
 *  バージョン不一致を疑ってみることを奨める。
 */
#ifdef __svchashini

QUEUE	svcinib_hash_table[SVCINIB_HASH_SIZE];

void
_kernel_svchash_initialize(void)
{
	UINT		i;
	for (i = 0; i < SVCINIB_HASH_SIZE; i++) {
		queue_initialize(&svcinib_hash_table[i]);
	}
}

#endif /* __svchashini */

/* 
 *  拡張サービスコール管理機能の初期化
 *
 *  この関数は、事前に__kernel_svchash_initialize() を呼び出しておくこと。
 *  通常はコンフィギュレータがkernel_cfg.cを自動生成してくれるので心配する
 *  必要は無いが、古いコンフィギュレータや古いkernel_cfg.cを用いると問題が
 *  出る可能性がある。svcinib_hash_table付近で奇妙な挙動をするときは、これらの
 *  バージョン不一致を疑ってみることを奨める。
 */
#ifdef __svcini

void
exservicecall_initialize(void)
{
	UINT		i;
	SVCCB		*svccb;
	SVCINIB		*svcinib;

	for (svccb = svccb_table, svcinib = svcinib_table, i = 0; i < _kernel_tnum_svcno; svccb++, svcinib++, i++) {
		UINT hash;
		hash = (UINT)svcinib->fncd % SVCINIB_HASH_SIZE;
		svccb->svcinib = svcinib;
		queue_insert_prev(&svcinib_hash_table[hash], &(svccb->queue));
	}
}

#endif /* __svcini */

/*
 *  拡張サービスコールの定義
 */
#ifdef __def_svc

SYSCALL
ER def_svc(FN fncd, const T_DSVC *pk_dsvc)
{
	ER ercd = E_OK;
	SIZE bufsz;
	SVCCB *svccb;
	UINT hash;
	QUEUE *queue = NULL;
	BOOL stat;

	if (fncd <= 0) {
		return E_RSFN;
	}

	if (pk_dsvc != NULL) {
		CHECK_ATTRIBUTE(pk_dsvc->svcatr, TA_HLNG | TA_ASM);
	}

	hash = (UINT)fncd % SVCINIB_HASH_SIZE;

	t_lock_cpu();
	while ((queue = queue_enumerate(&svcinib_hash_table[hash], queue)) != NULL) {
		SVCINIB *svcinib;
		svcinib = ((SVCCB *)queue)->svcinib;
		if (svcinib->fncd == fncd) {
			queue_delete(queue);
			/*
			 *  pk_dsvc == NULL は登録の解除を意味する。
			 */
			if (pk_dsvc == NULL) {
				t_unlock_cpu();
				return E_OK;
			}
			break;
		}
	}
	t_unlock_cpu();

	bufsz = sizeof(SVCCB) + sizeof(SVCINIB);
	t_lock_cpu();
	bufmgr_allocate(&stat, bufsz, (VP *)&svccb);
	t_unlock_cpu();
	if (!stat) {
		return E_NOMEM;
	}

	svccb->svcinib = (SVCINIB *)(svccb + 1);
	
	svccb->svcinib->fncd = fncd;
	svccb->svcinib->svcatr = pk_dsvc->svcatr;
	svccb->svcinib->svcrtn = pk_dsvc->svcrtn;
	queue_insert_prev(&svcinib_hash_table[hash], &(svccb->queue));

    exit:
	return ercd;
}

#endif /* __def_svc */

/*
 *  拡張サービスコールの呼出し
 */
#ifdef __cal_svc

SYSCALL
ER cal_svc(FN fncd, VP_INT arg1, VP_INT arg2, VP_INT arg3, VP_INT arg4, VP_INT arg5)
{
	ER_UINT ercd;
	QUEUE	*queue = NULL;

	UINT hash;

	if (fncd <= 0) {
		return E_RSFN;
	}

	hash = (UINT)fncd % SVCINIB_HASH_SIZE;

	while ((queue = queue_enumerate(&svcinib_hash_table[hash], queue)) != NULL) {
		SVCINIB *svcinib;
		svcinib = ((SVCCB *)queue)->svcinib;
		if (svcinib->fncd == fncd) {
			ER_UINT (*svcrtn)(VP_INT arg1, VP_INT arg2, VP_INT arg3, VP_INT arg4, VP_INT arg5)
				 = (ER_UINT (*)(VP_INT arg1, VP_INT arg2, VP_INT arg3, VP_INT arg4, VP_INT arg5))svcinib->svcrtn;
			ercd = svcrtn(arg1, arg2, arg3, arg4, arg5);
			return ercd;
		}
	}

	return E_RSFN;
}

#endif /* __cal_svc */

