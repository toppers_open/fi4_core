/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uITRON4.0 Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2003-2006 by Monami Software Limited Partnership, JAPAN.
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/config/sh3/tool_config.h 6183 2006-11-05T03:43:32.794177Z monaka  $
 */

/*
 *	開発環境依存モジュール
 *
 *  このインクルードファイルは，t_config.h のみからインクルードされる．
 *  他のファイルから直接インクルードしてはならない．
 */

#ifndef TOPPERS_TOOL_CONFIG_H
#define TOPPERS_TOOL_CONFIG_H

/*
 *  ラベルの別名を定義するためのマクロ
 */
#define	_LABEL_ALIAS(new_label, defined_label) \
	asm(".globl " #new_label "\n" #new_label " = " #defined_label);
#define LABEL_ALIAS(x, y) _LABEL_ALIAS(x, y)

/*
 *  atexit の処理とデストラクタの実行
 */
#ifndef _MACRO_ONLY

Inline void
call_atexit()
{
	extern void software_term_hook(void);
	void (*volatile fp)(void) = software_term_hook;


	if (fp != 0) {
		(*fp)();
	}
}

#endif /* _MACRO_ONLY */

/*
 *  トレースログの設定
 */

#define	LOG_INH_ENTER(inhno)		/* プロセッサ依存部 */
#define	LOG_INH_LEAVE(inhno)		/* プロセッサ依存部 */

#define	LOG_ISR_ENTER(intno)		/* プロセッサ依存部 */
#define	LOG_ISR_LEAVE(intno)		/* プロセッサ依存部 */

#define	LOG_CYC_ENTER(cyccb)
#define	LOG_CYC_LEAVE(cyccb)

#define LOG_ALM_ENTER(almcb)
#define LOG_ALM_LEAVE(almcb)

#define	LOG_EXC_ENTER(excno)		/* プロセッサ依存部 */
#define	LOG_EXC_LEAVE(excno)		/* プロセッサ依存部 */

#define	LOG_TEX_ENTER(texptn)
#define	LOG_TEX_LEAVE(texptn)

#define	LOG_TSKSTAT(tcb)

#define	LOG_DSP_ENTER(tcb)		/* プロセッサ依存部 */
#define	LOG_DSP_LEAVE(tcb)		/* プロセッサ依存部 */

#define LOG_CRE_TSK_ENTER(tskid, pk_ctsk)
#define LOG_CRE_TSK_LEAVE(ercd)
#define LOG_ACRE_TSK_ENTER(pk_ctsk)
#define LOG_ACRE_TSK_LEAVE(ercd)
#define LOG_DEL_TSK_ENTER(tskid)
#define LOG_DEL_TSK_LEAVE(ercd)
#define	LOG_ACT_TSK_ENTER(tskid)
#define	LOG_ACT_TSK_LEAVE(ercd)
#define	LOG_IACT_TSK_ENTER(tskid)
#define	LOG_IACT_TSK_LEAVE(ercd)
#define	LOG_CAN_ACT_ENTER(tskid)
#define	LOG_CAN_ACT_LEAVE(ercd)
#define LOG_STA_TSK_ENTER(tskid)
#define LOG_STA_TSK_LEAVE(ercd)
#define	LOG_EXT_TSK_ENTER()
#define	LOG_EXD_TSK_ENTER()
#define	LOG_TER_TSK_ENTER(tskid)
#define	LOG_TER_TSK_LEAVE(ercd)
#define	LOG_CHG_PRI_ENTER(tskid, tskpri)
#define	LOG_CHG_PRI_LEAVE(ercd)
#define	LOG_GET_PRI_ENTER(tskid, p_tskpri)
#define	LOG_GET_PRI_LEAVE(ercd, tskpri)
#define	LOG_REF_TSK_ENTER(tskid)
#define	LOG_REF_TSK_LEAVE(ercd)
#define	LOG_REF_TST_ENTER(tskid)
#define	LOG_REF_TST_LEAVE(ercd)
#define	LOG_SLP_TSK_ENTER()
#define	LOG_SLP_TSK_LEAVE(ercd)
#define	LOG_TSLP_TSK_ENTER(tmout)
#define	LOG_TSLP_TSK_LEAVE(ercd)
#define	LOG_WUP_TSK_ENTER(tskid)
#define	LOG_WUP_TSK_LEAVE(ercd)
#define	LOG_IWUP_TSK_ENTER(tskid)
#define	LOG_IWUP_TSK_LEAVE(ercd)
#define	LOG_CAN_WUP_ENTER(tskid)
#define	LOG_CAN_WUP_LEAVE(ercd)
#define	LOG_REL_WAI_ENTER(tskid)
#define	LOG_REL_WAI_LEAVE(ercd)
#define	LOG_IREL_WAI_ENTER(tskid)
#define	LOG_IREL_WAI_LEAVE(ercd)
#define	LOG_SUS_TSK_ENTER(tskid)
#define	LOG_SUS_TSK_LEAVE(ercd)
#define	LOG_RSM_TSK_ENTER(tskid)
#define	LOG_RSM_TSK_LEAVE(ercd)
#define	LOG_FRSM_TSK_ENTER(tskid)
#define	LOG_FRSM_TSK_LEAVE(ercd)
#define	LOG_DLY_TSK_ENTER(dlytim)
#define	LOG_DLY_TSK_LEAVE(ercd)
#define	LOG_RAS_TEX_ENTER(tskid, rasptn)
#define	LOG_RAS_TEX_LEAVE(ercd)
#define	LOG_IRAS_TEX_ENTER(tskid, rasptn)
#define	LOG_IRAS_TEX_LEAVE(ercd)
#define	LOG_DEF_TEX_ENTER(tskid)
#define	LOG_DEF_TEX_LEAVE(ercd)
#define	LOG_DIS_TEX_ENTER()
#define	LOG_DIS_TEX_LEAVE(ercd)
#define	LOG_ENA_TEX_ENTER()
#define	LOG_ENA_TEX_LEAVE(ercd)
#define	LOG_SNS_TEX_ENTER()
#define	LOG_SNS_TEX_LEAVE(state)
#define	LOG_REF_TEX_ENTER(tskid)
#define	LOG_REF_TEX_LEAVE(ercd)
#define LOG_CRE_SEM_ENTER(semid, pk_csem)
#define LOG_CRE_SEM_LEAVE(ercd)
#define LOG_ACRE_SEM_ENTER(pk_csem)
#define LOG_ACRE_SEM_LEAVE(ercd)
#define LOG_DEL_SEM_ENTER(semid)
#define LOG_DEL_SEM_LEAVE(ercd)
#define	LOG_SIG_SEM_ENTER(semid)
#define	LOG_SIG_SEM_LEAVE(ercd)
#define	LOG_ISIG_SEM_ENTER(semid)
#define	LOG_ISIG_SEM_LEAVE(ercd)
#define	LOG_WAI_SEM_ENTER(semid)
#define	LOG_WAI_SEM_LEAVE(ercd)
#define	LOG_POL_SEM_ENTER(semid)
#define	LOG_POL_SEM_LEAVE(ercd)
#define	LOG_TWAI_SEM_ENTER(semid, tmout)
#define	LOG_TWAI_SEM_LEAVE(ercd)
#define	LOG_REF_SEM_ENTER(semid)
#define	LOG_REF_SEM_LEAVE(ercd)
#define LOG_CRE_FLG_ENTER(flgid, pk_cflg)
#define LOG_CRE_FLG_LEAVE(ercd)
#define LOG_ACRE_FLG_ENTER(pk_cflg)
#define LOG_ACRE_FLG_LEAVE(ercd)
#define LOG_DEL_FLG_ENTER(flgid)
#define LOG_DEL_FLG_LEAVE(ercd)
#define	LOG_SET_FLG_ENTER(flgid, setptn)
#define	LOG_SET_FLG_LEAVE(ercd)
#define	LOG_ISET_FLG_ENTER(flgid, setptn)
#define	LOG_ISET_FLG_LEAVE(ercd)
#define	LOG_CLR_FLG_ENTER(flgid, clrptn)
#define	LOG_CLR_FLG_LEAVE(ercd)
#define	LOG_WAI_FLG_ENTER(flgid, waiptn, wfmode, p_flgptn)
#define	LOG_WAI_FLG_LEAVE(ercd, flgptn)
#define	LOG_POL_FLG_ENTER(flgid, waiptn, wfmode, p_flgptn)
#define	LOG_POL_FLG_LEAVE(ercd, flgptn)
#define	LOG_TWAI_FLG_ENTER(flgid, waiptn, wfmode, p_flgptn, tmout)
#define	LOG_TWAI_FLG_LEAVE(ercd, flgptn)
#define	LOG_REF_FLG_ENTER(flgid)
#define	LOG_REF_FLG_LEAVE(ercd)
#define LOG_CRE_DTQ_ENTER(dtqid, pk_cdtq)
#define LOG_CRE_DTQ_LEAVE(ercd)
#define LOG_ACRE_DTQ_ENTER(pk_cdtq)
#define LOG_ACRE_DTQ_LEAVE(ercd)
#define LOG_DEL_DTQ_ENTER(dtqid)
#define LOG_DEL_DTQ_LEAVE(ercd)
#define	LOG_SND_DTQ_ENTER(dtqid, data)
#define	LOG_SND_DTQ_LEAVE(ercd)
#define	LOG_PSND_DTQ_ENTER(dtqid, data)
#define	LOG_PSND_DTQ_LEAVE(ercd)
#define	LOG_IPSND_DTQ_ENTER(dtqid, data)
#define	LOG_IPSND_DTQ_LEAVE(ercd)
#define	LOG_TSND_DTQ_ENTER(dtqid, data, tmout)
#define	LOG_TSND_DTQ_LEAVE(ercd)
#define	LOG_FSND_DTQ_ENTER(dtqid, data)
#define	LOG_FSND_DTQ_LEAVE(ercd)
#define	LOG_IFSND_DTQ_ENTER(dtqid, data)
#define	LOG_IFSND_DTQ_LEAVE(ercd)
#define	LOG_RCV_DTQ_ENTER(dtqid, p_data)
#define	LOG_RCV_DTQ_LEAVE(ercd, data)
#define	LOG_PRCV_DTQ_ENTER(dtqid, p_data)
#define	LOG_PRCV_DTQ_LEAVE(ercd, data)
#define	LOG_TRCV_DTQ_ENTER(dtqid, p_data, tmout)
#define	LOG_TRCV_DTQ_LEAVE(ercd, data)
#define	LOG_REF_DTQ_ENTER(dtqid)
#define	LOG_REF_DTQ_LEAVE(ercd)
#define LOG_CRE_MBX_ENTER(mbxid, pk_cmbx)
#define LOG_CRE_MBX_LEAVE(ercd)
#define LOG_ACRE_MBX_ENTER(pk_cmbx)
#define LOG_ACRE_MBX_LEAVE(ercd)
#define LOG_DEL_MBX_ENTER(mbxid)
#define LOG_DEL_MBX_LEAVE(ercd)
#define	LOG_SND_MBX_ENTER(mbxid, pk_msg)
#define	LOG_SND_MBX_LEAVE(ercd)
#define	LOG_RCV_MBX_ENTER(mbxid, ppk_msg)
#define	LOG_RCV_MBX_LEAVE(ercd, pk_msg)
#define	LOG_PRCV_MBX_ENTER(mbxid, ppk_msg)
#define	LOG_PRCV_MBX_LEAVE(ercd, pk_msg)
#define	LOG_TRCV_MBX_ENTER(mbxid, ppk_msg, tmout)
#define	LOG_TRCV_MBX_LEAVE(ercd, pk_msg)
#define	LOG_REF_MBX_ENTER(mbxid)
#define	LOG_REF_MBX_LEAVE(ercd)
#define LOG_CRE_MTX_ENTER(mtxid, pk_cmtx)
#define LOG_CRE_MTX_LEAVE(ercd)
#define LOG_ACRE_MTX_ENTER(pk_xmtx)
#define LOG_ACRE_MTX_LEAVE(ercd)
#define LOG_DEL_MTX_ENTER(mtxid)
#define LOG_DEL_MTX_LEAVE(ercd)
#define LOG_LOC_MTX_ENTER(mtxid)
#define LOG_LOC_MTX_LEAVE(ercd)
#define LOG_LOC_MTX_ENTER(mtxid)
#define LOG_LOC_MTX_LEAVE(ercd)
#define LOG_PLOC_MTX_ENTER(mtxid)
#define LOG_PLOC_MTX_LEAVE(ercd)
#define LOG_TLOC_MTX_ENTER(mtxid)
#define LOG_TLOC_MTX_LEAVE(ercd)
#define LOG_UNL_MTX_ENTER(mtxid)
#define LOG_UNL_MTX_LEAVE(ercd)
#define LOG_REF_MTX_ENTER(mtxid)
#define LOG_REF_MTX_LEAVE(ercd)
#define LOG_CRE_MBF_ENTER(mbfid, pk_cmbf)
#define LOG_CRE_MBF_LEAVE(ercd)
#define LOG_ACRE_MBF_ENTER(pk_cmbf)
#define LOG_ACRE_MBF_LEAVE(ercd)
#define LOG_DEL_MBF_ENTER(mbfid)
#define LOG_DEL_MBF_LEAVE(ercd)
#define LOG_SND_MBF_ENTER(mbfid)
#define LOG_SND_MBF_LEAVE(ercd)
#define LOG_SND_MBF_ENTER(mbfid)
#define LOG_SND_MBF_LEAVE(ercd)
#define LOG_TSND_MBF_ENTER(mbfid)
#define LOG_TSND_MBF_LEAVE(ercd)
#define LOG_RCV_MBF_ENTER(mbfid)
#define LOG_RCV_MBF_LEAVE(ercd)
#define LOG_RCV_MBF_ENTER(mbfid)
#define LOG_RCV_MBF_LEAVE(ercd)
#define LOG_TRCV_MBF_ENTER(mbfid)
#define LOG_TRCV_MBF_LEAVE(ercd)
#define LOG_REF_MBF_ENTER(mbfid)
#define LOG_REF_MBF_LEAVE(ercd)
#define LOG_ACRE_POR_ENTER(pk_cpor)
#define LOG_ACRE_POR_LEAVE(ercd)
#define LOG_CRE_POR_ENTER(porid, pk_cpor)
#define LOG_CRE_POR_LEAVE(ercd)
#define LOG_DEL_POR_ENTER(porid)
#define LOG_DEL_POR_LEAVE(ercd)
#define LOG_REF_POR_ENTER(porid)
#define LOG_REF_POR_LEAVE(ercd)
#define LOG_REF_RDV_ENTER(rdvno)
#define LOG_REF_RDV_LEAVE(ercd)
#define LOG_CRE_MPF_ENTER(mpfid, pk_cmpf)
#define LOG_CRE_MPF_LEAVE(ercd)
#define LOG_ACRE_MPF_ENTER(pk_cmpf)
#define LOG_ACRE_MPF_LEAVE(ercd)
#define LOG_DEL_MPF_ENTER(mpfid)
#define LOG_DEL_MPF_LEAVE(ercd)
#define	LOG_GET_MPF_ENTER(mpfid, p_blk)
#define	LOG_GET_MPF_LEAVE(ercd, blk)
#define	LOG_PGET_MPF_ENTER(mpfid, p_blk)
#define	LOG_PGET_MPF_LEAVE(ercd, blk)
#define	LOG_TGET_MPF_ENTER(mpfid, p_blk, tmout)
#define	LOG_TGET_MPF_LEAVE(ercd, blk)
#define	LOG_REL_MPF_ENTER(mpfid, blk)
#define	LOG_REL_MPF_LEAVE(ercd)
#define	LOG_REF_MPF_ENTER(mpfid)
#define	LOG_REF_MPF_LEAVE(ercd)
#define	LOG_REF_MPF_ENTER(mpfid)
#define	LOG_REF_MPF_LEAVE(ercd)
#define LOG_CRE_MPL_ENTER(mpfid, pk_cmpl)
#define LOG_CRE_MPL_LEAVE(ercd)
#define LOG_ACRE_MPL_ENTER(pk_cmpl)
#define LOG_ACRE_MPL_LEAVE(ercd)
#define LOG_DEL_MPL_ENTER(mpfid)
#define LOG_DEL_MPL_LEAVE(ercd)
#define	LOG_GET_MPL_ENTER(mpfid, blksz, p_blk)
#define	LOG_GET_MPL_LEAVE(ercd, blk)
#define	LOG_PGET_MPL_ENTER(mpfid, blksz, p_blk)
#define	LOG_PGET_MPL_LEAVE(ercd, blk)
#define	LOG_TGET_MPL_ENTER(mpfid, blksz, p_blk, tmout)
#define	LOG_TGET_MPL_LEAVE(ercd, blk)
#define	LOG_REL_MPL_ENTER(mpfid, blk)
#define	LOG_REL_MPL_LEAVE(ercd)
#define	LOG_REF_MPL_ENTER(mpfid)
#define	LOG_REF_MPL_LEAVE(ercd)
#define	LOG_REF_MPL_ENTER(mpfid)
#define	LOG_REF_MPL_LEAVE(ercd)
#define	LOG_SET_TIM_ENTER(p_systim)
#define	LOG_SET_TIM_LEAVE(ercd)
#define	LOG_GET_TIM_ENTER(p_systim)
#define	LOG_GET_TIM_LEAVE(ercd, systim)
#define	LOG_ISIG_TIM_ENTER()
#define	LOG_ISIG_TIM_LEAVE(ercd)
#define LOG_CRE_CYC_ENTER(cycid, pk_ccyc)
#define LOG_CRE_CYC_LEAVE(ercd)
#define LOG_ACRE_CYC_ENTER(pk_ccyc)
#define LOG_ACRE_CYC_LEAVE(ercd)
#define LOG_DEL_CYC_ENTER(cycid)
#define LOG_DEL_CYC_LEAVE(ercd)
#define	LOG_STA_CYC_ENTER(cycid)
#define	LOG_STA_CYC_LEAVE(ercd)
#define	LOG_STP_CYC_ENTER(cycid)
#define	LOG_STP_CYC_LEAVE(ercd)
#define	LOG_REF_CYC_ENTER(cycid)
#define	LOG_REF_CYC_LEAVE(ercd)
#define LOG_CRE_ALM_ENTER(almid, pk_calm)
#define LOG_CRE_ALM_LEAVE(ercd)
#define LOG_ACRE_ALM_ENTER(pk_calm)
#define LOG_ACRE_ALM_LEAVE(ercd)
#define LOG_DEL_ALM_ENTER(almid)
#define LOG_DEL_ALM_LEAVE(ercd)
#define	LOG_STA_ALM_ENTER(almid)
#define	LOG_STA_ALM_LEAVE(ercd)
#define	LOG_STP_ALM_ENTER(almid)
#define	LOG_STP_ALM_LEAVE(ercd)
#define	LOG_REF_ALM_ENTER(almid)
#define	LOG_REF_ALM_LEAVE(ercd)
#define LOG_DEF_OVR_ENTER()
#define LOG_DEF_OVR_LEAVE(ercd)
#define	LOG_STA_OVR_ENTER(tskid)
#define	LOG_STA_OVR_LEAVE(ercd)
#define	LOG_STP_OVR_ENTER(tskid)
#define	LOG_STP_OVR_LEAVE(ercd)
#define	LOG_REF_OVR_ENTER(tskid)
#define	LOG_REF_OVR_LEAVE(ercd)
#define	LOG_ROT_RDQ_ENTER(tskpri)
#define	LOG_ROT_RDQ_LEAVE(ercd)
#define	LOG_IROT_RDQ_ENTER(tskpri)
#define	LOG_IROT_RDQ_LEAVE(ercd)
#define	LOG_GET_TID_ENTER(p_tskid)
#define	LOG_GET_TID_LEAVE(ercd, tskid)
#define	LOG_IGET_TID_ENTER(p_tskid)
#define	LOG_IGET_TID_LEAVE(ercd, tskid)
#define	LOG_LOC_CPU_ENTER()
#define	LOG_LOC_CPU_LEAVE(ercd)
#define	LOG_ILOC_CPU_ENTER()
#define	LOG_ILOC_CPU_LEAVE(ercd)
#define	LOG_UNL_CPU_ENTER()
#define	LOG_UNL_CPU_LEAVE(ercd)
#define	LOG_IUNL_CPU_ENTER()
#define	LOG_IUNL_CPU_LEAVE(ercd)
#define	LOG_DIS_DSP_ENTER()
#define	LOG_DIS_DSP_LEAVE(ercd)
#define	LOG_ENA_DSP_ENTER()
#define	LOG_ENA_DSP_LEAVE(ercd)
#define	LOG_SNS_CTX_ENTER()
#define	LOG_SNS_CTX_LEAVE(state)
#define	LOG_SNS_LOC_ENTER()
#define	LOG_SNS_LOC_LEAVE(state)
#define	LOG_SNS_DSP_ENTER()
#define	LOG_SNS_DSP_LEAVE(state)
#define	LOG_SNS_DPN_ENTER()
#define	LOG_SNS_DPN_LEAVE(state)
#define LOG_DEF_INH_ENTER(inhno)
#define LOG_DEF_INH_LEAVE(ercd)
#define LOG_CRE_ISR_ENTER(isrid, pk_cisr)
#define LOG_CRE_ISR_LEAVE(ercd)
#define LOG_ACRE_ISR_ENTER(pk_cisr)
#define LOG_ACRE_ISR_LEAVE(ercd)
#define LOG_DEL_ISR_ENTER(isrid)
#define LOG_DEL_ISR_LEAVE(ercd)
#define LOG_REF_ISR_ENTER(isrid)
#define LOG_REF_ISR_LEAVE(ercd)
#define LOG_REF_SYS_ENTER()
#define LOG_REF_SYS_LEAVE()
#define LOG_DEF_EXC_ENTER(excno)
#define LOG_DEF_EXC_LEAVE(ercd)

#define	LOG_VSNS_INI_ENTER()
#define	LOG_VSNS_INI_LEAVE(state)
#define	LOG_VXSNS_CTX_ENTER(p_excinf)
#define	LOG_VXSNS_CTX_LEAVE(state)
#define	LOG_VXSNS_LOC_ENTER(p_excinf)
#define	LOG_VXSNS_LOC_LEAVE(state)
#define	LOG_VXSNS_DSP_ENTER(p_excinf)
#define	LOG_VXSNS_DSP_LEAVE(state)
#define	LOG_VXSNS_DPN_ENTER(p_excinf)
#define	LOG_VXSNS_DPN_LEAVE(state)
#define	LOG_VXSNS_TEX_ENTER(p_excinf)
#define	LOG_VXSNS_TEX_LEAVE(state)
#define	LOG_VXGET_TIM_ENTER(p_sysutim)
#define	LOG_VXGET_TIM_LEAVE(ercd, sysutim)

#define	LOG_CHG_IPM_ENTER(ipm)
#define	LOG_CHG_IPM_LEAVE(ercd)
#define	LOG_GET_IPM_ENTER(p_ipm)
#define	LOG_GET_IPM_LEAVE(ercd, ipm)

#endif /* TOPPERS_TOOL_CONFIG_H */
