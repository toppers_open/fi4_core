/*

 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uITRON4.0 Kernel
 * 
 *  Copyright (C) 2003-2004 by Monami Software Limited Partnership, JAPAN.
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/config/sh3/ms7727cp01/hw_h8.c 2750 2006-03-07T16:15:24.532712Z monaka  $
 */

/*
 *  シリアルI/Oデバイス（SIO）ドライバ（MS7717CP01用）
 *  電源コントローラ H8/3048F-ONE
 */

#include "hw_h8.h"

#define H8_PINT	11

#define KEYCR	0x0060
#define KATIMER	0x0061
#define KEYSR	0x0062
#define KBITPR	0x0064

#define RTKISR	0x0090

#define	KEYCR_PONNMI	0x20
#define	KEYxR_PONSW	0x10
#define	KEYxR_ARKEY	0x08
#define	KEYxR_KEY_OFF	0x04
#define	KEYxR_KEY_ON	0x02
#define	KEYCR_KEY_STR	0x01

/*
 * ライトコマンド（同期）
 */
void
h8_write(W addr, UB data)
{
	int i;
	UB buf[5];

	/* ライトコマンドの生成 */
	buf[0] = 0x02;
	buf[1] = 0xc0 | 0x01;
	buf[2] = addr >> 8;
	buf[3] = addr;
	buf[4] = data;
	
	/* コマンド送信 */
	for(i=0;i<5;i++)
	{
		st16c2550_pol_putc(buf[i], 2);
	}
	
	/* コマンドの受信をポーリングで待つ */
	while( (UB)-1 == (buf[0] = st16c2550_pol_getc(2)) )	{ }
	while( (UB)-1 == (buf[1] = st16c2550_pol_getc(2)) )	{ }
	if(buf[0] == 0x15)
		return;
	while( (UB)-1 == (buf[2] = st16c2550_pol_getc(2)) )	{ }
	while( (UB)-1 == (buf[3] = st16c2550_pol_getc(2)) )	{ }
	while( (UB)-1 == (buf[4] = st16c2550_pol_getc(2)) )	{ }
}

/*
 * 電源管理コントローラの初期化
 */
void
h8_init(void)
{
	/* 割込み禁止 */
    sil_wrh_mem((VP)PINTER,
                (sil_reh_mem((VP)PINTER) & ~(1 << H8_PINT)));
    sil_wrh_mem((VP)IPRD,
                (sil_reh_mem((VP)IPRD) & 0xf0ff));
    
    /* TODO:レジスタのクリアも必要かも */
    
    /* キー入力の割込み開始 */
    h8_write(KEYCR, KEYCR_PONNMI | KEYxR_KEY_ON | KEYCR_KEY_STR);
    
    /* PINTイネーブル */
	sil_wrh_mem((VP)ICR2,
                (sil_reh_mem((VP)ICR2) & ~(1 << H8_PINT))); 
    sil_wrh_mem((VP)IPRD,
                (sil_reh_mem((VP)IPRD) | (H8_PINT << 8))); 
    sil_wrh_mem((VP)PINTER,
                (sil_reh_mem((VP)PINTER) | (1 << H8_PINT)));

}


void
h8_handler()
{

    /* PINTディスエーブル */
    sil_wrh_mem((VP)PINTER,
                (sil_reh_mem((VP)PINTER) & ~(1 << H8_PINT)));

	/* キー入力の割り込みをクリア */
    h8_write(KEYSR, 0x00);

    syslog(LOG_EMERG, "Power Controler Interrupt occurs.");

    /* PINTイネーブル */
    sil_wrh_mem((VP)PINTER,
                (sil_reh_mem((VP)PINTER) | (1 << H8_PINT)));
}

void
h8_key_isr(VP_INT exinf)
{

    /* PINTディスエーブル */
    sil_wrh_mem((VP)PINTER,
                (sil_reh_mem((VP)PINTER) & ~(1 << H8_PINT)));

	/* キー入力の割り込みをクリア */
    h8_write(KEYSR, 0x00);

    syslog(LOG_EMERG, "Power Controler Interrupt occurs.");

    /* PINTイネーブル */
    sil_wrh_mem((VP)PINTER,
                (sil_reh_mem((VP)PINTER) | (1 << H8_PINT)));
}
