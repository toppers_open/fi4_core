/* This file is generated from cpu_rename.def by genrename. */

#ifdef _CPU_UNRENAME_H_
#undef _CPU_UNRENAME_H_

#undef activate_r  
#undef ret_int     
#undef ret_exc     
#undef task_intmask
#undef int_intmask 
#undef int_table
#undef int_plevel_table
#undef exc_table

#ifdef LABEL_ASM

#undef _activate_r  
#undef _ret_int     
#undef _ret_exc     
#undef _task_intmask
#undef _int_intmask 
#undef _int_table
#undef _int_plevel_table
#undef _exc_table

#endif /* LABEL_ASM */
#endif /* TOPPERS_CPU_UNRENAME_H */
