/*
 *  TOPPERS/JSP Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Just Standard Profile Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2001-2003 by Industrial Technology Institute,
 *                              Miyagi Prefectural Government, JAPAN
 *  Copyright (C) 2006 by Monami Software Limited Partnership, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/config/sh3/sil_sh.h 6183 2006-11-05T03:43:32.794177Z monaka  $
 */

/*
 *  SH2内蔵の制御レジスタへのアクセス・ユーティリティ（ビット演算）
 *  
 *  sh2.hに記述するのがエレガントだが、sil.hとのインクルードの
 *  順番の関係で独立したファイルとする
 *  t_config.hを直接インクルードしたときにsil.hより先にこの
 *  ファイルがインクルードされる可能性がある。
 *  
 *  コンパイラの型チェック機能を有効にするため、ポインタ型は
 *  VB *、VH *、VW *を用いている。
 *  
 */

#ifndef TOPPERS_SIL_SH2_H
#define TOPPERS_SIL_SH2_H

#ifndef _MACRO_ONLY

/*
 *  8ビットレジスタのAND演算
 */
#ifdef __HITACHI_VERSION__
#pragma inline(sh2_b_and)
#endif
Inline void
sh2_b_and(VB *mem, VB data)
{
	VB reg = sil_reb_mem((VP)mem);
	reg &= data;
	sil_wrb_mem((VP)mem, reg);
}

/*
 *  8ビットレジスタのOR演算
 */
#ifdef __HITACHI_VERSION__
#pragma inline(sh2_b_or)
#endif
Inline void
sh2_b_or(VB *mem, VB data)
{
	VB reg = sil_reb_mem((VP)mem);
	reg |= data;
	sil_wrb_mem((VP)mem, reg);
}


/*
 *  16ビットレジスタのAND演算
 */
#ifdef __HITACHI_VERSION__
#pragma inline(sh2_h_and)
#endif
Inline void
sh2_h_and(VH *mem, VH data)
{
	VH reg = sil_reh_mem((VP)mem);
	reg &= data;
	sil_wrh_mem((VP)mem, reg);
}

/*
 *  16ビットレジスタのOR演算
 */
#ifdef __HITACHI_VERSION__
#pragma inline(sh2_h_or)
#endif
Inline void
sh2_h_or(VH *mem, VH data)
{
	VH reg = sil_reh_mem((VP)mem);
	reg |= data;
	sil_wrh_mem((VP)mem, reg);
}

/*
 *  32ビットレジスタのAND演算
 */
#ifdef __HITACHI_VERSION__
#pragma inline(sh2_w_and)
#endif
Inline void
sh2_w_and(VW *mem, VW data)
{
	VW reg = sil_rew_mem((VP)mem);
	reg &= data;
	sil_wrw_mem((VP)mem, reg);
}

/*
 *  32ビットレジスタのOR演算
 */
#ifdef __HITACHI_VERSION__
#pragma inline(sh2_w_or)
#endif
Inline void
sh2_w_or(VW *mem, VW data)
{
	VW reg = sil_rew_mem((VP)mem);
	reg |= data;
	sil_wrw_mem((VP)mem, reg);
}


#endif /* _MACRO_ONLY */
#endif /* TOPPERS_SIL_SH2_H */
