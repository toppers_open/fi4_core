/* This file is generated from cpu_rename.def by genrename. */

#ifndef TOPPERS_CPU_RENAME_H
#define TOPPERS_CPU_RENAME_H

#ifndef OMIT_RENAME

#define activate_r  		_kernel_activate_r  
#define ret_int     		_kernel_ret_int     
#define ret_exc     		_kernel_ret_exc     
#define task_intmask		_kernel_task_intmask
#define int_intmask 		_kernel_int_intmask 
#define int_table		_kernel_int_table
#define int_plevel_table	_kernel_int_plevel_table
#define exc_table		_kernel_exc_table

#ifdef LABEL_ASM

#define _activate_r  		__kernel_activate_r  
#define _ret_int     		__kernel_ret_int     
#define _ret_exc     		__kernel_ret_exc     
#define _task_intmask		__kernel_task_intmask
#define _int_intmask 		__kernel_int_intmask 
#define _int_table		__kernel_int_table
#define _int_plevel_table	__kernel_int_plevel_table
#define _exc_table		__kernel_exc_table

#endif /* LABEL_ASM */
#endif /* OMIT_RENAME */
#endif /* TOPPERS_CPU_RENAME_H */
