/*

 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uITRON4.0 Kernel
 * 
 *  Copyright (C) 2003-2004 by Monami Software Limited Partnership, JAPAN.
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/config/sh3/overrun_timer.c 2750 2006-03-07T16:15:24.532712Z monaka  $
 */

/*
 *  CPU依存オーバランハンドラタイマモジュール（SH3/4用）
 *  TMU1を使用
 */

/*
 *  タイマ割込みハンドラのINTEVT番号
 */
#define	INHNO_OVERRUN_TIMER	TMU1_INTEVT

#include "fi4_kernel.h"
#include "check.h"
#include "task.h"
#include <hw_timer.h>

/*
 *  タイマ割込みハンドラ
 */
void
overrun_timer_handler()
{
    /* タイマを停止 */
    sil_wrb_mem((VP)TMU_TSTR,
                (sil_reb_mem((VP)TMU_TSTR) & ~TMU_STR1));

    call_ovrhdr();		/* オーバランハンドラの呼出し */

    /* 割り込み要求をクリア */
    sil_wrh_mem((VP)TMU_TCR1,
                (sil_reh_mem((VP)TMU_TCR1) & ~TCR_UNF));
}



/*
 *  タイマの起動処理
 *
 *  タイマを初期化する．
 */
void
hw_overrun_timer_initialize()
{
    define_inh(TMU1_INTEVT, overrun_timer_handler);

	/*
	 *  タイマ関連の設定
	 */
    sil_wrb_mem((VP)TMU_TSTR,
                (sil_reb_mem((VP)TMU_TSTR) & ~TMU_STR1));  /* タイマ停止 */

    /*
	 *  割込み関連の設定
	 */
    define_int_plevel(TMU1_INTEVT,KTIM_INT_LV); /* 割り込みレベル設定(SF) */
    /* 割り込みレベル設定(HW) */
    sil_wrh_mem((VP)IPRA,
                ((sil_reh_mem((VP)IPRA) & 0x0fff) | (KTIM_INT_LV<<8)));
    /* 割り込み要求をクリア */
    sil_wrh_mem((VP)TMU_TCR1,
                (sil_reh_mem((VP)TMU_TCR1) & ~TCR_UNF));

    /* 分周比設定、割り込み許可 */
    sil_wrh_mem((VP)TMU_TCR1,(0x020 | TCR1_TPSC));
    sil_wrw_mem((VP)TMU_TCOR1, 0); /* timer constantレジスタをセット */
}

void
hw_overrun_timer_start()
{
    sil_wrw_mem((VP)TMU_TCNT1, runtsk->leftotm); /* カウンターをセット */
    /* タイマ1スタート */
    sil_wrb_mem((VP)TMU_TSTR,
                (sil_reb_mem((VP)TMU_TSTR) | TMU_STR1));
}

void
hw_overrun_timer_stop()
{
    /* タイマを停止 */
    sil_wrb_mem((VP)TMU_TSTR,
                (sil_reb_mem((VP)TMU_TSTR) & ~TMU_STR1));

    /* 割り込み要求をクリア */
    sil_wrh_mem((VP)TMU_TCR1,
                (sil_reh_mem((VP)TMU_TCR1) & ~TCR_UNF));

    runtsk->leftotm = sil_rew_mem((VP)TMU_TCNT1); /* カウンターの読取り */
}

/*
 *  タイマの停止処理
 */
void
hw_overrun_timer_terminate()
{
    /* タイマを停止 */
    sil_wrb_mem((VP)TMU_TSTR,
                (sil_reb_mem((VP)TMU_TSTR) & ~TMU_STR1));
    /* 割り込み要求をクリア */
    sil_wrh_mem((VP)TMU_TCR1,0);

    /* 割り込みレベル設定(HW) */
    sil_wrh_mem((VP)IPRA,
                ((sil_reh_mem((VP)IPRA) & 0x0fff) & ~(KTIM_INT_LV<<8)));
}


