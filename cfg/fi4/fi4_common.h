// $Header$


#ifndef FI4_COMMON_H
#define FI4_COMMON_H

#include "base/testsuite.h"

#include "base/mpstrstream.h"
#include "base/collection.h"
#include "base/except.h"
#include "base/event.h"

#include <string>


namespace ToppersFi4 {

		//カーネル構成ファイル
	class SpecialtyFile : public MultipartStream, public RuntimeObject
	{
	protected:
		SpecialtyFile(void) throw();

		template<class T>
		static T * createInstance(std::string filename) throw(Exception)
		{
			T * old;
			T * result = 0;

				//ROT登録解除 オブジェクト削除 (唯一性の保障 (だけどシングルトンではない) )
			old = dynamic_cast<T *>(RuntimeObjectTable::getInstance(typeid(T)));
			if(old != 0)
				delete old;

			result = new(std::nothrow) T;
			if(result == 0)
				ExceptionMessage("Internal error : Object creation failure [ToppersFi4::KernelCfg::createKernelCfg]","内部エラー : オブジェクト生成時エラー [createKernelCfg]") << throwException;

			try {
				result->setFilename(filename);
			}
			catch(...) {
				if(result != 0)
					delete result;
				throw;
			}
			return result;
		}

	};


	/*
	 *  Visual C++ 6.0 : fatal error C1001: INTERNAL COMPILER ERROR (msc1.cpp:1794) 対策 
	 *    (というより、多重登録しても古いインスタンスの登録チェックをしない)
	 */
#if defined(_MSC_VER) && (_MSC_VER < 1300)
#  define SPECIALTY_FILE(x)                                                             \
    class x : public SpecialtyFile {                                                    \
        public:                                                                         \
            struct CreationEvent { class x * file; };                                   \
            static inline x * createInstance(std::string filename) throw(Exception)     \
            {                                                                           \
                x * instance = new(std::nothrow) x;                                     \
                if(instance != 0)                                                       \
                    instance->setFilename(filename);                                    \
                return instance;                                                        \
            }                                                                           \
    }
#else
#  define SPECIALTY_FILE(x)                                                             \
    class x : public SpecialtyFile {                                                    \
        public:                                                                         \
            struct CreationEvent { class x * file; };                                   \
            static inline x * createInstance(std::string filename) throw(Exception)     \
            {   return SpecialtyFile::createInstance<x>(filename);   }                  \
    }
#endif

		//TOPPERS/FI4のコンフィギュレーションで出てくる特殊なファイル
	SPECIALTY_FILE(KernelCfg);
	SPECIALTY_FILE(KernelID);

		//バージョン間を越えた変換など
	std::string conv_includefile(std::string file) throw();	//インクルードファイル名
	std::string conv_kernelobject(std::string obj) throw();	//カーネルの変数名

		//定数など
//	std::string get_agreement(std::string filename = "") throw();	//文言の取得
}

#endif

