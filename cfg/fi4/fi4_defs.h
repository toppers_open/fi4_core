/*
 *  TOPPERS/FI4 Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Fullset uITRON4.0 Kernel
 * 
 *  Copyright (C) 2003-2004 by Monami Software Limited Partnership, JAPAN.
 *  Copyright (C) 2000-2002 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: /local/fi4-1.4/toppers/RELEASE_20061105_1/cfg/fi4/fi4_defs.h 2750 2006-03-07T16:15:24.532712Z monaka  $
 */

// $Header$

#ifndef __FI4_DEFS_H
#define __FI4_DEFS_H

#define OBJECTTREE			"object"
#define DYNAMICID			"dynamicid"
#define RESERVEDID			"reservedid"
#define SYSTEM				"system"

#define TASK                "task"
#define SEMAPHORE           "semaphore"
#define EVENTFLAG           "eventflag"
#define DATAQUEUE           "dataqueue"
#define MAILBOX             "mailbox"
#define FIXEDSIZEMEMORYPOOL "mempfix"
#define CYCLICHANDLER       "cyclic"
#define INTERRUPTHANDLER    "interrupt"
#define EXCEPTIONHANDLER    "exception"
#define INITIALIZER         "initializer"
#define TERMINATOR          "terminator"
#define FINALIZER           "finalizer"
#define INCLUDEFILE 		"include"
#define OBJECT_INITIALIZER	"object_initializer"
#define IDENTIFIER_VARIABLE "identifier"

#define VARSIZEMEMORYPOOL   "mempvar"
#define ALARMHANDLER        "alarm"
#define OVERRUNHANDLER      "overrun"
#define ATTACHED_ISR	    "attached_isr"
#define INTERRUPTSERVICE    "interrupt_service"
#define MESSAGEBUFFER       "messagebuffer"
#define MUTEX               "mutex"
#define RENDEZVOUSPORT      "rendezvous"
#define EXSERVICECALL       "exservicecall"

#define KERNELBUFFER	    "kernelbuffer"

#define KERNEL_CFG			"/file/kernel_cfg"
#define KERNEL_ID			"/file/kernel_id"

#define PREFIX				"/prefix"

#endif
